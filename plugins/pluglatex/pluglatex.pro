TEMPLATE = lib
INCLUDEPATH += /usr/local/include \
	/usr/local/include/libdigidoc \
      ../../src
DEFINES += QT_DEBUG SAFET_NO_DBXML
QT += core \
    sql \
    xml
# QT -= gui
HEADERS += latexplugin.h
SOURCES += latexplugin.cpp
LIBS += -L/usr/local/lib \
        -L../../src \
        -L/usr/local/lib \
        -ldigidoc \
       -lsafet
TARGET = $$qtLibraryTarget(pnp_latex)
CONFIG += plugin
target.path=/usr/local/lib/
# target.files = pluglatex.so*
INSTALLS += target
