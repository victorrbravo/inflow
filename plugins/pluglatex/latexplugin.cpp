/*
* SAFET Sistema Automatizado para la Firma Electrónica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigación en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los términos de la licencia de
* software GPL versión 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea útil, pero SI NINGUNA GARANTÍA;
* tampoco las implícitas garantías de MERCANTILIDAD o ADECUACIÓN A UN PROPÓSITO PARTICULAR.
* Consulte la licencia GPL para más detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5º Piso, Boston, MA 02110-1301, USA.
*
*/

#include "latexplugin.h"


QString latexPlugin::renderDocuments(const QString& docs, const QMap<QString,QString>& ) const  {

        QString str = "";

        qDebug("...rendering..latex...");
        //if (context.count() == 0 ) return docs;

        //foreach(QObject* myobj, context) {
        //       if (myobj) {
        //            qDebug("class: %s", myobj->metaObject()->className());
        //       }
        //}
        return str;
 }

QString  latexPlugin::parseCodeGraph(const QString& code, const QMap<QString,QString>&)  const {
     QString result = code;

     return result;
}

QString latexPlugin::renderGraph(const QString&  code, const QString& info, const QMap<QString,QString>& context) const {
     if ( info.isEmpty() ) ;
     if ( context.count() == 0 );

     return code;
}

Q_EXPORT_PLUGIN2(pnp_latex, latexPlugin)
