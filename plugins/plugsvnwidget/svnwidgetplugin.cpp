/*
* SAFET Sistema Automatizado para la Firma Electrónica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigación en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los términos de la licencia de
* software GPL versión 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea útil, pero SI NINGUNA GARANTÍA;
* tampoco las implícitas garantías de MERCANTILIDAD o ADECUACIÓN A UN PROPÓSITO PARTICULAR.
* Consulte la licencia GPL para más detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5º Piso, Boston, MA 02110-1301, USA.
*
*/
#include "svnwidgetplugin.h"
#include "svncpp/client.hpp"
#include "svncpp/revision.hpp"
#include "SafetYAWL.h"
#include <QTextCodec>



SvnWidgetPlugin::SvnWidgetPlugin() {
     _pwidget = NULL;

}

SvnWidgetPlugin::~SvnWidgetPlugin() {
     //qDebug("...~SvnWidgetPlugin...");
     if ( _pwidget != NULL )  {
         delete _pwidget;
     }
}

QString SvnWidgetPlugin::text() const {
       QString result;
       return result;
}

void SvnWidgetPlugin::buildWidget() {

}

void SvnWidgetPlugin::setConf(QMap<QString, QVariant>& c)  {
     _conf = c;
     _conf["namewidget"] = "ComboSvnWidget";
}


QMap<QString, QVariant>& SvnWidgetPlugin::conf() {
     if ( _conf.isEmpty() ) {
        _conf["namewidget"] = "ComboSvnWidget";
        _conf["Ruta del proyecto (svn)"] = QDir::currentPath();
        _conf["Entradas a visualizar"] = 7;
     }
     qDebug("...SvnWidgetPlugin::conf()...saliendo...");
     return _conf;
}

void SvnWidgetPlugin::setFocus(Qt::FocusReason reason) {
     if ( _pwidget ) {
          _pwidget->setFocus( reason );
     }
}





CmdWidget* SvnWidgetPlugin::getWidget(const QString& field, QWidget *parent) {
     _pwidget  = new ComboSvnWidget(field, parent);
     Q_CHECK_PTR(_pwidget);
     _pwidget->setConf(  conf() );
     return _pwidget;
}



bool SvnWidgetPlugin::doAction(const QString& key, const QString& info) {
    qDebug("..key:%s", qPrintable(key));
    qDebug("..info:%s", qPrintable(info));
    bool ok;
       QString text = QInputDialog::getText(QApplication::activeWindow(), tr("Inflow SVN Plugin"),
                                            tr("Firma:"), QLineEdit::Normal,
                                            QString(""), &ok);

     return true;
}


void ComboSvnWidget::setText(const QString &newText) {
    if (varbox ) {
        int newindex = varbox->findText(newText,Qt::MatchStartsWith);
        if (newindex != -1 ) {
            varbox->setCurrentIndex(newindex );
        }
    }
}

// *** M�todos de la Clase ComboSvnWidget


ComboSvnWidget::ComboSvnWidget(const QString& s, QWidget *parent)
    : CmdWidget(s, parent)
{
     varbox = NULL;

}

ComboSvnWidget::~ComboSvnWidget() {

//    if ( varbox ) {
//        delete varbox;
//    }
}


void ComboSvnWidget::updateCombo() {

    //SafetDocument doc;
     QString info;
     QStringList options;
     if ( varbox == NULL ) {
         qDebug("..varbox == NULL...");
         return;
     }
     if (conf().contains("options")) {
//         qDebug("conf()[\"options\"]=%s", qPrintable(conf()["options"].toString()));
         options = conf()["options"].toString().split(";");
         if (options.contains("commit",Qt::CaseSensitive)) {
                varbox->addItem(tr("subir cambios (commit)"));
                return;
         }
     }
     bool ok;
     int entriesmax = conf()["Entradas a visualizar"].toInt(&ok);
     svn::Context *context = new svn::Context();
     svn::Client client (context);

  //svn::Path target = svn::Path("svn://repositorio.cenditel.gob.ve/var/local/svn/seguridad");
     QString mypath = ".";
    if ( !conf().isEmpty() ) {
          mypath = conf()["Ruta del proyecto (svn)"].toString();
          qDebug("...**path...|%s|", qPrintable(mypath));
    }
    svn::Path target = svn::Path(qPrintable(mypath));
    const svn::LogEntries * entries =
    client.log (target.c_str (), svn::Revision::START,
                svn::Revision::HEAD, true, false);


   if (entries == 0)
      return;

    long index=0;
    svn::LogEntries::const_iterator it;

    QStringList vars;

   for (it=entries->begin (); it != entries->end (); it++ )
    {
      const svn::LogEntry & entry = *it;
      info.sprintf("%ld/%s", (long) entry.revision, entry.message.c_str ());
      index++;
      vars.append( info );
      if ( index >= entriesmax ) break;

    }

     varbox->addItems( vars );

     qDebug();

     qDebug("****...viendo el diff");
     svn::Path mytmppath = svn::Path(qPrintable(mypath+"/"));
     svn::Path mylocalpath = svn::Path(qPrintable(mypath));
     qDebug("mylocal path: %s", qPrintable(mypath));
     std::string mydiffs;
     try {
     mydiffs =
           client.diff(mytmppath,mylocalpath,svn::Revision::WORKING,svn::Revision::HEAD,
                         true,true,true);
      }
     catch(svn::ClientException e) {
         qDebug("client exception: %s", e.message());
     }

     qDebug("%s",mydiffs.c_str());
     qDebug("****...viendo el diff");
     qDebug();


}



void ComboSvnWidget::setConf(QMap<QString, QVariant>& c) {
//     qDebug("...ComboSvnWidget::setConf...(1)...");
     _conf = c;
//     qDebug("...ComboSvnWidget::setConf...(2)...");

}

QMap<QString, QVariant>& ComboSvnWidget::conf() {

     return _conf;
}


QString ComboSvnWidget::text() const {
     Q_CHECK_PTR( varbox );
     return varbox->currentText();
}


void ComboSvnWidget::buildWidget() {
     //CmdWidget::buildWidget();
     mainLayout = new QHBoxLayout;
     varbox = new QComboBox;
     okbutton = new QToolButton;
     okbutton->setGeometry(0,0,30,36);
     okbutton->setIcon(QIcon(":/yes.png"));
     quitbutton = new QToolButton;
     quitbutton->setText( "X");
     Q_CHECK_PTR( varbox );
     varbox->setEditable(true);
     mainLayout->addWidget(varbox);
     mainLayout->addWidget(quitbutton);
     mainLayout->addWidget(okbutton);
    connect(quitbutton, SIGNAL(clicked()), _texteditparent, SLOT(cancelAndClose()) );
      connect(okbutton, SIGNAL(clicked()), _texteditparent, SLOT(insertAndClose()) );
     setLayout(mainLayout);
     updateCombo();
}

void ComboSvnWidget::setFocus ( Qt::FocusReason reason ) {
     qDebug("...ComboSvnWidget::setFocus....");
     Q_CHECK_PTR( varbox );
     QWidget::setFocus ( reason );
     varbox->setFocus( reason);


}



//QString SvnVersionPlugin::getInfoVersion() const {
//     QString info = "info";
//   svn::Context *context = new svn::Context();
//   svn::Client client (context);
//
//  //svn::Path target = svn::Path("svn://repositorio.cenditel.gob.ve/var/local/svn/seguridad");
//   svn::Path target = svn::Path(".");
//  const svn::LogEntries * entries =
//    client.log (target.c_str (), svn::Revision::START,
//                svn::Revision::HEAD, true, false);
//
//   if (entries == 0)
//      return QString("S/INFO");
//
//    long index=0;
//    svn::LogEntries::const_iterator it;
//   for (it=entries->begin (); it != entries->end (); it++ )
//    {
//      const svn::LogEntry & entry = *it;
//      info.sprintf("revision: %ld Descripcion:%s", (long) entry.revision, entry.message.c_str ());
//      index++;
//      if ( index >= 1) break;
//    }
//     return info;
//
//}




Q_EXPORT_PLUGIN2(widget_svnwidget, SvnWidgetPlugin)

