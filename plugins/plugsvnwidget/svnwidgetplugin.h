/*
* SAFET Sistema Automatizado para la Firma Electrónica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigación en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los términos de la licencia de
* software GPL versión 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea útil, pero SI NINGUNA GARANTÍA;
* tampoco las implícitas garantías de MERCANTILIDAD o ADECUACIÓN A UN PROPÓSITO PARTICULAR.
* Consulte la licencia GPL para más detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5º Piso, Boston, MA 02110-1301, USA.
*
*/

#ifndef SVNWIDGETPLUGIN_H
#define SVNWIDGETPLUGIN_H

#include <QtCore>
#include "SafetInterfaces.h"
#include "cmdwidget.h"
#include "SafetPlugin.h"

class SvnWidgetPlugin : public SafetPlugin, public WidgetInterface
{
    Q_OBJECT
    Q_INTERFACES(WidgetInterface)

    QString _path;
    CmdWidget *_pwidget;
    QMap<QString,QVariant> _conf;

public:

     SvnWidgetPlugin();
     ~SvnWidgetPlugin();
     virtual QString text() const;
     virtual void buildWidget();
     virtual void setFocus(Qt::FocusReason reason);
     virtual int id() const { return 1002; }
     virtual QString descriptor() const { return QString("combosvn"); }
     virtual bool doAction(const QString& key, const QString& info);
     virtual QString path() const { return _path; }
     virtual void setPath(const QString& p) { _path = p; }
     CmdWidget* getWidget(const QString& field, QWidget *parent);
     virtual void setConf(QMap<QString, QVariant>&);
     virtual QMap<QString, QVariant>& conf();

};


class ComboSvnWidget : public CmdWidget
{
 Q_OBJECT


public:

    ComboSvnWidget(const QString& s, QWidget *parent = 0);
    ~ComboSvnWidget();
    virtual void setFocus ( Qt::FocusReason reason );
    virtual void buildWidget();
    virtual void setConf(QMap<QString, QVariant>& c);
    virtual QMap<QString, QVariant>& conf();
public slots:
    virtual QString text() const;
    void setText(const QString &newText);
    void updateCombo();

protected:
    QComboBox* varbox;
};



#endif
