TEMPLATE = lib
INCLUDEPATH += /usr/local/include/libdigidoc \
    /usr/include/subversion-1/ \
    /usr/include/apr-1.0 \
    /usr/include/libdigidoc \
    ../../inflow \
    ../../src
DEFINES += QT_DEBUG \
    SAFET_NO_DBXML
QT += core \
    sql \
    network \
    xml
# QT -= gui
HEADERS += svnwidgetplugin.h \
   ../../inflow/cmdwidget.h
SOURCES += svnwidgetplugin.cpp \
 ../../inflow/cmdwidget.cpp
LIBS +=  -L../../src \
    -L/usr/lib/svncpp \
    -lsvncpp \
    -lsafet
TARGET = $$qtLibraryTarget(widget_svnwidget)
CONFIG += plugin
target.path = /usr/lib/libsafet
INSTALLS += target
