TEMPLATE = lib
INCLUDEPATH += /usr/local/include/libdigidoc \
	/usr/include/graphviz \
      ../../src 

DEFINES += QT_DEBUG SAFET_NO_DBXML

DEFINES +=  SAFET_XML2 \
#    SAFET_OPENSSL \
#    SAFET_DIGIDOC \
    SAFET_TAR \
#    SAFET_GSOAP \

QT += core \
    sql \
    network \
    xml
#QT -= gui
HEADERS += graphvizplugin.h
SOURCES += graphvizplugin.cpp
LIBS += -L../../src \
	-L/usr/local/lib \
        -lgvc \
        -lgraph \
        -lsafet

TARGET = $$qtLibraryTarget(pnp_graphviz)
CONFIG += plugin
target.path = /usr/lib/libsafet
# target.files = plugviz.so*
INSTALLS += target
