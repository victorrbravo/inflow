/*
* SAFET Sistema Automatizado para la Firma Electrónica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigación en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los términos de la licencia de
* software GPL versión 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea útil, pero SI NINGUNA GARANTÍA;
* tampoco las implícitas garantías de MERCANTILIDAD o ADECUACIÓN A UN PROPÓSITO PARTICULAR.
* Consulte la licencia GPL para más detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5º Piso, Boston, MA 02110-1301, USA.
*
*/

#ifndef SCWidgetPLUGIN_H
#define SCWidgetPLUGIN_H

#include <QtCore>
 #include <QDesktopWidget>
#include "SafetInterfaces.h"
#include "cmdwidget.h"
#include "SafetPlugin.h"
#include <gst/gst.h>
#include <glib.h>
#include <unistd.h>

#include "regiongrabber.h"
#include "windowgrabber.h"


class SCWidgetPlugin : public SafetPlugin, public WidgetInterface
{
    Q_OBJECT
    Q_INTERFACES(WidgetInterface)

public:
     enum PageLoaded { NONE, LOGIN, UPLOADATTACH, DOUPLOAD, UPLOADSUCCESS };

private:
    QString _path;
    QString _key;
    QString _info;
     CmdWidget *_pwidget;
     QMap<QString,QVariant> _conf;
     PageLoaded jscriptload;
     QProcess *myProcess;
public:


     SCWidgetPlugin();
     ~SCWidgetPlugin();
     virtual QString text() const;
     virtual void buildWidget();    
     virtual void setFocus(Qt::FocusReason reason);
     virtual int id() const { return 1001; }
     virtual QString descriptor() const { return QString("linesc"); }
     virtual bool doAction(const QString& key, const QString& info) ;     
     virtual QString path() const { return _path; }
     virtual void setPath(const QString& p) { _path = p; }
     virtual CmdWidget* getWidget(const QString& field, QWidget *parent);          
     virtual void setConf(QMap<QString, QVariant>&);
     virtual QMap<QString, QVariant>& conf();
     
     QString evalJS(const QString &js);
     void doLogin();
     void doAttach();
     bool doScript();
     QString replaceCharsForCommandLine(const QString& s);
     QString extractRealPassword(const QString& s);
 private slots:
     void loadedPage(bool ok);
     void printStandardOutput();

};


class LineSCWidget : public CmdWidget
{
 Q_OBJECT
     QWidget* myapp;
     int valuetime;
     QDesktopWidget* deskwidget;
     RegionGrabber *rgnGrab;
     QWidget* grabber;
    QTimer grabTimer;
    QTimer updateTimer;
    QSystemTrayIcon *trayIcon;
    GstElement *pipeline; // Para el video
    QString fileNameCast;
    QRect grabSCRect;
public:
    enum CaptureMode { FullScreen=0, WindowUnderCursor=1, Region=2, ChildWindow=3 };
    LineSCWidget(const QString& s, QWidget *parent = 0);
    ~LineSCWidget();
    virtual void setFocus ( Qt::FocusReason reason );
    virtual void buildWidget();
    virtual void setConf(QMap<QString, QVariant>& c);
    virtual QMap<QString, QVariant>& conf();
    CaptureMode mode();
    int delay();
    QString combocurrentText(const QString& s);
    void checkSystemTray();
protected:
void hideEvent ( QHideEvent * event );
private slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
public slots:

    virtual void setText(const QString &newText);
    void createTrayIcon();
    void deleteTrayIcon();
    virtual QString text() const;
    void updateLine();
    void newScreenShot();
    void shotScreen();
    void saveScreenshot();
    void openFilename();
    void slotGrab();
    void performGrab();
    void slotRegionGrabbed( const QPixmap& );
    void slotRegionSCGrabbed( const QPixmap& );
    void grabTimerDone();
    void  grabCast();
    void  stopCast();
    void selectGrabCast();
    void selCaptureType();
    private:
    QToolButton *capturebutton;
    QToolButton *getfilebutton;
    QPixmap snapshot;
    QWidget* _parent;

};



#endif
