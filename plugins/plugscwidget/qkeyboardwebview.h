#ifndef QKEYBOARDWEBVIEW_H
#define QKEYBOARDWEBVIEW_H

#include <QWebView>

class QKeyboardWebView : public QWebView     {


public:
    QKeyboardWebView();

    void keyPress(int c, Qt::KeyboardModifiers modifiers = Qt::NoModifier);
protected:
     virtual void keyPressEvent ( QKeyEvent * event );
};

#endif // QKEYBOARDWEBVIEW_H
