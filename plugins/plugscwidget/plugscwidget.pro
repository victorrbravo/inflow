TEMPLATE = lib
INCLUDEPATH += /usr/local/include \
    /usr/include/libdigidoc \
    /usr/include/gstreamer-0.10 \
    /usr/include/glib-2.0 \
    /usr/lib/glib-2.0/include \
    /usr/include/libxml2 \
    ../../inflow \
    ../../src
DEFINES += QT_DEBUG \
    SAFET_NO_DBXML
QT += core \
    sql \
    network \
    xml

# QT -= gui
HEADERS += scwidgetplugin.h \
    ../../inflow/cmdwidget.h \
    regiongrabber.h \
    windowgrabber.h
#    qkeyboardwebview.h
SOURCES += scwidgetplugin.cpp \
    ../../inflow/cmdwidget.cpp \
    regiongrabber.cpp \
    windowgrabber.cpp
#    qkeyboardwebview.cpp
LIBS += -L/usr/local/lib \
     -L../../src \
    -lgstreamer-0.10 \
    -lgobject-2.0 \
#    -pthread \
#    -ldl \
    -lgthread-2.0 \
    -lrt \
    -lxml2 \
    -lglib-2.0 \
    -lsafet
    
TARGET = $$qtLibraryTarget(widget_scwidget)
CONFIG += plugin
target.path = /usr/lib/libsafet
RESOURCES = plugscwidget.qrc
INSTALLS += target

