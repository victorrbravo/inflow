#include <QKeyEvent>
#include "qkeyboardwebview.h"

QKeyboardWebView::QKeyboardWebView()
          :QWebView(){


}

void QKeyboardWebView::keyPressEvent ( QKeyEvent * event ) {
     QWidget::keyPressEvent(event);
}

void QKeyboardWebView::keyPress(int c, Qt::KeyboardModifiers modifiers) {
     QKeyEvent e(QEvent::User,c,modifiers);
     keyPressEvent(&e);

}
