/*
* SAFET Sistema Automatizado para la Firma Electrnica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigación en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los términos de la licencia de
* software GPL versión 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea útil, pero SI NINGUNA GARANTÍA;
* tampoco las implícitas garantías de MERCANTILIDAD o ADECUACIÓN A UN PROPÓSITO PARTICULAR.
* Consulte la licencia GPL para más detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5º Piso, Boston, MA 02110-1301, USA.
*
*/
#include "scwidgetplugin.h"
#include "SafetYAWL.h"
#include <QX11Info>

#define DEFAULTTRAC_URL "http://seguridad.cenditel.gob.ve/rootve"



SCWidgetPlugin::SCWidgetPlugin()
       {
     _pwidget = NULL;
     jscriptload = NONE;
     myProcess = NULL;

}


SCWidgetPlugin::~SCWidgetPlugin() {
    if ( _pwidget != NULL ) {
        delete _pwidget;
    }

}


void SCWidgetPlugin::buildWidget() {

}




void SCWidgetPlugin::setConf(QMap<QString, QVariant>& c)  {
     _conf = c;
     _conf["namewidget"] = "LineSCWidget";
}



QMap<QString, QVariant>& SCWidgetPlugin::conf() {
     if ( _conf.isEmpty() ) {
        _conf["namewidget"] = "LineSCWidget";
        _conf[trUtf8("Opción de captura")] = trUtf8("Pantalla,Región,Ventana");
        _conf[trUtf8("Tipo de captura")] = trUtf8("Imagen,Video");
        _conf[trUtf8("Directorio para guardar imagen")] = ".";
        _conf[trUtf8("Segundos a esperar para la captura")] = 2;
        _conf[trUtf8("Ocultar Ventana InFlow")] = true;
        _conf[trUtf8("Archivo Script/Python")] = "/home/vbravo/desarrollo/uploadtrac.py";
        _conf[trUtf8("Parámetros del script")] = "usuario;123456;http://seguridad.cenditel.gob.ve/rootve;";
     }
     // parametros del script: (vbravo), (pass), (url)
     return _conf;
}

void SCWidgetPlugin::setFocus(Qt::FocusReason reason) {
     if ( _pwidget ) {
          _pwidget->setFocus( reason );
     }
}


void SCWidgetPlugin::doLogin() {
     QUrl url(QString("%1/login").arg(DEFAULTTRAC_URL));
     jscriptload = LOGIN;

}

void SCWidgetPlugin::doAttach() {
     //QString parturl = QString("/attachment/ticket/%1/?action=new&attachfilebutton=Attach+file").arg(_key);
     QString parturl = QString("/attachment/ticket/%1/?action=new&attachfilebutton=Attach+file").arg("85");
//     qDebug("...parturl: %s", qPrintable(parturl));
     QString attstring = DEFAULTTRAC_URL +parturl;
     QUrl url(attstring);
     //_actionview.load(url);
     jscriptload = DOUPLOAD;

}


void SCWidgetPlugin::loadedPage(bool /*ok*/) {

}

bool SCWidgetPlugin::doAction(const QString& key, const QString& info)  {

//     qDebug("++++****SCWidgetPlugin::doAction........key: |%s|..info: |%s|", qPrintable(key), qPrintable(info));
     _key = QString("%1").arg(key);
     _info = QString("%1").arg(info);
     doScript();
     return true;
}

bool SCWidgetPlugin::doScript() {
    QStringList parlist;
    QString parstr;
    parstr = _conf[trUtf8("Parámetros del script")].toString();
    parlist = parstr.split(";",QString::SkipEmptyParts);
    QString program = _conf[trUtf8("Archivo Script/Python")].toString();
    qDebug("...doScripts: parlist %d",parlist.count());
    if (parlist.count() < 3 ) {
        return false;
    }

    QObject *parent = NULL;
    QStringList arguments;
    if ( parlist.count() < 5 ) {
        return false;
    }

    myProcess = new QProcess(parent);
    _info = _info.replace("'","");
    arguments << replaceCharsForCommandLine(parlist.at(0)) << extractRealPassword(parlist.at(1)) << _key
            << _info <<  parlist.at(4);

    connect(myProcess, SIGNAL(readyReadStandardOutput ()), this, SLOT(printStandardOutput()) );

    qDebug("...doScripts: program |%s|",qPrintable(program));
    foreach(QString s, arguments) {
        qDebug("arguments: %s",qPrintable(s));
    }

    myProcess->start(program, arguments);
    return true;

}
QString SCWidgetPlugin::text() const {
    if ( _pwidget ) {
            return _pwidget->text();
    }
    return QString("");
}


QString SCWidgetPlugin::extractRealPassword(const QString& s) {
    QString result = s;
    int len = result.length();
    if ( ( len % 2) != 0  )  {
//        qDebug("Cadena no es un \"pass\", la longitud es impar");
        return QString();
    }
    QString leftstring = result.mid(0,len/2);
    QString rightstring = result.mid(len/2);
    QString comodinstring;
    for(int i = 0; i < rightstring.length();i++) comodinstring.append("*");
    if (rightstring.compare(comodinstring) != 0)  {
        //qDebug("leftstring:%s",qPrintable(leftstring));
//        qDebug("Cadena no es un \"pass\", la segunda cadena \"%s\" no es del tipo comodin \"%s\"",
//               qPrintable(rightstring),qPrintable(comodinstring));
        return QString();
    }

    result = replaceCharsForCommandLine(leftstring);


    return result;
}

QString SCWidgetPlugin::replaceCharsForCommandLine(const QString& s) {
    QString result = s;
 //   qDebug("+++(1)result...:%s",qPrintable(result));
    result.replace("!","\\!");
//    qDebug("+++(2)result...:%s",qPrintable(result));
    return result;
}

void SCWidgetPlugin::printStandardOutput() {
    if ( myProcess == NULL ) {
        return;
    }
     QString message = myProcess->readAll();
     qDebug("mensaje: %s", qPrintable(message));

}

QString SCWidgetPlugin::evalJS(const QString& /*js*/) {
    //QWebFrame *frame = _actionview.page()->mainFrame();
    //Q_CHECK_PTR( frame);
    //return frame->evaluateJavaScript(js).toString();
    return QString("");
}

CmdWidget* SCWidgetPlugin::getWidget(const QString& field, QWidget *parent) {
     _pwidget  = new LineSCWidget(field, parent);
     Q_CHECK_PTR(_pwidget);
     _pwidget->setConf(  conf() );
     return _pwidget;
}



// *** Métodos de la Clase LineSCWidget




LineSCWidget::LineSCWidget(const QString& s, QWidget *parent)
    : CmdWidget(s, parent)
{
    lineedit = NULL;
    _parent = parent;
    valuetime = 0;
    getfilebutton = NULL;
    capturebutton = NULL;
    myapp = NULL;
    pipeline = NULL;
    connect( &grabTimer, SIGNAL( timeout() ), this, SLOT(  grabTimerDone() ) );
    grabber = new QWidget( 0,  Qt::X11BypassWindowManagerHint );
    grabber->move( -1000, -1000 );
    grabber->installEventFilter( this );

    grabber->show();
    grabber->grabMouse( Qt::WaitCursor );

    snapshot = QPixmap::grabWindow( QX11Info::appRootWindow() );

    grabber->releaseMouse();
    grabber->hide();    
    trayIcon = new QSystemTrayIcon(QIcon(":/camera16.png"), this);
    trayIcon->show();
    trayIcon->hide();
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));

}

LineSCWidget::~LineSCWidget() {

     //if (configurator) delete configurator;
    //delete grabber;

}

void LineSCWidget::setText(const QString &newText) {
    CmdWidget::setText(newText);

}


void LineSCWidget::updateLine() {

     QString info;
}


void LineSCWidget::newScreenShot() {
     bool ok;
     valuetime = conf()["Segundos a esperar para la captura"].toInt(&ok);
//     myapp = QApplication::activeWindow();
//     Q_CHECK_PTR( myapp );
//     if ( conf()["Ocultar Ventana InFlow"].toBool() ) {
//          myapp->hide();
//     }
//
     //QTimer::singleShot(valuetime * 1000, this, SLOT(shotScreen()));
     //QApplication::setOverrideCursor( Qt::CrossCursor );
     //slotGrab();

}


void LineSCWidget::slotGrab() {
 //   hide();

//    qDebug("...delay..:%d", delay());
//    qDebug("...mode()..:%d", mode());
    if ( delay() ) {
        grabTimer.setSingleShot( true );
        grabTimer.start( delay() * 1000 );
    }
    else {
        if ( mode() == Region ) {
            rgnGrab = new RegionGrabber();
            connect( rgnGrab, SIGNAL( regionGrabbed( const QPixmap & ) ),
                              SLOT( slotRegionGrabbed( const QPixmap & ) ) );
        }
        else {
            grabber->show();
            grabber->grabMouse( Qt::CrossCursor );
            saveScreenshot();
        }
    }
}

void LineSCWidget::hideEvent ( QHideEvent* /*event*/ ) {
   // qDebug("...deleteTrayIcon()...");
    //deleteTrayIcon();
}

void LineSCWidget::slotRegionGrabbed( const QPixmap &pix )
{
  if ( !pix.isNull() ) {
    snapshot = pix;
  }

  delete rgnGrab;
  QApplication::restoreOverrideCursor();

//  qDebug("myapp = QApplication::activeWindow()");

  if ( !pix.isNull() ) {
      saveScreenshot();
  }

}

void LineSCWidget::slotRegionSCGrabbed( const QPixmap &pix )
{
//    qDebug("...LineSCWidget::slotRegionSCGrabbed...");
  if ( !pix.isNull() ) {
    grabSCRect = rgnGrab->grabRect();
  }

  delete rgnGrab;
  QApplication::restoreOverrideCursor();
  int result = QMessageBox::information(QApplication::activeWindow(),
                                        trUtf8("Complemento de Inflow"),
                                        trUtf8("Ahora se iniciará el grabación de video de pantalla (screencast)"
                                               "\nRealice 'doble-click' en la ícono de la bandeja de entrada para "
                                               "\nDETENER la grabación"
                                               "\nPresione el botón 'Cancelar' para NO grabar"),
                                        QMessageBox::Ok | QMessageBox::Cancel );
  if ( result == QMessageBox::Cancel ) {
      deleteTrayIcon();
      return;
  }

  grabCast();

}

QString LineSCWidget::combocurrentText(const QString& s) {
    QStringList currentlist = s.split(",", QString::SkipEmptyParts );

    foreach(QString c, currentlist) {
        if ( c.endsWith("+") ) {
            c.chop(1);
            return c;
        }

    }

    return QString("");

}

LineSCWidget::CaptureMode LineSCWidget::mode() {
    QString option = combocurrentText(conf()[trUtf8("Opción de captura")].toString());
    if (  option == trUtf8("Pantalla") ) {
            return FullScreen;
    }
    else if ( option == trUtf8("Región") ) {
            return Region;
    }
    else if ( option == trUtf8("Ventana") ) {
            return WindowUnderCursor;
    }

    return FullScreen;

}

int LineSCWidget::delay() {
    bool ok;
    int result = conf()[trUtf8("Segundos a esperar para la captura")].toInt(&ok);;
    Q_ASSERT( ok );
    Q_ASSERT( result >= 0 );
    return result;

}

void LineSCWidget::grabTimerDone()
{
    QString option = combocurrentText(conf()[trUtf8("Tipo de captura")].toString());
//    qDebug("grabTimerDone...");
    if ( option == trUtf8("Imagen") ) {
        if ( mode() == Region ) {
            rgnGrab = new RegionGrabber();
                connect( rgnGrab, SIGNAL( regionGrabbed( const QPixmap & ) ),
                                  SLOT( slotRegionGrabbed( const QPixmap & ) ) );    }
        else {
            performGrab();
        }
    } else if (option == trUtf8("Video") ) {
//        qDebug("...grabTimerDone::Video...");
        if ( mode() == Region ) {
                qDebug("mode() == Region ");
                rgnGrab = new RegionGrabber();
                Q_CHECK_PTR( rgnGrab );
                connect( rgnGrab, SIGNAL( regionGrabbed( const QPixmap & ) ),
                                  SLOT( slotRegionSCGrabbed( const QPixmap & ) ) );
        }
        else if (mode() ==  WindowUnderCursor ) {
                qDebug("...grabTimerDone::Video...WindowUnderCursor");
                QRect windowrect;
                snapshot = WindowGrabber::grabCurrent(windowrect /*,includeDecorations()*/  );
                grabSCRect = windowrect;
                int result = QMessageBox::information(QApplication::activeWindow(),
                                                      trUtf8("Complemento de Inflow"),
                                                      trUtf8("Ahora se iniciará el grabación de video de pantalla (screencast)"
                                                             "\nRealice 'doble-click' en la ícono de la bandeja de entrada para "
                                                             "\nPresione el botón 'Cancelar' para NO grabar"),
                                                      QMessageBox::Ok | QMessageBox::Cancel );

                if ( result == QMessageBox::Cancel ) {
                    deleteTrayIcon();
                    return;
                }
                grabCast();
        }

        else if (mode() == FullScreen  ) {
            QDesktopWidget* desktopwidget  = QApplication::desktop();
              Q_CHECK_PTR( desktopwidget );
                grabSCRect = desktopwidget->geometry();
                int result = QMessageBox::information(QApplication::activeWindow(),
                                                      trUtf8("Complemento de Inflow"),
                                                      trUtf8("Ahora se iniciará el grabación de video de pantalla (screencast)"
                                                             "\nRealice 'doble-click' en la ícono de la bandeja de entrada para "
                                                             "\nPresione el botón 'Cancelar' para NO grabar"),
                                                      QMessageBox::Ok | QMessageBox::Cancel );

                if ( result == QMessageBox::Cancel ) {
                    deleteTrayIcon();
                    return;
                }
                grabCast();
        }
    }

}

void LineSCWidget::performGrab()
{
    grabber->releaseMouse();
    grabber->hide();
    grabTimer.stop();
    if ( mode() == ChildWindow ) {
        WindowGrabber wndGrab;
        connect( &wndGrab, SIGNAL( windowGrabbed( const QPixmap & ) ),
                           SLOT( slotWindowGrabbed( const QPixmap & ) ) );
        wndGrab.exec();
    }
    else if ( mode() == WindowUnderCursor ) {
//        qDebug("...WindowGrabber::grabCurrent...");
        QRect windowrect;
        snapshot = WindowGrabber::grabCurrent(windowrect /*,includeDecorations()*/  );
    }
    else {
        snapshot = QPixmap::grabWindow( QX11Info::appRootWindow() );
    }
    QApplication::restoreOverrideCursor();
    saveScreenshot();
//    show();
}

void LineSCWidget::shotScreen() {

//! [4]
    snapshot = QPixmap(); // clear image for low memory situations
                                // on embedded devices.
//! [5]
    deskwidget = QApplication::desktop();
    QCursor deskcursor(Qt::ArrowCursor);
    deskwidget->setCursor(deskcursor);
    Q_CHECK_PTR( deskwidget );
    deskwidget->showFullScreen();

    deskwidget->hide();
//    originalPixmap = QPixmap::grabWindow(deskwidget->winId());
//    updateScreenshotLabel();
//    if ( conf()["Ocultar Ventana InFlow"].toBool() ) {
//     myapp->show();
//    }
//    saveScreenshot();
//    setFocus(Qt::OtherFocusReason);
}




void LineSCWidget::saveScreenshot()
{

    QString format = "png";
    QString initialPath = QDir::currentPath() + tr("/sintitulo.") + format;

    QString fileName = QFileDialog::getSaveFileName(this, tr("Guardar como..."),
                               initialPath,
                               tr("%1 Archivos (*.%2);;Todos (*)")
                               .arg(format.toUpper())
                               .arg(format));
    if (!fileName.isEmpty()) {
       snapshot.save(fileName, format.toAscii());
        lineedit->setText( fileName );
   }


}

void LineSCWidget::setConf(QMap<QString, QVariant>& c) {
//     qDebug("...LineSCWidget::setConf...(1)...");
     _conf = c;
//     qDebug("...LineSCWidget::setConf...(2)...");

}

QMap<QString, QVariant>& LineSCWidget::conf() {

     return _conf;
}


QString LineSCWidget::text() const {
     Q_CHECK_PTR( lineedit );
     return lineedit->text();
}


void LineSCWidget::openFilename() {
     QString fileName = QFileDialog::getOpenFileName(this,
                                tr("Seleccione un archivo"),
                                lineedit->text(),
                                tr("Todos (*);;Archivos de Imgenes PNG (*.png)"));
    if (!fileName.isEmpty()) {
        lineedit->setText(fileName);
         lineedit->setFocus( Qt::PopupFocusReason );
    }

}
void LineSCWidget::buildWidget() {
     //CmdWidget::buildWidget();
     CmdWidget::buildWidget();
     capturebutton = new QToolButton;
     getfilebutton = new QToolButton;
     capturebutton->setIcon(QIcon(":/camera16.png"));
     capturebutton->setToolTip(tr("Captura la pantalla actual: <b>todas</b> las ventanas que estn en el escritorio"));
     Q_CHECK_PTR( capturebutton );
     getfilebutton->setText( "..." );
     getfilebutton->setToolTip(tr("Selecciona un archivo que indica una captura de pantalla"));
     mainLayout->insertWidget(3, capturebutton);
     mainLayout->insertWidget(4, getfilebutton);
     connect(capturebutton, SIGNAL(clicked()), this, SLOT(selCaptureType()) );
     connect(getfilebutton, SIGNAL(clicked()), this, SLOT(openFilename()) );

}

void LineSCWidget::selCaptureType() {
    QString option = combocurrentText(conf()[trUtf8("Tipo de captura")].toString());
//    qDebug("...selCaptureType:option : |%s|", qPrintable(option));
    if ( option == trUtf8("Imagen") )  {
//        qDebug("...slotGrab...");
        slotGrab();
        deleteTrayIcon();
    }
    else if (option == trUtf8("Video") ) {
//        qDebug("...grabCast...");
        selectGrabCast();
    }
    Q_ASSERT( option == trUtf8("Imagen") || option == trUtf8("Video"));

}


void LineSCWidget::setFocus ( Qt::FocusReason reason ) {
//     qDebug("...LineSCWidget::setFocus....");
     Q_CHECK_PTR( lineedit );
     QWidget::setFocus ( reason );
     lineedit->setFocus( reason);

}

// Métodos para grabación de screencast

void LineSCWidget::checkSystemTray() {

if (!QSystemTrayIcon::isSystemTrayAvailable()) {
        QMessageBox::critical(0, QObject::tr("Systray"),
                              QObject::tr("I couldn't detect any system tray "
                                          "on this system."));
    }

}


void LineSCWidget::createTrayIcon() {
    if ( trayIcon == NULL ) {
            trayIcon = new QSystemTrayIcon(this);
     }
     qDebug("...trayIcon->setVisible(true);...");
     trayIcon->setToolTip(trUtf8("Presione para parar la grabación del screencast"));
     trayIcon->setVisible(true);
}


void LineSCWidget::deleteTrayIcon() {
 //    qDebug("...trayIcon->setVisible(false);...");
     if ( trayIcon ) {
        trayIcon->setVisible(false);
    }
}

void LineSCWidget::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
        break;
    case QSystemTrayIcon::DoubleClick:
        stopCast();
        break;
    case QSystemTrayIcon::MiddleClick:
        break;
    default:
        ;
    }
}

void  LineSCWidget::stopCast() {
  gst_element_set_state (pipeline, GST_STATE_NULL);
  //delete argv[0];
  //delete argv;
  deleteTrayIcon();
  if (!fileNameCast.isEmpty()) {
      lineedit->setText(fileNameCast);
      setFocus( Qt::PopupFocusReason );
  }
//  qDebug ("Deleting pipeline\n");
  //g_free (pipeline);


}

void  LineSCWidget::selectGrabCast() {

    if ( delay() ) {
        grabTimer.setSingleShot( true );
        grabTimer.start( delay() * 1000 );
    }
    else {

        if ( mode() == Region ) {
//                qDebug("mode() == Region ");
                rgnGrab = new RegionGrabber();
                Q_CHECK_PTR( rgnGrab );
                connect( rgnGrab, SIGNAL( regionGrabbed( const QPixmap & ) ),
                                  SLOT( slotRegionSCGrabbed( const QPixmap & ) ) );
        }
        else if (mode() ==  WindowUnderCursor ) {
//                qDebug("...selectGrabCast::Video...WindowUnderCursor");
                QRect windowrect;
                snapshot = WindowGrabber::grabCurrent(windowrect /*,includeDecorations()*/  );
                grabSCRect = windowrect;
                int result = QMessageBox::information(QApplication::activeWindow(),
                                                        trUtf8("Complemento de Inflow"),
                                                        trUtf8("Ahora se iniciará el grabación de video de pantalla (screencast)"
                                                               "\nRealice 'doble-click' en la ícono de la bandeja de entrada para "
                                                               "\nDETENER la grabación"
                                                               "\nPresione el botón 'Cancelar' para NO grabar"),
                                                        QMessageBox::Ok | QMessageBox::Cancel );
                  if ( result == QMessageBox::Cancel ) {
                      deleteTrayIcon();
                      return;
                  }
                grabCast();
        }

        else if (mode() == FullScreen  ) {
            QDesktopWidget* desktopwidget  = QApplication::desktop();
              Q_CHECK_PTR( desktopwidget );
                grabSCRect = desktopwidget->geometry();
                int result = QMessageBox::information(QApplication::activeWindow(),
                                                      trUtf8("Complemento de Inflow"),
                                                      trUtf8("Ahora se iniciará el grabación de video de pantalla (screencast)"
                                                             "\nRealice 'doble-click' en la ícono de la bandeja de entrada para "
                                                             "\nPresione el botón 'Cancelar' para NO grabar"),
                                                      QMessageBox::Ok | QMessageBox::Cancel );

                if ( result == QMessageBox::Cancel ) {
                    deleteTrayIcon();
                    return;
                }
                grabCast();
        }
    }
}



void  LineSCWidget::grabCast() {


//   gchar *name;
  GError *error = NULL;


  /* Initialisation */
//  int argc = 1;
//  char **argv = NULL;
//  argv = new char*[1];
//  argv[0] = new char[10];
//  strcpy(argv[0],"inflow");
  //gst_init (&argc, &argv);
  createTrayIcon();
fileNameCast = QFileDialog::getSaveFileName(QApplication::activeWindow(), tr("Guardar archivo de video .ogg"),
                            trUtf8("sintitulo.ogg"),
                            trUtf8("Videos (*.ogg)"));
  gst_init (NULL, NULL);
  //gchar *pipeline_desc = g_strdup_printf ("istximagesrc name=videosource display-name=:0.0 screen-num=0 use-damage=false ! video/x-raw-rgb,framerate=10/1 ! videorate ! ffmpegcolorspace ! videoscale method=1 ! video/x-raw-yuv,width=1280,height=1024,framerate=10/1 ! theoraenc ! oggmux name=mux ! filesink location=/tmp/screencast.ogg");
//  gchar *pipeline_desc = g_strdup_printf ("istximagesrc name=videosource display-name=:0.0"
//  " screen-num=0 use-damage=false ! video/x-raw-rgb,framerate=10/1 ! videorate ! ffmpegcolorspace"
//  "! videoscale method=1 ! video/x-raw-yuv,width=2720,height=1024,framerate=10/1 ! theoraenc "
//  "! oggmux name=mux ! filesink location=%s",qPrintable(fileNameCast));

  // Imprimiendo para la grabación
//  qDebug("istximagesrc name=videosource display-name=:0.0 screen-num=0 "
//    " startx=%d starty=%d endx=%d endy=%d  use-damage=false show-pointer=false ! video/x-raw-rgb,framerate=10/1 "
//    "! videorate ! ffmpegcolorspace ! videoscale method=1 ! video/x-raw-yuv,width=%d,height=%d "
//    ",framerate=10/1 ! theoraenc ! oggmux name=mux ! filesink location=%s",grabSCRect.topLeft().x(),
//    grabSCRect.topLeft().y(),grabSCRect.bottomRight().x(), grabSCRect.bottomRight().y(),grabSCRect.width(),
//    grabSCRect.height(),qPrintable(fileNameCast));

   gchar *pipeline_desc = g_strdup_printf ("istximagesrc name=videosource display-name=:0.0 screen-num=0 "
    " startx=%d starty=%d endx=%d endy=%d  use-damage=false show-pointer=true ! video/x-raw-rgb,framerate=10/1 "
    "! videorate ! ffmpegcolorspace ! videoscale method=1 ! video/x-raw-yuv,width=%d,height=%d "
    ",framerate=10/1 ! theoraenc ! oggmux name=mux ! filesink location=%s "
//    "! mux. gconfaudiosrc name=audiosource ! audioconvert ! vorbisenc ! queue ! mux."
    ,grabSCRect.topLeft().x(),
    grabSCRect.topLeft().y(),grabSCRect.bottomRight().x(), grabSCRect.bottomRight().y(),grabSCRect.width(),
    grabSCRect.height(),qPrintable(fileNameCast));



//  qDebug ("Going to run pipeline %s\n", pipeline_desc);


  pipeline = gst_parse_launch (pipeline_desc, &error);
  if (error)
  {
//    qDebug ("Error parsing pipeline: %s\n", error->message);
    g_error_free (error);
    //return FALSE;
  }

  if (!pipeline /*|| !source || !demuxer || !decoder || !conv || !sink */) {
    qDebug("One element could not be created. Exiting.\n");
    //return -1;
  }

//  g_object_get (G_OBJECT (pipeline), "name", &name, NULL);
//  qDebug ("The name of the element is '%s'.\n", name);
//  g_free (name);
  gst_element_set_state (pipeline, GST_STATE_PLAYING);


}


Q_EXPORT_PLUGIN2(widget_SCWidget, SCWidgetPlugin)


