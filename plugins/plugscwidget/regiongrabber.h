/*
  Copyright (C) 2003 Nadeem Hasan <nhasan@kde.org>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or ( at your option ) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this library; see the file COPYING.  If not, write to
  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
  Boston, MA 02110-1301, USA.
*/

#ifndef REGIONGRABBER_H
#define REGIONGRABBER_H

#include <QLabel>
#include <QPixmap>
#include <QMouseEvent>
#include <QKeyEvent>

class QTimer;
class QRubberBand;

class SizeTip : public QLabel
{
  public:
    SizeTip( QWidget *parent );
    ~SizeTip() {}

  void setTip( const QRect &rect );
  void positionTip( const QRect &rect );
};

class RegionGrabber : public QWidget
{
  Q_OBJECT

  public:
    RegionGrabber();
    ~RegionGrabber();
    QRect grabRect() const { return grabrect; }
  protected slots:
    void initGrabber();
    void updateSizeTip();

  signals:
    void regionGrabbed( const QPixmap & );

  protected:
    void mousePressEvent( QMouseEvent *e );
    void mouseReleaseEvent( QMouseEvent *e );
    void mouseMoveEvent( QMouseEvent *e );
    void keyPressEvent( QKeyEvent *e );

    bool mouseDown;
    QRect grabrect;
    QPixmap pixmap;

    SizeTip *sizeTip;
    QTimer *tipTimer;
    QRubberBand *band;
};

#endif // REGIONGRABBER_H

