#!/usr/bin/python
# -*- coding: utf-8  -*-

import psycopg2
import sys
import os
import getopt
import getpass
   
def createTracUser(filename,user,passw,dbname,host="localhost"):
        if not os.path.exists(filename):
            print "No se puede leer el archivo ""%s""" % filename
            return
	f = open(filename,"r")
	lines = f.readlines()
	users = {}
	try:
		for l in lines:
			localuser = l.split(":")[0]
			localpassw = l.split(":")[1][:-1]
#			print "%s....|%s|" % (user,passw)        
			users[localuser] = localpassw
        finally:
            f.close()
	#print users
	stringconnect = "dbname='%s' host='%s' user='%s' password='%s'" % (dbname,host,user,passw)
	#print stringconnect
	conn = None
	try:	
		conn=psycopg2.connect(stringconnect)
	except Exception,inst:
		print "Error: %s" % inst.args[0][:-1]      # arguments stored in .args
		print "No se puede conectar a la base de datos %s" % dbname
		sys.exit()
	
	cur = conn.cursor()

	try:
		cur.execute("""select usename from pg_catalog.pg_shadow""")			
	except psycopg2.OperationalError,inst:
		#print type(inst)     # the exception instance
		print inst.args[0]
		sys.exit()
	rows = cur.fetchall()
	currentusers = []
	for row in rows:		
		currentusers.append(row[0])
	
#	print "--------------------------------------------------------------------"
#	print currentusers
#	print "--------------------------------------------------------------------"
	
	for key in users.keys():
		if key in currentusers:
			sttdrop = "DROP USER %s" % key			
			try:
				cur.execute(sttdrop)
				conn.commit()
				print "borrando: \"%s\" " % sttdrop
			except Exception,inst:
				print "Error eliminando usuario, del tipo: %s" % type(inst)
				sys.exit()			
		sttcreate = "CREATE USER %s PASSWORD '%s'" % (key,users[key])
		try:
			cur.execute(sttcreate)
			conn.commit()
			print "La sentencia '%s' se ejecutó correctamente" % ("CREATE USER %s PASSWORD '%s'" % (key,"XXXXXXXXXX"))
		except Exception,inst:
			print "Error: %s" % inst.args[0][:-1]      # arguments stored in .args
			sys.exit()
		
	conn.close()
        return users
	    
def listTracUser(user,passw,dbname,host="localhost", pusers = None):
	stringconnect = "dbname='%s' host='%s' user='%s' password='%s'" % (dbname,host,user,passw)
	#print stringconnect
	try:	
		conn=psycopg2.connect(stringconnect)
	except:
		print "listTracUser: No se puede conectar a la base de datos con la cadena: '%s'" % stringconnect
		sys.exit()
	cur = conn.cursor()
	try:
		cur.execute("""SELECT username,action FROM permission ORDER BY username""")
	except Exception,inst:
		print "Ocurrió el siguiente error: '%s' listando los permisos" % inst.args[0]
		sys.exit()
	
	rows = cur.fetchall()
	users = {}
	for row in rows:
		if row[0] in pusers or row[0] == "authenticated":
			plist = []
			if users.has_key( row[0] ) :
				plist = users[ row[0] ]
			plist.append(row[1])		
			users[ row[0] ] = plist
	currentlist = None
#	for key in users.keys():
#		print key
#		print users[ key ]
		
	statementlist = []
	statementgrantcreate = "GRANT INSERT ON ticket TO %s"
	statementgrantall = "GRANT ALL PRIVILEGES ON ticket TO %s"
	statementgrantmodify = "GRANT UPDATE ON ticket TO %s; GRANT INSERT ON ticket_change TO %s; GRANT UPDATE ON ticket_change TO %s;"
	statementgrantdelete = ""

	if users.has_key( "authenticated" ):
		currentlist = users [ "authenticated" ]	
		users.pop("authenticated")
		for key in users.keys():
			if "TICKET_CREATE" in currentlist or currentlist in users[ key ]:
				if (statementgrantcreate % key) not in statementlist:
					statementlist.append( statementgrantcreate % key )
			if "TICKET_MODIFY" in currentlist:
				if (statementgrantmodify % (key,key,key)) not in statementlist:
					statementlist.append( statementgrantmodify % (key,key,key) )
			if "TICKET_ADMIN" in currentlist:
				if (statementgrantall % key) not in statementlist:
					statementlist.append( statementgrantall % key )	
					
					
	for key in users.keys():
		if "TICKET_CREATE" in users[ key ] or ("TICKET_APPEND" in users[ key ]):
			if (statementgrantcreate % key) not in statementlist:
				statementlist.append( statementgrantcreate % key )
		if "TICKET_MODIFY" in users[ key ]:
			if (statementgrantmodify % (key,key,key)) not in statementlist:
				statementlist.append( statementgrantmodify % (key,key,key) )
		if "TICKET_ADMIN" in  users[ key ]:
			if (statementgrantall % key) not in statementlist:
				statementlist.append( statementgrantall % key )	
	
	print "Ejecutando las siguientes sentencias: ---------------------------"
	for statement in statementlist:
		print statement
	print "-----------------------------------------------------------------"
	for statement in statementlist: 
		try:
			cur.execute(statement)
		except Exception,inst:
			print "Ocurrió el siguiente error '%s' al tratar de realizar la ejecución de la siguiente sentencia: '%s'" % (inst.args[0][:-1],statement)    
			sys.exit()
	try:
		conn.commit()	
	except Exception,inst:
		print "Ocurrió el siguiente error '%s' al tratar de realizar commit" % inst.args[0][:-1]      # arguments stored in .args
		sys.exit()
	
	print "realizado con éxito...ok"
	
	
def usage():
	print "Aplicación de asignación  de usuarios del sistema de Gestión TRAC a usuarios Postgresql "
	print "Trac2Postgresql Versión 0.1.alpha"
	print ""
	print "Asigna los usuarios y sus respectivos permisos desde el modelo de datos del"
	print "sistema de gestión de Proyectos Trac (http://trac.edgewall.org) a  usuarios Postgresql"
	print ""
	print "Programación: Ing. Víctor Bravo Bravo vbravo@cenditel.gob.ve" 
	print "              http://www.victorbravo.info"
	print ""
	print "* Debe especificar obligatoriamente:"
	print "  -f <Arhivo htpasswd> o --htfile=<Arhivo htpasswd>: Archivo generado con la aplicación htpasswd"
	print "  -u  <Usuario> o --user=<Usuario>: Usuario de la base de datos a conectarse"
	print "  -d <BaseDeDatos> o --database=<BaseDeDatos>: Nombre de la base de datos a conectarse"
	print "* Parámetros opcionales:"
	print "  -h <Host> o --host=<Host>: Nombre del servidor o host a conectarse, si no se especifica se conectará a servidor 'localhost'"
	print "  -p <Contraseña> o --password=<Contraseña>: texto de la palabra clave o contraseña para acceder al servidor y base de datos"
	print ""
	



	
if __name__ == "__main__":
	# agregar la opción de leer los parámetros de la  línea de comando
#	args = sys.argv[1:]
	opts = None
	args = None
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hf:H:d:u:p:v", ["help","htfile=","host=", "database=","user=","password="])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)
#	o, a = getopt.getopt(sys.argv[1:], 'ct:f:i:h')                            
	nopts = {}
	#print opts
	for k,v in opts:    
		nopts[k] = v
	if  (not nopts.has_key('-f') or not nopts.has_key('-d') or not nopts.has_key('-u')) and not nopts.has_key('-h'):
		usage(); 
		sys.exit(0)
	output = None
	verbose = False
	htfile     = ""
	htdatabase = ""
	htuser     = ""
	htpassword = ""
	hthost     = ""
	for o, a in opts:
		if o == "-v":
			verbose = True
		elif o in ("-h", "-help"):
			usage()
			sys.exit()
		elif o in ("-f", "--htfile"):
		      htfile = a
		elif o in ("-u", "--user"):
			htuser = a
		elif o in ("-p", "--password"):
			htpassword = a
		elif o in ("-d", "--database"):
			htdatabase = a
		elif o in ("-H", "--host"):
			hthost = a		
		else:
			print "Opción desconocida: %s" %  o
			sys.exit()
	
	if len(hthost) == 0:
		hthost = "localhost"
	if len(htpassword) == 0:
		htpassword = getpass.getpass(prompt='Escriba la contraseña:')
	
	print "htpassword: |%s|" % htpassword
	users = createTracUser(htfile,htuser,htpassword,htdatabase,hthost)
        listTracUser(htuser,htpassword,htdatabase,hthost, users)
	#users = createTracUser("prueba","vbravo","$apr1$AFlrHm/I$Pi6YOEVbkW7FXjBEioBj10","tracrootve","seguridad.cenditel.gob.ve")
        #listTracUser("vbravo","$apr1$AFlrHm/I$Pi6YOEVbkW7FXjBEioBj10","tracrootve","seguridad.cenditel.gob.ve", users)


   
#rows = cur.fetchall()
#print "Registros modificados: %d" % len(row)

