<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >
<xsl:output method = "text" />

<xsl:template match = "/" >
<xsl:text > Documento a Firmar:
</xsl:text>
<xsl:apply-templates select = "//Document[1]/child::*" mode = "print" />
<xsl:text >
</xsl:text>               
</xsl:template>
<xsl:template match = "*" mode = "print" >
<xsl:value-of select = "concat(position(),'.-',name(),': ')" /><xsl:value-of select = "." />
<!--<xsl:if test = "not(position()=last())" >
                    <xsl:text > - </xsl:text>
               </xsl:if>-->
<xsl:text >
</xsl:text> 
</xsl:template>
</xsl:stylesheet> 
