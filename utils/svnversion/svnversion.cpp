#include "svnversion.h"
#include <QFile>
#include <QTemporaryFile>
#include <QByteArray>
#include <QTextStream>
#include <QRegExp>
#include <QString>
#include "svncpp/client.hpp"
#include "svncpp/revision.hpp"
#define OINFO_VERSION "Sistema Automatizado para la Firma y el Estampado Electrónica de Tiempo (SAFET) \\nVictor Bravo (vbravo@cenditel.gob.ve) Antonio Araujo (aaraujo@cenditel.gob.ve) Cenditel 2009-2010.\\nLicencia GPLv2 http://www.gnu.org/licenses/gpl-2.0.html"


int main(int argc, char* argv[]) {
   svn::Context *context = new svn::Context();
  svn::Client client (context);
  QString infoRev,infoLastLog,infoNumber;
  char bufferRev[500], bufferLastLog[500],bufferNumber[500];
  if (argc < 2 ) {
       fprintf(stderr,"Numero de parametros invalidos, formato: svnversion <archivo_version> [titulo_info] [info]\n");
       return 1;
  }
  //svn::Path target = svn::Path("svn://repositorio.cenditel.gob.ve/var/local/svn/seguridad");
  svn::Path target = svn::Path(".");
  const svn::LogEntries * entries =
    client.log (target.c_str (), svn::Revision::START,
                svn::Revision::HEAD, true, false);

   infoNumber = "\"0.0.0.0\"\n";
   if ( argc == 3 ) {
	infoNumber = "\""+QString(argv[2])+"\"\n";	
   }  

  if (entries == 0)
      return 0;

    long index=0;
    svn::LogEntries::const_iterator it;
   for (it=entries->begin (); it != entries->end (); it++ )
    {
      const svn::LogEntry & entry = *it; 
      sprintf(bufferRev, "\"%ld\"\n", (long) entry.revision);
      sprintf(bufferLastLog, "\"%s\"\n", entry.message.c_str ());
      sprintf(bufferNumber, qPrintable(infoNumber));

      index++;
      if ( index >= 1) break;
    }

  infoRev = bufferRev;
  infoLastLog = bufferLastLog;




   QTemporaryFile filetmp;
   QString name = argv[1];
    if (filetmp.open())  {
       QFile file(name);


     if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
         return 1;

     QTextStream out(&filetmp);
     QRegExp rxNumber;
     QRegExp rxLastlog;
     QRegExp rxRevision;
     rxNumber.setPattern("NUMBER_VERSION\\s.*");
     rxRevision.setPattern("REVISION_VERSION\\s.*");
     rxLastlog.setPattern("LASTLOG_VERSION\\s.*");
     while (!file.atEnd()) {
         QString line = file.readLine().data();
         line.replace(QRegExp(rxNumber),"NUMBER_VERSION "+infoNumber);
         line.replace(QRegExp(rxRevision),"REVISION_VERSION "+infoRev);
         line.replace(QRegExp(rxLastlog),"LASTLOG_VERSION "+infoLastLog);
         out << line;
     }
     file.remove();
    }
    QString nametmp = filetmp.fileName();

    filetmp.close();

    QFile::copy(nametmp, name);
    return 0;
}

