/*
* SAFET Sistema Automatizado para la Firma ElectrÃ³nica y Estampado de Tiempo
* Copyright (C) 2008 VÃ­ctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*     
* CENDITEL Fundacion Centro Nacional de Desarrollo e InvestigaciÃ³n en TecnologÃ­as Libres 
*  
* Este programa es software libre; Usted puede usarlo bajo los tÃ©rminos de la licencia de 
* software GPL versiÃ³n 2.0 de la Free Software Foundation. 
* 
* Este programa se distribuye con la esperanza de que sea Ãºtil, pero SI NINGUNA GARANTÃA;
* tampoco las implÃ­citas garantÃ­as de MERCANTILIDAD o ADECUACIÃN A UN PROPÃSITO PARTICULAR.
* Consulte la licencia GPL para mÃ¡s detalles. Usted debe recibir una copia de la GPL junto 
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5Âº Piso, Boston, MA 02110-1301, USA.
*
*  safet: AplicaciÃ³n para la lÃ­nea de comandos
*/

//
// C++ Implementation: safetconsoleapp
//
// Description: 
//
//
// Author: VÃ­ctor R. Bravo <vbravo@cenditel.gob.ve>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//

#ifndef SVNVERSION_H
#define SVNVERSION_H

#endif
