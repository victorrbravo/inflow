TEMPLATE = app
INCLUDEPATH += /usr/local/include \
        /usr/local/rapidsvn-0.9.4/include \
        /usr/local/rapidsvn-0.9.4/include/svncpp \
        /usr/include/subversion-1/ \
       /usr/include/apr-1.0 \
      ../../src
DEFINES += QT_DEBUG SAFET_NO_DBXML
QT += core \
    sql \
    xml
#QT -= gui
HEADERS += svnversion.h
SOURCES += svnversion.cpp
LIBS += -L/usr/local/lib \
       -L../../src \
       -lsafet \
       -lsvncpp
CONFIG += qt \
 ordered \
 thread \
 warn_off \
 debug_and_release
target.path =/usr/local/bin
target.files = svnversion
INSTALLS += target
