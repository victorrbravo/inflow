TARGET = safet
TEMPLATE = lib
INCLUDEPATH += /usr/include \
#    /usr/include/digidocpp \
    /usr/include/libdigidoc \
    /usr/include/libxml2 \
    ../gsoap \
    ../src \
    ../inflow
DEFINES += QT_DEBUG \
    SAFET_NO_DBXML

DEFINES +=  SAFET_XML2 \
#    SAFET_OPENSSL \
#    SAFET_DIGIDOC \
    SAFET_TAR \
#    SAFET_GSOAP \


QT += core \
    sql \
    network \
    webkit \
    gui \
    svg \
    xml

# QT -= gui
HEADERS += ../src/SafetSQLParser.h \
    ../src/SafetParser.h \
    ../src/SafetNode.h \
    ../src/SafetToken.h \
    ../src/SafetConnection.h \
    ../src/SafetPort.h \
    ../src/SafetCondition.h \
    ../src/SafetTask.h \
    ../src/SafetVariable.h \
    ../src/SafetDocument.h \
    ../src/SafetWorkflow.h \
    ../src/SafetConfiguration.h \
    ../src/SafetXmlObject.h \
    ../src/SafetYAWL.h \
    ../src/SafetXmlRepository.h \
    ../src/SafetConfFile.h \
    ../src/SafetLog.h \
    ../src/SafetDirXmlRepository.h \
    ../src/SafetInterfaces.h \
    ../src/SafetStats.h \
    ../src/SafetPlugin.h \
#    ../src/SslCertificate.h \
    ../src/SafetAutofilter.h \
    ../src/libdotar.h \ 
    ../src/SafetRecursiveFilter.h \
#    ../src/safetpkcs12.h \
    ../src/SafetCipherFile.h \
    ../src/SafetParameter.h 
#    ../inflow/mainwindow.h
SOURCES += ../src/SafetSQLParser.cpp \
    ../src/SafetParser.cpp \
    ../src/SafetNode.cpp \
    ../src/SafetToken.cpp \
    ../src/SafetConnection.cpp \
    ../src/SafetPort.cpp \
    ../src/SafetCondition.cpp \
    ../src/SafetTask.cpp \
    ../src/SafetYAWL.cpp \
    ../src/SafetVariable.cpp \
    ../src/SafetDocument.cpp \
    ../src/SafetWorkflow.cpp \
    ../src/SafetConfiguration.cpp \
    ../src/SafetXmlObject.cpp \
    ../src/SafetXmlRepository.cpp \
    ../src/SafetConfFile.cpp \
    ../src/SafetLog.cpp \
    ../gsoap/stdsoap2.cpp \
    ../gsoap/soapC.cpp \
    ../gsoap/soapClient.cpp \
    ../src/SafetDirXmlRepository.cpp \
    ../src/SafetStats.cpp \
    ../src/SafetPlugin.cpp \
#    ../src/SslCertificate.cpp \
    ../src/SafetAutofilter.cpp \
    ../src/libdotar.c \
    ../src/SafetRecursiveFilter.cpp \
#    ../src/safetpkcs12.cpp \
    ../src/SafetCipherFile.cpp \ 
    ../src/SafetParameter.cpp 
#     ../inflow/mainwindow.h

LIBS += -L/usr/lib \
    -L/usr/lib/graphviz \
    -ldl \
    -ltar \ 
    -lxml2 \
    -L../inflow
CONFIG += qt \
    ordered \
    thread \
    warn_off \
    debug_and_release \
    dll
VERSION = 0.1.0
contains( DEFINES, SAFET_DBXML ) { 
    # DEFINES contains 'SAFET_DBXML'
    message( "Configuring for dbxml..." )
    HEADERS += SafetDbXmlRepository.h
    SOURCES += SafetDbXmlRepository.cpp
    INCLUDEPATH += /usr/local/dbxml/install/include \
        /usr/local/dbxml/install/include/dbxml
    LIBS += -L/usr/local/dbxml/install/lib \
        -ldbxml \
        -ldb \
        -ldb_cxx \
        -lxqilla
}
target.path = /usr/lib/libsafet
target.files = libsafet.so*
INSTALLS += target
