/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Thu Apr 4 17:00:46 2013
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../inflow/mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
      75,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,   12,   11,   11, 0x0a,
      29,   12,   11,   11, 0x0a,
      47,   42,   11,   11, 0x0a,
      60,   11,   11,   11, 0x2a,
      69,   11,   11,   11, 0x0a,
      87,   11,   11,   11, 0x0a,
     111,  109,   11,   11, 0x0a,
     140,   11,   11,   11, 0x08,
     157,   11,   11,   11, 0x08,
     165,   11,   11,   11, 0x08,
     190,  177,   11,   11, 0x08,
     223,  216,   11,   11, 0x28,
     244,   11,   11,   11, 0x28,
     258,   11,   11,   11, 0x08,
     275,   11,   11,   11, 0x08,
     289,   11,   11,   11, 0x08,
     308,   11,   11,   11, 0x08,
     323,   11,   11,   11, 0x08,
     342,   11,   11,   11, 0x08,
     361,   11,   11,   11, 0x08,
     383,  379,   11,   11, 0x08,
     405,   11,   11,   11, 0x08,
     422,   11,   11,   11, 0x08,
     431,   11,   11,   11, 0x08,
     449,  441,   11,   11, 0x08,
     476,   11,   11,   11, 0x08,
     493,   11,   11,   11, 0x08,
     512,   11,   11,   11, 0x08,
     531,   11,   11,   11, 0x08,
     548,   11,   11,   11, 0x08,
     568,   11,   11,   11, 0x08,
     588,   11,   11,   11, 0x08,
     610,   11,   11,   11, 0x08,
     645,   11,   11,   11, 0x08,
     665,   11,   11,   11, 0x08,
     686,   11,   11,   11, 0x08,
     705,   11,   11,   11, 0x08,
     724,   11,   11,   11, 0x08,
     744,   11,   11,   11, 0x08,
     763,   11,   11,   11, 0x08,
     783,   11,   11,   11, 0x08,
     801,   11,   11,   11, 0x08,
     841,   11,   11,   11, 0x08,
     859,  857,   11,   11, 0x08,
     903,   11,   11,   11, 0x28,
     925,  857,   11,   11, 0x08,
     970,   11,   11,   11, 0x28,
     995,  993,   11,   11, 0x08,
    1035, 1026,   11,   11, 0x08,
    1057,   11,   11,   11, 0x08,
    1075,   11,   11,   11, 0x08,
    1090,   11,   11,   11, 0x08,
    1104,   11,   11,   11, 0x08,
    1121,   11,   11,   11, 0x08,
    1139,   11,   11,   11, 0x08,
    1161,   11,   11,   11, 0x08,
    1188, 1183,   11,   11, 0x08,
    1228,   11, 1220,   11, 0x08,
    1256, 1253,   11,   11, 0x08,
    1293, 1289, 1220,   11, 0x08,
    1325,   11,   11,   11, 0x0a,
    1337,   11,   11,   11, 0x0a,
    1349,   12,   11,   11, 0x0a,
    1373,   12,   11,   11, 0x0a,
    1401, 1399,   11,   11, 0x0a,
    1425,   11,   11,   11, 0x2a,
    1445,   11,   11,   11, 0x0a,
    1459,   11, 1454,   11, 0x0a,
    1471,   11,   11,   11, 0x0a,
    1495,   11, 1485,   11, 0x0a,
    1513,   11,   11,   11, 0x0a,
    1528,   11,   11,   11, 0x0a,
    1554,   11,   11,   11, 0x0a,
    1583, 1574,   11,   11, 0x0a,
    1627, 1624,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0url\0browse(QUrl)\0mailTo(QUrl)\0"
    "sign\0toSend(bool)\0toSend()\0toDoGoPanelEdit()\0"
    "createInitialTables()\0,\0"
    "ftpCommandFinished(int,bool)\0"
    "timeHideResult()\0about()\0toLoadWeb()\0"
    "action,isgui\0toInputForm(QString,bool)\0"
    "action\0toInputForm(QString)\0toInputForm()\0"
    "toInputConsole()\0toInputSign()\0"
    "toInputConfigure()\0toInputUsers()\0"
    "toDelAllWorkflow()\0toDelOneWorkflow()\0"
    "toClearTextEdit()\0opt\0checkSelInputTab(int)\0"
    "selInputTab(int)\0doQuit()\0doPrint()\0"
    "printer\0doPrintDocument(QPrinter*)\0"
    "doPrintPreview()\0doGeneralOptions()\0"
    "doWidgetsOptions()\0setToInputForm()\0"
    "setToInputConsole()\0setToInputReports()\0"
    "setToInputFlowGraph()\0"
    "setToInputManagementSignDocument()\0"
    "doGetSignDocument()\0doSendSignDocument()\0"
    "checkGoPrincipal()\0addToHistoryList()\0"
    "editToHistoryList()\0delToHistoryList()\0"
    "saveToHistoryList()\0loadEditActions()\0"
    "insertFromHistoryList(QListWidgetItem*)\0"
    "showSmartMenu()\0a\0"
    "showSmartMenuWidget(DockSbMenu::ShowAction)\0"
    "showSmartMenuWidget()\0"
    "showShowResultWidget(DockSbMenu::ShowAction)\0"
    "showShowResultWidget()\0m\0"
    "showSuccessfulMessage(QString)\0filename\0"
    "drawWorkflow(QString)\0doAssistantHelp()\0"
    "toChangeUser()\0doSaveGraph()\0"
    "doRestoreGraph()\0doCompareGraphs()\0"
    "doLoadConfiguration()\0doSaveConfiguration()\0"
    "path\0setPathOfSafetDocument(QString)\0"
    "QString\0getPathOfSafetDocument()\0rx\0"
    "doInsertInAuthConfFile(QRegExp&)\0key\0"
    "searchFieldsInAuthConf(QString)\0"
    "doLoadFTP()\0doSaveFTP()\0linkClickedSbMenu(QUrl)\0"
    "linkClickedSbResult(QUrl)\0e\0"
    "setEnabledToolBar(bool)\0setEnabledToolBar()\0"
    "doExit()\0bool\0maybeSave()\0goPrincipal()\0"
    "TextEdit*\0currentTextEdit()\0threadEndJob()\0"
    "processMainWindowThread()\0manageDataSources()\0"
    "dbConfig\0manageDataSources(DatabaseConfigDialog*)\0"
    "ok\0executeJSCodeAfterLoad(bool)\0"
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: browse((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 1: mailTo((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 2: toSend((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: toSend(); break;
        case 4: toDoGoPanelEdit(); break;
        case 5: createInitialTables(); break;
        case 6: ftpCommandFinished((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 7: timeHideResult(); break;
        case 8: about(); break;
        case 9: toLoadWeb(); break;
        case 10: toInputForm((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 11: toInputForm((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 12: toInputForm(); break;
        case 13: toInputConsole(); break;
        case 14: toInputSign(); break;
        case 15: toInputConfigure(); break;
        case 16: toInputUsers(); break;
        case 17: toDelAllWorkflow(); break;
        case 18: toDelOneWorkflow(); break;
        case 19: toClearTextEdit(); break;
        case 20: checkSelInputTab((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: selInputTab((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 22: doQuit(); break;
        case 23: doPrint(); break;
        case 24: doPrintDocument((*reinterpret_cast< QPrinter*(*)>(_a[1]))); break;
        case 25: doPrintPreview(); break;
        case 26: doGeneralOptions(); break;
        case 27: doWidgetsOptions(); break;
        case 28: setToInputForm(); break;
        case 29: setToInputConsole(); break;
        case 30: setToInputReports(); break;
        case 31: setToInputFlowGraph(); break;
        case 32: setToInputManagementSignDocument(); break;
        case 33: doGetSignDocument(); break;
        case 34: doSendSignDocument(); break;
        case 35: checkGoPrincipal(); break;
        case 36: addToHistoryList(); break;
        case 37: editToHistoryList(); break;
        case 38: delToHistoryList(); break;
        case 39: saveToHistoryList(); break;
        case 40: loadEditActions(); break;
        case 41: insertFromHistoryList((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 42: showSmartMenu(); break;
        case 43: showSmartMenuWidget((*reinterpret_cast< DockSbMenu::ShowAction(*)>(_a[1]))); break;
        case 44: showSmartMenuWidget(); break;
        case 45: showShowResultWidget((*reinterpret_cast< DockSbMenu::ShowAction(*)>(_a[1]))); break;
        case 46: showShowResultWidget(); break;
        case 47: showSuccessfulMessage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 48: drawWorkflow((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 49: doAssistantHelp(); break;
        case 50: toChangeUser(); break;
        case 51: doSaveGraph(); break;
        case 52: doRestoreGraph(); break;
        case 53: doCompareGraphs(); break;
        case 54: doLoadConfiguration(); break;
        case 55: doSaveConfiguration(); break;
        case 56: setPathOfSafetDocument((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 57: { QString _r = getPathOfSafetDocument();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 58: doInsertInAuthConfFile((*reinterpret_cast< QRegExp(*)>(_a[1]))); break;
        case 59: { QString _r = searchFieldsInAuthConf((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 60: doLoadFTP(); break;
        case 61: doSaveFTP(); break;
        case 62: linkClickedSbMenu((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 63: linkClickedSbResult((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 64: setEnabledToolBar((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 65: setEnabledToolBar(); break;
        case 66: doExit(); break;
        case 67: { bool _r = maybeSave();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 68: goPrincipal(); break;
        case 69: { TextEdit* _r = currentTextEdit();
            if (_a[0]) *reinterpret_cast< TextEdit**>(_a[0]) = _r; }  break;
        case 70: threadEndJob(); break;
        case 71: processMainWindowThread(); break;
        case 72: manageDataSources(); break;
        case 73: manageDataSources((*reinterpret_cast< DatabaseConfigDialog*(*)>(_a[1]))); break;
        case 74: executeJSCodeAfterLoad((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 75;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
