/*
* SAFET Sistema Automatizado para la Firma Electrónica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*     
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigación en Tecnologías Libres 
*  
* Este programa es software libre; Usted puede usarlo bajo los términos de la licencia de 
* software GPL versión 2.0 de la Free Software Foundation. 
* 
* Este programa se distribuye con la esperanza de que sea útil, pero SI NINGUNA GARANTÍA;
* tampoco las implícitas garantías de MERCANTILIDAD o ADECUACIÓN A UN PROPÓSITO PARTICULAR.
* Consulte la licencia GPL para más detalles. Usted debe recibir una copia de la GPL junto 
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5º Piso, Boston, MA 02110-1301, USA.
*
*  safet: Aplicación para la línea de comandos
*/

//
// C++ Interface: safetconsoleapp
//
// Description: 
//
//
// Author: Víctor R. Bravo <vbravo@cenditel.gob.ve>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//
#ifndef SAFETCONSOLEAPP_H
#define SAFETCONSOLEAPP_H


#include <QCoreApplication>
#include <QFile>
#include <QTextStream>
#include <QMap>
#include "SafetYAWL.h"
#include "SafetLog.h"

/**
	@author Víctor R. Bravo <vbravo@cenditel.gob.ve>, Antonio Araujo Brett <aaraujo@cenditel.gob.ve>
*/

class SafetConsoleApp : public QCoreApplication
{

        SafetYAWL *configurator;
	bool parsed;
	QString gopt;	
        int verbose_flag;
        QMap<int,QString> commands;

public:
    SafetConsoleApp(int &argc, char **argv);
    SafetConsoleApp();
    void executeParsed();
    bool parse(int &argc, char **argv);

    ~SafetConsoleApp();

	/** 
	* \brief Lista las tareas de un flujo de trabajo
	*/
	void listTasks();
	void listDocuments();
	void manageData();
	void genGraph();
	void makeTemplate();
	void signDocument();
	/** 
	* \brief Calcula varios tipos de estadisticas 
	*/
 	void calStatistics();
    // Funcion de depuracion	
	/** 
	* \brief Muestra la ayuda del uso de la aplicacion de consola safet 
	*/
 	void displayUsage();

	void debuglog();
	void version();

        /*
          * \brief Genera una Consola (Shell)
          */
        void doShell();
        void processCommand(int command);
        void setCommands(const QMap<int,QString>& cmds) { commands = cmds; }


    /** 
    * \brief Funcion delegada (Callback) para gestionar un error
    */
    static void evalEventOnExit(SafetLog::Level);


    /** 
    * \brief Funcion delegada (Callback) para gestionar la entrada
    */
    static QString evalEventOnInput(SafetYAWL::TypeInput,const QString&, bool&);

};

#endif
