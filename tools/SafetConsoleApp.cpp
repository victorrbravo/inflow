/*
* SAFET Sistema Automatizado para la Firma Electrónica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*     
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigación en Tecnologías Libres 
*  
* Este programa es software libre; Usted puede usarlo bajo los términos de la licencia de 
* software GPL versión 2.0 de la Free Software Foundation. 
* 
* Este programa se distribuye con la esperanza de que sea útil, pero SI NINGUNA GARANTÍA;
* tampoco las implícitas garantías de MERCANTILIDAD o ADECUACIÓN A UN PROPÓSITO PARTICULAR.
* Consulte la licencia GPL para más detalles. Usted debe recibir una copia de la GPL junto 
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5º Piso, Boston, MA 02110-1301, USA.
*
*  safet: Aplicación para la línea de comandos
*/

//
// C++ Implementation: safetconsoleapp
//
// Description: 
//
//
// Author: Víctor R. Bravo <vbravo@cenditel.gob.ve>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
//


#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>

#include <ctype.h>
#include <termios.h>
#include <signal.h>


#include "SafetConsoleApp.h"
#include "SafetTask.h"
#include "SafetWorkflow.h"
#include "SafetTextParser.h"

void sig_caught(int signum);

SafetConsoleApp::SafetConsoleApp(int &argc, char **argv)
 : QCoreApplication(argc,argv) {

	verbose_flag = 0;
        configurator = NULL;
	SafetYAWL::setEvalExit( evalEventOnExit );// colocar antes del parse
        SafetYAWL::setEvalInput( evalEventOnInput );// colocar antes del parse
        configurator = new SafetYAWL();
        parse(argc,argv);

//	debuglog();
//	Q_CHECK_PTR( configurator->getWorkflows().at(0) );
//	SafetConsoleApp* me = static_cast<SafetConsoleApp*>(this);
//	connect(configurator->getWorkflows().at(0),SIGNAL(evalEventOnExit(SafetLog::Level)),me,
//	SLOT(evalEventOnExit(SafetLog::Level)));

}



SafetConsoleApp::~SafetConsoleApp() {
     SafetYAWL::streamlog <<  SafetLog::Action << tr("libreria SAFET finalizada correctamente...OK!");
  // qDebug("...saliendo...~SafetConsoleApp...");

     if ( configurator ) delete configurator;

}


void SafetConsoleApp::executeParsed() {

   if (!parsed) return;
   QMap<int, QString>::const_iterator i;
   for (i = commands.constBegin(); i != commands.constEnd(); ++i) {
	if ( i.key() != 'f' ) {
		processCommand(i.key() );
	}		     
  }

 configurator->closeDataSources();
 //   qDebug("*..executeParsed..saliendo...");
}


bool SafetConsoleApp::parse(int &argc, char **argv) {
       int c;
       int verbose_flag = 0;

	parsed = true;
	if (arguments().count() == 1 ) {
                doShell();
		//SafetYAWL::streamout << tr("Tipee 'safet ---help' o 'safet -h' para ver el modo de uso.") << endl;
                return true;
	}
       while (1)
         {
           static struct option long_options[] =
             {
               /* These options set a flag. */
               {"verbose", no_argument,       &verbose_flag , 1},
               {"brief",   no_argument,       &verbose_flag, 0},
               /* These options don't set a flag.
                  We distinguish them by their indices. */
               {"listtasks",   no_argument,       0, 'l'},
	       {"listconditions",   no_argument,       0, 'C'},
               {"check",    no_argument,       0, 'c'},
	       {"listdocuments",    no_argument,       0, 'd'},
	       {"data",    no_argument,       0, 'D'},
               {"file",  required_argument, 0, 'f'},
               {"autofilter",  required_argument, 0, 'a'},
	       {"task",  required_argument, 0, 't'},
		{"template",  no_argument, 0, 'T'},
	       {"variable",  required_argument, 0, 'v'},
               {"json",  required_argument, 0, 'j'},
	       {"graph",  required_argument, 0, 'g'},
	       {"sign",  no_argument, 0, 's'},
               {"keyvalue",  required_argument, 0, 'k'},
               {"plugins",  required_argument, 0, 'p'},
	       {"stat",  required_argument, 0, 'S'},
	       {"help",  no_argument, 0, 'h'},
	       {"version",  no_argument, 0, 'V'},
               {0, 0, 0, 0}
             };
           /* getopt_long stores the option index here. */
           int option_index = 0;
     
           c = getopt_long (argc, argv, "lCcf:a:t:Tj:dv:g:k:sS:hVD:p:",
                            long_options, &option_index);
     
           /* Detect the end of the options. */
           if (c == -1)
             break;

           switch (c)
             {
             case 0:
               /* If this option set a flag, do nothing else now. */
               if (long_options[option_index].flag != 0)
                 break;
               SafetYAWL::streamout << tr("Opcion: ") << long_options[option_index].name ;
               if (optarg)
                 SafetYAWL::streamout << tr(" con argumento: ") <<  optarg << endl;
               break;
             case 'l':
			commands['l'] = "list";	
               break;     
             case 'C':
			commands['C'] = "list";	
               break;     	
             case 'd':
			commands['d'] = "documents";			
               break;     
             case 'D':
			commands['D'] = optarg;			
               break;     
             case 'c':
			commands['c'] = "check";     	
	               break;         
             case 'p':
                        commands['p'] = optarg;
                       break;
              case 'f':
			commands['f'] = optarg;		               
	               break;
              case 'a':
                        commands['a'] = optarg;
                       break;
              case 'g':
			commands['g'] = optarg;		         
	               break;
     	     case 'v':
			commands['v'] = optarg;		               
                        //(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(!commands['v'].trimmed().startsWith("-"), tr("Debe especificar un nombre de variable, falta este parametro")) );                        
                        SafetYAWL::evalAndCheckExit(!commands['v'].trimmed().startsWith("-"), tr("Debe especificar un nombre de variable, falta este parametro"));
	               break;
	     case 'k':
			commands['k'] = optarg;		               
//			(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(!commands['k'].trimmed().startsWith("-"), tr("Debe especificar un valor para la clave, falta este parametro")) );		
        		break;
	     case 't':
			commands['t'] = optarg;		               
			break;
	     case 'T':
			commands['T'] = "template";		               
			break;
	     case 's':
			commands['s'] = "sign";		               
			break;
	     case 'S':
			commands['S'] = optarg;		               
			break;
	     case 'h':

			commands['h'] = "help";		               
			break;
	     case 'V': 
			commands['V'] = "version";
             case '?':
               /* getopt_long already printed an error message. */
               break;
     
             default:
			parsed = false;
             }
         }   

       //	qDebug("!commands.contains('f'): %d", !commands.contains('f'));
	if ( commands.contains('h') || commands.contains('V') || commands.contains('T')) return true;
	if ( !commands.contains('f')  ) {
		SafetYAWL::streamout << tr("*** No se especifico la ruta del archivo de flujo de trabajo (.xml) *** \n");
		SafetYAWL::streamout <<  tr("Opcion: -f <archivo> o --file <archivo> \n");
		SafetYAWL::streamout.flush();
		parsed = false;
		processCommand('f');
	}

        /* Print any remaining command line arguments (not options). */
       if (optind < argc)
         {
//	   qDebug("optind: %d argc: %d", optind, argc);
           SafetYAWL::streamout << tr("Parametro(s) invalido(s): ");
           while (optind < argc)
             SafetYAWL::streamout <<  argv[optind++];
             SafetYAWL::streamout << endl;
		parsed = false;
         }
     
	if (!parsed ) return false;

        configurator->openXML(commands['f']);
        configurator->openDataSources();
        configurator->setAutofilters( commands['a'] );
        configurator->convertXMLtoObjects();

	// llamada a la funcion que habilita las fuentes de datos
        //qDebug("SafetConsoleApp: configurator->openDataSources()");

	//qDebug("SafetYAWL::databaseOpen: %d", SafetYAWL::databaseOpen);
	
        //configurator->loadPlugins();

	if ( commands.contains('c') ) {
		SafetYAWL::streamout << tr("Chequeado...!") << endl;
	}

	return parsed;
}


void SafetConsoleApp::processCommand(int command) {
	bool e;
        Q_CHECK_PTR( configurator->getWorkflows().at(0) );
        switch ( command ) {
	case 'f':
		if (commands['f'].length() == 0 ) return;
// 		e = QFile::exists( commands['f'] );
// 		if (!e) {
// 			SafetYAWL::streamout << tr("No es posible leer el archivo de flujo de trabajo de nombre: \"%1\" ").arg(commands['f']) << endl;
// 			parsed = false;
// 			quit();
// 		}
		break;
	case 'l':
		listTasks();	
		break;
	case 'd': 
		listDocuments();
		break;
	case 'D': 
		manageData();
		break;
	case 'g':
		genGraph();
		break;
	case 's':
		signDocument();
		break;
	case 'S':
		calStatistics();
		break;
	case 'T':
		makeTemplate();
		break;
	case 'h':
		displayUsage();
		break;
	case 'V':
		version();
		break;

	default:;
	}
	
}



void SafetConsoleApp::displayUsage() {
SafetYAWL::streamout << tr("Uso: safet --file <archivo de flujo de trabajo> [comandos]") << endl;
SafetYAWL::streamout << tr("     safet -f <archivo de flujo de trabajo> [comandos]") << endl;  
SafetYAWL::streamout << tr("Cliente Safet de linea de comandos, version 0.1.beta") << endl;
SafetYAWL::streamout << tr("Tipee 'safet --help ' para ayuda sobre los comandos.") << endl;
SafetYAWL::streamout << tr("Tipee 'safet --version' o 'safet -V' para ver la version del cliente de linea de comandos.") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("Los comandos reciben parametros de tipo variable, clave o subcomando, o algunos") << endl;
SafetYAWL::streamout << tr("de los comandos no requieren ningun parametro. Si no se proveen parámetros a") << endl;
SafetYAWL::streamout << tr("estos comandos, se generara un mensaje de error.") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("Comandos disponibles:") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("-f --file=ARCHIVO\tIndica el archivo (ARCHIVO) de flujo de trabajo a procesar]") << endl; 
SafetYAWL::streamout << tr("\t\t\tGeneramente debe tener extension .xml y se valida con el archivo de") << endl;
SafetYAWL::streamout << tr("\t\t\tdefinicion yawlworkflow.dtd") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("-v --variable=NOMBRE\tSelecciona el nombre de la variable a procesar. La variable") << endl;
SafetYAWL::streamout << tr("\t\t\tdebe estar incluida en el archivo de flujo de trabajo a procesar, e identificada") << endl;
SafetYAWL::streamout << tr("\t\t\tcon el atributo 'id'.") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("-k --key=CLAVE\t\tIndica la clave para un comando de firma, u otro comando que") << endl;
SafetYAWL::streamout << tr("\t\t\tlo requiera, generalmente debe ir acompanada del parametro que selecciona el nombre") <<endl;
SafetYAWL::streamout << tr("\t\t\tde la variable") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("-s --stat=SUBCOMANDO\tGenera la estadistica seleccionada en el subcomando SUBCOMANDO.") << endl;
SafetYAWL::streamout << tr("\t\t\tLos subcomandos posibles son \"ndocs\" (lista la cantidad de documentos para todas las") << endl; 
SafetYAWL::streamout << tr("\t\t\tvariables) y \"path\" (indica en que actividad/tarea se encuentra el token segun") << endl;
SafetYAWL::streamout << tr("\t\t\tla clave indicada con el comando -k o --key)") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("-g --graph=ARCHIVO\tGenera un archivo grafico (png,svg,etc.) del flujo") << endl;
SafetYAWL::streamout << tr("\t\t\tde trabajo. La extension del archivo indica el formato en que se escribe.") << endl;
SafetYAWL::streamout << tr("\t\t\tSi no se especifica parametro se genera por defecto un archivo con ") << endl;
SafetYAWL::streamout << tr("\t\t\tel nombre 'output.png' en el directorio actual") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("-c --check\t\tChequea que el archivo de flujo de trabajo indicado") << endl;
SafetYAWL::streamout << tr("\t\t\ten la opcion -f o --file cumpla todas las reglas (Sintaxis,enlace con") << endl;
SafetYAWL::streamout << tr("\t\t\trepositorio de datos, etc.) para ser procesado por un comando") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("-l --listtasks\t\tLista todas las tareas/actividades que contiene el archivo") << endl;
 SafetYAWL::streamout << tr("\t\t\tde flujo de trabajo indicado en la opcion -f o --file") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("-V --version\t\tMuestra la version actual de la aplicacion de linea ") << endl;
SafetYAWL::streamout << tr("\t\t\tde comandos 'safet'.") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("-h --help\t\tMuestra este texto de ayuda") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("-d --listdocuments\tMuestra los documentos presentes en la variable indicada") << endl;
SafetYAWL::streamout << tr(" \t\t\tcon el parametro -v o --variable") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("Safet es una herramienta para gestion de flujos de trabajos basados en Redes de ") << endl;
SafetYAWL::streamout << tr("Petri y patrones (AND,OR,XOR, etc), y que incorpora Firma ") << endl;
SafetYAWL::streamout << tr("Electronica bajo el modelo de Infraestructura de Clave Publica.") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("  Software utilizado principalmente en safet:") << endl;
SafetYAWL::streamout << endl;
SafetYAWL::streamout << tr("  Libreria Qt:  referirse a: http://www.trolltech.com") << endl;
SafetYAWL::streamout << tr("   LibDigidoc:  referirse a: http://www.openxades.org") << endl;
SafetYAWL::streamout << tr("     Graphviz:  referirse a: http://www.graphviz.org") << endl;
SafetYAWL::streamout << tr("        DbXml:  referirse a: http://www.sleepycat.com") << endl;
SafetYAWL::streamout << endl;

}


void SafetConsoleApp::calStatistics() {
	
	if ( commands['S'].compare("ndocs", Qt::CaseInsensitive ) == 0 ) {
		SafetYAWL::streamout << "Estadisticas para el archivo: <" << commands['f'] << ">:" << endl;
                foreach(QString name,configurator->getWorkflows().at(0)->variablesId()){
                        SafetVariable *v = configurator->getWorkflows().at(0)->searchVariable( name );
			Q_CHECK_PTR( v );
                        SafetYAWL::streamout << tr("Variable <") << name << tr(">  Numero de fichas: <") << configurator->getWorkflows().at(0)->numberOfTokens(*v) << ">" << endl;
		}
		SafetYAWL::streamout << endl;
	} else if ( commands['S'].compare("path", Qt::CaseInsensitive ) == 0 ) {
		//(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(commands['k'].length() > 0 , tr("No se especifico el valor de la clave (--keyvalue <valor>) para firmar (sign) el documento")) );
		SafetYAWL::evalAndCheckExit(commands['k'].length() > 0 , tr("No se especifico el valor de la clave (--keyvalue <valor>) para firmar (sign) el documento"));

		QString info = 	commands['k'];
		(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(commands['t'].length() > 0 , tr("No se especifico un nombre de tarea  (--task <nombre>) para calcular e imprimir los caminos")) );
                QString result = configurator->getWorkflows().at(0)->printPaths(commands['t'],info);
		SafetYAWL::streamout << tr("Camino(s) para : ") << info << endl;
		SafetYAWL::streamout << result << endl;
	} else {
		SafetYAWL::streamout << tr("Argumento del comando Estadisticas -S <arg> o -stat <arg> invalido. Opciones: ndocs, percent, path") << endl;
	}
}

void SafetConsoleApp::signDocument() {
	//(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(commands['k'].length() > 0 , tr("No se especifico el valor de la clave (--keyvalue <valor>) para firmar (sign) el documento")) );
	SafetYAWL::evalAndCheckExit(commands['k'].length() > 0 , tr("No se especifico el valor de la clave (--keyvalue <valor>) para firmar (sign) el documento"));

	//(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(commands['v'].length() > 0 , tr("No se especifico un nombre de variable  (--variable <nombre>) para firmar (sign) el documento")) );
	SafetYAWL::evalAndCheckExit(commands['v'].length() > 0 , tr("No se especifico un nombre de variable  (--variable <nombre>) para firmar (sign) el documento"));


        SafetVariable *v = configurator->getWorkflows().at(0)->searchVariable( commands['v'] );

	//(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(v != NULL , tr("No existe la variable : \"%1\" en el documento de flujo de trabajo").arg(commands['v'])) );
	SafetYAWL::evalAndCheckExit(v != NULL , tr("No existe la variable : \"%1\" en el documento de flujo de trabajo").arg(commands['v']));
	
        QString derror;
        QString documentid = configurator->getWorkflows().at(0)->signDocument(v, commands['k'],derror);
        if ( documentid.length() > 0 ) {
          SafetYAWL::streamout << tr("Documento con id:")  << documentid << " firmado correctamente OK!" << endl;
        } else {
          SafetYAWL::streamout << " NO se introdujo la CLAVE del documento a firmar. NO se realizo la FIRMA." << endl;
        }

}


void SafetConsoleApp::makeTemplate() {
        SafetYAWL::streamout << tr("Funcionalidad que debe ser reescrita");
//	QVariant var = SafetYAWL::getConfFile().getValue("Templates", "templates.workflow.new");
//	QString filename = var.toString();
//	Q_ASSERT( filename.length() > 0 );
//	QFile file(filename);
// 	bool open = file.open(QFile::WriteOnly | QFile::Truncate);
//	SafetYAWL::evalAndCheckExit(open,tr("No se pudo crear el archivo \"%1\" (plantilla de nuevo flujo de trabajo)").arg(filename));
//
//   	QTextStream out(&file);
//	QStringList params;
//	params.push_back(filename);
//     	SafetYAWL::getNewTemplate(out,params);
//	file.close();
//	SafetYAWL::streamout << tr("\"%1\" --> generado documento de flujo (xml) desde plantilla").arg(filename) << endl;

}


void SafetConsoleApp::genGraph() {
       char buffer[512];
	int len = commands['g'].length();
	Q_ASSERT_X(len < 512, qPrintable(tr("genGraph")), qPrintable(tr("La longitud de la ruta del archivo debe ser menor que 512")));

        QString info;
        if ( commands.contains('p') ) {
          configurator->loadPlugins(commands['p']);
        }

	if ( commands.contains('k') ) {		
                info = 	commands['k'];
                qDebug("...info: |%s|", qPrintable(info));
	}

//	qDebug("commands['g'].length() == 0:? %d",commands['g'].length() == 0);
	if ( commands['g'].length() == 0 ) {		
		strncpy(buffer, "png", 3);
                QString img = configurator->getWorkflows().at(0)->generateGraph(buffer, info);
		QFile::rename(img, tr("output.png"));
		return;
	}


       QString ext = commands['g'].section('.', -1);
       len = commands['g'].section('.', -1).length();
       strncpy(buffer, qPrintable(ext),len);
       buffer[len]=0;
       QString img = configurator->getWorkflows().at(0)->generateGraph(buffer, info);

       bool result = QFile::remove(commands['g']);
       result = QFile::rename(img, commands['g']);
	
	//(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(result, tr("No es posible escribir el archivo en la ubicacion: %1").arg(commands['g'])) );
        SafetYAWL::evalAndCheckExit(result, tr("No es posible escribir el archivo en la ubicacion: %1").arg(commands['g']));

       SafetYAWL::streamlog << tr("Se escribio el archivo de grafico de flujo de trabajo: %1").arg(commands['g']) << endl;
       SafetYAWL::streamout << tr("Nombre del archivo: %1").arg(commands['g']) << endl;	

}

/*!
    \fn SafetConsoleApp::listTasks()
 */
void SafetConsoleApp::listTasks()
{
        QString outtasks = configurator->getWorkflows().at(0)->listTasks( commands['C'].length() > 0 );
	SafetYAWL::streamout << "Lista de identificadores  de tareas/actividades:"  << endl;
                SafetYAWL::streamout << outtasks;
}


void SafetConsoleApp::manageData() {
//	SafetYAWL::streamout << "Data: <" << commands['D'] << ">" << endl;	
	SafetTextParser parser;
	parser.setStr( commands['D'] );
	QString str = "agregar,eliminar,actualizar,mostrar";
	QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
	parser.setCommands(str.split(","),commandstr.split(","));
	QString xml = parser.toXml();
	SafetYAWL::streamlog << SafetLog::Debug << tr("Cadena XML generada de la entrada: \n\"%1\"").arg(xml);
//	qDebug("\n\n....xml:\n%s", qPrintable(xml));
	parser.processInput( xml );
	parser.openXml();	
	parser.processXml();
}

void SafetConsoleApp::doShell() {
          //QTextStream stream(stdin);

     struct termios new_flags, old_flags;
        int i, fd;
        char c;

        if ( signal(SIGINT, sig_caught) == SIG_ERR ) {
                puts("Fallo SIGINT");
        }

        if ( signal(SIGQUIT, sig_caught) == SIG_ERR ) {
                puts("Fallo SIGQUIT");
        }

        if ( signal(SIGTERM, sig_caught) == SIG_ERR ) {
                puts("Fallo SIGTERM");
        }

     qDebug("Shell interactive...");
     fd = fileno(stdin);
tcgetattr(fd, &old_flags);
        new_flags = old_flags;
        new_flags.c_lflag &= ~( ICANON | ISIG );
        new_flags.c_iflag &= ~(BRKINT | ICRNL );
        new_flags.c_oflag &= ~OPOST;
        new_flags.c_cc[VTIME] = 0;
        new_flags.c_cc[VMIN] = 1;

        if ( tcsetattr(fd, TCSAFLUSH, &new_flags) < 0 ) {
                puts("Falla atributo");
        }

        puts("En modo RAW. Borrar para salir");
     while ((i=read(fd, &c, 1 )) == 1 ) {

                if ( c == 13 ) break;
                //if (c>= 'a' && c<='z') printf("%c", c);
                //else if (c>= 'A' && c<='Z') putc(c, stdout);
                fflush(stdout);
        }

     tcsetattr(fd, TCSANOW, &old_flags);

     /*  Loop until user presses 'q'  */

      //  while ( (ch = getch()) != 'q' );
    return;

}


void sig_caught(int signum) {
        printf("senal captada: %d\n", signum);
}

void SafetConsoleApp::version() {

//SafetYAWL::streamout << "Copyright (C) 2008 Victor Bravo (vbravo@cenditel.gob.ve)" << endl;
//SafetYAWL::streamout << "                   Antonio Araujo (aaraujo@cenditel.gob.ve)" << endl;
//SafetYAWL::streamout << "CENDITEL Fundacion Centro Nacional de Desarrollo e Investigacion" << endl;
//SafetYAWL::streamout << "         en Tecnologias Libres " << endl;
//SafetYAWL::streamout << "Este programa es software libre; Usted puede usarlo bajo" << endl;
//SafetYAWL::streamout << "los terminos de la licencia GPLv2" << endl;
//SafetYAWL::streamout << "software GPL version 2.0 de la Free Software Foundation." << endl;
//SafetYAWL::streamout << "Version 0.1.beta" << endl;
//SafetYAWL::streamout << endl;

     SafetYAWL::streamout << INFO_VERSION << endl;


}

void SafetConsoleApp::listDocuments()
{
        SafetVariable* var = configurator->getWorkflows().at(0)->searchVariable( commands['v'] );
	if (var == NULL ) {
		SafetYAWL::streamout << tr("La variable \"%1\" NO existe en la especificacion de flujo de trabajo. Revise el nombre en el documento de flujo de trabajo").arg(commands['v']) << endl;
		return;
	}

//qDebug("----- antes de configurator->getWorkflows().at(0)->getDocuments");
        if ( commands.contains('p') ) {
          configurator->loadPlugins(commands['p']);
        }
        QList<QSqlField> fields;
        QString documents = configurator->getWorkflows().at(0)->getDocuments(commands['v'],
        fields, SafetWorkflow::JSON, "");

//qDebug("----- despues de configurator->getWorkflows().at(0)->getDocuments");
	SafetYAWL::streamout << endl << tr("Documentos para la variable \"%1\":").arg(commands['v']) << endl;
	SafetYAWL::streamout << endl << tr("Documentos:\n%1").arg(documents) << endl;
	SafetYAWL::streamout << endl;

}

void SafetConsoleApp::evalEventOnExit(SafetLog::Level l) {
    	if ( l == SafetLog::ExitApp ) {
		SafetYAWL::streamout << tr("** Error: Ocurrio un error.No se pudo completar la accion.");
		SafetYAWL::streamout << tr(" * Importante: Revise el registro de eventos (log)") << endl;
		::exit(0);
	}
}



QString SafetConsoleApp::evalEventOnInput(SafetYAWL::TypeInput p, const QString& message, bool &ok) {
	QString str;
	QTextStream stream(stdin);	
	char *buffer;
        ok = true;
	switch ( p ) {
                case SafetYAWL::PIN: // Ver una implementacion mas segura
			str = QString(getpass(qPrintable(message)));		
                        if ( str.isEmpty() ) ok  = false;
		break;
		default:;
	}
	return str;
}


void SafetConsoleApp::debuglog() {
	qDebug("Off all Level: %d", SafetYAWL::streamlog.level() );
	SafetYAWL::streamlog.turnOnAll();
	qDebug("On all Level: %d", SafetYAWL::streamlog.level() );
	SafetYAWL::streamlog.turnOffAll();
	qDebug("Off all Level: %d", SafetYAWL::streamlog.level() );
// Prueba para Prendido de todo
	SafetYAWL::streamlog.turnOn(SafetLog::Debug);
	qDebug("On Debug Level: %d", SafetYAWL::streamlog.level() );
	SafetYAWL::streamlog.turnOn(SafetLog::Action);
	qDebug("On Action Level: %d", SafetYAWL::streamlog.level() );
	SafetYAWL::streamlog.turnOn(SafetLog::Action);
	qDebug("On Action Level(2): %d", SafetYAWL::streamlog.level() );

	SafetYAWL::streamlog.turnOn(SafetLog::Error);
	qDebug("On Error Level: %d", SafetYAWL::streamlog.level() );
	SafetYAWL::streamlog.turnOn(SafetLog::Warning);
	qDebug("On Warning Level: %d", (int)SafetYAWL::streamlog.level() );
	SafetYAWL::streamlog.turnOn(SafetLog::Warning);
	qDebug("On Warning Level(2): %d", (int)SafetYAWL::streamlog.level() );

// Prueba para Apagado de todo
	SafetYAWL::streamlog.turnOff(SafetLog::Warning);
	qDebug("Out Warning Level: %d", (int)SafetYAWL::streamlog.level() );
	SafetYAWL::streamlog.turnOff(SafetLog::Warning);
	qDebug("Out Warning Level(2): %d", (int)SafetYAWL::streamlog.level() );
	SafetYAWL::streamlog.turnOff(SafetLog::Error);
	qDebug("Out Error Level: %d", SafetYAWL::streamlog.level() );
	SafetYAWL::streamlog.turnOff(SafetLog::Action);
	qDebug("Out Action Level: %d", SafetYAWL::streamlog.level() );
	SafetYAWL::streamlog.turnOff(SafetLog::Action);
	qDebug("Out Action Level(2): %d", SafetYAWL::streamlog.level() );
	SafetYAWL::streamlog.turnOff(SafetLog::Debug);
	qDebug("Out Debug Level: %d", SafetYAWL::streamlog.level() );

}

