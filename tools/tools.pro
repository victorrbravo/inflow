TARGET = safet
TEMPLATE = app
INCLUDEPATH += /usr/include \
#   /usr/include/digidocpp \
   /usr/include/libdigidoc \
   /usr/include/libxml2 \
   ../gsoap \
   /usr/include/libxslt \
   ../src
DEFINES += QT_DEBUG \
        SAFET_NO_DBXML
QT += core \
    sql \
    network \
    xml
#QT -= gui
HEADERS += SafetConsoleApp.h
SOURCES += SafetConsoleApp.cpp \
	main.cpp
LIBS += -L/usr/lib \
    -L../lib \
    -ldigidoc \
#    -ldigidocpp \
    -lsafet
CONFIG += qt \
 ordered \
 thread \
 warn_off \
 debug_and_release
target.path =/usr/bin
target.files = safet
INSTALLS += target
man.path = /usr/share/man/man1
man.files = ../debian/safet.1.gz
INSTALLS += man