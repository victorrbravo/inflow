TEMPLATE = subdirs
SUBDIRS += src \    
    lib \
    plugins/plugviz \
    inflow 
# DEFINES = QT_DEBUG
QT += core \
    sql \
    xml
QT -= gui
CONFIG += qt \
 ordered \
 thread \
 warn_off \
 debug_and_release \

# Mensaje
message( "Configuración y compilación de SAFET" )

!exists( /usr/include/libxml2/libxml/globals.h ) {
       error(" ... Ocurrió un error en la búsqueda de la librería Libxml2. Por favor instale la librería Libxml")
}
message( "... Verificación de librería Libxml2: OK" )

# Libreria Libgraphviz
!exists( /usr/include/graphviz/gvc.h ) {
       error(" ... Ocurrió un error en la búsqueda de encabezados de Libgraphviz. Por favor instale la librería Libgraphviz")
}
message( "... Verificación de encabezados de Libgraphviz: OK" )

!exists( /usr/lib/libgvc.so ) {
       error(" ... Ocurrió un error en la búsqueda de la librería Libgraphviz. Por favor instale la librería Libgraphviz")
}
message( "... Verificación de librería Libgraphviz: OK" )

# Libreria libtar-dev
!exists( /usr/include/libtar.h ) {
       error(" ... Ocurrió un error en la búsqueda de encabezados de libtar-dev. Por favor instale la librería libtar-dev")
}
!exists( /usr/lib/libtar.a ) {
       error(" ... Ocurrió un error en la búsqueda de encabezados de libtar-dev. Por favor instale la librería libtar-dev")
}
message( "... Verificación de encabezados de libtar-dev: OK" )

# Compilador g++
!exists( /usr/bin/g++ ) {
       error(" ... Ocurrió un error en la búsqueda del compilador g++. Por favor instale el compilador g++")
}
message( "... Verificación de compilador g++ : OK" )

OTHER_FILES += \
    bar-descriptor.xml



