
create table CA (
NOMBRE_COMUN_CA      VARCHAR(100)         not null,
ESTADO               VARCHAR(20)          null,
LOCALIDAD            VARCHAR(100)         null,
DIRECCION            VARCHAR(100)         null,
CORREO_E_CA          VARCHAR(50)          null,
ORGANIZACION         VARCHAR(100)         null,
UNIDAD_ORGANIZACION  VARCHAR(100)         null,
URL_CA     VARCHAR(255)         null,
URL_RA     VARCHAR(255)         null,
URL_PUB   VARCHAR(255)         null,
FECHA_SOLICITUD      TIMESTAMP          null DEFAULT current_timestamp,
FECHA_CREACION       TIMESTAMP          null DEFAULT current_timestamp,
NIVEL_CA             VARCHAR(100)           null,
CODIGO_PSC           VARCHAR(15)           null,
NUMERO_ID_PSC        VARCHAR(25)           null,
constraint PK_CA primary key (NOMBRE_COMUN_CA)
);

comment on table CA is
'tabla que almacena informacion sobre una autoridad de certificacion';


create table ORGANIZACION  (
NOMBRE_ORGARNIZACION      VARCHAR(100)         not null,
PAIS                 VARCHAR(5)           null DEFAULT 'VE',
ESTADO               VARCHAR(20)          null,
LOCALIDAD            VARCHAR(100)         null,
SECTOR               VARCHAR(100)         null,
DIRECCION  VARCHAR(100)         null,
TELEFONO_1 VARCHAR(12)          null,
TELEFONO_2  VARCHAR(12)          null,
DIRECCION_POSTAL VARCHAR(100)         null,
CORREO_E   VARCHAR(50)          null,
constraint PK_ORGANIZACION  primary key (NOMBRE_ORGARNIZACION)
);

comment on table ORGANIZACION  is
'tabla que almacena informacion sobre organizaciones que poseen la CA';


create table CERTIFICADO (
ID_CERTIFICADO        VARCHAR(15)              not null,
SERIAL               NUMERIC            not null, 
DESCRIPCION        VARCHAR(50)          not null,
FECHA_EMISION        TIMESTAMP          null DEFAULT current_timestamp,
FECHA_EXPIRACION     TIMESTAMP          null DEFAULT current_timestamp,
ARCHIVOCER           BYTEA              null,
RUTACER              VARCHAR(255)       null,
USE_HSM_KEY          BOOLEAN            null DEFAULT FALSE,
HARDWARE_KEY_ID      VARCHAR(25)        null,
AUTOFIRMADO          BOOLEAN            null DEFAULT FALSE,
constraint PK_CERTIFICADO primary key (ID_CERTIFICADO)
);

comment on table CERTIFICADO is
'tabla que almacena informacion sobre los certificados';


create table CERTIFICADOCA (
constraint PK_CERTIFICADOCA primary key (ID_CERTIFICADO)
) INHERITS (CERTIFICADO);

comment on table CERTIFICADOCA is
'tabla que almacena informacion sobre los certificados de las CA (Autoridades)';

create table CERTIFICADOEF  (
constraint PK_CERTIFICADOEF primary key (ID_CERTIFICADO)
) INHERITS (CERTIFICADO);

comment on table CERTIFICADOEF is
'tabla que almacena informacion sobre los certificados de las Entidades Finales';

create table CRL (
ID_CRL               VARCHAR(15)        not null,
ID_CERTIFICADO       VARCHAR(15)        not null,
DESCRIPCION          VARCHAR(50)        not null,
FECHA_EMISION        TIMESTAMP          null DEFAULT current_timestamp,
FECHA_EXPIRACION     TIMESTAMP          null DEFAULT current_timestamp,
ARCHIVOCRL           BYTEA              null,
constraint PK_CRL primary key (DESCRIPCION)
);

comment on table CRL is
'tabla que almacena informacion sobre las listas de revocacion de certificados';


create table LOG (
ID_LOG               NUMERIC              not null,
CEDULA               VARCHAR(10)          null,
DIRECCION_IP         VARCHAR(15)          null,
NUMERO_SESION        NUMERIC                 null,
FECHA_SESION         TIMESTAMP          null DEFAULT current_timestamp,
constraint PK_LOG primary key (ID_LOG)
);

comment on table LOG is
'tabla que almacena informacion de registro de las sesiones de usuario
dentro del sistema';



create table PERSONA (
NACIONALIDAD	     VARCHAR(1)		  not null DEFAULT 'V',
CEDULA               VARCHAR(10)           not null,
NOMBRE_COMUN         VARCHAR(100)         null,
PAIS                 VARCHAR(5)           null DEFAULT 'VE',
ESTADO               VARCHAR(20)          null,
LOCALIDAD            VARCHAR(100)         null,
ORGANIZACION         VARCHAR(100)         null,
UNIDAD_ORGANIZACION  VARCHAR(100)         null,
CARGO                VARCHAR(100)         null,
DIRECCION_HABITACION VARCHAR(100)         null,
CORREO_E             VARCHAR(50)          null,
TELEFONO_1_PERSONA   VARCHAR(12)          null,
TELEFONO_2_PERSONA   VARCHAR(12)          null,
FECHA_CREACION       TIMESTAMP          null DEFAULT current_timestamp,
ESTATUS              VARCHAR(20)          null,
IMAGEN               BYTEA                     null DEFAULT NULL,
CLAVEPUBLICA         BYTEA                null DEFAULT NULL,
SERIAL_TARJETA       VARCHAR(10)          null DEFAULT '000000',
FECHA_EMISION_TARJETA VARCHAR(20)         null DEFAULT 'N/A',
PIN_INICIAL          BOOLEAN              null DEFAULT TRUE,
constraint PK_PERSONA  primary key (CEDULA)
);


comment on table PERSONA is 'tabla base para los tipos usuarios y cliente';


CREATE TABLE USUARIO  (
LOGIN                VARCHAR(15) constraint MAYOR_QUE_DOS_EL_LOGIN  check (length(LOGIN)>2) not null,
NIVEL_ACCESO         TEXT                       null,
AUTENTICACION        VARCHAR(20) constraint ESCOGER_UNA_OPCION_AUTENTICACION check (length(AUTENTICACION) > 0 )         not null,
CONTRASENA           VARCHAR(100)          null,
UNIQUE (LOGIN)
) INHERITS (PERSONA);


CREATE TABLE CLIENTE (
constraint PK_CLIENTE  primary key (CEDULA)
)  INHERITS (PERSONA);

comment on table CLIENTE  is
'tabla que almacena informacion sobre entidades finales, representantes de CA';

create table SERVIDOR (
NOMBRE_COMUN_SERVIDOR VARCHAR(100)         not null,
URL_SERVIDOR         VARCHAR(50)          null,
ESTADO               VARCHAR(20)          null,
LOCALIDAD            VARCHAR(100)         null,
ORGANIZACION         VARCHAR(100)         null,
CEDULA_PERSONA_CONTACTO VARCHAR(10)           null,
UNIDAD_ORGANIZACION_PERSONA VARCHAR(100)         null,
constraint PK_SERVIDOR primary key (NOMBRE_COMUN_SERVIDOR)
);



comment on table SERVIDOR is
'tabla que almacena informacion sobre un servidor seguro';

create table SOLICITUD (
ID_SOLICITUD        VARCHAR(15)              not null,
FECHA_SOLICITUD      TIMESTAMP          null DEFAULT current_timestamp,
ESTATUS              VARCHAR(20)          null,
DESCRIPCIONCSR              VARCHAR(50)         null,
RUTACSR              VARCHAR(255)         null,
ARCHIVOCSR           BYTEA                null,
USE_HSM_KEY          BOOLEAN              null DEFAULT FALSE,
HARDWARE_KEY_ID      VARCHAR(25)          null,
constraint PK_SOLICITUD primary key (ID_SOLICITUD)
);


create table SOLICITUDCA (
NOMBRE_COMUN_CA      VARCHAR(100)         null,
NOMBRE_COMUN_SERVIDOR VARCHAR(100)         null,
EMITIDA_POR          VARCHAR(15)          null,
constraint PK_SOLICITUDCA primary key (ID_SOLICITUD)
) INHERITS (SOLICITUD);


create table SOLICITUDEF (
CEDULA               VARCHAR(10)           null,
constraint PK_SOLICITUDEF primary key (ID_SOLICITUD)
) INHERITS (SOLICITUD);



CREATE TABLE FIRMA (
    NOMBRE character varying(150) NOT NULL,
    FIRMA bytea   NULL,
    longitud integer NULL DEFAULT 0,
    usuario character varying(150) NULL,
    acceso timestamp   NULL,
    CONSTRAINT debe_tener_un_nombre_de_tabla  PRIMARY KEY (nombre)
);


CREATE TABLE TIPOCA (
    NOMBRE_TIPOCA  character varying(100) NOT NULL,
        NIVEL integer NULL DEFAULT 0,
        COMENTARIO character varying(255) NULL,
    CONSTRAINT debe_tener_nombre_tipoca  PRIMARY KEY (NOMBRE_TIPOCA)
);

insert INTO TIPOCA (NOMBRE_TIPOCA,NIVEL,COMENTARIO) VALUES ('ROOTCA',0,'Tipo para la CA Raiz'); 
INSERT INTO TIPOCA (NOMBRE_TIPOCA,NIVEL,COMENTARIO) VALUES ('Sub-CA',1,'Tipo para la CA de 2do Nivel');
INSERT INTO TIPOCA (NOMBRE_TIPOCA,NIVEL,COMENTARIO) VALUES ('Proveedor-CA',2,'Proveedor CA');

CREATE TABLE NULA ();


COMMENT ON TABLE TIPOCA IS
'tabla que almacena los tipos de Autoridades de Certificacion';


COMMENT ON TABLE FIRMA IS
'tabla que almacena las firmas de todas las tablas';



COMMENT ON COLUMN firma.nombre IS 'almacena el nombre de la tabla firmada';



COMMENT ON COLUMN firma.firma IS 'almacena la firma digital de la tabla';


COMMENT ON COLUMN firma.longitud IS 'almacena la longitud de la firma';



COMMENT ON COLUMN firma.usuario IS 'almacena el nombre del usuario que realiza la firma';


COMMENT ON COLUMN firma.acceso IS 'almacena informacion de fecha y hora en que se realiza la firma';



comment on table SOLICITUD is
'tabla que almacena informacion sobre las solicitudes de certificados';

comment on table SOLICITUDCA is
'tabla que almacena informacion sobre las solicitudes de certificados para CA (Autoridades)';

comment on table SOLICITUDEF is
'tabla que almacena informacion sobre las solicitudes de certificados para Entidades Finales';

alter table CERTIFICADOCA
   add constraint FK_CERT_GEN_SOLCA  foreign key
(ID_CERTIFICADO)
      references SOLICITUDCA (ID_SOLICITUD)
      on delete cascade on update restrict;

alter table CERTIFICADOEF
   add constraint FK_CERT_GEN_SOLEF  foreign key
(ID_CERTIFICADO)
      references SOLICITUDEF (ID_SOLICITUD)
      on delete cascade on update restrict;

alter table CRL
   add constraint FK_GEN_CERTCA foreign key (ID_CERTIFICADO)
      references CERTIFICADOCA (ID_CERTIFICADO)
      on delete cascade on update restrict;

      
alter table LOG
   add constraint FK_LOG_MANTIENE_PERSONA foreign key (CEDULA)
      references PERSONA (CEDULA)
      on delete cascade on update restrict;

alter table SOLICITUDEF
   add constraint CEDULA_DEBE_CORRESPONDER_CON_UN_CLIENTE  foreign key (CEDULA)
      references CLIENTE (CEDULA);

alter table SOLICITUDCA
   add constraint NOMBRECA_DEBE_CORRESPONDER_CON_UNA_CA  foreign key (NOMBRE_COMUN_CA)
      references CA (NOMBRE_COMUN_CA);

alter table CA
   add constraint FK_CA_EMITE_ORGANIZACION  foreign key (ORGANIZACION)
      references ORGANIZACION (NOMBRE_ORGARNIZACION)
      on delete cascade on update restrict;

alter table CA add constraint FK_CA_TIPOCA  foreign key (NIVEL_CA) references TIPOCA (NOMBRE_TIPOCA);


alter table SERVIDOR
   add constraint FK_SERVIDOR_EMITE_ORGANIZACION  foreign key (ORGANIZACION)
      references ORGANIZACION (NOMBRE_ORGARNIZACION)
      on delete cascade on update restrict;

create sequence seqsolicitud;

create sequence seqcrl;


INSERT INTO usuario   (NACIONALIDAD, CEDULA, NOMBRE_COMUN, PAIS, ESTADO, LOCALIDAD, ORGANIZACION, UNIDAD_ORGANIZACION,
CARGO, DIRECCION_HABITACION, CORREO_E, TELEFONO_1_PERSONA, TELEFONO_2_PERSONA, FECHA_CREACION,
ESTATUS, IMAGEN, LOGIN, CONTRASENA, NIVEL_ACCESO,CLAVEPUBLICA, AUTENTICACION, SERIAL_TARJETA, FECHA_EMISION_TARJETA, PIN_INICIAL) VALUES ('V', 100, 'Usuario raiz', '', 'Distrito Capital', '', '', '', '', '', '', '', '', date_trunc('second', current_timestamp), ' ', null, 'root',  '63:A9:F0:EA:7B:B9:80:50:79:6B:64:9E:85:48:18:45', '<Objects><object name="users"><permise on="true">view</permise><permise on="true">insert</permise><permise on="true">update</permise><permise on="true">delete</permise><permise on="true">change image</permise><permise on="true">change public key</permise><permise on="true">set/unset password</permise><permise on="true">set/unset smartcard</permise><permise on="true">configure access module</permise></object><object name="Settings" ><permise on="true" >view</permise><permise on="true" >modify</permise></object></Objects>','\\323\\207n\\353\\314\\034\\237\\372\\342\\231\\271\\317\\372\\244[\\035\\220\\354\\013#,\\256\\225\\232\\273q\\226\\031r;@\\353\\213\\012uG\\250\\273\\012L\\330\\015X\\261\\177\\374\\242m]\\013Vf\\\\\\316\\216S\\212\\014\\305\\000\\024\\227\\217\\030\\263\\266\\027\\264\\235`\\0120)M\\315\\242\\333\\262x\\034\\373\\224[\\253\\216#\\241lp\\312=\\012\\206Y\\321\\275\\320\\300\\343\\334;\\205\\205\\335\\323\\003l\\305\\317e\\376\\212z\\213Y\\243{\\003\\356\\023L\\021\\2520\\326S2\\277\\325\\033\\263.\\322\\352\\001\\2305E\\2766\\202\\215\\333\\017\\005\\223w\\212\\012\\364\\266\\302wE\\035\\315\\263\\256\\000{6\\360\\252\\\\\\252\\354\\026\\342\\254\\177pN\\244\\244\\353\\355', 'Contrasena','000000', 'N/A', 'TRUE');

INSERT INTO FIRMA (NOMBRE) VALUES ('CA');
INSERT INTO FIRMA (NOMBRE) VALUES ('SERVIDOR');
INSERT INTO FIRMA (NOMBRE) VALUES ('PERSONA');
INSERT INTO FIRMA (NOMBRE) VALUES ('USUARIO');
INSERT INTO FIRMA (NOMBRE) VALUES ('CLIENTE');
INSERT INTO FIRMA (NOMBRE) VALUES ('SOLICITUD');
INSERT INTO FIRMA (NOMBRE) VALUES ('SOLICITUDCA');
INSERT INTO FIRMA (NOMBRE) VALUES ('SOLICITUDEF');
INSERT INTO FIRMA (NOMBRE) VALUES ('CERTIFICADO');
INSERT INTO FIRMA (NOMBRE) VALUES ('CERTIFICADOCA');
INSERT INTO FIRMA (NOMBRE) VALUES ('CERTIFICADOEF');
INSERT INTO FIRMA (NOMBRE) VALUES ('LOG');
INSERT INTO FIRMA (NOMBRE) VALUES ('ORGANIZACION');
INSERT INTO FIRMA (NOMBRE) VALUES ('TIPOCA');
INSERT INTO FIRMA (NOMBRE) VALUES ('NULA');
INSERT INTO FIRMA (NOMBRE) VALUES ('CRL');
