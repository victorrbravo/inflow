/*
* SAFET Sistema Automatizado para la Firma Electrónica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigación en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los términos de la licencia de
* software GPL versión 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea útil, pero SI NINGUNA GARANTÍA;
* tampoco las implícitas garantías de MERCANTILIDAD o ADECUACIÓN A UN PROPÓSITO PARTICULAR.
* Consulte la licencia GPL para más detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5º Piso, Boston, MA 02110-1301, USA.
*
*/
#include <QtCore>
#include <QCoreApplication>
#include "../SafetYAWL.h"
#include "../SafetToken.h"
#include "../SafetYAWL.h"
#include "../SafetConfFile.h"

int main( int argc, char *argv[] )
{




/* Configurator */

	QCoreApplication myapp(argc,argv);
	


/* ******************************************* */
/* Para probar la generación de documentos XML */

	

	qDebug("Aplicacion corriendo...");
 	SafetYAWL configurator;
//    	QSet<QString> keys;
//	QSet<SafetDocument> documents;
	configurator.openXML("model/safetexample6.xml");
	configurator.convertXMLtoObjects();

	if ( myapp.arguments().at(1).compare("sign", Qt::CaseInsensitive) == 0 ) {

		if ( myapp.argc() < 3 ) {
			qDebug("Sintáxis: libsafet sign <opcion>");
			return 0;
		}	
	
		SafetVariable *v = configurator.getWorkflows().at(0)->searchVariable( "Facturas" );
		Q_CHECK_PTR( v );
		configurator.getWorkflows().at(0)->signDocument(v,myapp.arguments().at(2), "Victor Bravo");
	}
	else if ( myapp.arguments().at(1).compare("verify", Qt::CaseInsensitive) == 0 ) {
		int ntokenspago = configurator.getWorkflows().at(0)->numberOfTokens("Gerencia");
		qDebug("            Número de fichas gerencia: %d", ntokenspago);
		return 0;
	}
	else if ( myapp.arguments().at(1).compare("documents", Qt::CaseInsensitive) == 0 ) {
		if ( myapp.argc() < 3 ) {
			qDebug("Sintáxis: libsafet documents <tarea>");
			return 0;
		}	
		QString documents = configurator.getWorkflows().at(0)->getDocuments(myapp.arguments().at(2), 
		SafetWorkflow::JSON, "");
		qDebug("JSON:\n%s", qPrintable(documents));
		return 0;
	}
	else {
		qDebug("NO se realizó ninguna operación");
	}
	return 0;

//     QVariant myvar = SafetYAWL::getConfFile().getValue("WebInterface", "activecolor");
 //    qDebug("Color activo : %s", qPrintable(myvar.toString()));

//** Agregar una firma
//	QString sql = "SELECT apellidos,proveedor_cedula,factura_numero,factura_monto from factura JOIN proveedor ON factura_cedulaproveedor = proveedor_cedula";
 //	QSqlQuery query(sql);
 //	query.next();

/* Configurator */

//     qDebug("QuerySign 0: %s", 	qPrintable(configurator.getWorkflows().at(0)->getTasks().at(0)->getPorts().at(0)->querysign()));

//     qDebug("QuerySign 1: %s", 	qPrintable(configurator.getWorkflows().at(0)->getTasks().at(1)->getPorts().at(0)->querysign()));

//     int ntokens = configurator.getWorkflows().at(0)->numberOfTokens("Corriente");
	int ntokenspago = configurator.getWorkflows().at(0)->numberOfTokens("Gerencia");


//    int ntokenspre = configurator.getWorkflows().at(0)->numberOfTokens("Presupuesto");
/*     keys = configurator.getWorkflows().at(0)->getKeys("Pago");
	qDebug("Numero de claves: %d", keys.count());
     for (QSet<QString>::const_iterator i = keys.begin(); i != keys.end(); ++i)
     			qDebug("Clave : %s", qPrintable(*i) );*/
//  documents = configurator.getWorkflows().at(0)->getDocuments("Pago");
// QString img = configurator.getWorkflows().at(0)->generateGraph("json", "stats:coloured");
//    QString img = configurator.getWorkflows().at(0)->generateGraphKey("V12643114","svg");
//    QString str =  configurator.getWorkflows().at(0)->getDocuments("Factura", SafetWorkflow::JSON);
//    QList<QVariant::Type> list =  configurator.getWorkflows().at(0)->getInfosOfDocument("Factura");
//	for (int i = 0; i < list.size(); ++i) {
//		qDebug("Campo %i: %d", i, list.at(i));
//	}
//     qDebug("\n%s", qPrintable(img));
//    qDebug("      Número de fichas corriente: %d", ntokens);

//    qDebug("Número de fichas Presupuesto: %d", ntokenspre);
//	qDebug("Cadena XML:\n%s", qPrintable(str) );
//	qDebug("Cadena JSON:\n%s", qPrintable(str) );
//    return 0;
}
