/*
* SAFET Sistema Automatizado para la Firma Electrónica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*     
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigación en Tecnologías Libres 
*  
* Este programa es software libre; Usted puede usarlo bajo los términos de la licencia de 
* software GPL versión 2.0 de la Free Software Foundation. 
* 
* Este programa se distribuye con la esperanza de que sea útil, pero SI NINGUNA GARANTÍA;
* tampoco las implícitas garantías de MERCANTILIDAD o ADECUACIÓN A UN PROPÓSITO PARTICULAR.
* Consulte la licencia GPL para más detalles. Usted debe recibir una copia de la GPL junto 
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5º Piso, Boston, MA 02110-1301, USA.
*
*/
#include <QtCore>
#include <QCoreApplication>
#include "SafetYAWL.h"
#include "SafetToken.h"


int main( /*int argc, char *argv[] */)
{

	qDebug("Aplicacion corriendo...");
	SafetYAWL configurator;
	configurator.openXML("model/said.xml");
    configurator.convertXMLtoObjects();
    QSet<QString> keys;
    QSet<SafetDocument> documents;
     configurator.getConfigurations().at(0)->open();
     int ntokens = configurator.getWorkflows().at(0)->numberOfTokens("Directo");
     //int ntokenspago = configurator.getWorkflows().at(0)->numberOfTokens("Pago");
//    int ntokenspre = configurator.getWorkflows().at(0)->numberOfTokens("Presupuesto");
/*     keys = configurator.getWorkflows().at(0)->getKeys("Pago");
	qDebug("Numero de claves: %d", keys.count());
     for (QSet<QString>::const_iterator i = keys.begin(); i != keys.end(); ++i)
     			qDebug("Clave : %s", qPrintable(*i) );*/
//  documents = configurator.getWorkflows().at(0)->getDocuments("Pago");
//    QString img = configurator.getWorkflows().at(0)->generateGraph("json");    
//    qDebug("\n%s", qPrintable(img));
    qDebug("      Número de fichas directo: %d", ntokens);
//    qDebug("            Número de fichas pago: %d", ntokenspago);
//    qDebug("Número de fichas Presupuesto: %d", ntokenspre);
    return 0;
}
