--
-- PostgreSQL database dump
--

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'Standard public schema';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: lista; Type: TABLE; Schema: public; Owner: vbravo; Tablespace: 
--

CREATE TABLE lista (
    id integer NOT NULL,
    titulo character(50),
    descripcion character(255),
    estado character(15) DEFAULT 'nuevo'::bpchar NOT NULL,
    asignado character(12),
    propietario character(12)
);


ALTER TABLE public.lista OWNER TO vbravo;

--
-- Name: listaid; Type: SEQUENCE; Schema: public; Owner: vbravo
--

CREATE SEQUENCE listaid
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.listaid OWNER TO vbravo;

--
-- Name: listaid; Type: SEQUENCE SET; Schema: public; Owner: vbravo
--

SELECT pg_catalog.setval('listaid', 9, true);


--
-- Data for Name: lista; Type: TABLE DATA; Schema: public; Owner: vbravo
--

COPY lista (id, titulo, descripcion, estado, asignado, propietario) FROM stdin;
1	ContrasenaAMD5                                    	\N	nuevo          	vbravo      	vbravo      
3	Pruebas SOAP                                      	\N	resuelto       	vbravo      	aaraujo     
2	Cambio Acceso BD                                  	\N	pospuesto      	vbravo      	aaraujo     
4	Video1                                            	\N	pospuesto      	vbravo      	aaraujo     
5	Mensajes de log para conexion a Base de datos     	\N	asignado       	vbravo      	vbravo      
7	Edicion de Video                                  	\N	asignado       	vbravo      	vbravo      
6	Validacion con lista en manageData                	\N	resuelto       	aaraujo     	vbravo      
8	Lista Drop                                        	\N	pospuesto      	aaraujo     	vbravo      
9	hacer video 2                                     	\N	resuelto       	vbravo      	aaraujo     
\.


--
-- Name: claveid; Type: CONSTRAINT; Schema: public; Owner: vbravo; Tablespace: 
--

ALTER TABLE ONLY lista
    ADD CONSTRAINT claveid PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

