/*
* SAFET Sistema Automatizado para la Firma Electrónica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*     
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigación en Tecnologías Libres 
*  
* Este programa es software libre; Usted puede usarlo bajo los términos de la licencia de 
* software GPL versión 2.0 de la Free Software Foundation. 
* 
* Este programa se distribuye con la esperanza de que sea útil, pero SI NINGUNA GARANTÍA;
* tampoco las implícitas garantías de MERCANTILIDAD o ADECUACIÓN A UN PROPÓSITO PARTICULAR.
* Consulte la licencia GPL para más detalles. Usted debe recibir una copia de la GPL junto 
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5º Piso, Boston, MA 02110-1301, USA.
*
*/
#include <QtCore>
#include <QCoreApplication>
#include <QtGlobal>
#include <QSqlDatabase>

#include "SafetYAWL.h"
#include "SafetToken.h"
#include "SafetDocument.h"
#include "SafetXmlRepository.h"


int main( /*int argc, char *argv[] */)
{

    qDebug("Aplicacion corriendo...");

    qDebug("Creacion de objeto SafetDocument ...");
    SafetDocument doc;

    doc.initializeLibdigidoc();
    qDebug("Inicializada de libreria ...");


    doc.createOpenXAdESContainer();
    qDebug("Creado contenendor OpenXAdES");

    // ************* prueba para abrir el archivo de configuracion de digidoc ******
    doc.initDigidocConfigStore("/usr/local/libdigidocModificada/etc/digidoc.conf");


    // prueba de dbxml
    //doc.testdbxml();
    DbEnv *myEnv = new DbEnv(0);
    XmlManager *myManager = NULL;
    QString home("/tmp/envHome");

    SafetXmlRepository * xmlRepository = new SafetXmlRepository(myEnv, myManager, home);
    qDebug("Paso el constructor");

    // crear de un contenedor
    //XmlContainer container = xmlRepository->createContainer("/tmp/envHome/container1.dbxml", "XmlContainer::WholedocContainer");

    // abrir un contenedor existente
    XmlContainer container = xmlRepository->openContainer("/tmp/envHome/container1.dbxml");

    DbXml::setLogLevel(DbXml::LEVEL_ALL, true);
    DbXml::setLogCategory(DbXml::CATEGORY_ALL, true);

    // creacion de un alias del contenedor para que no ocurran problemas al momento de construir
    // un contexto de consulta
    container.addAlias("mycontainer");

    // crear un contexto 
    XmlUpdateContext context = xmlRepository->createContextUpdate();

    // agregar un documento xml
    //xmlRepository->addXmlDocumentToContainer("/home/antonio/desarrollo/safet/salida/dosfirmas.ddoc", "dosfirmas", container, context);
    //xmlRepository->addXmlDocumentToContainer("/home/antonio/desarrollo/safet/salida/libsafet-salida.ddoc", "libsafet-salida", container, context);
    //xmlRepository->addXmlDocumentToContainer("/home/antonio/desarrollo/safet/salida/pruebafirmada.ddoc", "pruebafirmada", container, context);



    // fin de ejemplo de creacion de contenedor con 2 archivos agregados.
    //xmlRepository->~SafetXmlRepository();
    //qDebug("Paso el desconstructor");
    //return 0;





    //crea un contexto de consulta
    XmlQueryContext contextQ = xmlRepository->getXmlManager()->createQueryContext();

    // crear un espacio de nombres
    contextQ.setNamespace("", "http://www.sk.ee/DigiDoc/v1.3.0#");
    //contextQ.setNamespace("1", "http://www.w3.org/2000/09/xmldsig#");


    qDebug("antes de la consulta");

    //consulta: se debe especificar la ruta desde donde comenzar la busqueda!!!!
    QString query = "collection('mycontainer')/SignedDoc/DataFile/@Filename";

    qDebug("antes de preparar la consulta");
    //preparar la consulta
    XmlQueryExpression qe = xmlRepository->getXmlManager()->prepare(qPrintable(query), contextQ);
    //XmlQueryExpression qe = myManager->prepare(qPrintable(query), contextQ);

    qDebug("antes de la ejecutar la consulta");
    // resultado de la consulta
    XmlResults results = qe.execute(contextQ, 0);

    qDebug("se encontraron: %d documentos para la consulta", results.size());
    if(results.size()>0)
    {
	qDebug("results.hasNext() !!!");
    }
    else
    {
	qDebug("NO results.hasNext()");
    }

    QString nombreArchivo("");

    // Display the result set
    XmlValue value;
    while (results.next(value)) {
    	XmlDocument theDoc = value.asDocument();
	
	// para convertir una cadena std::string a const char * utilizar la funcion c_str()
	QString cadena(theDoc.getName().c_str());
	qDebug("Nombre del documento en el contenedor: %s",qPrintable(cadena));

	//ubicar el nombre del archivo xml que corresponde a este documento
	
	// obtener el contenido del documento y convertirlo en un archivo xml que pueda ser 
	// usado en libdigidoc
	XmlData data = theDoc.getContent();
	nombreArchivo = xmlRepository->writeXmlFileFromDb(cadena, data);
	qDebug("Archivo xml escrito: %s", qPrintable(nombreArchivo));

    } // fin del while





    //xmlRepository->~SafetXmlRepository();
    //qDebug("Paso el destructor");
    //return 0;




    //SignedDoc * pSD = doc.signedDoc();
    //Q_CHECK_PTR(pSD);
    //SignatureInfo* pSinfo = doc.SigInfo();
/*

    // *************************** XML *************************************************    
    qDebug("PRUEBA DE GENERACION DE ARCHIVO XML A PARTIR DE SENTENCIA SQL:");

    // creacion de conexion con la base de datos rootve de postgres
    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName("localhost");
    db.setDatabaseName("rootve");
    db.setUserName("antonio");
    db.setPassword("123456");
    bool ok = db.open();

    if (!ok)
    {
    	qDebug("FALLO LA CONEXION CON LA BASE DE DATOS rootve");
    	return 1;
    }
    qDebug("Creada conexion con la base de datos rootve.");


    doc.createXMLFileFromSQL("SELECT nombre_orgarnizacion,pais,estado,localidad,sector,direccion,telefono_1,correo_e FROM organizacion",
    		"/home/antonio/desarrollo/safet/salida/sql_smart_card.xml");
    qDebug("creado archivo xml desde sql en: /home/antonio/desarrollo/safet/salida/sql_smart_card.xml");
    // *************************** FIN XML *********************************************



    // ************* prueba para agregar un archivo a contenedor digidoc ******
    doc.addFileToOpenXAdESContainer(doc.signedDoc(), "/home/antonio/desarrollo/safet/salida/sql_smart_card.xml",
	SafetDocument::SAFET_CONTENT_EMBEDDED_BASE64,
	"application/xml");

    // ************* prueba para agregar atributos a un archivo existente en un contenedor digidoc ******
    doc.addAttributeToFile(doc.dataFile(), "/home/antonio/desarrollo/safet/salida/sql_smart_card.xml", "Fecha", "Martes 27/05/08");
    doc.addAttributeToFile(doc.dataFile(), "/home/antonio/desarrollo/safet/salida/sql_smart_card.xml", "Hora", "13:45pm");



    // ************* prueba para abrir el archivo de configuracion de digidoc ******
    doc.initDigidocConfigStore("/usr/local/libdigidocModificada/etc/digidoc.conf");


    // ************* prueba para cargar driver de la tarjeta inteligente ********
    //doc.testSmartCardDriver(0);

    // ************* prueba firma con tarjeta inteligente***********************
    doc.signWithSmartCard(doc.signedDoc(), "r96F29bE", "Desarrollador","Merida", "Merida", "5101", "Venezuela");

    // ************* prueba segunda firma con tarjeta inteligente***********************
    doc.signWithSmartCard(doc.signedDoc(), "r96F29bE", "Desarrollador2","Merida2", "Merida2", "51012", "Venezuela2");

    // ************* prueba para escribir contenedor en sistema de archivos ***************
    doc.writeOpenXAdESContainerToFile("/home/antonio/desarrollo/safet/salida/pruebafirmada.ddoc");
    qDebug("escrito contenedor digidoc en: /home/antonio/desarrollo/safet/salida/pruebafirmada.ddoc");



*/
//

    // ************* prueba para leer un contenedor desde un archivo en el sistema de archivos*************
    //doc.readOpenXAdESContainerFile("/home/antonio/desarrollo/safet/salida/pruebafirmada.ddoc");
    doc.readOpenXAdESContainerFile(nombreArchivo);
    qDebug(qPrintable(QString::number(doc.numberOfDataFileAttributes())));

    // ************* prueba para verificar firma con tarjeta inteligente***********************
    //int r = doc.verifySignMadeWithSmartCard("/home/antonio/desarrollo/safet/salida/pruebafirmada.ddoc", 1);
    int r = doc.verifySignMadeWithSmartCard(nombreArchivo, 0);
    if (r == ERR_OK)
    {
	qDebug("la verificacion fue correcta, no hubo errores");
	doc.closeLibdigidoc();
	return 0;
    }
    else
    {
	qDebug("la verificacion fue INCORRECTA, HUBO errores");
	doc.closeLibdigidoc();
	return 1;
    }






    
/*    
    int rc = ddocSaxReadSignedDocFromFile(&pSD, "/tmp/salida.ddoc", 0, 0);
    	Q_ASSERT_X(rc == ERR_OK, "SafetDocument::addSignatureToExistingDigidocFile()",qPrintable(tr("ddocSaxReadSignedDocFromFile != ERR_OK")));
    	qDebug("ddocSaxReadSignedDocFromFile(&pSigDoc, qPrintable(inFile), 0, 0)");
    	
    	// add new signature with default id
    	rc = SignatureInfo_new(&pSinfo, pSD, NULL);
    	Q_ASSERT_X(rc == ERR_OK, "SafetDocument::addSignatureToExistingDigidocFile()",qPrintable(tr("SignatureInfo_new != ERR_OK")));
    	qDebug("SignatureInfo_new(&pSigInfo, pSigDoc, NULL)");
    	
    	// automatically calculate doc-info elements for this signature
    	rc = addAllDocInfos(pSD, pSinfo);
    	Q_ASSERT_X(rc == ERR_OK, "SafetDocument::addSignatureToExistingDigidocFile()",qPrintable(tr("addAllDocInfos != ERR_OK")));
    	qDebug("addAllDocInfos(pSigDoc, pSigInfo)");
    	
    	// add signature production place
    	rc = setSignatureProductionPlace(pSinfo, "Merida", "Merida", "5101","Venezuela");
    	Q_ASSERT_X(rc == ERR_OK, "SafetDocument::addSignatureToExistingDigidocFile()",qPrintable(tr("setSignatureProductionPlace != ERR_OK")));
    	qDebug("setSignatureProductionPlace(pSigInfo, Merida, Merida, 5101,Venezuela)");
    	
    	// add user roles
    	rc = addSignerRole(pSinfo, 0, "VIP", -1, 0);
    	Q_ASSERT_X(rc == ERR_OK, "SafetDocument::addSignatureToExistingDigidocFile()",qPrintable(tr("addSignerRole != ERR_OK")));
    	qDebug("addSignerRole(pSigInfo, 0, VIP, -1, 0)");
    	
    	
    	qDebug("JUSTO ANTES DE calculateSigInfoSignature");
    	qDebug(getErrorString(rc)); 
    	//rc = calculateSigInfoSignature(pSigDoc,pSigInfo,int nSignType, const char* keyfile, const char* passwd, const char* certfile);
    	rc = calculateSigInfoSignature(pSD,pSinfo,SIGNATURE_RSA, 
    			"/home/antonio/desarrollo/safet/ejemplosOpenxades/ca/claveUlises.pem", 
    			"123456", 
    			"/home/antonio/desarrollo/safet/ejemplosOpenxades/ca/UlisesEstecche.crt");
    	
    	printf("\nError en calculateSigInfoSignature: %s\n", getErrorString(rc)) ;
    	qDebug(getErrorString(rc)); 
    	
    	Q_ASSERT_X(rc == ERR_OK, "SafetDocument::addSignatureToExistingDigidocFile()",qPrintable(tr("calculateSigInfoSignature != ERR_OK")));
    	qDebug("calculateSigInfoSignature(pSigDoc,pSigInfo,SIGNATURE_RSA, qPrintable(keyFile), qPrintable(passwd), qPrintable(certFile));");
    	
    	
    	
    	rc = createSignedDoc(pSD, "/tmp/salida.ddoc", "/tmp/signedFile.ddoc");
    	Q_ASSERT_X(rc == ERR_OK, "SafetDocument::addSignatureToExistingDigidocFile()",qPrintable(tr("createSignedDoc != ERR_OK")));
    	qDebug("createSignedDoc(pSigDoc, qPrintable(inFile), qPrintable(outFile));");
    
*/




    //addSignatureToExistingDigidocFile(QString keyFile, QString passwd, QString certFile, QString inFile, QString outFile);
    doc.addSignatureToExistingDigidocFile(
	"/home/antonio/desarrollo/safet/ejemplosOpenxades/ca/claveUlises.pem",
    	//"/home/antonio/descargas/digidockey.pem",
    	//"/tmp/usuario1KEY.pem",
	
	"123456", 

	"/home/antonio/desarrollo/safet/ejemplosOpenxades/ca/UlisesEstecche.crt",
    		//"/home/antonio/descargas/digidoccert.pem",
    		//"/tmp/usuario1.crt",	
    	"/home/antonio/desarrollo/safet/salida/libsafet-salida.ddoc", 
	"/home/antonio/desarrollo/safet/salida/nuevasalida.ddoc");

    qDebug("Aplicada firma en /home/antonio/desarrollo/safet/salida/nuevasalida.ddoc");



    // *************************** XML *************************************************    
/*    qDebug("PRUEBA DE GENERACION DE ARCHIVO XML A PARTIR DE SENTENCIA SQL:");

    // creacion de conexion con la base de datos rootve de postgres

    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName("localhost");
    db.setDatabaseName("rootve");
    db.setUserName("antonio");
    db.setPassword("123456");
    bool ok = db.open();

    if (!ok)
    {
	qDebug("FALLO LA CONEXION CON LA BASE DE DATOS rootve");
	return 1;
    }
    qDebug("Creada conexion con la base de datos rootve.");

    qDebug("");
    doc.createXMLFileFromSQL("SELECT nombre_orgarnizacion,pais,estado,localidad,sector,direccion,telefono_1,correo_e FROM organizacion",
    		"/home/antonio/desarrollo/safet/salida/sql.xml");
*/ 
    // *************************** FIN XML *********************************************

    // ----- funciones de prueba para pkcs#11
    //LIBHANDLE ptr = 0;
    //doc.initPKCS11Lib("/usr/lib/libopensc-ceres.so");
    //doc.initPKCS11Lib("/usr/lib/opensc-pkcs11.so");
    //qDebug("inicializada PKCS#11 ");

    // ----------------------------------
    //return 0;
}
