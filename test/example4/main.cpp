/*
* SAFET Sistema Automatizado para la Firma Electrónica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*     
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigación en Tecnologías Libres 
*  
* Este programa es software libre; Usted puede usarlo bajo los términos de la licencia de 
* software GPL versión 2.0 de la Free Software Foundation. 
* 
* Este programa se distribuye con la esperanza de que sea útil, pero SI NINGUNA GARANTÍA;
* tampoco las implícitas garantías de MERCANTILIDAD o ADECUACIÓN A UN PROPÓSITO PARTICULAR.
* Consulte la licencia GPL para más detalles. Usted debe recibir una copia de la GPL junto 
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5º Piso, Boston, MA 02110-1301, USA.
*
*/
#include <QtCore>
#include <QCoreApplication>
#include <QtGlobal>
#include <QSqlDatabase>

#include "SafetYAWL.h"
#include "SafetToken.h"
#include "SafetDocument.h"
#include "SafetXmlRepository.h"


int main( /*int argc, char *argv[] */)
{

qDebug("Aplicacion corriendo...");

    DbEnv *myEnv = new DbEnv(0);
    XmlManager *myManager = NULL;
 	SafetYAWL configurator;	
    QString home("/home/vbravo/desarrollo/safet/salida/dbxml");
    QString homefile = "container1.dbxml";
    	
    SafetXmlRepository * xmlRepository = new SafetXmlRepository(myEnv, myManager, home);
    qDebug("Paso el constructor");

    // crear de un contenedor
//   XmlContainer container = xmlRepository->createContainer(home +"/"+ homefile, "XmlContainer::WholedocContainer");

	qDebug("Repository: %s", qPrintable(home + "/" +homefile));
    // abrir un contenedor existente
    XmlContainer container = xmlRepository->openContainer(home + "/" +homefile);


//    DbXml::setLogLevel(DbXml::LEVEL_ALL, true);
//    DbXml::setLogCategory(DbXml::CATEGORY_ALL, true);

    // creacion de un alias del contenedor para que no ocurran problemas al momento de construir
    // un contexto de consulta
   container.addAlias("mycontainer");

    // crear un contexto 
  // XmlUpdateContext context = xmlRepository->createContextUpdate();

    // agregar un documento xml
  //xmlRepository->addXmlDocumentToContainer("/home/vbravo/desarrollo/safet/salida/dosfirmas.ddoc", "dosfirmas", container, context);
    //xmlRepository->addXmlDocumentToContainer("/home/antonio/desarrollo/safet/salida/libsafet-salida.ddoc", "libsafet-salida", container, context);
    //xmlRepository->addXmlDocumentToContainer("/home/antonio/desarrollo/safet/salida/pruebafirmada.ddoc", "pruebafirmada", container, context);


    // aqui se esta buscando un documento dentro del contenedor especificado, si exsite se retorna verdadero
    QString docName = "dosfirmas";

    bool answer = false;
    answer = xmlRepository->searchDocumentInContainer(docName, container);

    QString archivoEscrito("");

    if (answer)
    {
	qDebug("SE ENCONTRO EL DOCUMENTO %s EN EL CONTENEDOR %s ", qPrintable(docName), "/tmp/envHome/container1.dbxml");
	// aqui se va a obtener el archivo xml asociado al documento del dbxml
	XmlDocument theDoc = xmlRepository->getXmlDocument(docName, container);
	qDebug("docname: %s", qPrintable(docName));
	archivoEscrito = xmlRepository->writeXmlFileFromDb(docName, container);
	qDebug("archivoEscrito: %s", qPrintable(archivoEscrito));
    }
    else {
	qDebug("NOOOOO SE ENCONTRO EL DOCUMENTO %s EN EL CONTENEDOR %s ", qPrintable(docName), "/tmp/envHome/container1.dbxml");
	}

    xmlRepository->~SafetXmlRepository();
    qDebug("Paso el destructor");
    //return 0;


    qDebug("Creacion de objeto SafetDocument ...");
    SafetDocument doc;

    doc.initializeLibdigidoc();
    qDebug("Inicializada de libreria ...");

    doc.createOpenXAdESContainer();
    qDebug("   contenendor OpenXAdES");

    // ************* prueba para abrir el archivo de configuracion de digidoc ******
    doc.initDigidocConfigStore("/etc/digidoc.conf");

    // ************* prueba para leer un contenedor desde un archivo en el sistema de archivos*************
    doc.readOpenXAdESContainerFile(archivoEscrito);
    //qDebug("numero de atributos: %s", qPrintable(QString::number(doc.numberOfDataFileAttributes())));

/*
    int indiceFirma = 1;
    QString cn = doc.getCN(indiceFirma);
    if (cn.isEmpty())
    {
	qDebug("NO SE ENCONTRO EL CN DEL FIRMANTE EN LA FIRMA CON INDICE %d", indiceFirma);
	return 1;
    }
    qDebug("\tVALOR DE CN= %s", qPrintable(cn));
*/


    QString commonName="Antonio Araujo Brett";
    int newIndex = doc.getSignerIndex(commonName);
    if (newIndex == -1)
    {
	qDebug("NO SE ENCONTRO NINGUNA FIRMA CON CN: %s en el contenedor digidoc", qPrintable(commonName));
	return 1;
    }


    // ************* prueba para verificar firma con tarjeta inteligente***********************
    int r = doc.verifySignMadeWithSmartCard(archivoEscrito, newIndex);

    if (r == ERR_OK)
    {
	qDebug("la verificacion fue correcta, no hubo errores");
	doc.closeLibdigidoc();
	return 0;
    }
    else
    {
	qDebug("la verificacion fue INCORRECTA, HUBO errores");
	doc.closeLibdigidoc();
	return 1;
    }

    //return 0;







} // fin del main
