/*
* SAFET Sistema Automatizado para la Firma Electrónica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*     
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigación en Tecnologías Libres 
*  
* Este programa es software libre; Usted puede usarlo bajo los términos de la licencia de 
* software GPL versión 2.0 de la Free Software Foundation. 
* 
* Este programa se distribuye con la esperanza de que sea útil, pero SI NINGUNA GARANTÍA;
* tampoco las implícitas garantías de MERCANTILIDAD o ADECUACIÓN A UN PROPÓSITO PARTICULAR.
* Consulte la licencia GPL para más detalles. Usted debe recibir una copia de la GPL junto 
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5º Piso, Boston, MA 02110-1301, USA.
*
*  safet: Aplicación para la línea de comandos
*/

#include "SafetConsoleApp.h"
#include "SafetDocument.h"
#include "SafetDirXmlRepository.h"

// programa de ejemplo para aplicar firma electronica a un documento digidoc utilizando clave
// privada almacenada en un archivo en formato PEM y su correspondiente certificado (tambien
// en formato PEM).


int main( int argc, char *argv[] ) {

	SafetYAWL yawl;
	SafetYAWL::setEvalExit( SafetConsoleApp::evalEventOnExit );

	SafetYAWL::streamlog <<  SafetLog::Action << "Programa iniciado correctamente...OK!";


	SafetDocument doc;
	doc.initializeLibdigidoc();
	doc.initDigidocConfigStore("/usr/local/libdigidocModificada/etc/digidoc.conf");
	doc.createOpenXAdESContainer();

//QString keyFile = "/home/antonio/desarrollo/safet/docs/certificadosPrueba/probadorKey.pem";
	//QString keyFile = "/home/antonio/desarrollo/safet/docs/certificadosPrueba/prueba1Key.pem";
	QString keyFile = "/home/antonio/desarrollo/safet/docs/certificadosPrueba/presupuestoKey.pem";
	
	// esta variable es solo necesaria si la clave esta protegida por contrasena
	QString passwd = "123456";
	
	//QString certFile = "/home/antonio/desarrollo/safet/docs/certificadosPrueba/probador.crt";
	//QString certFile = "/home/antonio/desarrollo/safet/docs/certificadosPrueba/prueba1.crt";
	QString certFile = "/home/antonio/desarrollo/safet/docs/certificadosPrueba/presupuesto.crt";
	
	//QString inFile = "/home/antonio/desarrollo/safet/docs/certificadosPrueba/documentoSinFirma.ddoc";
	//QString inFile = "/tmp/contenedor.ddoc";
	//QString inFile = "/tmp/firmadoConClaveEnArchivo.ddoc";
	QString inFile = "/tmp/firmadoConClaveEnArchivo2.ddoc";

	//QString outFile = "/tmp/firmadoConClaveEnArchivo2.ddoc";
	QString outFile = "/tmp/firmadoConClaveEnArchivo3.ddoc";
	
	//QString manifest = "Rol de prueba";
	QString manifest = "Jefe de Presupuesto";
	
	QString city = "Maracaibo";
	QString state = "Zulia";
	QString zip = "3030";
	QString country = "Venezuela";
	QString notaryUrl = "http://localhost:2560";
	QString proxyHost = "localhost";
	QString proxyPort = "2560";

	int salida = 0;
	salida = doc.signWithPrivateKeyOnFile(keyFile, passwd, certFile, inFile, outFile, manifest, city, state, zip, country, notaryUrl, proxyHost, proxyPort);
	
	qDebug("valor retornado de doc.signWithPrivateKeyOnFile: %d",salida);


	return 0;

}
