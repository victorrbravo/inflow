/********************************************************************************
** Form generated from reading UI file 'sendsigndocumentdialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SENDSIGNDOCUMENTDIALOG_H
#define UI_SENDSIGNDOCUMENTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_SendSignDocumentDialog
{
public:
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *closeButton;
    QFrame *frame;
    QGridLayout *gridLayout;
    QLabel *label;
    QListWidget *listFiles;
    QHBoxLayout *horizontalLayout;
    QToolButton *selectButton;
    QToolButton *delButton;
    QSpacerItem *horizontalSpacer;
    QPushButton *getContainerButton;
    QPushButton *sendButton;
    QLabel *labelDididocInfo;

    void setupUi(QDialog *SendSignDocumentDialog)
    {
        if (SendSignDocumentDialog->objectName().isEmpty())
            SendSignDocumentDialog->setObjectName(QString::fromUtf8("SendSignDocumentDialog"));
        SendSignDocumentDialog->resize(425, 383);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/add.png"), QSize(), QIcon::Normal, QIcon::Off);
        SendSignDocumentDialog->setWindowIcon(icon);
        gridLayout_2 = new QGridLayout(SendSignDocumentDialog);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalSpacer_2 = new QSpacerItem(294, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 1, 0, 1, 1);

        closeButton = new QPushButton(SendSignDocumentDialog);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/del.png"), QSize(), QIcon::Normal, QIcon::Off);
        closeButton->setIcon(icon1);

        gridLayout_2->addWidget(closeButton, 1, 1, 1, 1);

        frame = new QFrame(SendSignDocumentDialog);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        listFiles = new QListWidget(frame);
        listFiles->setObjectName(QString::fromUtf8("listFiles"));

        gridLayout->addWidget(listFiles, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        selectButton = new QToolButton(frame);
        selectButton->setObjectName(QString::fromUtf8("selectButton"));
        selectButton->setIcon(icon);

        horizontalLayout->addWidget(selectButton);

        delButton = new QToolButton(frame);
        delButton->setObjectName(QString::fromUtf8("delButton"));
        delButton->setEnabled(true);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/del.png"), QSize(), QIcon::Normal, QIcon::On);
        delButton->setIcon(icon2);

        horizontalLayout->addWidget(delButton);

        horizontalSpacer = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        getContainerButton = new QPushButton(frame);
        getContainerButton->setObjectName(QString::fromUtf8("getContainerButton"));
        getContainerButton->setIcon(icon);

        horizontalLayout->addWidget(getContainerButton);

        sendButton = new QPushButton(frame);
        sendButton->setObjectName(QString::fromUtf8("sendButton"));
        sendButton->setEnabled(true);
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/yes.png"), QSize(), QIcon::Normal, QIcon::Off);
        sendButton->setIcon(icon3);

        horizontalLayout->addWidget(sendButton);


        gridLayout->addLayout(horizontalLayout, 2, 0, 1, 1);

        labelDididocInfo = new QLabel(frame);
        labelDididocInfo->setObjectName(QString::fromUtf8("labelDididocInfo"));
        labelDididocInfo->setFrameShape(QFrame::Box);
        labelDididocInfo->setFrameShadow(QFrame::Raised);
        labelDididocInfo->setLineWidth(1);
        labelDididocInfo->setTextFormat(Qt::RichText);
        labelDididocInfo->setWordWrap(true);

        gridLayout->addWidget(labelDididocInfo, 3, 0, 1, 1);


        gridLayout_2->addWidget(frame, 0, 0, 1, 2);


        retranslateUi(SendSignDocumentDialog);
        QObject::connect(closeButton, SIGNAL(clicked()), SendSignDocumentDialog, SLOT(close()));

        QMetaObject::connectSlotsByName(SendSignDocumentDialog);
    } // setupUi

    void retranslateUi(QDialog *SendSignDocumentDialog)
    {
        SendSignDocumentDialog->setWindowTitle(QApplication::translate("SendSignDocumentDialog", "Enviar Archivos XADES (digidoc)", 0, QApplication::UnicodeUTF8));
        closeButton->setText(QApplication::translate("SendSignDocumentDialog", "&Cerrar", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SendSignDocumentDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Archivos <span style=\" color:#00007f;\">firmados electr\303\263nicamente</span> (<span style=\" font-weight:600;\">ddoc</span>) a enviar</p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
        selectButton->setText(QApplication::translate("SendSignDocumentDialog", "...", 0, QApplication::UnicodeUTF8));
        delButton->setText(QApplication::translate("SendSignDocumentDialog", "...", 0, QApplication::UnicodeUTF8));
        getContainerButton->setText(QApplication::translate("SendSignDocumentDialog", "Contenedor...", 0, QApplication::UnicodeUTF8));
        sendButton->setText(QApplication::translate("SendSignDocumentDialog", "&Enviar", 0, QApplication::UnicodeUTF8));
        labelDididocInfo->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class SendSignDocumentDialog: public Ui_SendSignDocumentDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SENDSIGNDOCUMENTDIALOG_H
