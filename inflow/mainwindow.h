/****************************************************************************
**
** Copyright (C) 2006-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.trolltech.com/products/qt/opensource.html
**
** If you are unsure which license is appropriate for your use, please
** review the following information:
** http://www.trolltech.com/products/qt/licensing.html or contact the
** sales department at sales@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
/*
* SAFET Sistema Automatizado para la Firma Electr�nica y Estampado de Tiempo
* Copyright (C) 2008 V�ctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigaci�n en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los t�rminos de la licencia de
* software GPL versi�n 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea �til, pero SI NINGUNA GARANT�A;
* tampoco las impl��citas garant��as de MERCANTILIDAD o ADECUACIÓN A UN PROP�SITO PARTICULAR.
* Consulte la licencia GPL para m�s detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5� Piso, Boston, MA 02110-1301, USA.
*
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H
 #include <QWebView>
#include <QMainWindow>
#include <QTextStream>
#include <QSettings>
#include <QGraphicsSvgItem>
#include <QMutex>
#include <QWaitCondition>
#include <QApplication>
//#include <QTcpServer>
#include <QStackedWidget>
#include <QtNetwork>

#include "SafetYAWL.h"

#include "highlighter.h"
#include "graphicsworkflow.h"
#include "graphicssvgitemlive.h"
#include "threadconsole.h"
#include "threadinfoserver.h"
//#include "SafetConsoleApp.h"
#include "principalframe.h"
#include "docksbmenu.h"
#include "SafetTextParser.h"
//#include "DigiDoc.h"
#include "SafetDocument.h"
#include "QProgressIndicator.h"
#include "iconwidget.h"
//#include "inflowthread.h"
#include "threadconsole.h"
#include "databaseconfigdialog.h"

class QAbstractItemModel;
class QComboBox;
class QCmdCompleter;
class QLabel;
class QLineEdit;
class QProgressBar;
class TextEdit;
class QDomDocument;
class DomModel;
class ThreadConsole;
class GraphicsWorkflow;
class GraphicsSvgItemLive;
class Assistant;
class InflowThread;
//class ThreadConsole;


const QString SAFETCONSOLEFILE = "defconsole.xml";
const QString  SAFETCONFIGFILE = "defconfigure.xml";
const QString  SAFETUSERSFILE = "defusers.xml";
const QString  SAFETMANAGEMENTSIGNFILE = "defmanagementsignfile.xml";

namespace Safet {
    const QString LOADFLOWFILE_NAMEFIELD = QObject::tr("Cargar_archivo_flujo");
    const QString TABLE_NAMEFIELD = QObject::tr("Tabla[a-zA-Z0-9_\\-]*");
const QString AUTOFILTERFLOW_NAMEFIELD = QObject::tr("Autofiltro");
const QString RECURSIVEFILTERFLOW_NAMEFIELD = QObject::tr("Filtro_recursivo");
const QString OPSEPARATOR = QObject::tr(":::");

const QString COLONMARK = "##SAFETCOLON##";

const QString TEMPLATEFLOWFILE = ""
        "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
        "<!--\n"
        "Documento  : tickets.xml\n"
        "Creado     : 16/10/08 09:27 AM\n"
        "Autor      : nombre_autor\n"
        "Descripcion: Archivo generado por plantilla de la Libreria SAFET\n"
        "-->        \n"
        "<!--<!DOCTYPE yawl SYSTEM \"http://trac-safet.cenditel.gob.ve/dtd/yawlworkflow.dtd\">-->\n"
        "<!DOCTYPE yawl SYSTEM \"file://%7/.safet/dtd/yawlworkflow.dtd\">\n"
        "<yawl  version=\"0.01\">\n"
        "<workflow id=\"%6\">\n"
        "        <token key=\"%1\"  keysource=\"%2\">\n"
        "        </token>\n"
        "<parameter title=\"Tarea a borrar\" path=\"%10\" options=\"\" type=\"combotask\" mandatory=\"no\"/>\n"
        " <parameter title=\"Nodo anterior\" path=\"%10\" options=\"\" type=\"combotask\" mandatory=\"no\"/>\n"
        "<parameter title=\"Nodo a agregar\" path=\"%10\" options=\"\" type=\"string\" mandatory=\"no\"/>\n"
        "<parameter title=\"Textualinfo\"  options=\"\" type=\"string\" mandatory=\"no\"/>\n"
        "<parameter title=\"Paralelo al nodo siguiente\"  options=\"No::No::No,Si::Si::Si\" type=\"combolistliteral\" mandatory=\"no\"/>\n"

        "        <condition id=\"inicial\" type=\"start\">\n"
        "                <port side=\"forward\" type=\"split\"   >\n"
        "                         <connection source=\"%8\" query=\"select %3 from %2\"  options=\"%4\" >\n"
        "                        </connection>\n"
        "                </port>\n"
        "        </condition>\n"
        "        <task id=\"%8\"  title=\"%9\">\n"
        "                <port side=\"forward\" type=\"split\" >\n"
        "                        <connection source=\"final\" query=\"true\" options=\"\" >\n"
        "                        </connection>\n"
        "                </port>\n"
        "                <variable id=\"v%8\" scope=\"task\"  type=\"sql\" config=\"1\"   tokenlink=\"\" documentsource=\"select %5 from %2\" >\n"
        "                </variable>\n"
        "        </task>\n"

        "<condition id=\"final\">\n"
        "                <port side=\"forward\" type=\"split\">\n"
        "                        <connection source=\"\">\n"
        "                        </connection>\n"
        "                </port>\n"
        "        </condition>\n"
        "</workflow>\n"
        "        <configuration id=\"1\" connectstring=\"psql:seguridad.cenditel.gob.ve:ticket:vbravo:vbravo_\">\n"
        "        </configuration>\n"
        "</yawl>\n";

}


class MainWindow : public QMainWindow
{
    Q_OBJECT
    static QStatusBar *mystatusbar;


    QToolBar *standardbar;
    QTextEdit *myoutput;
    QTableView *listviewoutput;
    GraphicsWorkflow *gviewoutput;
    QPushButton *changeUserButton;
    QLabel *titleApplication;

    QStandardItemModel *listviewoutputmodel;
    QWebView* weboutput;
    QString _dirInput; // Directorio de archivos imágenes, iconos, archivos utilitarios

    // *** Valores de formato para listado y gráfico de flujo
    QString _listprincipaltitle;
    int _listprincipalcount;
    QString _listprincipalvariable;
    QString _listprincipalkey;

    // *** Valores de formato para listado y gráfico de flujo
    QString  _pathdoc; // Archivo de entrada para generar la consulta
    QString gopt;
    int verbose_flag;
    bool parsed;

    QMap<int,QString> commands;

    QStringList filecompleters;
    QStringList _plugs;

    GraphicsSvgItemLive* _itemredraw;

    QString styleDigidoc;
    friend class ThreadConsole;

    QString _currentjson;
    bool initApp; // para iniciar la aplicacion
    bool _issmartmenuvisible;

     QMutex *mutexconsole;
     bool runningConsole;


     QString _currconfpath; // Path actual

    // Cadena que mantiene la ruta completa de un documento SafetDocument
    // cargado con SafetDocument::readDigidocFile
    QString pathOfSafetDocument;

     bool _checkgoprincipal; // Chequeo para habilitación de barra de botones


//      QQueue<QWaitCondition*> queueconsole;
    QToolButton *menusbbutton;
     //DigiDoc	*digiDocument;
      QMenu  *editMenu,*fileMenu, *toolsMenu;
    QAction *actionCut, *actionCopy, *actionPaste, *actionUndo, *actionRedo, *actionDelfield;
    QAction *actionModifyfield;
    QAction *saveGraph,*restoreGraph, *compareGraphs;
    QAction *exitAction;
    QAction *fileLoadConfiguration,*fileSaveConfiguration;
    QAction *fileLoadFTPConfiguration,*fileSaveFTPConfiguration;
    QAction *fileToChangeUser;

    QAction *delWorkflowAction, *delOneWorkflowAction, *clearTextAction, *toexecAction,
        *toGoPanelEdit, *delNodeflowAction,*addNodeflowAction, *reloadNodeflowAction;

    bool _isloadeditactions; // Se cargaron las acciones

    // indicador de progreso
    //QProgressIndicator* progressIndicator;

    IconWidget * iconWidget;

    /** variable para thread
    *
    */
    //static ThreadConsole myThreadConsole;


    bool _isgui;

    QFtp  *ftp; // Objeto para transferencia Ftp

public:

    // Estructura utilitaria para listar documentos por clave
    struct Miles {
        QString nametask;
        QString rol;
        QDateTime ts;
        int secondswait;
        QString humanwait;
        QString humandate;
    };



    // indicador de progreso
    QProgressIndicator* progressIndicator;

    // Enumeraciones


    // Datos estaticos
    static SafetYAWL *configurator;
    static MainWindow *mymainwindow;
    static DockSbMenu *docksbMenu;
    static DockSbMenu *dockShowResult;
    static QStringList sbmenulist;
    static int gviewoutputwidth;
    static int gviewoutputheight;

    static bool _issigning;
    static QString _currentCommonName;

    // puntero a Thread
    static InflowThread * mythread;

    static QString showString;
    static bool _isrestarting;

    static int _seconstohide; // Segundos para ocultar panel de resultados
    static QTimer _panelinfotimer;


    QMap<QString,QPair<QString,QString> > _graphs;
    // Primer campo: nombre de la operacion
    // Primer elemento del QStringList corresponde a los usuarios para esa operación
    // Segundo elemento del QStringList corresponde a las acciones que pueden ejecutar los usuarios
    // Tercer elemento del QStringList corresponde al SHA1 (firma de la operacion)

    static QMap<QString,QStringList> permises; // Estructura para guardar los permisos

    static QString currentaccount; // cuenta actual de usuario
    static QString currentrole; // rol de la cuenta actual de usuario
    static QString currentrealname;
    static QString currentgraphpath;
    // Funciones
    /**
      \brief ejecuta un funcion de safet segun los parametros que se encuentran en memoria
      (como consola)
     \return retorna true si se ejecutó correctamente, false en caso contrario
     */
    bool executeParsed();
    bool parse(int &argc, char **argv);
    bool parseArgs(const QString& a);
    QStringList& plugs() { return _plugs;}
    void refreshListView(const QString& doc);
    void buildModel(QMap<QString,QVariant>& mymap);

    bool isGui() const { return _isgui;}

    QStringList& getFileCompleters() { return filecompleters;}
    /*
      \brief Funcion que revisa la pila de errores y devuelve Verdadero (true)
             si existen errores. También muestra el panel de errores con los mensajes de errores
      \return Verdadero (true) si existen errores en la pila, Falso (false) de otra forma
      */
    bool queryForErrors();
    QPrinter printer;
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setupToolbar();
    void setupTabWidget();



    QMutex* mutexConsole() { Q_CHECK_PTR(mutexconsole); return mutexconsole;}


    /**
         * @brief delNodeToXMLWorkflow Borrar un nodo de un gr�fico, si es solo un nodo no lo borra
         *
         * @param fname Nombre del archivo a borrar
         * @param nodename Nombre del nodo aborrar
         * @return Si es vacio no se realiz� la eliminaci�n
         */

        QString delNodeToXMLWorkflow(const QString &fname, const QString &nodename);


        /**
             \brief Funciones para la gestion de archivos de SafetWorkflow
                   add - agregar estado
                   del - borrar estado
                   modify - modificar estado

             */
        QString addNodeToXMLWorkflow(const QString &fname,
                                     const QString& beforenode = QString(),
                                     const QString &nodename = QString("newnode"),
                                     bool isparallel = false,
                                     const QString &options = QString(),
                                     const QString &query = QString(),
                                     const QString &nodetitle = QString(),
                                     const QString &documentsource = QString(),
                                     const QString &rolfield = QString(),
                                     const QString &tsfield = QString(),
                                     const QString &textualinfo = QString()

                );


    /**
           \brief
           \param fname Nombre del flujo de trabajo
           \param currnode Nodo a cambiar la conexi�n
           \param nextnode proximo Nodo la conexi�n a cambiar
           \param newnode Nuevo Nodo a la que se cambiar� la conexi�n
           \param  nombre del archivo modificado (flujo de trabajo) generalmente "fname"
           */
         QString changeConnXMLWorkflow(const QString &fname,
                                       const QString &currnode,
                                       const QString &nextnode,
                                       const QString &newnode,
                                       const QString& newoptions = QString(),
                                      const QString&  newquery  = QString());

    /**
     ** \brief Obtiene la plantilla de estilo principal
     * \return Plantilla de estilo en formato CSS
     */



    QString getPrincipalCSS() const;

    /**
    * @brief convertOpToTitle Convierte el nombre de un titulo a una operacion
    * @param op titulo
    * @return  titulo con espacios
    */

   static QString convertOpToTitle(const QString &op);

   /**
    * @brief convertTitleToOp Al reves
    * @param op  Titulo
    * @return
    */

   static QString convertTitleToOp(const QString &op);
    /**
    * \brief Borra todos los archivos que se encuentran en el directorio d
      \param d Directorio donde se eliminarán los archivos
    */
    static void doDelAllFilesOnDirectory(const QString& d);
    /**
    * \brief Funcion delegada (Callback) para gestionar un error
    */
    static void evalEventOnExit(SafetLog::Level);


    /**
      * \brief Retorna el mapa de los grafos visibles (nombre, codigo para ser generado)
      */
    QMap<QString,QPair<QString,QString> >& graphs() { return _graphs; }

    /**
     * \brief Evalua si la firma de una operacion es válida segun un determinado permiso
              (read,view,modify)
        \param op Firma de la operación
        \param permise Tipo de permiso (read,view,modify);

     */
    static bool doPermiseExecOperation(const QString& op, const QString& permise = "read");

        /*
          *  \brief Coloca el conjunto de comandos para ser utilizado en la consola
          * \param cmds conjunto de comandos
          */
        void setCommands(QMap<int,QString>& cmds) { commands = cmds;}

       /*
         * \brief Coloca el item a redibujar
         * \brief item a redibujar
         */
        void setItemRedraw(GraphicsSvgItemLive* i = 0) { _itemredraw = i; }
    /**
    * \brief Funcion delegada (Callback) para gestionar la entrada
    */
    static QString evalEventOnInput(SafetYAWL::TypeInput,const QString&, bool&);

     // Metodos para la Consola
    void listTasks();
    void listDocuments(const QString& key);
    void listDocuments();
    void manageData();
    bool genGraph();
    void signDocument();
    void calStatistics();
    /**
        * \brief Muestra la ayuda del uso de la aplicacion de consola safet
        */
    void displayUsage();

    void version();

    /*
          * \brief Genera una Consola (Shell)
          * \param command número de parámetro
          * \return true si el resultado fue exitoso, falso en caso contrario
          */
    bool  processCommand(int command);

    /**
       \brief Dada una sentencia SQL (sql) se procesa un archivo de texto ( por defecto se procesa safet.conf)
              modificando las diferentes opciones seg�n  la expresi�n SQL
       \param sql Sentencia SQL (INSERT, UPDATE, etc..)
       \param filename ruta completa  del archivo de configuraci�n, si no se especifica se toma por defecto safet.conf
       \param multiplefields Admite sustituciones de campo multiples terminados en ".*"
       \param numberreg Número de registro a cambiar
       */
    void proccessConfFile(const QString& sql, const QString& filename = "", bool multiplefields = false);
    void setCommands(const QMap<int,QString>& cmds) { commands = cmds; }
    void setModelCompleter(int opt);
    TextEdit*  centraledit() { return completingTextEditForm;}
    bool loadWidgetPlugins(const QString& f, bool add = false);
    void loadSettings();
    void writeSettings();
    QString evalJS(const QString &js);


    /**
      \brief Traduce el contenido de la pila "stack" a una cadena en formato HTML, en funcion
      de del tipo de mensaje denotado por "l"
      \param stack Pila de mensajes, al finalizar esta pila estara vacia
      \param l Tipo del mensaje (Error, Warning, Action,Debug)
      \return Cadena en formato HTML que contiene hora y texto de cada mensaje
      */
    QString renderMessageStack(QStack<QPair<QDateTime,QString> >& stack, SafetLog::Level l);

    /**
      * Verificaciones
      */
    void successVerification(QStringList list = QStringList(), const QString& msg = "");

    /** Funcion agregada para eliminar dependencia de digidocpp realiza la verificacion
    * de un contenedor. Se utiliza para desplegar el widget SafetSignatureWidget
    * \param list lista de cadena
    * \param msg mensaje
    * \param doc objeto SafetDocument asociado al contenedor
    */
    void successVerification(QStringList list, const QString& msg, SafetDocument doc);


    /**
      Copia los archivos iniciales para el directorio "home" / .safet
      **/
    void copyInitialFiles();


    QStackedWidget* getStackedWidget() { return stackWidget;}

    /**
      \brief Cifrar o descifrar  un archivo indicado por pathname
      \param pathname ruta completa del archivo a cifrar
      \param operation 0 cifrar y 1 para descifrar
      */
    void doCipherFile(bool iscipher = false);


    /**
      \brief Comparar dos gráficos (first,second) y generar un tercero con la diferencia de fichas
             entre los dos
      \param first Fuente del primer gráfico
      \param second Fuente del segundo gráfico
      \return Fuente del gráfico con la estadística
      */
    QString generateGraphCompared(const QString& first, const QString& second);


    /**
      \brief Dibuja el gráfico especificado por "code" en la pestana de flujos de trabajos (Gráfico)
      y le agrega la etiqueta de fecha "datetag"
      \param code Código del gráfico a dibujar
      \param datetag Etiqueta de la fecha
      */
    void doRenderGraph(const QString& code, const QString& datetag);

    /**
      \brief Habilita/Deshabilita los botones enviar de las pestanas una vez que se conecta
      a la fuente de datos correctamente
      **/
    void setEnableCompletingButtons(bool b);

    /**
     * @brief numberOfTask Cuenta el n�mero de nodos tareas (Task en una lista de nodos XML)
     * @param list
     * @return
     */
    static uint numberOfTask(const QDomNodeList &list);

    /**
      * \brief Chequea la lista de argumentos que se pasa en un cuadro de edición , según la
                lista colocada en el archivo de configuración (widgets.arguments.*)
      * \param s Cadena a chequar en el formato <campo>:<value>
      */
    static void checkSafetArguments(const QString &s);


    /**
      \brief Carga la plantilla que se muestra en la pestaña de "Reportes"
      */
    void loadReportTemplate();


    /**
      \brief Colocar una serie de valores de configuración intermedia (Solo mientras permanezca
        el objeto en memoria)
        \param mymap lista de valores y sus respectivos valores
        */
    void setConffileValues(const QMap<QString, QString> &mymap);

    /**
      \brief Construye una lista de acciones separadas por el caracter
      definido en Safet::LISTSEPARATORCHARACTER
      en
        \param cadena a procesar
        \return Lista de acciones
      */
    QStringList splitActions(const QString &ls);




    QString replaceMarks(const QString &s);



    QString genPagesXMLWorkflow(const QString &fname, const QString &tablename);


public Q_SLOTS: // Slots para el manejo de los servicios de Escritorio (QDesktopServices)

        void browse( const QUrl &url );
        void mailTo( const QUrl &url );

            void toSend(bool sign = false);
            void toDoGoPanelEdit();
    void createInitialTables();
        void ftpCommandFinished(int, bool);
private slots:


    void timeHideResult();

    void about();

    void toLoadWeb();
    void toInputForm(const QString& action = "", bool isgui = false);
    void toInputConsole(const QString&  action = "");
    void toInputSign(const QString& action = "");
    void toInputConfigure();
    void toInputUsers(const QString& sentence = "");
    void toDelAllWorkflow();
    void toDelOneWorkflow();

    void toAddNodeflow();
    void toDelNodeflow();

    void toReloadNodeflow();

    void toClearTextEdit();
//    void selInputCombo(int);
    void checkSelInputTab(int opt);
    void selInputTab(int);
    void doQuit();
    void doPrint();
    void doPrintDocument(QPrinter *printer);
    void doPrintPreview();
    void doGeneralOptions();
    void doWidgetsOptions();
    void setToInputForm();
    void setToInputConsole();
    void setToInputReports();
    void setToInputFlowGraph();
    void setToInputManagementSignDocument();
    void doGetSignDocument();
    void doSendSignDocument();
    void checkGoPrincipal();

    void addToHistoryList();
    void editToHistoryList();
    void delToHistoryList();
    void saveToHistoryList();
    void loadEditActions();
    void insertFromHistoryList(QListWidgetItem *);
    void showSmartMenu();
    void showSmartMenuWidget(DockSbMenu::ShowAction a = DockSbMenu::Default);
    void showShowResultWidget(DockSbMenu::ShowAction a = DockSbMenu::Default);
    void showSuccessfulMessage(const QString& m);

    void drawWorkflow(const QString& filename);


    void doAssistantHelp();
    void toChangeUser();
    void doSaveGraph();
    void doRestoreGraph();
    void doCompareGraphs();
    void doLoadConfiguration();
    void doSaveConfiguration();




    // establece la ruta completa de un documento SafetDocument
    // cargado con SafetDocument::readDigidocFile
    void setPathOfSafetDocument(QString path) { pathOfSafetDocument = path; }

    // retorna la ruta completa de un documento SafetDocument
    // cargado con SafetDocument::readDigidocFile
    QString getPathOfSafetDocument() { return pathOfSafetDocument; }

    void doInsertInAuthConfFile(QRegExp& rx); // Insertar un grupo de campos en una sección
    /**
     \brief Busca los campos que coinciden con la clave key y devuelve los
            nombres separados por ","
     \param key Clave en el archivo de autorización (auth conf) de la
     \return Campos separados por el caracter ","
     */
    QString searchFieldsInAuthConf(const QString& key);

public slots:
    void doLoadFTP();
    void doSaveFTP();

    void linkClickedSbMenu(const QUrl& url);
    void linkClickedSbResult(const QUrl& url);
    void setEnabledToolBar(bool e = true); // Habilitar o Desahabilitar barra de herramientas
    void doExit();
    /**
      \brief Funcion para preguntar antes de salir
      */
    bool maybeSave();
    /*
      * \brief va a la pantalla principal
      */
    void goPrincipal();
    TextEdit* currentTextEdit() { return completingTextEdit;}



    // slot para ejecutar acciones cuando se termina de ejecutar el thread de MainWindow
    void threadEndJob();

    // se encarga de ejecutar executedParsed() a traves del thread de MainWindow
    void processMainWindowThread();

    // slot para desplegar el dialogo para modificar la configuracion de la fuente de datos
    void manageDataSources();
    void manageDataSources(DatabaseConfigDialog * dbConfig);

protected:
    void saveWindowState();
    void restoreWindowState();

    void closeEvent(QCloseEvent *event);    
    void moveEvent(QMoveEvent *event);
    void showEvent(QShowEvent *event);


    QPoint m_lastPos;
    QSize m_lastSize;

    QString outputText;
    QTextStream streamText;
    void sendStreamToOutput();
    void iterateMap(const QMap<QString,QVariant>& map);
    void createDockWindow();    
    void createDockShowResultWindow();
    virtual void resizeEvent ( QResizeEvent * event );    
    void buildMenuOnPanelsbMenu(const QString& option);


    /**
       *\brief Genera la lista de archivos (en "myfiles") para un paquete tar
       *\param folder Nombre de ruta completa de la carpeta para generar el archivo tar
       *\param myfiles parámetro de salida con la lista de archivos agregada
       *\param exts Lista de extensiones para cargar los archivos (xml,png,html,etc...)
       *\return nombre del archivo .tar
       */
    QString generateListForTar(const QString& folder, QStringList& myfiles,
                               const QStringList& exts);


    /**
      \brief Genera una cadena en formato JSON dado un conjunto de hitos
      \param miles Lista de hitos ordenados por fecha (orden cronologico)
      \param Parametro de entrada (campos que se agregan en el documento JSON)
      \return cadena JSON
      */

    QString getJSONMiles(const QList<MainWindow::Miles>& miles,QList<QSqlField>& myfields);
private:

    bool doCreateCRUD(const ParsedSqlToData& data);


    /**
      * \brief Procesa un archivo tar (f) y descomprime los archivos en
      el directorio de configuración por defecto
      * \param f archivo con extensión .tar a descomprimir
      * \param isprincipal si es el directorio principal
      * \return retorna verdadero si la operación fue exitosa, falso en caso contrario
      */
    bool processDirTar(const QString& f, bool isprincipal = false);
    bool searchInHistoryList(const QString& str);
    QString getScriptLen(const QSqlField& f); // Calcular la longitud del campo para la tabla generada en Listar_Datos

    void createMenu();
    void configureStatusBar();
    QAbstractItemModel *modelFromFile(const QString& fileName);

    void generateJScriptreports(const QString& documents, const QList<QSqlField>& fields);
    void setupStackedWebviews(const QIcon& icon, const QString& name, const QString& desc = QString(""));

    TextEdit *completingTextEdit; // current edit
    TextEdit *completingTextEditForm, *completingTextEditCons, *completingTextEditSign;
    TextEdit *completingTextEditConf; // Ventana de edici�n para la configuraci�n
    TextEdit *completingTextEditUsers; // Ventana de edici�n para la configuraci�n

    QListWidget *listEditForm, *listEditCons, *listEditSign, *listEditConf;
    QToolButton *buttonListEditConsAdd,*buttonListEditConsEdit, *buttonListEditConsDel;
    QToolButton *buttonListEditConsSave;

    QCommandLinkButton* completingButtonForm;
    QCommandLinkButton* cancelButtonForm;
    QCommandLinkButton* showdockButtonForm;

    QCommandLinkButton* completingButtonCons;
    QCommandLinkButton* cancelButtonCons;
    QCommandLinkButton* showdockButtonCons;

    QCommandLinkButton* completingButtonSign;
    QCommandLinkButton* cancelButtonSign;
    QCommandLinkButton* showdockButtonSign;


    QCommandLinkButton* completingButtonConf;
    QCommandLinkButton* cancelButtonConf;
    QCommandLinkButton* showdockButtonConf;

    QCommandLinkButton* completingButtonUsers;
    QCommandLinkButton* cancelButtonUsers;
    QCommandLinkButton* showdockButtonUsers;

    //    QComboBox *comboMode;
    DomModel *dommodel;
    Highlighter *highlighterForm, *highlighterCons, *highlighterConf,
    *highlighterSign,*highlighterUsers;
    QTabBar *centralBar;
    QTabWidget *centralWidget;
    QStackedWidget *stackWidget;
    QList<QWebView*> stackedwebviews;

    PrincipalFrame *principalFrame;

    QCmdCompleter *completer;

    // Utilizado para reporte en web
    QString jscriptarray;
    QString jscriptcolumns;
    bool jscriptload;
    QString currentDocuments;
    QList<QSqlField> currentFields;

    // para mostrar el Assistant
    Assistant *assistant;

    // Objeto SafetDocument asociado a MainWindow
    //SafetDocument safetDocument;

    bool addCRUDtoXML(const ParsedSqlToData &data);
private slots:
    void executeJSCodeAfterLoad(bool ok );



};


#endif // MAINWINDOW_H
