/*
* SAFET Sistema Automatizado para la Firma Electr�nica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigaci�n en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los t�rminos de la licencia de
* software GPL versi�n 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea �til, pero SI NINGUNA GARANT�A;
* tampoco las impl��citas garant��as de MERCANTILIDAD o ADECUACIÓN A UN PROP�SITO PARTICULAR.
* Consulte la licencia GPL para m�s detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5� Piso, Boston, MA 02110-1301, USA.
*
*/

#ifndef COMBOVARWIDGET_H
#define COMBOVARWIDGET_H

#include "cmdwidget.h"
#include "SafetYAWL.h"
#include "textedit.h"

class ComboWidget : public CmdWidget
{
 Q_OBJECT

public:
    enum Type { ListTable, VariableSafet, AutofilterSafet, ListLiteral, Flow, RecursivefilterSafet,
            ConffileSafet, ColorSafet, FieldsSafet,TaskSafet};
private:
    Type _type;
    QStringList _itemvaluelist,_itemrealvaluelist;
    QString hashtext;
    QString proccessWhereOption(const QString& w);
public:
     Type type() const { return _type; }
     void setType(ComboWidget::Type t) { _type = t; }
public:

    ComboWidget(const QString& s, ComboWidget::Type t = ComboWidget::VariableSafet,
                QWidget *parent = 0, bool istextparent = true);
    ~ComboWidget();
    virtual void setFocus ( Qt::FocusReason reason );
    virtual void buildWidget();
    void buildButtonWidget(const QStringList& l);
    virtual QRect getGeoParams() const;
    virtual bool isValid(QString& value);



public slots:
    virtual QString text() const;
    virtual void setText(const QString &newText);
    void updateCombo();
    void updateComboVariableSafet(bool inwidget = true);
    void updateComboTaskSafet(bool inwidget = true);
    void updateComboAutofilterSafet(bool inwidget = true);
    void updateComboRecursivefilterSafet(bool inwidget = true);
    void updateComboListTable(bool inwidget = true);
    void updateComboListLiteral();
    void updateVarGlobal(const QString& value);
    void updateComboFlow(bool inwidget = true);
    void updateComboConffileSafet(bool inwidget = true);
    void updateComboColorSafet(bool inwidget = true);
    void updateComboFields(bool inwidget = true);
    void viewdoc();
    void selColor();
    virtual void insertAndClose();
    /**
     * @brief getRealValue obtiene el valor de titulo (title) del widget
     * @param s valor de titulo (title)
     * @return Valor real
     */
    QString getRealValue(const QString& s);
    /**
     * @brief getTitleValue obtiene el valor de titulo dado el valor de real
     * @param s valor real
     * @return Valor de titulo
     */
    QString getTitleValue(const QString &s);
    QString getVarValue(const QString& s);
    QStringList& itemsValueList() { return _itemvaluelist;}
    QStringList& itemsRealValueList() { return _itemrealvaluelist;}
    QComboBox* combo() { return varbox; }
protected:
    QComboBox* varbox;
    QPushButton* signbutton;
    QToolButton* viewdocbutton;
    QToolButton* getcolorbutton;
    QStringList infos;
    QMap<QString,QString>  realvalues;
    QMap<QString,QString>  varvalues;
    QString _cursigners;
    void generateLiterals(const QString& s, bool inwidget = true);

};

#endif // COMBOVARWIDGET_H

