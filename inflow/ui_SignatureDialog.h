/********************************************************************************
** Form generated from reading UI file 'SignatureDialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SIGNATUREDIALOG_H
#define UI_SIGNATUREDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFormLayout>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SignatureDialog
{
public:
    QVBoxLayout *SignatureDialogLayout;
    QLabel *title;
    QLabel *error;
    QTabWidget *tabWidget;
    QWidget *signer;
    QGridLayout *signerLayout;
    QGroupBox *signerLocationGroup;
    QFormLayout *signerLocationGroupLayout;
    QLabel *signerCityLabel;
    QLineEdit *signerCity;
    QLabel *signerStateLabel;
    QLineEdit *signerState;
    QLabel *signerCountryLabel;
    QLineEdit *signerCountry;
    QLabel *signerZipLabel;
    QLineEdit *signerZip;
    QSpacerItem *verticalSpacer;
    QGroupBox *signerRoleGroup;
    QVBoxLayout *signerRoleGroupLayout;
    QLineEdit *signerRole;
    QLineEdit *signerResolution;
    QSpacerItem *signerSpacer;
    QPushButton *signerShowCert;
    QWidget *signature;
    QGridLayout *signatureLayout;
    QTreeWidget *signatureView;
    QSpacerItem *signatureSpacer;
    QPushButton *signatureShowCert;
    QWidget *ocsp;
    QGridLayout *verifyLayout;
    QTreeWidget *ocspView;
    QSpacerItem *ocspSpacer;
    QPushButton *ocspShowCert;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SignatureDialog)
    {
        if (SignatureDialog->objectName().isEmpty())
            SignatureDialog->setObjectName(QString::fromUtf8("SignatureDialog"));
        SignatureDialog->resize(400, 335);
        SignatureDialogLayout = new QVBoxLayout(SignatureDialog);
        SignatureDialogLayout->setObjectName(QString::fromUtf8("SignatureDialogLayout"));
        title = new QLabel(SignatureDialog);
        title->setObjectName(QString::fromUtf8("title"));

        SignatureDialogLayout->addWidget(title);

        error = new QLabel(SignatureDialog);
        error->setObjectName(QString::fromUtf8("error"));

        SignatureDialogLayout->addWidget(error);

        tabWidget = new QTabWidget(SignatureDialog);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        signer = new QWidget();
        signer->setObjectName(QString::fromUtf8("signer"));
        signerLayout = new QGridLayout(signer);
        signerLayout->setObjectName(QString::fromUtf8("signerLayout"));
        signerLocationGroup = new QGroupBox(signer);
        signerLocationGroup->setObjectName(QString::fromUtf8("signerLocationGroup"));
        signerLocationGroupLayout = new QFormLayout(signerLocationGroup);
        signerLocationGroupLayout->setObjectName(QString::fromUtf8("signerLocationGroupLayout"));
        signerCityLabel = new QLabel(signerLocationGroup);
        signerCityLabel->setObjectName(QString::fromUtf8("signerCityLabel"));

        signerLocationGroupLayout->setWidget(0, QFormLayout::LabelRole, signerCityLabel);

        signerCity = new QLineEdit(signerLocationGroup);
        signerCity->setObjectName(QString::fromUtf8("signerCity"));
        signerCity->setReadOnly(true);

        signerLocationGroupLayout->setWidget(0, QFormLayout::FieldRole, signerCity);

        signerStateLabel = new QLabel(signerLocationGroup);
        signerStateLabel->setObjectName(QString::fromUtf8("signerStateLabel"));

        signerLocationGroupLayout->setWidget(1, QFormLayout::LabelRole, signerStateLabel);

        signerState = new QLineEdit(signerLocationGroup);
        signerState->setObjectName(QString::fromUtf8("signerState"));
        signerState->setReadOnly(true);

        signerLocationGroupLayout->setWidget(1, QFormLayout::FieldRole, signerState);

        signerCountryLabel = new QLabel(signerLocationGroup);
        signerCountryLabel->setObjectName(QString::fromUtf8("signerCountryLabel"));

        signerLocationGroupLayout->setWidget(2, QFormLayout::LabelRole, signerCountryLabel);

        signerCountry = new QLineEdit(signerLocationGroup);
        signerCountry->setObjectName(QString::fromUtf8("signerCountry"));
        signerCountry->setReadOnly(true);

        signerLocationGroupLayout->setWidget(2, QFormLayout::FieldRole, signerCountry);

        signerZipLabel = new QLabel(signerLocationGroup);
        signerZipLabel->setObjectName(QString::fromUtf8("signerZipLabel"));

        signerLocationGroupLayout->setWidget(3, QFormLayout::LabelRole, signerZipLabel);

        signerZip = new QLineEdit(signerLocationGroup);
        signerZip->setObjectName(QString::fromUtf8("signerZip"));
        signerZip->setReadOnly(true);

        signerLocationGroupLayout->setWidget(3, QFormLayout::FieldRole, signerZip);

        verticalSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        signerLocationGroupLayout->setItem(4, QFormLayout::FieldRole, verticalSpacer);


        signerLayout->addWidget(signerLocationGroup, 0, 0, 3, 1);

        signerRoleGroup = new QGroupBox(signer);
        signerRoleGroup->setObjectName(QString::fromUtf8("signerRoleGroup"));
        signerRoleGroupLayout = new QVBoxLayout(signerRoleGroup);
        signerRoleGroupLayout->setObjectName(QString::fromUtf8("signerRoleGroupLayout"));
        signerRole = new QLineEdit(signerRoleGroup);
        signerRole->setObjectName(QString::fromUtf8("signerRole"));
        signerRole->setReadOnly(true);

        signerRoleGroupLayout->addWidget(signerRole);

        signerResolution = new QLineEdit(signerRoleGroup);
        signerResolution->setObjectName(QString::fromUtf8("signerResolution"));

        signerRoleGroupLayout->addWidget(signerResolution);


        signerLayout->addWidget(signerRoleGroup, 0, 1, 1, 1);

        signerSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        signerLayout->addItem(signerSpacer, 1, 1, 1, 1);

        signerShowCert = new QPushButton(signer);
        signerShowCert->setObjectName(QString::fromUtf8("signerShowCert"));

        signerLayout->addWidget(signerShowCert, 2, 1, 1, 1);

        tabWidget->addTab(signer, QString());
        signature = new QWidget();
        signature->setObjectName(QString::fromUtf8("signature"));
        signatureLayout = new QGridLayout(signature);
        signatureLayout->setObjectName(QString::fromUtf8("signatureLayout"));
        signatureView = new QTreeWidget(signature);
        signatureView->setObjectName(QString::fromUtf8("signatureView"));
        signatureView->setRootIsDecorated(false);

        signatureLayout->addWidget(signatureView, 0, 0, 1, 2);

        signatureSpacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        signatureLayout->addItem(signatureSpacer, 1, 0, 1, 1);

        signatureShowCert = new QPushButton(signature);
        signatureShowCert->setObjectName(QString::fromUtf8("signatureShowCert"));

        signatureLayout->addWidget(signatureShowCert, 1, 1, 1, 1);

        tabWidget->addTab(signature, QString());
        ocsp = new QWidget();
        ocsp->setObjectName(QString::fromUtf8("ocsp"));
        verifyLayout = new QGridLayout(ocsp);
        verifyLayout->setObjectName(QString::fromUtf8("verifyLayout"));
        ocspView = new QTreeWidget(ocsp);
        ocspView->setObjectName(QString::fromUtf8("ocspView"));
        ocspView->setRootIsDecorated(false);

        verifyLayout->addWidget(ocspView, 0, 0, 1, 2);

        ocspSpacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        verifyLayout->addItem(ocspSpacer, 1, 0, 1, 1);

        ocspShowCert = new QPushButton(ocsp);
        ocspShowCert->setObjectName(QString::fromUtf8("ocspShowCert"));

        verifyLayout->addWidget(ocspShowCert, 1, 1, 1, 1);

        tabWidget->addTab(ocsp, QString());

        SignatureDialogLayout->addWidget(tabWidget);

        buttonBox = new QDialogButtonBox(SignatureDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Close);

        SignatureDialogLayout->addWidget(buttonBox);


        retranslateUi(SignatureDialog);
        QObject::connect(buttonBox, SIGNAL(clicked(QAbstractButton*)), SignatureDialog, SLOT(reject()));
        QObject::connect(signatureShowCert, SIGNAL(clicked()), SignatureDialog, SLOT(showCertificate()));
        QObject::connect(signerShowCert, SIGNAL(clicked()), SignatureDialog, SLOT(showCertificate()));
        QObject::connect(ocspShowCert, SIGNAL(clicked()), SignatureDialog, SLOT(showCertificate()));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(SignatureDialog);
    } // setupUi

    void retranslateUi(QDialog *SignatureDialog)
    {
        signerLocationGroup->setTitle(QApplication::translate("SignatureDialog", "Signer location", 0, QApplication::UnicodeUTF8));
        signerCityLabel->setText(QApplication::translate("SignatureDialog", "Ciudad", 0, QApplication::UnicodeUTF8));
        signerStateLabel->setText(QApplication::translate("SignatureDialog", "Estado/Dep", 0, QApplication::UnicodeUTF8));
        signerCountryLabel->setText(QApplication::translate("SignatureDialog", "Pa\303\255s", 0, QApplication::UnicodeUTF8));
        signerZipLabel->setText(QApplication::translate("SignatureDialog", "\303\201rea (C\303\263digoi)", 0, QApplication::UnicodeUTF8));
        signerRoleGroup->setTitle(QApplication::translate("SignatureDialog", "Rol / resoluci\303\263n", 0, QApplication::UnicodeUTF8));
        signerShowCert->setText(QApplication::translate("SignatureDialog", "Mostrar Certificado", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(signer), QApplication::translate("SignatureDialog", "Firmante", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem = signatureView->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("SignatureDialog", "Valor", 0, QApplication::UnicodeUTF8));
        ___qtreewidgetitem->setText(0, QApplication::translate("SignatureDialog", "Propiedad", 0, QApplication::UnicodeUTF8));
        signatureShowCert->setText(QApplication::translate("SignatureDialog", "Mostrar certificado", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(signature), QApplication::translate("SignatureDialog", "Firma", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem1 = ocspView->headerItem();
        ___qtreewidgetitem1->setText(1, QApplication::translate("SignatureDialog", "Valor", 0, QApplication::UnicodeUTF8));
        ___qtreewidgetitem1->setText(0, QApplication::translate("SignatureDialog", "Propiedad", 0, QApplication::UnicodeUTF8));
        ocspShowCert->setText(QApplication::translate("SignatureDialog", "Mostrar certificado", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(ocsp), QApplication::translate("SignatureDialog", "Verificaci\303\263n", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(SignatureDialog);
    } // retranslateUi

};

namespace Ui {
    class SignatureDialog: public Ui_SignatureDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SIGNATUREDIALOG_H
