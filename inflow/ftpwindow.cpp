/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
**     the names of its contributors may be used to endorse or promote
**     products derived from this software without specific prior written
**     permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtGui>
#include <QtNetwork>

#include "ftpwindow.h"
#include "SafetYAWL.h"

FtpWindow::FtpWindow(QWidget *parent, FtpWindow::Type t)
    : QDialog(parent), ftp(0), networkSession(0)
{
    _type = t;
    ftpServerLabel = new QLabel(tr("Servidor &FTP:"));

    QString defaultserver = SafetYAWL::getConf()["Ftp/default.server"].isEmpty()
                            ? "safet.org" : SafetYAWL::getConf()["Ftp/default.server"];
    QString defaultaccount = SafetYAWL::getConf()["Ftp/default.account"].isEmpty()
                             ? "safet" : SafetYAWL::getConf()["Ftp/default.account"];


    ftpServerLineEdit = new QLineEdit(defaultserver);
    ftpServerLabel->setBuddy(ftpServerLineEdit);

    ftpAccountLabel = new QLabel(tr("Usuario:"));
    ftpAccountLineEdit = new QLineEdit(defaultaccount);
    ftpAccountLabel->setBuddy(ftpAccountLabel);

    ftpPassLabel = new QLabel(tr("Contrase�a:"));
    ftpPassLineEdit = new QLineEdit("");
    ftpPassLineEdit->setEchoMode(QLineEdit::Password);
    ftpPassLabel->setBuddy(ftpPassLabel);

    _currentTotalBytes = -1;
    statusLabel = new QLabel(tr("Por favor, escriba el nombre del servidor FTP."));
#ifdef Q_OS_SYMBIAN
    // Use word wrapping to fit the text on screen
    statusLabel->setWordWrap( true );
#endif

    fileList = new QTreeWidget;
    fileList->setEnabled(false);
    fileList->setRootIsDecorated(false);
    fileList->setHeaderLabels(QStringList() << tr("Nombre") << tr("Tama�o") << tr("Propietario")
                              << tr("Grupo") << tr("Fecha"));
    fileList->header()->setStretchLastSection(false);

    connectButton = new QPushButton(tr("Conectar"));
    connectButton->setDefault(true);

    cdToParentButton = new QPushButton;
    cdToParentButton->setIcon(QPixmap(":/cdtoparent.png"));
    cdToParentButton->setEnabled(false);


    downloadButton = new QPushButton(tr("&Bajar"));
    downloadButton->setEnabled(false);


    uploadButton = new QPushButton(tr("&Subir"));
    uploadButton->setEnabled(false);


    quitButton = new QPushButton(tr("&Cerrar"));

    buttonBox = new QDialogButtonBox;
    if ( type() == Uploader) {
        buttonBox->addButton(uploadButton, QDialogButtonBox::ActionRole);
    }
    else {
        buttonBox->addButton(downloadButton, QDialogButtonBox::ActionRole);
    }
    buttonBox->addButton(quitButton, QDialogButtonBox::RejectRole);

    progressDialog = new QProgressDialog(this);

    connect(fileList, SIGNAL(itemActivated(QTreeWidgetItem*,int)),
            this, SLOT(processItem(QTreeWidgetItem*,int)));
    connect(fileList, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)),
            this, SLOT(enableDownloadButton()));
    connect(fileList, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)),
            this, SLOT(enableUploadButton()));

    connect(progressDialog, SIGNAL(canceled()), this, SLOT(cancelDownload()));
    connect(connectButton, SIGNAL(clicked()), this, SLOT(connectOrDisconnect()));
    connect(cdToParentButton, SIGNAL(clicked()), this, SLOT(cdToParent()));
    connect(downloadButton, SIGNAL(clicked()), this, SLOT(downloadFile()));
    connect(uploadButton, SIGNAL(clicked()), this, SLOT(uploadFile()));
    connect(quitButton, SIGNAL(clicked()), this, SLOT(close()));

    QGridLayout *topLayout = new QGridLayout;
    topLayout->addWidget(ftpServerLabel,0,0);
    topLayout->addWidget(ftpServerLineEdit,0,1);
    topLayout->addWidget(ftpAccountLabel,1,0);
    topLayout->addWidget(ftpAccountLineEdit,1,1);
    topLayout->addWidget(ftpPassLabel,2,0);
    topLayout->addWidget(ftpPassLineEdit,2,1);


#ifndef Q_OS_SYMBIAN
    topLayout->addWidget(cdToParentButton);
    topLayout->addWidget(connectButton);
#else
    // Make app better lookin on small screen
    QHBoxLayout *topLayout2 = new QHBoxLayout;
    topLayout2->addWidget(cdToParentButton);
    topLayout2->addWidget(connectButton);
#endif

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(topLayout);
#ifdef Q_OS_SYMBIAN
    // Make app better lookin on small screen
    mainLayout->addLayout(topLayout2);
#endif
    mainLayout->addWidget(fileList);
    mainLayout->addWidget(statusLabel);
    mainLayout->addWidget(buttonBox);
    setLayout(mainLayout);


#if (QT_VERSION > QT_VERSION_CHECK(4, 6, 3))
    QNetworkConfigurationManager manager;
    if (manager.capabilities() & QNetworkConfigurationManager::NetworkSessionRequired) {
        // Get saved network configuration
        QSettings settings(QSettings::UserScope, QLatin1String("Trolltech"));
        settings.beginGroup(QLatin1String("QtNetwork"));
        const QString id = settings.value(QLatin1String("DefaultNetworkConfiguration")).toString();
        settings.endGroup();

        // If the saved network configuration is not currently discovered use the system default
        QNetworkConfiguration config = manager.configurationFromIdentifier(id);
        if ((config.state() & QNetworkConfiguration::Discovered) !=
            QNetworkConfiguration::Discovered) {
            config = manager.defaultConfiguration();
        }

        networkSession = new QNetworkSession(config, this);
        connect(networkSession, SIGNAL(opened()), this, SLOT(enableConnectButton()));

        connectButton->setEnabled(false);
        statusLabel->setText(tr("Abriendo sesi�n de red."));
        networkSession->open();
    }

#endif

    setWindowTitle(tr("FTP"));
}


FtpWindow::~FtpWindow() {
    if (type() == Downloader ) {
        delete uploadButton;
    }
    if (type() == Uploader ) {
        delete downloadButton;
    }

}

QSize FtpWindow::sizeHint() const
{
    return QSize(500, 300);
}



void FtpWindow::connectOrDisconnect()
{
    if (ftp) {
        ftp->abort();
        ftp->deleteLater();
        ftp = 0;
//![0]
        fileList->setEnabled(false);
        cdToParentButton->setEnabled(false);
        downloadButton->setEnabled(false);
        connectButton->setEnabled(true);
        connectButton->setText(tr("Conectar"));
#ifndef QT_NO_CURSOR
        setCursor(Qt::ArrowCursor);
#endif
        statusLabel->setText(tr("Por favor, escriba el nombre del servidor FTP."));
        return;
    }

#ifndef QT_NO_CURSOR
    setCursor(Qt::WaitCursor);
#endif

    ftp = new QFtp(this);
    connect(ftp, SIGNAL(commandFinished(int,bool)),
            this, SLOT(ftpCommandFinished(int,bool)));
    connect(ftp, SIGNAL(listInfo(QUrlInfo)),
            this, SLOT(addToList(QUrlInfo)));
    connect(ftp, SIGNAL(dataTransferProgress(qint64,qint64)),
            this, SLOT(updateDataTransferProgress(qint64,qint64)));

    fileList->clear();
    currentPath.clear();
    isDirectory.clear();
    QUrl url(ftpServerLineEdit->text());
    if (!url.isValid() || url.scheme().toLower() != QLatin1String("ftp")) {
        ftp->connectToHost(ftpServerLineEdit->text(), 21);
        ftp->login(ftpAccountLineEdit->text(),ftpPassLineEdit->text());
    } else {
        ftp->connectToHost(url.host(), url.port(21));

        if (!url.userName().isEmpty())
            ftp->login(QUrl::fromPercentEncoding(url.userName().toLatin1()), url.password());
        else
            ftp->login(ftpAccountLineEdit->text(),ftpPassLineEdit->text());
        if (!url.path().isEmpty())
            ftp->cd(url.path());
    }
//![2]

    fileList->setEnabled(true);
    connectButton->setEnabled(false);
    connectButton->setText(tr("Desconectar"));
    statusLabel->setText(tr("Conectando al servidor FTP %1...")
                         .arg(ftpServerLineEdit->text()));
}

//![3]
void FtpWindow::uploadFile()
{

    qDebug("....FtpWindow::uploadFile...(1)...");
    myputfile = new QFile(_localFileName);

    qDebug("....FtpWindow::uploadFile...(2)...");

    if (!myputfile->open(QIODevice::ReadOnly)) {
        qDebug("...error en putfile...");
        delete myputfile;
    }
    ftp->put(myputfile,_remoteFileName);


    qDebug("....FtpWindow::uploadFile...(3)...");
    progressDialog->setLabelText(tr("Subiendo %1...").arg(_localFileName));
    qDebug("....FtpWindow::uploadFile...(4)...");
    progressDialog->exec();

    delete myputfile;



}
void FtpWindow::downloadFile()
{


    QString fileName = fileList->currentItem()->text(0);

    _localFileName = QDir::tempPath() + "/" + fileName;
    qDebug("....FtpWindow::downloadFile()...fileName:%s",qPrintable(fileName));

    file = new QFile(_localFileName);
    if (!file->open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        QMessageBox::information(this, tr("Bajando configuraci�n FTP"),
                                 tr("No es posible guardar el archivo %1: %2.")
                                 .arg(_localFileName).arg(file->errorString()));
        delete file;
        return;
    }

    ftp->get(fileList->currentItem()->text(0), file);

    progressDialog->setLabelText(tr("Descargando a %1...").arg(_localFileName));
    downloadButton->setEnabled(false);
    progressDialog->exec();
}

void FtpWindow::cancelDownload()
{
    ftp->abort();
}

void FtpWindow::ftpCommandFinished(int, bool error)
{
#ifndef QT_NO_CURSOR
    setCursor(Qt::ArrowCursor);
#endif

    if (ftp->currentCommand() == QFtp::ConnectToHost) {
        if (error) {
            QMessageBox::information(this, tr("FTP"),
                                     tr("No es posible conectar al servidor FTP "
                                        "wn %1. Chequee el nombre del servidor "
                                        )
                                     .arg(ftpServerLineEdit->text()));
            connectOrDisconnect();
            return;
        }
        statusLabel->setText(tr("Conectado a %1.")
                             .arg(ftpServerLineEdit->text()));
        fileList->setFocus();
        downloadButton->setDefault(true);
        connectButton->setEnabled(true);
        return;
    }
    if (ftp->currentCommand() == QFtp::Login)
        ftp->list();
    if (ftp->currentCommand() == QFtp::Get) {
        if (error) {
            statusLabel->setText(tr("Se cancel� la descarga de %1.")
                                 .arg(file->fileName()));
            file->close();
            file->remove();
        } else {
            statusLabel->setText(tr("Descargado archivo a \"%1\"")
                                 .arg(file->fileName()));
            file->close();
        }
        delete file;
        enableDownloadButton();
        progressDialog->hide();
        accept();
    } else if (ftp->currentCommand() == QFtp::Put) {
        qDebug("....FtpWindow::ftpCommandFinished...put...(1)...");
        accept();

    } else if (ftp->currentCommand() == QFtp::List) {
        if (isDirectory.isEmpty()) {
            fileList->addTopLevelItem(new QTreeWidgetItem(QStringList() << tr("<vac�o>")));
            fileList->setEnabled(false);
        }
    }
//![9]
}

//![10]
void FtpWindow::addToList(const QUrlInfo &urlInfo)
{
    if ( !urlInfo.isDir() &&  !urlInfo.name().endsWith(".tar")) {
        return;
    }

    QTreeWidgetItem *item = new QTreeWidgetItem;

    item->setText(0, urlInfo.name());
    item->setText(1, QString::number(urlInfo.size()));
    item->setText(2, urlInfo.owner());
    item->setText(3, urlInfo.group());
    item->setText(4, urlInfo.lastModified().toString("MMM dd yyyy"));

    QPixmap pixmap(urlInfo.isDir() ? ":/dir.png" : ":/file.png");
    item->setIcon(0, pixmap);

    isDirectory[urlInfo.name()] = urlInfo.isDir();
    fileList->addTopLevelItem(item);
    if (!fileList->currentItem()) {
        fileList->setCurrentItem(fileList->topLevelItem(0));
        fileList->setEnabled(true);
    }
}
//![10]

//![11]
void FtpWindow::processItem(QTreeWidgetItem *item, int /*column*/)
{
    QString name = item->text(0);
    if (isDirectory.value(name)) {
        fileList->clear();
        isDirectory.clear();
        currentPath += '/';
        currentPath += name;
        ftp->cd(name);
        ftp->list();
        cdToParentButton->setEnabled(true);
#ifndef QT_NO_CURSOR
        setCursor(Qt::WaitCursor);
#endif
        return;
    }
}
//![11]

//![12]
void FtpWindow::cdToParent()
{
#ifndef QT_NO_CURSOR
    setCursor(Qt::WaitCursor);
#endif
    fileList->clear();
    isDirectory.clear();
    currentPath = currentPath.left(currentPath.lastIndexOf('/'));
    if (currentPath.isEmpty()) {
        cdToParentButton->setEnabled(false);
        ftp->cd("/");
    } else {
        ftp->cd(currentPath);
    }
    ftp->list();
}

void FtpWindow::updateDataTransferProgress(qint64 readBytes,
                                           qint64 totalBytes)
{
    if ( totalBytes == -1 ) {
        if (_currentTotalBytes == -1 ) {
            progressDialog->setMaximum(100);
            progressDialog->setValue(50);
        }
        else {
            progressDialog->setMaximum(_currentTotalBytes);
            progressDialog->setValue(readBytes);
        }
    }
    else {
       progressDialog->setMaximum(totalBytes);
       progressDialog->setValue(readBytes);
    }


       SYD  << tr("...FtpWindow::updateDataTransferProgress...readBytes:|%1|"
                  "...totalBytes: |%2|")
            .arg(readBytes)
            .arg(totalBytes);

}

void FtpWindow::enableDownloadButton()
{
    QTreeWidgetItem *current = fileList->currentItem();

    if (current) {
        QString currentFile = current->text(0);
        downloadButton->setEnabled(!isDirectory.value(currentFile));
        bool ok;
        _currentTotalBytes = (qint64) current->text(1).toLongLong(&ok);

        SYD << tr("...FtpWindow::enableDownloadButton..._currentTotalBytes:|%1|")
                .arg(_currentTotalBytes);

    } else {
        downloadButton->setEnabled(false);
    }
}
//![14]

void FtpWindow::enableUploadButton()
{
    QTreeWidgetItem *current = fileList->currentItem();
    if (current) {
        QString currentFile = current->text(0);
        //uploadButton->setEnabled(!isDirectory.value(currentFile));
        if (!currentFile.isEmpty()) {
                uploadButton->setEnabled(true);
        }
        else {
            uploadButton->setEnabled(false);
        }
    } else {
        uploadButton->setEnabled(false);
    }

}


void FtpWindow::enableConnectButton()
{
#if (QT_VERSION > QT_VERSION_CHECK(4, 6, 3))
    // Save the used configuration
    QNetworkConfiguration config = networkSession->configuration();
    QString id;
    if (config.type() == QNetworkConfiguration::UserChoice)
        id = networkSession->sessionProperty(QLatin1String("UserChoiceConfiguration")).toString();
    else
        id = config.identifier();

    QSettings settings(QSettings::UserScope, QLatin1String("Trolltech"));
    settings.beginGroup(QLatin1String("QtNetwork"));
    settings.setValue(QLatin1String("DefaultNetworkConfiguration"), id);
    settings.endGroup();

    connectButton->setEnabled(networkSession->isOpen());
    statusLabel->setText(tr("Please enter the name of an FTP server."));
#endif
}

