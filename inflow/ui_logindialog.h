/********************************************************************************
** Form generated from reading UI file 'logindialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINDIALOG_H
#define UI_LOGINDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_LoginDialog
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_2;
    QFrame *frame;
    QGridLayout *gridLayout_2;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout;
    QLabel *labelUser;
    QComboBox *comboUsers;
    QLineEdit *linePassword;
    QLabel *validmessageLabel;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout;
    QPushButton *cancelButton;
    QPushButton *okButton;

    void setupUi(QDialog *LoginDialog)
    {
        if (LoginDialog->objectName().isEmpty())
            LoginDialog->setObjectName(QString::fromUtf8("LoginDialog"));
        LoginDialog->resize(379, 182);
        LoginDialog->setContextMenuPolicy(Qt::NoContextMenu);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/logosafet.png"), QSize(), QIcon::Normal, QIcon::Off);
        LoginDialog->setWindowIcon(icon);
        gridLayout = new QGridLayout(LoginDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        frame = new QFrame(LoginDialog);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        frame_2 = new QFrame(frame);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setMaximumSize(QSize(200, 16777215));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);

        gridLayout_2->addWidget(frame_2, 0, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        labelUser = new QLabel(frame);
        labelUser->setObjectName(QString::fromUtf8("labelUser"));
        labelUser->setMaximumSize(QSize(400, 35));
        labelUser->setTextFormat(Qt::RichText);
        labelUser->setAlignment(Qt::AlignCenter);
        labelUser->setWordWrap(false);

        verticalLayout->addWidget(labelUser);

        comboUsers = new QComboBox(frame);
        comboUsers->setObjectName(QString::fromUtf8("comboUsers"));
        comboUsers->setMinimumSize(QSize(220, 0));

        verticalLayout->addWidget(comboUsers);


        gridLayout_2->addLayout(verticalLayout, 0, 1, 1, 1);


        verticalLayout_2->addWidget(frame);

        linePassword = new QLineEdit(LoginDialog);
        linePassword->setObjectName(QString::fromUtf8("linePassword"));
        linePassword->setMinimumSize(QSize(0, 25));
        linePassword->setEchoMode(QLineEdit::Password);

        verticalLayout_2->addWidget(linePassword);


        gridLayout->addLayout(verticalLayout_2, 0, 0, 1, 1);

        validmessageLabel = new QLabel(LoginDialog);
        validmessageLabel->setObjectName(QString::fromUtf8("validmessageLabel"));
        QFont font;
        font.setFamily(QString::fromUtf8("DejaVu Sans"));
        font.setPointSize(8);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        validmessageLabel->setFont(font);
        validmessageLabel->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"border-color: rgb(0, 0, 0);\n"
"gridline-color: rgb(0, 0, 0);\n"
"font: 8pt \"DejaVu Sans\";"));
        validmessageLabel->setTextFormat(Qt::RichText);
        validmessageLabel->setScaledContents(false);
        validmessageLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(validmessageLabel, 1, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(128, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        cancelButton = new QPushButton(LoginDialog);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/del.png"), QSize(), QIcon::Normal, QIcon::Off);
        cancelButton->setIcon(icon1);
        cancelButton->setFlat(false);

        horizontalLayout->addWidget(cancelButton);

        okButton = new QPushButton(LoginDialog);
        okButton->setObjectName(QString::fromUtf8("okButton"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/yes.png"), QSize(), QIcon::Normal, QIcon::Off);
        okButton->setIcon(icon2);
        okButton->setDefault(true);

        horizontalLayout->addWidget(okButton);


        horizontalLayout_2->addLayout(horizontalLayout);


        gridLayout->addLayout(horizontalLayout_2, 2, 0, 1, 1);


        retranslateUi(LoginDialog);

        QMetaObject::connectSlotsByName(LoginDialog);
    } // setupUi

    void retranslateUi(QDialog *LoginDialog)
    {
        LoginDialog->setWindowTitle(QApplication::translate("LoginDialog", "SAFET - Ingreso al sistema", 0, QApplication::UnicodeUTF8));
        labelUser->setText(QApplication::translate("LoginDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Selecciona Usuario/Cuenta</span>:</p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
        cancelButton->setText(QApplication::translate("LoginDialog", "Cancelar", 0, QApplication::UnicodeUTF8));
        okButton->setText(QApplication::translate("LoginDialog", "Aceptar", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class LoginDialog: public Ui_LoginDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINDIALOG_H
