/*
* SAFET Sistema Automatizado para la Firma Electr�nica y Estampado de Tiempo
* Copyright (C) 2008 V�ctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigaci�n en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los t�rminos de la licencia de
* software GPL versi�n 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea �til, pero SI NINGUNA GARANT�A;
* tampoco las impl��citas garant��as de MERCANTILIDAD o ADECUACIÓN A UN PROP�SITO PARTICULAR.
* Consulte la licencia GPL para m�s detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5� Piso, Boston, MA 02110-1301, USA.
*
*/
#include "datetimewidget.h"
#include "SafetYAWL.h"

DateTimeWidget::DateTimeWidget(const QString& t, QWidget *parent,bool istextparent)
    :CmdWidget(t,parent,istextparent) {
     datetimeedit = NULL;
     if ( !conf().contains("format")) {
         SafetYAWL::streamlog
                 << SafetLog::Warning
                 << tr("Falta opci�n de formato (format) para el campo de entrada \"datetime\" llamado \"%1\"")
                 .arg(caption());
         _format = "time_t";
     }
     else {
         _format = conf()["format"].toString();
     }


}

void DateTimeWidget::setText(const QString &newText) {
    if ( datetimeedit ) {
        bool ok;
        uint myseconds = newText.toInt(&ok);
        if (ok ) {
            datetimeedit->setDateTime(QDateTime::fromTime_t(myseconds));
        }
    }

}


QRect DateTimeWidget::getGeoParams() const {
     QRect result;
     result.setHeight( 40 );
     result.setWidth( 230 );
     return result;
}

bool DateTimeWidget::isValid(QString& value) {
    QString strvalid;
    if ( conf().contains("validation"))  {
        strvalid = conf()["validation"].toString().split("::").at(0);
    }
    qDebug(".DateTimeWidget... strvalid:  |%s|", qPrintable(strvalid));
    qDebug(".DateTimeWidget... value:  |%s|", qPrintable(value));
    if (strvalid.isEmpty()) {
        return true;
    }

    qDebug();
    qDebug("**fieldspair.first: |%s|",qPrintable(SafetYAWL::fieldsname));
    qDebug("**fieldspair.second: |%s|",qPrintable(SafetYAWL::fieldsvalue));

    QString localfieldsvalue = SafetYAWL::fieldsvalue;
    if ( localfieldsvalue.endsWith(",")) {
        localfieldsvalue.chop(1);
    }

    QString resultvalue;
    bool result = SafetYAWL::createTableSignDb(SafetYAWL::fieldsname,
                                               localfieldsvalue,
                                               strvalid.arg(value),
                                               resultvalue);
    if (!result) {
        return false;
    }

    qDebug("...resultvalue:%s",qPrintable(resultvalue));
    if ( resultvalue == "1") {
        return true;
    }

    return false;
}

void DateTimeWidget::buildWidget() {
     //CmdWidget::buildWidget();
     qDebug("...TextEditWidget...buildWidget...");
     //mainLayout = new QHBoxLayout;
     datetimeedit = new QDateTimeEdit();
     if ( conf().contains("options"))  {

         principalWidget = datetimeedit;
         setOptionsProperties(conf()["options"].toString().split(",")); // Colocar las propiedades

     }


     datetimeedit->setGeometry(0,0,230,30);

     datetimeedit->setDateTime(QDateTime::currentDateTime());



     datetimeedit->setTime(QTime::fromString("11:59pm","hh:mmap"));
     if (isTextParent()) {
         okbutton = new QToolButton;
         okbutton->setGeometry(0,0,25,30);
         okbutton->setIcon(QIcon(":/yes.png"));
         quitbutton = new QToolButton;
         quitbutton->setGeometry(0,0,25,30);
         quitbutton->setText( "X");
     }
     mainLayout = new QHBoxLayout;
     mainLayout->addWidget(datetimeedit);
     QString mytip = tr("Seleccione la fecha del cuadro\nEscriba Ctrl+L para finalizar");
     if ( conf().contains("validation")) {
         QStringList mylist = conf()["validation"].toString().split("::");
         if (mylist.count() > 1 ) {
             QString usertip = mylist.at(1);
             mytip = usertip;
         }
     }
     _usertooltip = mytip;
     datetimeedit->setToolTip(mytip);
     mainLayout->setSpacing( 1 );
     if (isTextParent()) {
         mainLayout->addWidget(quitbutton);
         mainLayout->addWidget(okbutton);

         connect(okbutton, SIGNAL(clicked()), _texteditparent, SLOT(insertAndClose()) );
         connect(quitbutton, SIGNAL(clicked()), _texteditparent, SLOT(cancelAndClose()) );
     }

     setLayout(mainLayout);
}

void DateTimeWidget::setFocus ( Qt::FocusReason reason ) {
     qDebug("...TextEditWidget::setFocus....(text)");
     QWidget::setFocus ( reason );
     datetimeedit->setFocus( reason);

}



QString DateTimeWidget::text() const {
    QString result;
    if (datetimeedit != NULL )  {        
        QString pattern = "%1";
        if ( _format.compare("time_t",Qt::CaseInsensitive) == 0 ) {
            result = pattern.arg(datetimeedit->dateTime().toTime_t());
        }
        else {
            result = datetimeedit->dateTime().toString(_format);
        }
    }
       return result;
 }
