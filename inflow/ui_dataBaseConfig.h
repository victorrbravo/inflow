/********************************************************************************
** Form generated from reading UI file 'dataBaseConfig.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DATABASECONFIG_H
#define UI_DATABASECONFIG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTabWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DatabaseConfigDialog
{
public:
    QGridLayout *gridLayout_2;
    QTabWidget *tabDatabaseConfig;
    QWidget *tab;
    QGridLayout *gridLayout_3;
    QLabel *label_7;
    QGridLayout *gridLayout;
    QLabel *label_8;
    QComboBox *comboBoxDriver;
    QLabel *label;
    QLineEdit *lineEditName;
    QLabel *label_2;
    QLineEdit *lineEditHost;
    QLabel *label_3;
    QLineEdit *lineEditPort;
    QLabel *label_4;
    QLineEdit *lineEditDatabase;
    QLabel *label_5;
    QLineEdit *lineEditUser;
    QLabel *label_6;
    QLineEdit *lineEditPassword;
    QLabel *label_9;
    QCheckBox *checkBoxDBLocal;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout;
    QPushButton *cancelButton;
    QPushButton *okButton;

    void setupUi(QDialog *DatabaseConfigDialog)
    {
        if (DatabaseConfigDialog->objectName().isEmpty())
            DatabaseConfigDialog->setObjectName(QString::fromUtf8("DatabaseConfigDialog"));
        DatabaseConfigDialog->resize(433, 392);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/logosafet.png"), QSize(), QIcon::Normal, QIcon::Off);
        DatabaseConfigDialog->setWindowIcon(icon);
        gridLayout_2 = new QGridLayout(DatabaseConfigDialog);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        tabDatabaseConfig = new QTabWidget(DatabaseConfigDialog);
        tabDatabaseConfig->setObjectName(QString::fromUtf8("tabDatabaseConfig"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout_3 = new QGridLayout(tab);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_7 = new QLabel(tab);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setPixmap(QPixmap(QString::fromUtf8(":/database-process-icon.png")));

        gridLayout_3->addWidget(label_7, 0, 0, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_8 = new QLabel(tab);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout->addWidget(label_8, 0, 0, 1, 1);

        comboBoxDriver = new QComboBox(tab);
        comboBoxDriver->setObjectName(QString::fromUtf8("comboBoxDriver"));

        gridLayout->addWidget(comboBoxDriver, 0, 1, 1, 2);

        label = new QLabel(tab);
        label->setObjectName(QString::fromUtf8("label"));
        label->setTextFormat(Qt::RichText);

        gridLayout->addWidget(label, 1, 0, 1, 1);

        lineEditName = new QLineEdit(tab);
        lineEditName->setObjectName(QString::fromUtf8("lineEditName"));

        gridLayout->addWidget(lineEditName, 1, 1, 1, 2);

        label_2 = new QLabel(tab);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setTextFormat(Qt::RichText);

        gridLayout->addWidget(label_2, 2, 0, 1, 1);

        lineEditHost = new QLineEdit(tab);
        lineEditHost->setObjectName(QString::fromUtf8("lineEditHost"));

        gridLayout->addWidget(lineEditHost, 2, 1, 1, 2);

        label_3 = new QLabel(tab);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setTextFormat(Qt::RichText);

        gridLayout->addWidget(label_3, 3, 0, 1, 1);

        lineEditPort = new QLineEdit(tab);
        lineEditPort->setObjectName(QString::fromUtf8("lineEditPort"));

        gridLayout->addWidget(lineEditPort, 3, 1, 1, 2);

        label_4 = new QLabel(tab);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setTextFormat(Qt::RichText);

        gridLayout->addWidget(label_4, 4, 0, 1, 1);

        lineEditDatabase = new QLineEdit(tab);
        lineEditDatabase->setObjectName(QString::fromUtf8("lineEditDatabase"));

        gridLayout->addWidget(lineEditDatabase, 4, 1, 1, 2);

        label_5 = new QLabel(tab);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setTextFormat(Qt::RichText);

        gridLayout->addWidget(label_5, 5, 0, 1, 1);

        lineEditUser = new QLineEdit(tab);
        lineEditUser->setObjectName(QString::fromUtf8("lineEditUser"));

        gridLayout->addWidget(lineEditUser, 5, 1, 1, 2);

        label_6 = new QLabel(tab);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setTextFormat(Qt::RichText);

        gridLayout->addWidget(label_6, 6, 0, 1, 1);

        lineEditPassword = new QLineEdit(tab);
        lineEditPassword->setObjectName(QString::fromUtf8("lineEditPassword"));
        lineEditPassword->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(lineEditPassword, 6, 1, 1, 2);

        label_9 = new QLabel(tab);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setTextFormat(Qt::RichText);

        gridLayout->addWidget(label_9, 7, 0, 1, 2);

        checkBoxDBLocal = new QCheckBox(tab);
        checkBoxDBLocal->setObjectName(QString::fromUtf8("checkBoxDBLocal"));

        gridLayout->addWidget(checkBoxDBLocal, 7, 2, 1, 1);


        gridLayout_3->addLayout(gridLayout, 0, 1, 1, 1);

        tabDatabaseConfig->addTab(tab, QString());

        gridLayout_2->addWidget(tabDatabaseConfig, 0, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(128, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        cancelButton = new QPushButton(DatabaseConfigDialog);
        cancelButton->setObjectName(QString::fromUtf8("cancelButton"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/del.png"), QSize(), QIcon::Normal, QIcon::Off);
        cancelButton->setIcon(icon1);
        cancelButton->setFlat(false);

        horizontalLayout->addWidget(cancelButton);

        okButton = new QPushButton(DatabaseConfigDialog);
        okButton->setObjectName(QString::fromUtf8("okButton"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/yes.png"), QSize(), QIcon::Normal, QIcon::Off);
        okButton->setIcon(icon2);
        okButton->setDefault(true);

        horizontalLayout->addWidget(okButton);


        horizontalLayout_2->addLayout(horizontalLayout);


        gridLayout_2->addLayout(horizontalLayout_2, 1, 0, 1, 1);


        retranslateUi(DatabaseConfigDialog);

        tabDatabaseConfig->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(DatabaseConfigDialog);
    } // setupUi

    void retranslateUi(QDialog *DatabaseConfigDialog)
    {
        DatabaseConfigDialog->setWindowTitle(QApplication::translate("DatabaseConfigDialog", "SAFET - Fuente de datos", 0, QApplication::UnicodeUTF8));
        label_7->setText(QString());
        label_8->setText(QApplication::translate("DatabaseConfigDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p align=\"left\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Controlador</span>:</p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        comboBoxDriver->setToolTip(QApplication::translate("DatabaseConfigDialog", "Controlador para la fuente de datos", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label->setText(QApplication::translate("DatabaseConfigDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p align=\"left\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Nombre</span>:</p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        lineEditName->setToolTip(QApplication::translate("DatabaseConfigDialog", "Identificador para la fuente de datos", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_2->setText(QApplication::translate("DatabaseConfigDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p align=\"left\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Host</span>:</p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        lineEditHost->setToolTip(QApplication::translate("DatabaseConfigDialog", "Nombre de host o direccion IP del servidor", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_3->setText(QApplication::translate("DatabaseConfigDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p align=\"left\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Puerto</span>:</p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        lineEditPort->setToolTip(QApplication::translate("DatabaseConfigDialog", "N\303\272mero de puerto para realizar la conexi\303\263n", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_4->setText(QApplication::translate("DatabaseConfigDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p align=\"left\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Base de datos</span>:</p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        lineEditDatabase->setToolTip(QApplication::translate("DatabaseConfigDialog", "Nombre de la base de datos", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_5->setText(QApplication::translate("DatabaseConfigDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p align=\"left\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Usuario</span>:</p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        lineEditUser->setToolTip(QApplication::translate("DatabaseConfigDialog", "Usuario con privilegios para realizar la conexi\303\263n", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_6->setText(QApplication::translate("DatabaseConfigDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p align=\"left\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Contrase\303\261a</span>:</p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        lineEditPassword->setToolTip(QApplication::translate("DatabaseConfigDialog", "Contrase\303\261a del usuario", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        label_9->setText(QApplication::translate("DatabaseConfigDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'DejaVu Sans';\"><span style=\" font-weight:600;\">Crear base de datos local para pruebas</span>:</p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
        checkBoxDBLocal->setText(QString());
        tabDatabaseConfig->setTabText(tabDatabaseConfig->indexOf(tab), QApplication::translate("DatabaseConfigDialog", "Configuraci\303\263n", 0, QApplication::UnicodeUTF8));
        cancelButton->setText(QApplication::translate("DatabaseConfigDialog", "Cancelar", 0, QApplication::UnicodeUTF8));
        okButton->setText(QApplication::translate("DatabaseConfigDialog", "Aceptar", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class DatabaseConfigDialog: public Ui_DatabaseConfigDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DATABASECONFIG_H
