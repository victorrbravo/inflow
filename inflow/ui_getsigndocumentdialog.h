/********************************************************************************
** Form generated from reading UI file 'getsigndocumentdialog.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GETSIGNDOCUMENTDIALOG_H
#define UI_GETSIGNDOCUMENTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QToolButton>

QT_BEGIN_NAMESPACE

class Ui_GetSignDocumentDIalog
{
public:
    QGridLayout *gridLayout_2;
    QFrame *frame;
    QGridLayout *gridLayout;
    QLabel *label;
    QListWidget *listFiles;
    QHBoxLayout *horizontalLayout;
    QToolButton *selectButton;
    QToolButton *delButton;
    QSpacerItem *horizontalSpacer;
    QPushButton *toGetButton;
    QLabel *labelDididocInfo;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *closeButton;

    void setupUi(QDialog *GetSignDocumentDIalog)
    {
        if (GetSignDocumentDIalog->objectName().isEmpty())
            GetSignDocumentDIalog->setObjectName(QString::fromUtf8("GetSignDocumentDIalog"));
        GetSignDocumentDIalog->resize(425, 383);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/add.png"), QSize(), QIcon::Normal, QIcon::Off);
        GetSignDocumentDIalog->setWindowIcon(icon);
        gridLayout_2 = new QGridLayout(GetSignDocumentDIalog);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        frame = new QFrame(GetSignDocumentDIalog);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        listFiles = new QListWidget(frame);
        listFiles->setObjectName(QString::fromUtf8("listFiles"));

        gridLayout->addWidget(listFiles, 1, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        selectButton = new QToolButton(frame);
        selectButton->setObjectName(QString::fromUtf8("selectButton"));
        selectButton->setIcon(icon);

        horizontalLayout->addWidget(selectButton);

        delButton = new QToolButton(frame);
        delButton->setObjectName(QString::fromUtf8("delButton"));
        delButton->setEnabled(false);
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/del.png"), QSize(), QIcon::Normal, QIcon::On);
        delButton->setIcon(icon1);

        horizontalLayout->addWidget(delButton);

        horizontalSpacer = new QSpacerItem(68, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        toGetButton = new QPushButton(frame);
        toGetButton->setObjectName(QString::fromUtf8("toGetButton"));
        toGetButton->setEnabled(false);
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/yes.png"), QSize(), QIcon::Normal, QIcon::Off);
        toGetButton->setIcon(icon2);

        horizontalLayout->addWidget(toGetButton);


        gridLayout->addLayout(horizontalLayout, 2, 0, 1, 1);

        labelDididocInfo = new QLabel(frame);
        labelDididocInfo->setObjectName(QString::fromUtf8("labelDididocInfo"));
        labelDididocInfo->setFrameShape(QFrame::Box);
        labelDididocInfo->setFrameShadow(QFrame::Raised);
        labelDididocInfo->setLineWidth(1);
        labelDididocInfo->setTextFormat(Qt::RichText);
        labelDididocInfo->setWordWrap(true);

        gridLayout->addWidget(labelDididocInfo, 3, 0, 1, 1);


        gridLayout_2->addWidget(frame, 0, 0, 1, 2);

        horizontalSpacer_2 = new QSpacerItem(294, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 1, 0, 1, 1);

        closeButton = new QPushButton(GetSignDocumentDIalog);
        closeButton->setObjectName(QString::fromUtf8("closeButton"));
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/del.png"), QSize(), QIcon::Normal, QIcon::Off);
        closeButton->setIcon(icon3);

        gridLayout_2->addWidget(closeButton, 1, 1, 1, 1);


        retranslateUi(GetSignDocumentDIalog);
        QObject::connect(closeButton, SIGNAL(clicked()), GetSignDocumentDIalog, SLOT(close()));

        QMetaObject::connectSlotsByName(GetSignDocumentDIalog);
    } // setupUi

    void retranslateUi(QDialog *GetSignDocumentDIalog)
    {
        GetSignDocumentDIalog->setWindowTitle(QApplication::translate("GetSignDocumentDIalog", "Incluir Archivos XADES (digidoc)", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("GetSignDocumentDIalog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Archivos <span style=\" color:#00007f;\">firmados electr\303\263nicamente</span> (<span style=\" font-weight:600;\">ddoc</span>) a incluir</p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
        selectButton->setText(QApplication::translate("GetSignDocumentDIalog", "...", 0, QApplication::UnicodeUTF8));
        delButton->setText(QApplication::translate("GetSignDocumentDIalog", "...", 0, QApplication::UnicodeUTF8));
        toGetButton->setText(QApplication::translate("GetSignDocumentDIalog", "Incluir", 0, QApplication::UnicodeUTF8));
        labelDididocInfo->setText(QString());
        closeButton->setText(QApplication::translate("GetSignDocumentDIalog", "&Cerrar", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class GetSignDocumentDIalog: public Ui_GetSignDocumentDIalog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GETSIGNDOCUMENTDIALOG_H
