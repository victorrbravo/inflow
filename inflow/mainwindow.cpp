/****************************************************************************
**
** Copyright (C) 2006-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.trolltech.com/products/qt/opensource.html
**
** If you are unsure which license is appropriate for your use, please
** review the following information:
** http://www.trolltech.com/products/qt/licensing.html or contact the
** sales department at sales@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
/*
* SAFET Sistema Automatizado para la Firma Electr?nica y Estampado de Tiempo
* Copyright (C) 2008 V?ctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigaci?n en Tecnolog�as Libres
*
* Este programa es software libre; Usted puede usarlo bajo los t?rminos de la licencia de
* software GPL versi?n 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea ?til, pero SI NINGUNA GARANT?A;
* tampoco las impl??citas garant??as de MERCANTILIDAD o ADECUACI�N A UN PROP?SITO PARTICULAR.
* Consulte la licencia GPL para m?s detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5? Piso, Boston, MA 02110-1301, USA.
*
*/
#include <QtGui>
#include <QtXml>
#include <QCloseEvent>
#include <QGraphicsSvgItem>
#include <QToolBar>
#include <QLatin1String>
#include <QWebFrame>
#include <QDesktopServices>

#include <getopt.h>
#include "mainwindow.h"
#include "textedit.h"
#include "dommodel.h"
#include "qcmdcompleter.h"
#include "SafetTextParser.h"
#include "optionsdialog.h"
#include "configdialog.h"
#include "docksbmenu.h"
#include "SignatureDialog.h"
#include "ui/getsigndocumentdialog.h"
#include "ui/sendsigndocumentdialog.h"
#include "assistant.h"
#include "logindialog.h"
#include "threadconsole.h"
#ifdef SAFET_OPENSSL
#include <openssl/rand.h>
#endif
#include "SafetCipherFile.h"
#include "databaseconfigdialog.h"
#include "dialogflowparameters.h"
#include "ftpwindow.h"
#include "SafetBinaryRepo.h"
// Variables Est�ticas

SafetYAWL* MainWindow::configurator = NULL;
QStatusBar* MainWindow::mystatusbar = NULL;
DockSbMenu *MainWindow::docksbMenu = NULL;
DockSbMenu *MainWindow::dockShowResult = NULL;
QStringList  MainWindow::sbmenulist;
MainWindow *MainWindow::mymainwindow = NULL;
QString MainWindow::currentaccount = "";
QString MainWindow::currentrole = "";
QString MainWindow::currentrealname = "";
QString MainWindow::currentgraphpath = "";
QString MainWindow::showString = "";
bool MainWindow::_issigning = false;
QString MainWindow::_currentCommonName = "";

//ThreadConsole MainWindow::myThreadConsole(NULL);



QMap<QString,QStringList> MainWindow::permises; // Estructura para guardar los permisos

int MainWindow::gviewoutputwidth = 800;
int MainWindow::gviewoutputheight = 600;
bool MainWindow::_isrestarting = false;
QTimer MainWindow::_panelinfotimer;
int MainWindow::_seconstohide = 0;

//ThreadConsole * MainWindow::myThreadConsole = NULL;


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), completer(0), dommodel( 0 ), streamText( &outputText ),
    stackWidget(new QStackedWidget(this) ), centralWidget(new QTabWidget(this) ),
    principalFrame( new PrincipalFrame(this) ), centralBar( 0 ),
    editMenu(0),fileMenu(0), toolsMenu( 0 ),
    actionCut(0), actionCopy(0), actionPaste(0),actionUndo(0),actionRedo(0),actionDelfield(0),
    actionModifyfield(0), exitAction(0),
    saveGraph(0),restoreGraph(0), compareGraphs(0),
      _isgui(true)

{

    ftp = NULL;
    if ( ftp == NULL ) {
        ftp = new QFtp(this);
        connect(ftp, SIGNAL(commandFinished(int, bool)),
                     this, SLOT(ftpCommandFinished(int, bool)));

        qDebug("...MainWindow::doSaveConfiguration...connect");
    }


    MainWindow::_isrestarting = false;
     //** Inicializacion de variables    
    //** ...


     _isloadeditactions = false;  // No se han cargado las acciones
     SafetYAWL::listDynWidget = NULL;
     myoutput = NULL;
     listviewoutput  = NULL;
     gviewoutput = NULL;
     listviewoutputmodel = NULL;
     completingTextEdit  = NULL;
     completingTextEditForm = NULL;
     completingTextEditCons = NULL;
     listEditCons = NULL;
     buttonListEditConsAdd = NULL;
     buttonListEditConsEdit = NULL;
     buttonListEditConsDel = NULL;
     buttonListEditConsSave = NULL;
     completingTextEditSign = NULL;
     completingTextEditConf = NULL;
     completingTextEditUsers = NULL;
     menusbbutton = NULL;
     changeUserButton = NULL;
     titleApplication = NULL;
     centralBar = new QTabBar(centralWidget);
     parsed = false;
     standardbar = NULL;
     MainWindow::mymainwindow = this; // Colocando direcci?n de MainWindow
     _listprincipalcount = 0; // Contador para la variable actual

     _checkgoprincipal = false;
     _issmartmenuvisible = false;
    // ** Colocar los manejadores para Los servicioes del Escritorio (QDesktopServices)


     // ** Configurando el Menu de la pantalla principal
     createMenu();
     // ** Configurando el Menu de la pantalla principal


     QDesktopServices::setUrlHandler( "browse", this, "browse" );
     QDesktopServices::setUrlHandler( "mailto", this, "mailTo" );

          // Colocar estilo principal
     setStyleSheet(getPrincipalCSS());


        _currconfpath = QDir::homePath();

          SafetYAWL::pathconf = _currconfpath + "/" + Safet::datadir;

          MainWindow::configurator = new SafetYAWL(_currconfpath + "/" + Safet::datadir);

        SafetYAWL::loadAllConfFiles(3 /* AuthConf */ );


     _dirInput = SafetYAWL::getConf()["Input/path"];
     if ( !_dirInput.endsWith("/") ) _dirInput.append("/");

     _pathdoc = SafetYAWL::getConf()["Input/file"];



     qDebug("MainWindow::MainWindow............(5)...");

     setupTabWidget();

     filecompleters.append( _dirInput+_pathdoc);

     filecompleters.append( _dirInput + SAFETCONSOLEFILE );
     filecompleters.append( _dirInput + SAFETCONFIGFILE );
     filecompleters.append( _dirInput + SAFETMANAGEMENTSIGNFILE );
     filecompleters.append( _dirInput + SAFETUSERSFILE );


     setModelCompleter(0);

     qDebug("MainWindow::MainWindow............(*6)...");
     stackWidget->addWidget(principalFrame);
     stackWidget->addWidget(centralWidget);
     centralWidget->setTabShape( QTabWidget::Rounded );
     setCentralWidget(stackWidget);
     qDebug("MainWindow::MainWindow............(*6.1)...");
     createDockWindow();
     createDockShowResultWindow();

   qDebug("MainWindow::MainWindow............(*6.2)...");

     setWindowTitle(tr("SAFET - InFlow/%1-%2")
                    .arg(SafetYAWL::getNumberVersion())
                    .arg(SafetYAWL::getRevisionVersion()) );
     highlighterForm = new Highlighter(completingTextEditForm->document());
     highlighterCons = new Highlighter(completingTextEditCons->document());
     highlighterConf = new Highlighter(completingTextEditConf->document());
     highlighterSign = new Highlighter(completingTextEditSign->document());
     highlighterUsers = new Highlighter(completingTextEditUsers->document());

     qDebug("MainWindow::MainWindow............(*6.3)...");

     setupToolbar();
     setEnabledToolBar(false);



     MainWindow::mystatusbar = statusBar();
     MainWindow::mystatusbar->showMessage(tr("Iniciado correctamente"));
     SafetYAWL::setEvalExit( evalEventOnExit );// colocar antes del parse
     SafetYAWL::setEvalInput( evalEventOnInput );// colocar antes del parse
     MainWindow::mystatusbar->showMessage(tr("Directorio de archivos: %1").arg(_dirInput));
     loadSettings();



     qDebug("MainWindow::MainWindow............(*6.4)...");
     // ********* Cargar Widgets Plugins
     int countplug = 0;
 //    SafetYAWL::fileout.close();
     SafetYAWL::filelog.close();
     QVariant myvar = SafetYAWL::getConfFile().getValue("Log", "log.filepath");
     foreach( QString s, plugs() ) {
          if ( loadWidgetPlugins( s  ) ) {
              qDebug("...loadWidgetPlugins: %s",qPrintable(s));
              countplug++;
          }
     }

    qDebug("MainWindow::MainWindow............(*6.5)...");

   //  SafetYAWL::fileout.open(stdout, QIODevice::WriteOnly);
     //SafetYAWL::streamout.setDevice(&SafetYAWL::fileout);



     qDebug("MainWindow::MainWindow............(7)...");
     SafetYAWL::filelog.setFileName(myvar.toString() + /*"/safet.log"*/ SafetYAWL::FILENAMELOG);
     qDebug("..asignado archivo log: %s",
            qPrintable(myvar.toString() + /*"/safet.log"*/ SafetYAWL::FILENAMELOG));
     SafetYAWL::filelog.open(QIODevice::Append);
     SafetYAWL::streamlog.setDevice(&SafetYAWL::filelog);

     SafetYAWL::streamlog.turnOnAll();

    qDebug("MainWindow::MainWindow............(8)...");


     _itemredraw = NULL;
     runningConsole = false;

     QApplication::setApplicationVersion(SafetYAWL::getNumberVersion()); // Version

     jscriptload = false; // No cargar el script

     connect(weboutput->page(), SIGNAL(loadFinished (bool)), this, SLOT(executeJSCodeAfterLoad(bool)) );

     // Conectando el timer del panel de resultados
     connect(&_panelinfotimer, SIGNAL(timeout()), this, SLOT(timeHideResult()));


    restoreWindowState();

    initApp = false;

    // *** Conexiones
    connect(principalFrame->getToInputButton(), SIGNAL(clicked()), SLOT(setToInputForm()) );
    connect(principalFrame->getToConsoleButton(), SIGNAL(clicked()), SLOT(setToInputConsole()) );
    connect(principalFrame->getToSignButton(), SIGNAL(clicked()), SLOT(setToInputManagementSignDocument()) );
    connect(principalFrame->getToExitButton(), SIGNAL(clicked()), SLOT(doExit()) );
    connect(principalFrame->getToHelpButton(), SIGNAL(clicked()), SLOT(doAssistantHelp()) );
    setWindowIcon(QIcon(":/logosafet.png"));


    // para mostrar el assistant
    assistant = new Assistant();
    if (assistant == NULL) {
        SafetYAWL::streamlog
                << SafetLog::Warning
                << tr("El asistente para la ayuda (help) no se inici� por falta de Memoria");

    }

    // Colocar la Configuracion Local
     QLocale::setDefault(QLocale::C);


    // Configurar lista de mensajes de Digidoc
    SafetDocument::configureDigidocErrorString();


    MainWindow::mystatusbar->clearMessage();

    // indicador de progreso en la barra de estado
    progressIndicator = new QProgressIndicator();
    progressIndicator->setAnimationDelay(70);

    MainWindow::mystatusbar->clearMessage();

    iconWidget = new IconWidget();

// ------------------------
    // verificar la configuracion de la fuente de datos
    // se coloca luego de new SafetYAWL() para que los archivos de
    // configuracion ya esten leidos
    QString authconfbin = QDir::homePath()+"/.safet/auth.conf.bin";
    if (!QFile::exists(authconfbin)) // no existe el auth.conf.bin asi que se hace todo el proceso
    {
    DatabaseConfigDialog * dbConfig = new DatabaseConfigDialog(this);
    //DatabaseConfigDialog * dbConfig = new DatabaseConfigDialog();
     if (!dbConfig->testDataSources()){

         if (dbConfig->getConfigFieldEmpty()){
            // mostrar el dialogo de fuente de datos
            //manageDataSources();
            manageDataSources(dbConfig);
            //qDebug("...despues de manageDataSources");

            if (dbConfig->getCancelFlag()){
                dbConfig->setInitialStage(true);
                QMessageBox::information(this,
                                         "Fuente de datos",
                                         "No se establecieron los valores de conexion a la fuente de datos.\nDebe establecer estos valores para que esten disponibles todas\nlas funcionalidades de la aplicacion.\n\nUtilice el menu: Herramientas->Fuente de datos.");
            }
            else{
                //qDebug("getCancelFlag == false  se presiono aceptar");
            }
         }
         else{
            dbConfig->setInitialStage(true);
         }
     }

     // +++
     if (dbConfig->getInitialStage()){

         // deshabilitar el boton de enviar consulta hasta que se configure la fuente de datos
         completingButtonForm->setEnabled(false);
         completingButtonCons->setEnabled(false);
         //completingButtonSign->setEnabled(false);
         completingButtonConf->setEnabled(false);
         completingButtonUsers->setEnabled(false);
         // deshabilitar el boton de enviar de la barra de herramientas
         QAction * action = standardbar->actionAt(76,66);
         if (action == 0){
             qDebug("action == 0");
         }
         else
         {
             qDebug("action != 0");
             action->setEnabled(false);
         }
     }


    }
//  -----------------------



    if ( countplug > 0 ) {
        SafetYAWL::pathconf = QDir::homePath() + "/" + Safet::datadir;
        SafetYAWL::loadAllConfFiles(3 /* AuthConf */ );
    }

    if( MainWindow::configurator ) {
        MainWindow::configurator->openDataSources();
    }
    configureStatusBar();



}

void MainWindow::loadReportTemplate() {


    if ( weboutput != NULL ) {
        QObject::disconnect(weboutput, 0, this, 0);
        delete weboutput;
        weboutput = new QWebView(this);
        Q_CHECK_PTR( weboutput );

       // centralWidget->removeTab(3);
        int index = centralWidget->insertTab(3,weboutput, QIcon(":/web.png"), tr("Reportes")); // 3
        qDebug("...loadReportTemplate...index: %d", index);
        //centralWidget->setT
        connect(weboutput->page(), SIGNAL(loadFinished (bool)), this, SLOT(executeJSCodeAfterLoad(bool)) );
    }
    QString nameProtocol = SafetYAWL::getConf()["Reports/protocol"]+ "://";
    QString namePath = SafetYAWL::getConf()["Reports/path"];
    QString namePage = SafetYAWL::getConf()["Reports/general.template"];
    namePage = namePage.section("/",-1);
    qDebug("...namePage: |%s|",qPrintable(namePage));

    QString fullUrl = nameProtocol + namePath + "/" + namePage;
    QUrl url(fullUrl);
    qDebug("***...fullUrl: |%s|",qPrintable(fullUrl));
    QWebView *myweboutput = (qobject_cast<QWebView*>(centralWidget->widget(3)));
    if  (myweboutput) {
        myweboutput->load(url);

        SYA
                << tr("Cargando plantilla de reporte en la ubicaci�n: \"%1\" ").arg(fullUrl);
    }

}


MainWindow::~MainWindow() {

    if (SafetYAWL::listPlugins != NULL ) {
        for(int i=0; i < SafetYAWL::listPlugins->count();i++) {
            QPluginLoader* myplug = SafetYAWL::listPlugins->at(i);
            if (myplug) {

                QObject* myobject = myplug->instance();
                if (myobject) {
                    delete myobject;
                }
                myplug->unload();
            }
        }
        SafetYAWL::listPlugins->clear();
        SafetYAWL::listDynWidget->clear();
        delete SafetYAWL::listPlugins;
        delete SafetYAWL::listDynWidget;
        SafetYAWL::listPlugins = NULL;
        SafetYAWL::listDynWidget = NULL;

    }

}


void MainWindow::setToInputManagementSignDocument() {
    stackWidget->setCurrentIndex( 1 );
    QTabWidget* mytabwidget = qobject_cast<QTabWidget*>(stackWidget->widget(1));
    if ( mytabwidget == NULL ) return;
    centralWidget->setCurrentIndex(2);
    selInputTab(2);

    //showSmartMenuWidget(DockSbMenu::Show);
}

void MainWindow::setToInputReports() {
    stackWidget->setCurrentIndex( 1 );
      showSmartMenuWidget(DockSbMenu::Hide);
    QTabWidget* mytabwidget = qobject_cast<QTabWidget*>(stackWidget->widget(1));
    if ( mytabwidget == NULL ) return;
    centralWidget->setCurrentIndex(3);
    selInputTab(3);


}



void MainWindow::setToInputFlowGraph() {
    showSmartMenuWidget(DockSbMenu::Hide);
    stackWidget->setCurrentIndex( 1 );

    QTabWidget* mytabwidget = qobject_cast<QTabWidget*>(stackWidget->widget(1));

    if ( mytabwidget == NULL ) return;
    centralWidget->setCurrentIndex(4);
    selInputTab(4);



}


QString MainWindow::getPrincipalCSS() const {
    return QString::fromUtf8(
    "* {\n"
    "color: #355670;\n"
    "font: 12px \"Arial, Liberation Sans\";\n"
    "}\n"
    "\n"
    "QTabWidget {"
    "    background-image: url(:/background.png);} \n"
    "#background\n"
    "{ background-image: url(\":/background.png\"); }\n"
    "\n"
    "/* widgets */\n"
    "QPushButton\n"
    "{\n"
    "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #406A9E, stop: 0.75 #00355F);\n"
    "border: 0px;\n"
    "border-radius: 2px;\n"
    "color: white;\n"
    "margin-left: 1px;\n"
    "margin-right: 1px;\n"
    "min-width: 75px;\n"
    "padding: 1px 7px;\n"
    "}\n"
    "\n"
    "QDialogButtonBox QPushButton, #home QPushButton\n"
    "{ padding: 4px 7px; }\n"
    "\n"
    "QPushButton:hover, QPushButton:focus\n"
    "{\n"
    "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F1C157, stop:0.28 #FADF91, stop:1 #EBA927);\n"
    "color: #00355F;\n"
    "}\n"
    "\n"
    "QPushButton:disabled\n"
    "{ background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #A3AFC0, stop: 0.75 #80909D); }\n"
    "\n"
    "/* header */\n"
    "#headerLine, #headerLine2\n"
    "{ background-color: #00355F; }\n"
    "\n"
    "#cards, #languages\n"
    "{\n"
    "background-color: #00355F;"
            "\n"
    "border: 0px;\n"
    "margin-right: 0px;\n"
    "color: white;\n"
    "}\n"
    "\n"
    "#cards QListView, #languages QListView\n"
    "{\n"
    "background-color: white;\n"
    "border: 0px;\n"
    "color: #355670;\n"
    "}\n"
    "\n"
    "#cards::down-arrow, #languages::down-arrow\n"
    "{ image: url(\":/images/languages_button.png\"); }\n"
    "\n"
    "#cards::drop-down, #languages::drop-down\n"
    "{ border: 0px; }\n"
    "\n"
    "#settings, #help\n"
    "{\n"
    "background-color: #00355F;\n"
    "color: #afc6d1;\n"
    "font: bold;\n"
    "min-width: 0px;\n"
    "}\n"
    "\n"
    "#settings:hover, #settings:focus\n"
    "#help:hover, #help:focus\n"
    "{ text-decoration: underline; }\n"
    "\n"
    "#infoFrame\n"
    "{\n"
    "background-color: rgba( 255, 255, 255, 153 );\n"
    "border-radius: 3px;\n"
    "color: #668696;\n"
    "padding-left: 10px;\n"
    "padding-right: 10px;\n"
    "}\n"
    "\n"
    "#infoCard\n"
    "{ font: bold; }\n"
    "\n"
    "/* content */\n"
    "#homeButtonLabel,\n"
    "#viewContentLabel, #viewKeysLabel\n"
    "{\n"
    "color: #00355F;\n"
    "font: bold;\n"
    "}\n"
    "\n"
    "#home, #intro, #view,\n"
    "#viewContentView\n"
    "{ background-color: tra"
            "nsparent; }\n"
    "\n"
    "#viewContentFrame\n"
    "{\n"
    "background-color: rgba( 255, 255, 255, 200 );\n"
    "border: 1px solid gray;\n"
    "color: #355670;\n"
    "padding: 5px;\n"
    "}\n"
    "\n"
    "#viewKeysFrame\n"
    "{\n"
    "background-color: rgba( 255, 255, 255, 200 );\n"
    "border: 1px solid gray;\n"
    "padding: 5px;\n"
    "}\n"
    "\n"
    "#viewKeys, #viewKeysScroll\n"
    "{\n"
    "background-color: transparent;\n"
    "color: #71889A;\n"
    "}");
}

void MainWindow::setToInputForm() {
    stackWidget->setCurrentIndex( 1 );
    QTabWidget* mytabwidget = qobject_cast<QTabWidget*>(stackWidget->widget(1));

    if ( mytabwidget == NULL ) return;
    centralWidget->setCurrentIndex(0);
    selInputTab(0);

    //showSmartMenuWidget(DockSbMenu::Show);
    //showShowResultWidget(DockSbMenu::Hide);

}


void MainWindow::goPrincipal() {

    stackWidget->setCurrentIndex( 0 );
    setEnabledToolBar(false);
    showSmartMenuWidget(DockSbMenu::Hide);
    showShowResultWidget(DockSbMenu::Hide);
    qDebug("centralWidget->count():%d",centralWidget->count());

}

void MainWindow::setToInputConsole() {
    stackWidget->setCurrentIndex( 1 );

    QTabWidget* mytabwidget = qobject_cast<QTabWidget*>(stackWidget->widget(1));

    if ( mytabwidget == NULL ) return;
    centralWidget->setCurrentIndex(1);


    selInputTab(1);

    //showSmartMenuWidget(DockSbMenu::Show);


}


void MainWindow::linkClickedSbMenu(const QUrl& url) {
    QStringList mylist;
    mylist = url.toString().split(Safet::OPSEPARATOR, QString::SkipEmptyParts);
    if (mylist.count() < 2) {
        SYE << tr("Hubo un error al procesar el enlace de la operaci�n \"%1\"")
               .arg(url.toString());
        return;
   }

    QString action = mylist.at(1);
    qDebug("..........MainWindow::linkClickedSbMenu....action:|%s|",qPrintable(action));

    Q_ASSERT(!action.isEmpty());
    completingTextEdit->execSbMenuAction(action);
}

void MainWindow::linkClickedSbResult(const QUrl& url) {

    qDebug("....MainWindow::linkClickedSbResult..........url:|%s|",qPrintable(url.toString()));
    QStringList mylist = url.toString().split(Safet::OPSEPARATOR, QString::SkipEmptyParts);
    if (mylist.count() < 2 ) {
        SYE << tr("Hubo un error al procesar el enlace \"%1\"")
               .arg(url.toString());
        return;
    }

    QString action = mylist.at(1);
    //QString action = url.toString().split(SafetYAWL::LISTSEPARATORCHARACTER, QString::SkipEmptyParts).at(1);
    qDebug(qPrintable(action));
    Q_ASSERT(!action.isEmpty());


    if ( action.startsWith(tr("Ver reporte") )  )  {


            setToInputReports();
            return;

    }
    else if (action.startsWith(tr("Ver gr�fico de flujo de trabajo") ) ) {

            setToInputFlowGraph();

    }
    else if ( action.startsWith(tr("Ver firmas del documento"))) {
        qDebug("*-*-*-*-*-*-*-*Ver firmas del documento*-*-*-*-*");
        // TODO: esto se debe cambiar para eliminar la dependencia de digidocpp
        /*
        QList<DigiDocSignature> signatures = digiDocument->signatures();
        int i = 0;
        if ( signatures.count() > 0 ) {
            SafetSignatureDialog( signatures.at(0), this ).exec();
        }
        */
    }
    else if (action == tr("Mostrar carpeta del contenedor")) {
        //if ( digiDocument == NULL ) return;
        //QUrl url = QUrl::fromLocalFile( digiDocument->fileName() );
        QUrl url = QUrl::fromLocalFile( getPathOfSafetDocument() );
        url.setScheme( "browse" );
        QDesktopServices::openUrl( url );
    }
    else if (action == tr("Enviar contenedor por correo-e")) {
        //if ( digiDocument == NULL ) return;
        QUrl url;
        url.setScheme( "mailto" );
        //url.addQueryItem( "subject", QFileInfo( digiDocument->fileName() ).fileName() );
        url.addQueryItem( "subject", QFileInfo( getPathOfSafetDocument() ).fileName() );
        //url.addQueryItem( "attachment", digiDocument->fileName() );
        url.addQueryItem( "attachment", getPathOfSafetDocument() );
        QDesktopServices::openUrl( url );
    }


}

void MainWindow::createDockWindow() {
    MainWindow::docksbMenu = new DockSbMenu(tr("Acciones"), this);
    if (MainWindow::docksbMenu == NULL ) {
        return;
    }
    buildMenuOnPanelsbMenu("form");


    MainWindow::docksbMenu->setFeatures(QDockWidget::DockWidgetFloatable);

    addDockWidget(Qt::NoDockWidgetArea,docksbMenu);


}
void MainWindow::createDockShowResultWindow() {
    MainWindow::dockShowResult = new DockSbMenu(tr("Resultado"), this);
    dockShowResult->changeMask(DockSbMenu::Results);
    addDockWidget(Qt::NoDockWidgetArea,dockShowResult);

 }




void MainWindow::buildMenuOnPanelsbMenu(const QString& option) {
    if (dommodel ) {
        dommodel->getCommands();
        MainWindow::docksbMenu->addOptions(completingTextEdit,false);
    }
}






void MainWindow::loadSettings() {
     int count;
     bool ok;
     QMap<QString,QVariant> conf;
     QSettings settings("CENDITEL","Safet");
     settings.beginGroup("Inflow");
     QVariant vcount(count);
     QVariant vconf(conf);
     QVariant myvar(_plugs);
     _plugs = settings.value("plugs", myvar).toStringList();
     count = settings.value("confscount", vcount).toInt(&ok);
     if ( ok == false ) {
         count = 0;
     }

     Q_CHECK_PTR(completingTextEdit);
     completingTextEdit->listConfs().clear();
     for ( int i = 0; i < count; i++) {
          QString valuename = QString("confscount%1").arg(i);
          conf = settings.value(valuename, conf).toMap();
          completingTextEdit->listConfs().append( conf );
     }

     settings.endGroup();

}

void MainWindow::writeSettings() {

     Q_CHECK_PTR(completingTextEdit);
     QSettings settings("CENDITEL","Safet");
     settings.beginGroup("Inflow");
     settings.setValue("plugs", _plugs);
     int count = completingTextEdit->listConfs().count();
     settings.setValue("confscount", count);

     for(int i = 0; i < count;i++) {
          QString valuename = QString("confscount%1").arg(i);
          settings.setValue(valuename, completingTextEdit->listConfs().at(i));
     }
     settings.endGroup();


 }


void MainWindow::setModelCompleter(int opt) {

    if ( opt >= filecompleters.count() ) {
        SafetYAWL::streamlog
                << SafetLog::Debug
                << tr("Consultar: El numero de archivos de entradas \"%1\" es menor o igual al que "
                      "se desea seleccionar \"%2\"")
                .arg(filecompleters.count())
                .arg(opt);
        return;
    }

     QString file = filecompleters.at(opt);
     if (!QFile::exists(file)) {
         SafetYAWL::streamlog
                 << SafetLog::Error
                 << tr("El archivo de entrada (input) \"%1\" no se puede leer")
                 .arg(file);
         return;
     }
     if ( completer != NULL ) {
          Q_CHECK_PTR( dommodel);
          dommodel->setTextEdit(completingTextEdit);
          completingTextEdit->setCompleter(completer);
          dommodel->domModelFromFile(file);
          dommodel->readFile();
          completingTextEdit->updateDockSbMenu();
          return;
     }

     completer = new QCmdCompleter(this);
     Q_CHECK_PTR( completer);
     completer->reset();

     dommodel = new DomModel( file, completer, completingTextEdit);
     Q_CHECK_PTR( dommodel);

     completer->setDomModel( dommodel );
     completer->setModelSorting(QCompleter::CaseInsensitivelySortedModel);
     completer->setCaseSensitivity(Qt::CaseInsensitive);
     completingTextEdit->setCompleter(completer);
     dommodel->readFile();


}


void MainWindow::toDelAllWorkflow() {
     if ( gviewoutput == NULL ) return;
     gviewoutput->initialize();
     MainWindow::currentgraphpath = "";

}

void MainWindow::toDelOneWorkflow() {
     if ( gviewoutput == NULL ) return;
     gviewoutput->deleteCurrentWorkflow();
     MainWindow::currentgraphpath = "";

}

void MainWindow::toAddNodeflow()
{

    qDebug("....toAddNodeflow.....(1)...");
    if ( MainWindow::currentgraphpath.isEmpty() ) {

            qDebug("......No existe un grafico activo!");
           return;

    }

    qDebug("....toAddNodeflow.....(1)...MainWindow::currentgraphpath:|%s|",
           qPrintable(MainWindow::currentgraphpath));

    qDebug("....toAddNodeflow.....(1)...MainWindow::currentgraphpath:|%s|",
           qPrintable(MainWindow::currentgraphpath));
    SafetWorkflow* mywf = configurator->getWorkflow();


    qDebug();


    int n = mywf->getParameterlist().count();
    qDebug(".......mywf->getParameterlist().count():%d",
           n);
    QString mypath;

    if ( n > 0 ) {
        DialogFlowParameters mydialog(this);
        mydialog.setIsBeforeGraph(false);

        for(int i = 0; i < n; i++) {
                      qDebug("title:|%s|", qPrintable(mywf->getParameterlist().at(i)->title()));
            if (!mywf->getParameterlist().at(i)->configurekey().isEmpty() ) {
                continue;
            }
            qDebug("**(2)....title:|%s|", qPrintable(mywf->getParameterlist().at(i)->title()));
//            qDebug("configurekey:|%s|", qPrintable(mywf->getParameterlist().at(i)->configurekey()));

            if (mywf->getParameterlist().at(i)->title() == tr("Nodo a agregar")  ) {

                    mypath = mywf->getParameterlist().at(i)->path();
                    mydialog.parameters().append(mywf->getParameterlist().at(i));
            }
            else if (mywf->getParameterlist().at(i)->title() == tr("Textualinfo")  ) {

                    mydialog.parameters().append(mywf->getParameterlist().at(i));
            }

            else if (mywf->getParameterlist().at(i)->title() == tr("Paralelo al nodo siguiente")  ) {

                    mydialog.parameters().append(mywf->getParameterlist().at(i));
            }

            else if (mywf->getParameterlist().at(i)->title() == tr("Nodo anterior")  ) {
                qDebug("Yes.. nodo anterior!");
                    mydialog.parameters().append(mywf->getParameterlist().at(i));
            }



        }
        mydialog.layoutParameters();

        if ( mydialog.exec() == QDialog::Rejected ) {
            return;
        }

        QString myconsult;


        QMap<QString,QString> parvalues = mydialog.getValues();

        qDebug(".........currentpath:|%s|.....parvalues.count():|%d|",qPrintable(mypath), parvalues.count());
        foreach(QString k, parvalues.keys()) {

            qDebug("%s -> %s", qPrintable(k), qPrintable(parvalues[k]));
        }


        if (!parvalues.contains(tr("Nodo a agregar"))) {
            qDebug("No se encuentra el parametro \"Nodo a agregar\" tarea a borrar");
            return;

        }
        if (!parvalues.contains(tr("Nodo anterior"))) {
            qDebug("No se encuentra el parametro \"Nodo anterior\" tarea a borrar");
            return;

        }

        QString isparall = "No";
        if (parvalues.contains(tr("Paralelo al nodo siguiente"))) {
            isparall = parvalues[tr("Paralelo al nodo siguiente")];
        }

        QString mytextualinfo;

        if ( parvalues.contains(tr("Textualinfo"))) {
            mytextualinfo = parvalues["Textualinfo"];
        }

        myconsult = QString("operacion:Agregar_nodo_a_gr�fico_de_flujo Cargar_archivo_flujo: %1\n"
                                        "Nodo_anterior: %2\n"
                                        "Nombre_nuevo_nodo: %3 \nEs_paralelo_al_nodo_anterior:%4 Textualinfo:%5")
                                .arg(mypath)
                                .arg(parvalues["Nodo anterior"])
                                .arg(parvalues["Nodo a agregar"])
                                .arg(isparall)
                                .arg(mytextualinfo)
                                .toLatin1();


        qDebug("..........toAddNode...plaintext...myconsult:|%s|", qPrintable(myconsult));


        completingTextEditSign->setPlainText(myconsult);
        completingTextEdit = completingTextEditSign;
        setModelCompleter(3); // Colocar para 2


        toInputSign();


        /*
                 *      FIXME: Chequear par�metros de configuraci�n
                 */


        qDebug("..................*****myconsult:\n|%s|",qPrintable(myconsult));
    }

}


void MainWindow::toReloadNodeflow() {

    toDelAllWorkflow();
    completingTextEdit = completingTextEditCons;
    setModelCompleter(1); // Colocar para 2
     toInputConsole();
    qDebug("toReloadNodeflow...");
}


void MainWindow::toDelNodeflow()
{

    qDebug("....toDelNodeflow.....(1)...");

    if ( MainWindow::currentgraphpath.isEmpty() ) {

            qDebug("......No existe un grafico activo!");
           return;

    }


    qDebug("....toAddNodeflow.....(1)...MainWindow::currentgraphpath:|%s|",
           qPrintable(MainWindow::currentgraphpath));
    SafetWorkflow* mywf = configurator->getWorkflow();


    qDebug();


    int n = mywf->getParameterlist().count();
    qDebug(".......mywf->getParameterlist().count():%d",
           n);
    QString mypath;

    if ( n > 0 ) {
        DialogFlowParameters mydialog(this);
        mydialog.setIsBeforeGraph(false);

        for(int i = 0; i < n; i++) {
                      qDebug("title:|%s|", qPrintable(mywf->getParameterlist().at(i)->title()));
            if (!mywf->getParameterlist().at(i)->configurekey().isEmpty() ) {
                continue;
            }
            qDebug("**(2)....title:|%s|", qPrintable(mywf->getParameterlist().at(i)->title()));
//            qDebug("configurekey:|%s|", qPrintable(mywf->getParameterlist().at(i)->configurekey()));
          if (mywf->getParameterlist().at(i)->title() == tr("Tarea a borrar")  ) {
                mypath = mywf->getParameterlist().at(i)->path();
                mydialog.parameters().append(mywf->getParameterlist().at(i));
          }
        }
        mydialog.layoutParameters();

        if ( mydialog.exec() == QDialog::Rejected ) {
            return;
        }

        QString myconsult;




        qDebug(".....................SafetWorkflow::executedParsed.....(1)...");

        QMap<QString,QString> parvalues = mydialog.getValues();

        qDebug(".........currentpath:|%s|.....parvalues.count():|%d|",qPrintable(mypath), parvalues.count());

        if (!parvalues.contains(tr("Tarea a borrar"))) {
            qDebug("No se encuentra el parametro tarea a borrar");
            return;

        }



    myconsult = QString("operacion:Borrar_nodo_de_gr�fico \n"
                        "Cargar_archivo_flujo:%1 Nodo_a_borrar: %2")
                        .arg(mypath)
                        .arg(parvalues[tr("Tarea a borrar")]);

        completingTextEditSign->setPlainText(myconsult);
        completingTextEdit = completingTextEditSign;
        setModelCompleter(3); // Colocar para 2


        toInputSign();

        /*
                 *      FIXME: Chequear par�metros de configuraci�n
                 */

        qDebug("..................*****myconsult:\n|%s|",qPrintable(myconsult));
    }


}

void MainWindow::toClearTextEdit() {
    if ( completingTextEdit ) {
        CmdWidget* curwidget = completingTextEdit->currentWidget();
        if (curwidget != NULL ) {
            curwidget->hide();
            completingTextEdit->setCurrentWidget(NULL);
        }
        completingTextEdit->clear();

        selInputTab(centralWidget->currentIndex());
    }
    // limpiar la barra de estado
    statusBar()->removeWidget(iconWidget);

}


bool MainWindow::doPermiseExecOperation(const QString& op, const QString& permise) {

    if (!MainWindow::permises.contains(op))  {

        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("La operaci�n <b>\"%1\"</b> no existe en el archivo de autorizaci�n<br/>."
                      "Consulte con el administrador para que asigne el permiso solicitado").arg(op);
        return false;
    }

    if ( MainWindow::permises[ op ].count() < 3) {
        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("La operaci�n <b>\"%1\"</b> solo tiene \"%2\" elementos de informaci�n.<br/>Es "
                      "probable que el archivo de autorizaci�n no est� correctamente formado")
                .arg(op).arg(MainWindow::permises[ op ].count());
        return false;
    }
    QStringList myusers = MainWindow::permises[ op ].at(0).split(";");
    QStringList myroles = MainWindow::permises[ op ].at(2).split(";");
    if (!myusers.contains(currentaccount) && !myroles.contains(MainWindow::currentrole)) {
        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("El usuario <b>%1</b> no est� autorizado para utilizar la operaci�n <b>\"%2\"</b>.<br/>"
                      "Consulte con el administrador para que asigne el permiso solicitado")
                .arg(currentaccount).arg(op);
        return false;
    }
    QStringList mytypes = MainWindow::permises[ op ].at(1).split(";");

    if (!mytypes.contains(permise)) {
        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("El usuario \"%1\" no tiene autorizado  el permiso del tipo \"%3\" "
                      "que le permita utilizar la operaci�n \"%2\"<br/>"
                      "Consulte con el administrador para que asigne el permiso solicitado")
                .arg(currentaccount).arg(op).arg(permise);
        return false;
    }


    return true;
}

void MainWindow::createInitialTables() {
    QSqlQuery myquery( SafetYAWL::currentDb );

    QString mysql =
            ""
            "CREATE TABLE ticket"
            "("
              "id integer NOT NULL,"
              "type text,"
              "time integer,"
              "changetime integer,"
              "component text,"
              "severity text,"
              "priority text,"
              "owner text,"
              "reporter text,"
              "cc text,"
              "version text,"
              "milestone text,"
              "status text,"
              "resolution text,"
              "summary text,"
              "description text,"
              "keywords text,"
              "parenttask text,"
              "flowfilepath text,"
              "CONSTRAINT ticket_pkey PRIMARY KEY (id)"
            ")"
            ;

    if ( myquery.exec(mysql) ) {
        SafetYAWL::streamlog << SafetLog::Debug << tr (" Funciona la creacion de las tablas iniciales");
    }
    else {
        SafetYAWL::streamlog << SafetLog::Debug << tr ("NO Funciona la creacion de las tablas iniciales");
    }

    mysql =
    "CREATE TABLE milestone"
    "("
      "name text NOT NULL,"
      "due integer,"
      "completed integer,"
      "description text,"
      "CONSTRAINT milestone_pkey PRIMARY KEY (name)"
    ")";
    QSqlQuery oquery( SafetYAWL::currentDb );
    if ( oquery.exec(mysql) ) {
        SafetYAWL::streamlog << SafetLog::Debug << tr (" Funciona la creacion de las tablas iniciales (milestone)");
    }
    else {
        SafetYAWL::streamlog << SafetLog::Debug << tr ("NO Funciona la creacion de las tablas iniciales (milestone)");
    }


}

void MainWindow::setupToolbar() {

    QAction *a;

    if ( standardbar ) {
        delete standardbar;
    }
     standardbar = new QToolBar(QString::fromLatin1("Est�ndar"), this);
     Q_CHECK_PTR( standardbar );
     //QAction *toexecAction = new QAction(QIcon(":/toexecon.xpm"), tr("Enviar"), standardbar);
     toexecAction = new QAction(QIcon(":/toexecon.xpm"), tr("Enviar (Ctrl+S)"), standardbar);
     Q_CHECK_PTR( toexecAction );
     toexecAction->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_S));

     toGoPanelEdit = new QAction(QIcon(":/list.png"), tr("Ir a panel de edici�n actual "
                                                                "(Ctrl+W)"), standardbar);
     toGoPanelEdit->setShortcut( QKeySequence(Qt::CTRL + Qt::Key_W));

     connect(toGoPanelEdit, SIGNAL(triggered()), this, SLOT(toDoGoPanelEdit()));

     connect(toexecAction, SIGNAL(triggered()), this, SLOT(toSend()));
     QAction *toWelcomeAction = new QAction(QIcon(":/logosafet-small.png"), tr("Principal"), standardbar);
     Q_CHECK_PTR( toWelcomeAction );
     connect(toWelcomeAction, SIGNAL(triggered()), this, SLOT(checkGoPrincipal()));
     standardbar->addAction(toWelcomeAction);


     standardbar->addAction(toexecAction);

     standardbar->addSeparator();




     addNodeflowAction = new QAction(QIcon(":/plus.png"),
                                     tr("Agrega un nodo al grafo\n "
                                        "seleccionado"),
                                     standardbar);
     standardbar->addAction(addNodeflowAction);
     connect(addNodeflowAction, SIGNAL(triggered()), this, SLOT(toAddNodeflow()));


     delNodeflowAction = new QAction(QIcon(":/ico_delete.png"),
                                     tr("Elimina un nodo al grafo\n "
                                        "seleccionado"),
                                     standardbar);
     standardbar->addAction(delNodeflowAction);
     connect(delNodeflowAction, SIGNAL(triggered()), this, SLOT(toDelNodeflow()));

     reloadNodeflowAction = new QAction(QIcon(":/reload128.png"),
                                     tr("Actualiza el �ltimo\n "
                                        "gr�fico"),
                                     standardbar);
     standardbar->addAction(reloadNodeflowAction);
     connect(reloadNodeflowAction, SIGNAL(triggered()), this, SLOT(toReloadNodeflow()));


     standardbar->addSeparator();

     /*

// Accion de eliminar todos los elementos
     QAction *delOneWorkflowAction = new QAction(QIcon(":/deloneworkflow.png"),
                                                 tr("Borrar el gr�fico de flujo\n "
                                                    "de trabajo seleccionado"),
                                                 standardbar); */
    delOneWorkflowAction = new QAction(QIcon(":/deloneworkflow.png"),
                                                   tr("Borrar el gr�fico de flujo\n "
                                                      "de trabajo seleccionado"),
                                                   standardbar);


     Q_CHECK_PTR( delOneWorkflowAction);
     connect(delOneWorkflowAction, SIGNAL(triggered()), this, SLOT(toDelOneWorkflow()));
     standardbar->addAction(delOneWorkflowAction);
     delOneWorkflowAction->setShortcut(QKeySequence::Delete);
     /*
     QAction *delWorkflowAction = new QAction(QIcon(":/delworkflow.png"),
                                              tr("Borrar todos los gr�ficos de\n flujo de trabajo"),
                                              standardbar); */

     delWorkflowAction = new QAction(QIcon(":/delworkflow.png"),
                                               tr("Borrar todos los gr�ficos de\n flujo de trabajo"),                                               
                                               standardbar);

     Q_CHECK_PTR( delWorkflowAction );
     connect(delWorkflowAction, SIGNAL(triggered()), this, SLOT(toDelAllWorkflow()));
     standardbar->addAction(delWorkflowAction);
     // Accion de eliminar todos los elementos
     standardbar->addSeparator();
          standardbar->addAction(toGoPanelEdit);
     //QAction *clearTextAction = new QAction(QIcon(":/clear.png"), tr("Borrar el cuadro de edici�n actual"), standardbar);
     clearTextAction = new QAction(QIcon(":/clear.png"), tr("Borrar el cuadro de edici�n actual (Ctrl+D)"), standardbar);

     clearTextAction->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_D));
     connect(clearTextAction, SIGNAL(triggered()), this, SLOT(toClearTextEdit()));
     standardbar->addAction(clearTextAction);

     a = actionUndo = new QAction(QIcon(":/editundo.png"), tr("&Deshacer"), this);
     a->setShortcut(QKeySequence::Undo);
     standardbar->addAction(a);
     editMenu->addAction(a);
     a = actionRedo = new QAction(QIcon( ":/editredo.png"), tr("&Rehacer"), this);
     a->setShortcut(QKeySequence::Redo);
     standardbar->addAction(a);
     editMenu->addAction(a);
     editMenu->addSeparator();
     standardbar->addSeparator();
     a = actionCut = new QAction(QIcon(":/editcut.png"), tr("Cor&tar"), this);
     a->setShortcut(QKeySequence::Cut);
     editMenu->addAction(a);

     standardbar->addAction(a);
     a = actionCopy = new QAction(QIcon(":/editcopy.png"), tr("&Copiar"), this);
     a->setShortcut(QKeySequence::Copy);
     standardbar->addAction(a);
     editMenu->addAction(a);
     a = actionPaste = new QAction(QIcon(":/editpaste.png"), tr("&Pegar"), this);
     a->setShortcut(QKeySequence::Paste);
     standardbar->addAction(a);
     standardbar->addSeparator();
     editMenu->addAction(a);

     a = actionModifyfield = new QAction(QIcon(":/modifyfield.png"), tr("&Modificar Campo"), this);
     a->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_M));
     standardbar->addAction(a);

     a = actionDelfield = new QAction(QIcon(":/delfield.png"), tr("&Borrar Campo"), this);
     a->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_P));
     standardbar->addAction(a);


     standardbar->addSeparator();
     standardbar->addAction(exitAction);


     editMenu->addAction(a);

     actionCut->setEnabled(false);
     actionCopy->setEnabled(false);


     connect(centralWidget,SIGNAL(currentChanged (int)), this, SLOT(checkSelInputTab(int)));

     addToolBar(standardbar);

}


void MainWindow::toDoGoPanelEdit() {

    qDebug("...MainWindow::toDoGoPanelEdit....completingTextEdit->setFocus();....(1)");
    if ( completingTextEdit ) {
        completingTextEdit->setFocus();
        qDebug("...MainWindow::toDoGoPanelEdit...completingTextEdit->setFocus();....(2)");
    }
    createInitialTables();
}

void MainWindow::setEnabledToolBar(bool e) {
    if ( standardbar ) {

        standardbar->setEnabled(e);
        if ( menusbbutton ) {
            if ( e ) {
                menusbbutton->show();
            }
            else {
                menusbbutton->hide();
            }
        }


    }

}


void MainWindow::checkSelInputTab(int opt) {
    if (centralWidget == NULL ) {
        return;
    }

    QString nameoperation = centralWidget->tabText(opt);
    if (nameoperation.endsWith("*")) {
        nameoperation.chop(1);
    }
    
    qDebug("...checkSelInputTab: nameoperation: %s",qPrintable(nameoperation));
    if (!_checkgoprincipal ) {
        if (!MainWindow::doPermiseExecOperation(nameoperation) ) {
            goPrincipal();
            QMessageBox::warning(this, trUtf8("SAFET - Inflow"),
                                 tr("El usuario <font size=\"4\"> <b>%1</b></font> no est� autorizado"
                                    "<br/> para ejecutar el m�dulo <font size=\"4\"><b>%2</b></font>")
                                 .arg(MainWindow::currentaccount)
                                 .arg(nameoperation),
                                 QMessageBox::Ok);


            return;
        }
    }
    else {
        _checkgoprincipal = false;
    }


    selInputTab(opt);
    setEnabledToolBar(true);

}


void MainWindow::selInputTab(int opt)  {
     qDebug("option: %d", opt);

     actionDelfield->setEnabled(false);

      if (completingTextEdit != NULL ) {
          completingTextEdit->unsetCompleter(completer);
          disconnect(completingTextEdit,0,0,0);
          disconnect(actionCut,0,0,0);
          disconnect(actionCopy,0,0,0);
          disconnect(actionPaste,0,0,0);
          disconnect(actionUndo,0,0,0);
          disconnect(actionDelfield,0,0,0);
          disconnect(actionModifyfield,0,0,0);
          disconnect(actionRedo,0,0,0);
     }




     switch(opt) {
          case 0:
               completingTextEdit = completingTextEditForm;
               setModelCompleter(0);
               showShowResultWidget(DockSbMenu::Hide);
               actionDelfield->setEnabled(true);
               delOneWorkflowAction->setEnabled(false);
               delWorkflowAction->setEnabled(false);
               actionModifyfield->setEnabled(true);
               toexecAction->setEnabled(true);
               
            break;
          case 1:
               completingTextEdit = completingTextEditCons;
               setModelCompleter(1);
               showShowResultWidget(DockSbMenu::Hide);
               loadEditActions();
               actionDelfield->setEnabled(true);
               delOneWorkflowAction->setEnabled(false);
               delWorkflowAction->setEnabled(false);
               actionPaste->setEnabled(true);
               clearTextAction->setEnabled(true);
               actionModifyfield->setEnabled(true);
               toexecAction->setEnabled(true);
//               setEnabledToolBar();
          break;
          case 6:
               centralWidget->setTabText(opt, tr("Salida"));
               showSmartMenuWidget(DockSbMenu::Hide);
               showShowResultWidget(DockSbMenu::Hide);
               actionDelfield->setEnabled(false);
               actionPaste->setEnabled(false);
               clearTextAction->setEnabled(false);
               actionModifyfield->setEnabled(false);
               toexecAction->setEnabled(false);

               break;
          case 5:
               completingTextEdit = completingTextEditConf;
               setModelCompleter(2); // Colocar para 2
               showShowResultWidget(DockSbMenu::Hide);
               opt = 2;
               actionDelfield->setEnabled(true);
               delOneWorkflowAction->setEnabled(false);
               delWorkflowAction->setEnabled(false);
               actionPaste->setEnabled(true);
               clearTextAction->setEnabled(true);
               actionModifyfield->setEnabled(true);
               toexecAction->setEnabled(true);
                 break;
          case 7:
               completingTextEdit = completingTextEditUsers;
               setModelCompleter(4); // Colocar para 2
               showShowResultWidget(DockSbMenu::Hide);
               opt = 2;
               actionDelfield->setEnabled(true);
               delOneWorkflowAction->setEnabled(false);
               delWorkflowAction->setEnabled(false);
               actionPaste->setEnabled(true);
               clearTextAction->setEnabled(true);
               actionModifyfield->setEnabled(true);
               toexecAction->setEnabled(true);
               break;

        case 3: // Reportesi
               centralWidget->setTabText(opt, tr("Reportes"));
               showShowResultWidget(DockSbMenu::Hide);
               showSmartMenuWidget(DockSbMenu::Hide);
               setEnabledToolBar();
               actionDelfield->setEnabled(false);
               actionModifyfield->setEnabled(false);
               delOneWorkflowAction->setEnabled(false);
               delWorkflowAction->setEnabled(false);
               actionPaste->setEnabled(false);
               clearTextAction->setEnabled(false);
               actionModifyfield->setEnabled(false);
               toexecAction->setEnabled(false);
               break;

           case 4:
               centralWidget->setTabText(opt, tr("Flujo"));
               showShowResultWidget(DockSbMenu::Hide);
               showSmartMenuWidget(DockSbMenu::Hide);
               setEnabledToolBar();
               actionDelfield->setEnabled(false);
               delOneWorkflowAction->setEnabled(true);
               delWorkflowAction->setEnabled(true);
               actionPaste->setEnabled(false);
               clearTextAction->setEnabled(false);
               actionModifyfield->setEnabled(false);
               toexecAction->setEnabled(false);
               break;

           case 2:
               completingTextEdit = completingTextEditSign;
               setModelCompleter(3); // Colocar para 2
               showShowResultWidget(DockSbMenu::Hide);
               setEnabledToolBar();
               actionDelfield->setEnabled(true);
               delOneWorkflowAction->setEnabled(false);
               delWorkflowAction->setEnabled(false);
               actionPaste->setEnabled(true);
               clearTextAction->setEnabled(true);
               actionModifyfield->setEnabled(true);
               toexecAction->setEnabled(true);
               break;

          default:
               break;
     }
     connect(actionCut, SIGNAL(triggered()), completingTextEdit, SLOT(cut()));
     connect(actionCopy, SIGNAL(triggered()), completingTextEdit, SLOT(copy()));
     connect(actionPaste, SIGNAL(triggered()), completingTextEdit, SLOT(paste()));
     connect(actionDelfield, SIGNAL(triggered()), completingTextEdit, SLOT(delCurrentField()));
     connect(actionModifyfield, SIGNAL(triggered()), completingTextEdit, SLOT(modifyCurrentField()));

     connect(completingTextEdit, SIGNAL(copyAvailable(bool)), actionCut, SLOT(setEnabled(bool)));
     connect(completingTextEdit, SIGNAL(copyAvailable(bool)), actionCopy, SLOT(setEnabled(bool)));
     connect(completingTextEdit->document(), SIGNAL(undoAvailable(bool)),
                 actionUndo, SLOT(setEnabled(bool)));
     connect(completingTextEdit->document(), SIGNAL(redoAvailable(bool)),
             actionRedo, SLOT(setEnabled(bool)));
     actionUndo->setEnabled(completingTextEdit->document()->isUndoAvailable());
     actionRedo->setEnabled(completingTextEdit->document()->isRedoAvailable());

     connect(actionUndo, SIGNAL(triggered()), completingTextEdit, SLOT(undo()));
     connect(actionRedo, SIGNAL(triggered()), completingTextEdit, SLOT(redo()));


}

void MainWindow::toLoadWeb() {

    QString code;
    code = "SAFETlist = ["
     "{areacode: \"201\", state: \"Merida\"},"
     "{areacode: \"203\", state: \"Zulia\"},"
     "{areacode: \"204\", state: \"Trujillo\"},"
     "{areacode: \"205\", state: \"Bolivar\"}"
     "];"
     "myColumnDefs = ["
     "{key:\"areacode\",label:\"C?digo\",width:100,resizeable:true,sortable:true},"
     "{key:\"state\",label:\"Estado\",width:100,resizeable:true,sortable:true}"
     "];"
     " safetproccessData()";

    //code = "doListTable()";

    evalJS(code);

}


QString MainWindow::getScriptLen(const QSqlField& f) {

//    qDebug("...MainWindow::getScriptLen...f.type():%d",f.type());
    QString result = "40";
    switch ( f.type()) {

    case QVariant::String:
        if (f.value().toString().length() > 50 || f.name().contains("desc")) {
            result = "300";
        }
        else {
            result = "130";
        }
        break;
    case QVariant::Date:
    case QVariant::DateTime:
                result = "150";
    default:;

    }
//    qDebug("...MainWindow::getScriptLen...f.type():%d...result:|%s|...len:%d",f.type(),
//           qPrintable(result),f.value().toString().length());
//    qDebug();
    return result;
}

void MainWindow::executeJSCodeAfterLoad(bool ok ) {
    if ( !jscriptload ) {
        return;
    }
    if ( !ok ) {
        qDebug("Error de REPORTE... (1)..");
       SYE
                << tr("Ocurrio un error al cargar el reporte (%1)")
                .arg(weboutput->url().toString());
        return;
    }


    // ** Reemplazar caracteres especiales, hacen falta pruebas aqui
     currentDocuments.replace("\n","");
     // ** Reemplazar caracteres especiales, hacen falta pruebas aqui
     QString data;
    data += QString("safetlistcount = %1;\n").arg(_listprincipalcount);
    data += QString( "safetvariable = '%1';\n").arg(_listprincipalvariable);
    data += QString("safetkey = '%1';\n").arg(_listprincipalkey);
    data += QString("safettitle = '%1';\n").arg(_listprincipaltitle);

    data += "safetlist = [";
    data += currentDocuments;
    data += "];\n";


    SYD << tr("data:|%1|").arg(data);

    jscriptcolumns = "safetcolumns = [";
    int i = 0;
    foreach(QSqlField f, currentFields ) {

        QString scriptlen = getScriptLen(f);
         jscriptcolumns
                 += QString("{ key: \"%1\",label:\"%1\",width:%2,"
                            "resizeable:true,sortable:true},\n")
                    .arg(f.name())
                    .arg(scriptlen);

         i++;
    }
    if ( i > 0 ) {
        jscriptcolumns.chop(2);
    }
    jscriptcolumns += "];";

    QString code;
    code = data;
    code += " ";
    code += jscriptcolumns;
    code += " ";


   evalJS(code);


    code = "safetproccessData();";

    evalJS(code);



}

void MainWindow::generateJScriptreports(const QString& documents,
                                        const QList<QSqlField>& fields) {

     currentDocuments = documents;
     //qDebug("..currentDocuments...: %s", qPrintable(currentDocuments));
     qDebug("..fields.count()...: %d", fields.count());
     currentFields = fields;
     jscriptload = true;
     weboutput->reload();

}


QString MainWindow::evalJS(const QString &js) {
     Q_CHECK_PTR( weboutput );
     qDebug(".....evalJS...QWebFrame *frame = weboutput->page()->mainFrame();");
     QWebFrame *frame = weboutput->page()->mainFrame();

     SYD << tr(".............EVALUATING script--- js:|%1|").arg(js);

    QString myresult =  frame->evaluateJavaScript(js).toString();

    SYD << tr(".............EVALUATING script--- myresult:|%1|").arg(myresult);

    return myresult;


}

void MainWindow::toSend(bool sign) {

    MainWindow::_issigning = sign;
//     Q_ASSERT(  comboMode );
    if ( completingTextEdit == NULL ) {
        SafetYAWL::streamlog
                << SafetLog::Debug
                << tr("Puntero de cuadro de edici�n (completingTextEdit) es NULO (NULL)");
        return;
    }
    CmdWidget* curwidget = completingTextEdit->currentWidget();
    if (curwidget != NULL ) {
        curwidget->insertAndClose();
        //completingTextEdit->setCurrentWidget(NULL);
    }
    if (completingTextEdit->toPlainText().trimmed().isEmpty() ) {
        SafetYAWL::streamlog.initAllStack();
        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("El <font color=#7E2217>Cuadro de Edici�n</font> que se muestra arriba se encuentra <b>vac�o</b>.<br/>"
                      "No se realiz� ninguna acci�n.");
       queryForErrors();
        return;
    }



     int index = centralWidget->currentIndex();
     QStringList myactions;

     QString texto;
     switch( index  ) {
          case 0: // Formulario
                    Q_CHECK_PTR(completingTextEdit);
                    texto = completingTextEdit->toPlainText().toLatin1();
                    myactions = splitActions(texto);
                    qDebug(".......myactions...count():%d", myactions.count());
                    foreach(QString a, myactions) {
                        a = a.trimmed();
                        completingTextEdit->setText(a);
                        qDebug("\n...myactions...:\n|%s|\n",
                               qPrintable(a));
                        toInputForm(a,true);
                    }
                    break;
          case 1:  // Console
                    toInputConsole();
                    break;
          case 2:  // Certificates Doc signed Management
                    qDebug("..................CALLING toInputSign().....");
                    toInputSign();
                    break;
          case 5:  // Configuration
                    toInputConfigure();
                    break;
            case 7:
                    toInputUsers();
                    break;
          default:
                    break;
     }
     if (index == -1 ) {
          statusBar()->showMessage(tr("No se ha seleccionado el tipo de entrada (Formulario,Consola, etc..)"));

     }

}

void MainWindow::toInputUsers(const QString& sentence) {

    QString texto = "";
    SafetTextParser parser(this);

    if (!sentence.isEmpty()) {

        completingTextEdit->setText(sentence);
        qDebug("colocando sentence...1");
    }

        Q_CHECK_PTR ( MainWindow::mystatusbar );
        Q_CHECK_PTR ( completingTextEdit );
        MainWindow::mystatusbar->showMessage(tr("Enviando modificaci�n..."));

        texto = completingTextEdit->toPlainText().toLatin1();


    qDebug(".. ...(1)..texto: \n|%s|\n", qPrintable(texto));

    parser.setStr( texto.toLatin1() );
    QString str = "agregar,eliminar,actualizar,mostrar";
    QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
    parser.setCommands(str.split(","),commandstr.split(","));
    //qDebug("....xml...(1): \n|%s|\n", qPrintable(parser.str()));
    QString xml = parser.toXml();
    //qDebug("....xml...(2): \n|%s|\n", qPrintable(xml));
    SYD << tr("Cadena XML generada de la entrada: \n\"%1\"").arg(xml);
    myoutput->setPlainText(xml);

    qDebug(".. ...(1)..xml: \n|%s|\n", qPrintable(xml));


    centralWidget->setTabText(6, tr("Salida*"));

    parser.processInput( xml.toLatin1() );
    QString filepath = SafetYAWL::getConfFile().getValue("Input", "input.path").toString();
    QString filename = "defusers.xml";

    parser.openXml(filepath + "/" + filename);
    QStringList names = parser.loadNameConfs();

    foreach(QString n, names) {
         QMap<QString,QVariant> conf = centraledit()->findConf(n);
         parser.addConf(conf, n);
    }

    QStringList results = parser.processXml(false);

   if ( queryForErrors() ) {
       qDebug(".......****..QUERYERRORS....");
       SYD << tr("...MainWindow::toInputUsers....(error)....");
            return;
    }

    QString fileconf = SafetYAWL::pathconf+ "/" + "auth.conf";
    qDebug("...FILECONF...fileconf:|%s|", qPrintable(fileconf));

    if (QFile::exists(fileconf)) {
         foreach(QString s, results) {
            qDebug("(toInputUsers)toInputConfigure.......result: %s", qPrintable(s));
            qDebug("ProcessConfFile...1");

            proccessConfFile(s,fileconf,true);
        }


         //doCipherFile(false);
         SafetYAWL::readAuthConfFile();
         qDebug("     ----toInputUsers...SafetYAWL::readAuthConfFile() ");
     }
    else {
        SafetYAWL::streamlog.initAllStack();
        SYE
                << tr("No se pueden realizar los cambios. No se encuentra el archivo :\"%1\"")
                .arg(fileconf);

        if ( queryForErrors() ) {
            qDebug(".......****..queryforerror....(2)");
                 return;
         }


    }

    showSuccessfulMessage(tr("La operaci�n de <b>configuraci�n</b> fue exitosa"
                             "....<b>ok!</b>"));

}

void MainWindow::showSuccessfulMessage(const QString& m) {
    statusBar()->showMessage(tr("Enviando......ok!"));
    QString message = QString("<table><tr><td><font color=green>%1</font>"
                              "</td></tr></table>")
            .arg(m);
    dockShowResult->addHeadHtml(message);
    dockShowResult->renderMessages(false);
    showShowResultWidget(DockSbMenu::Hide);
    showShowResultWidget(DockSbMenu::Show);

}

void MainWindow::configureStatusBar() {

    if (MainWindow::mystatusbar != NULL ) {
        menusbbutton = new QToolButton;
        QSize sizeButton;
        sizeButton.setHeight(8);
        sizeButton.setWidth(8);
        menusbbutton->setIconSize( sizeButton);


        menusbbutton->setIcon(QIcon(":/list.png"));
        menusbbutton->hide();

        MainWindow::mystatusbar->addPermanentWidget(menusbbutton);
        connect(menusbbutton,SIGNAL(clicked()),this,SLOT(showSmartMenuWidget()));
    }


}

void MainWindow::showSmartMenuWidget(DockSbMenu::ShowAction a) {


    if ( MainWindow::docksbMenu ) {
        if ( MainWindow::docksbMenu->isVisible() || a == DockSbMenu::Hide ) {
            _issmartmenuvisible = false;
            MainWindow::docksbMenu->hide();
        }
       else if ( !MainWindow::docksbMenu->isVisible() || a == DockSbMenu::Show )  {
           _issmartmenuvisible = true;
            MainWindow::docksbMenu->show();
        }

    }


}

void MainWindow::showSmartMenu() {


    qDebug("**");
    qDebug("...MainWindow::showSmartMenu().....(1)...isVisible:%d",MainWindow::docksbMenu->isVisible());
    qDebug();
        if ( MainWindow::docksbMenu->isVisible() ) {

            MainWindow::docksbMenu->hide();

        }
       else   {

            MainWindow::docksbMenu->show();

        }


}


void MainWindow::timeHideResult() {
    if ( MainWindow::dockShowResult == NULL ) {
        return;
    }
    if ( MainWindow::dockShowResult->isVisible() ) {
        qDebug("Ocultando..");
        MainWindow::dockShowResult->hide();
        _issmartmenuvisible = false;
        _panelinfotimer.stop();
    }


}

void MainWindow::showShowResultWidget(DockSbMenu::ShowAction a) {



    if ( MainWindow::dockShowResult ) {
        if ( MainWindow::dockShowResult->isVisible() || a == DockSbMenu::Hide ) {

            MainWindow::dockShowResult->hide();
            _issmartmenuvisible = false;
        }
       else if ( !MainWindow::docksbMenu->isVisible() || a == DockSbMenu::Show )  {
            MainWindow::dockShowResult->show();
            bool ok;
            QString seconds = SafetYAWL::getConf()["DefaultValues/panel.info.secondstohide"];
            _seconstohide = seconds.toInt(&ok);
            qDebug("..._seconstohide:%d",_seconstohide);
            if (!ok  || _seconstohide <= 0) {
                _seconstohide = 2000;
            }


            _panelinfotimer.start(_seconstohide);
            _issmartmenuvisible = true;
        }

    }


}




bool MainWindow::queryForErrors() {


    QStack<QPair<QDateTime,QString> > mystack = SafetYAWL::streamlog.stopErrorStack();
    SafetYAWL::streamlog << SafetLog::Debug << trUtf8("Hay \"%1\" errores guardados en la lista. De inmediato se procede"
                                                      " a inicializar la lista de errores.")
            .arg(mystack.count());
    SafetYAWL::streamlog.stopAllStack();
    if (mystack.count() > 0 ) { // Verificacion NO exitosa
        qDebug("........mostrando ... queryerror: (******)");
        QString message = renderMessageStack(mystack,SafetLog::Error);
        QString messagew = renderMessageStack(mystack,SafetLog::Warning);
        Q_CHECK_PTR(dockShowResult);
        dockShowResult->addHeadHtml(message+messagew);
        dockShowResult->renderMessages(false);
        showShowResultWidget(DockSbMenu::Hide);
        showShowResultWidget(DockSbMenu::Show);
        return true;
    }
    return false;

}

void MainWindow::checkSafetArguments(const QString &s) {
    SafetYAWL::argsflow.clear();
    SafetYAWL::argsflow.resize(10);
    qDebug("...entrando...MainWindow::checkSafetArguments---");
    QString argument = SafetYAWL::getConf()["Widgets/arguments.*"];
    QStringList arguments  = argument.
                     split(SafetYAWL::LISTSEPARATORCHARACTER,QString::SkipEmptyParts);

    QStringList  input = s.split(":");
    if ( input.count() != 2 ) {
        return;
    }

    qDebug("..procesando lista de argumentos....(1)...arguments.count():%d",arguments.count());
    int i = 0;
    foreach(QString a, arguments) {
        QString inputfield = "_"+a;
        qDebug("...inputfield: |%s|", qPrintable(inputfield));
        qDebug("...input.at(0).trimmed(): |%s|", qPrintable(input.at(0).trimmed()));
        if ( inputfield == input.at(0).trimmed() ) {
            qDebug("..procesando i:%d...|%s|",i,qPrintable(input.at(1).trimmed()));
            SafetYAWL::argsflow[i] = input.at(1).trimmed();
        }
        i++;

    }
    qDebug("...i: %d",i);


}


QStringList MainWindow::splitActions(const QString& ls) {
    QStringList result;

    result = ls.split(SafetYAWL::LISTSEPARATORCHARACTER,QString::SkipEmptyParts);


    return result;
}


void MainWindow::toInputForm(const QString& action,bool isgui) {

    Q_CHECK_PTR ( MainWindow::mystatusbar );
     Q_CHECK_PTR ( completingTextEdit );

     MainWindow::mystatusbar->showMessage(tr("Enviando..."));
     SafetTextParser parser(this);


     QString texto;

     if ( action.isEmpty() ) {
        texto = completingTextEdit->toPlainText().toLatin1();
     }
     else {
         _isgui = isgui;
         texto = action;
     }



     SafetYAWL::streamlog.initAllStack();

     if (texto.indexOf(SafetYAWL::ENDOFFLOW) >= 0 ) {
         qDebug("*SafetYAWL::ENDOFFLOW: |%s|", qPrintable(SafetYAWL::ENDOFFLOW));
         QString message =   tr("El estado de un documento no puede cambiar al estado \"%1\"")
                             .arg(SafetYAWL::ENDOFFLOW);
         message.replace("<","&lt;");
         message.replace(">","&gt;");
         SafetYAWL::streamlog
                 << SafetLog::Error
                 << message;
         queryForErrors();
         return;

     }

     parser.setStr( texto.toLatin1() );
     QString str = "agregar,eliminar,actualizar,mostrar";
     QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
     parser.setCommands(str.split(","),commandstr.split(","));

     QString xml = parser.toXml();
     qDebug("xml:\n%s",qPrintable(xml));

     SafetYAWL::streamlog << SafetLog::Debug << tr("Cadena XML generada de la entrada: \n\"%1\"").arg(xml);
     myoutput->setPlainText(xml);

     centralWidget->setTabText(6, tr("Salida*"));

     parser.processInput( xml.toLatin1() );
     parser.openXml();
     QStringList names = parser.loadNameConfs();
     qDebug("...toInputForm...entrando a names...");
     foreach(QString n, names) {
          qDebug("...toInputForm.....names:|%s|", qPrintable(n));
          QMap<QString,QVariant> conf = centraledit()->findConf(n);
          parser.addConf(conf, n);
     }

     QStringList results = parser.processXml(true,true);

     if ( results.isEmpty() ) {
            queryForErrors();
     }
     else {

         QString message = QString("<table><tr><td><font color=green>%1</font>"
                                   "</td></tr></table>").arg(tr("La operaci�n fue exitosa"
                                   "....<b>ok!</b>"));

         if ( isGui()) {
             dockShowResult->addHeadHtml(message);
             dockShowResult->renderMessages(false);
            showShowResultWidget(DockSbMenu::Hide);
            showShowResultWidget(DockSbMenu::Show);
         }

     }

}


void MainWindow::toInputConsole(const QString& action) {
    Q_CHECK_PTR ( MainWindow::mystatusbar );
    Q_CHECK_PTR ( completingTextEdit );



    // icono de progreso en barra de estado
    statusBar()->clearMessage();
    //progressIndicator->startAnimation();
    //statusBar()->addWidget(progressIndicator);
    //progressIndicator->show();

    qDebug("...SafetTextParser parser(this);.....(1)");
    SafetTextParser parser(this);
    qDebug("...SafetTextParser parser(this);.....(2)");
    // Cadena para mostar el enlace en el panel de resultados
    // esta cadena es ahora una variable estatica de MainWindow
    //QString showString;


    QString texto;
    if ( action.isEmpty() ) {
        texto = completingTextEdit->toPlainText().toLatin1();
    }
    else {
        texto = action;
    }


    parser.setStr( texto.toLatin1() );
    QString str = "agregar,eliminar,actualizar,mostrar";
    QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
    parser.setCommands(str.split(","),commandstr.split(","));

    QString xml = parser.toXml();

    SYD << tr("Cadena XML generada de la entrada: \n\"%1\"").arg(xml);
    myoutput->setPlainText(xml);

    centralWidget->setTabText(6, tr("Salida*"));

    parser.processInput( xml.toLatin1() );
    QString filepath = SafetYAWL::getConfFile().getValue("Input", "input.path").toString();
    QString filename = "defconsole.xml";


    SafetYAWL::streamlog.initAllStack();
    parser.openXml(filepath + "/" + filename);
    qDebug("...console...SafetYAWL::streamlog.initAllStack();....(2)...");
    if ( queryForErrors() ) {
        return;
    }

    QStringList results = parser.processXml(false,true);
    if ( results.isEmpty() ) {
        if ( queryForErrors() ) {
            return;
        }
    }


    ParsedSqlToData data;
    showString = QString("<a href=\"operacion:::%1\" title=\"%1\">%1</a>&nbsp;&nbsp;&nbsp;").arg("Ver gr�fico de flujo de trabajo");
    if ( results.count() > 0) {
        data = SafetTextParser::parseSql(results.at(0),true);
        if ( parser.operationName().compare("Listar_datos",Qt::CaseSensitive) == 0 ) {
            showString = QString("<a href=\"operacion:::%1\" title=\"%1\">%1</a>&nbsp;&nbsp;&nbsp;").arg("Ver reporte");
            QString texto = QString("-f %1 -d -v %2")
                               .arg(data.map["Cargar_archivo_flujo"])
                               .arg(data.map["Variable"]);
            parseArgs( texto );

                loadReportTemplate();
                if (! executeParsed() ) {
                    return;
                }


        }
        else if (parser.operationName().compare("Listar_datos_con_autofiltro",Qt::CaseSensitive) == 0 ) {
            showString = QString("<a href=\"operacion:::%1\" title=\"%1\">%1</a>&nbsp;&nbsp;&nbsp;").arg("Ver reporte");
            QString texto = QString("-f %1 -d -v %2 -a %3")
                               .arg(data.map["Cargar_archivo_flujo"])
                               .arg(data.map["Variable"])
                               .arg(data.map["Autofiltro"]);

            qDebug("*Listar_datos_con_autofiltro.....:|%s|",qPrintable(texto));
            parseArgs( texto );

                loadReportTemplate();
                if (! executeParsed() ) {
                    return;
                }



        }
        else if (parser.operationName().compare("Listar_datos_con_filtrorecursivo",Qt::CaseSensitive) == 0 ) {
            showString = QString("<a href=\"operacion:::%1\" title=\"%1\">%1</a>&nbsp;&nbsp;&nbsp;").arg("Ver reporte");
            QString texto = QString("-f %1 -d -v %2 -r %3")
                               .arg(data.map["Cargar_archivo_flujo"])
                               .arg(data.map["Variable"])
                               .arg(data.map["Filtro_recursivo"]);

            qDebug("*Listar_datos_con_filtrorecursivo.....:|%s|",qPrintable(texto));
            parseArgs( texto );

                loadReportTemplate();
                if (! executeParsed() ) {
                    return;
                }



        }
        else if (parser.operationName().compare("Listar_datos_para_clave",Qt::CaseSensitive) == 0 ) {
            showString = QString("<a href=\"operacion:::%1\" title=\"%1\">%1</a>&nbsp;&nbsp;&nbsp;")
                         .arg("Ver reporte");
            QString texto = QString("-f %1 -d -k %2")
                               .arg(data.map["Cargar_archivo_flujo"])
                               .arg(data.map["Clave"]);

            qDebug("***Listar_datos_para_clave.....:|%s|",qPrintable(texto));
            parseArgs( texto );

            qDebug("...**loadReportTemplate();...(1)...");
                loadReportTemplate();
            qDebug("...**loadReportTemplate();...(2)...");
                if (! executeParsed() ) {
                    return;
                }



        }

        else if ( parser.operationName().compare(QString::fromLatin1("Generar_gr�fico_b�sico"),Qt::CaseSensitive) == 0) {
            QString texto = QString("-f %1 -p graphviz -g %2")
                               .arg(data.map["Cargar_archivo_flujo"]);

            parseArgs( texto );
            if (! executeParsed() ) {
                return;
            }


           //processMainWindowThread();
            //return;

        }
        else if ( parser.operationName().compare("Generar_gr�fico_coloreado",Qt::CaseSensitive) == 0) {
            QString texto = QString("-f %1 -p graphviz -g -k coloured")
                               .arg(data.map["Cargar_archivo_flujo"]);

            parseArgs( texto );
            if (! executeParsed() ) {
                qDebug("...no generado grafico coloreado...");
                return;
            }

            MainWindow::currentgraphpath = data.map["Cargar_archivo_flujo"];


        }
        else if ( parser.operationName().compare("Generar_gr�fico_con_autofiltro",Qt::CaseSensitive) == 0) {
            QString texto = QString("-f %1 -p graphviz -g -k coloured -a %2")
                               .arg(data.map["Cargar_archivo_flujo"])
                               .arg(data.map["Autofiltro"]);

            qDebug("Listar_datos_con_autofiltro.....:|%s|",qPrintable(texto));
            parseArgs( texto );
            if (! executeParsed() ) {
                return;
            }


            //processMainWindowThread();
            //return;

        }
        else if ( parser.operationName().compare("Generar_gr�fico_con_filtrorecursivo",Qt::CaseSensitive) == 0) {
            QString texto = QString("-f %1 -p graphviz -g -k coloured -r %2")
                               .arg(data.map["Cargar_archivo_flujo"])
                               .arg(data.map["Filtro_recursivo"]);
            qDebug("**** FILTRO RECURSIVO TEXTO: ||%s||", qPrintable(texto));
            parseArgs( texto );
            qDebug( "****  commands.count(): %d", commands.count());
            if (! executeParsed() ) {
                return;
            }


            //processMainWindowThread();
            //return;

        }
        else if (parser.operationName().compare("Generar_gr�fico_para_clave",Qt::CaseSensitive) == 0) {
            QString texto = QString("-f %1 -p graphviz -g -k %2 ")
                               .arg(data.map["Cargar_archivo_flujo"])
                               .arg(data.map["Clave"]);
            qDebug("**** TEXTO: ||%s||", qPrintable(texto));
            parseArgs( texto );
            qDebug( "****  commands.count(): %d", commands.count());
            if (! executeParsed() ) {
                return;
            }


            //processMainWindowThread();
            //return;
        }

    }

    Q_CHECK_PTR(dockShowResult);
    QString message = QString("<table><tr><td><font color=green>%1</font>"
                                 "</td></tr></table>").arg(tr("Consulta fue exitosa"
                                 "....<b>ok!</b><br/>%1").arg(showString));;
    dockShowResult->addHeadHtml(message);
    dockShowResult->renderMessages(false);
    showShowResultWidget(DockSbMenu::Hide);
    showShowResultWidget(DockSbMenu::Show);
    //statusBar()->showMessage(tr("Gesti�n de Firmas.....Enviando......ok!"));
    statusBar()->removeWidget(progressIndicator);
    statusBar()->showMessage(tr("ok!"), 3000);

}

void MainWindow::threadEndJob()
{



    Q_CHECK_PTR(dockShowResult);
    QString message = QString("<table><tr><td><font color=green>%1</font>"
                              "</td></tr></table>").arg(tr("Consulta fue exitosa"
                              "....<b>ok!</b><br/>%1").arg(showString));
    dockShowResult->addHeadHtml(message);
    dockShowResult->renderMessages(false);
    showShowResultWidget(DockSbMenu::Hide);
    showShowResultWidget(DockSbMenu::Show);

    //habilitar botones de enviar
    completingButtonForm->setEnabled(true);
    completingButtonCons->setEnabled(true);
    completingButtonSign->setEnabled(true);
    completingButtonConf->setEnabled(true);
    completingButtonUsers->setEnabled(true);

    // habilitar boton de enviar de la barra de herramientas
    standardbar->actionAt(60,30)->setEnabled(true);

    statusBar()->removeWidget(progressIndicator);
    statusBar()->showMessage(tr("ok!"), 3000);

}

void MainWindow::processMainWindowThread(){
    // deshabilitar el boton de enviar consulta hasta que termine el thread
    completingButtonForm->setEnabled(false);
    completingButtonCons->setEnabled(false);
    completingButtonSign->setEnabled(false);
    completingButtonConf->setEnabled(false);
    completingButtonUsers->setEnabled(false);
    // deshabilitar el boton de enviar de la barra de herramientas
    standardbar->actionAt(60,30)->setEnabled(false);

//    qDebug("despues de myThreadConsole->start();");
}

void MainWindow::toInputSign(const QString& action) {

    qDebug("....MainWindow::toInputSign....(*1*)....");

    Q_CHECK_PTR ( MainWindow::mystatusbar );
    Q_CHECK_PTR ( completingTextEdit );
    showShowResultWidget(DockSbMenu::Hide);
    // icono de progreso en barra de estado
    statusBar()->clearMessage();
    progressIndicator->startAnimation();
    statusBar()->addWidget(progressIndicator);
    progressIndicator->show();


    SafetTextParser parser(this);
    QString texto;
    if (action.isEmpty()) {
        texto = completingTextEdit->toPlainText().toLatin1();
    }
    else {
        texto = action;
    }
        SYD << tr("*****toInputSign....texto: \n|%1|\n").arg(texto);


    parser.setStr( texto.toLatin1() );
    QString str = "agregar,eliminar,actualizar,mostrar";
    QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
    parser.setCommands(str.split(","),commandstr.split(","));
    QString xml = parser.toXml();
    SYD << tr("Cadena XML generada de la entrada: \n\"%1\"").arg(xml);
    myoutput->setPlainText(xml);


    centralWidget->setTabText(6, tr("Salida*"));
    parser.processInput( xml.toLatin1() );
    QString filepath = SafetYAWL::getConfFile().getValue("Input", "input.path").toString();
    QString filename = "defmanagementsignfile.xml";

    parser.openXml(filepath + "/" + filename);


    QStringList results = parser.processXml(false,true);
    if ( queryForErrors() ) {
        return;
    }

    SYD << tr("...........MainWindow::toInputSign...name operation: |%1|")
           .arg(parser.operationName());

    SYD << tr("...........MainWindow::toInputSign...results.count() |%1|")
           .arg(results.count());
    if (results.count() ==  0 ) {
        SYD << tr("Sin resultados");
        SYW << tr("No existen resultados para ejecutar");
        return;
    }

    ParsedSqlToData data;
    data = SafetTextParser::parseSql(results.at(0),true);
    SYD << tr("...........MainWindow::toInputSign...results.at(0):|%1|")
           .arg(results.at(0));
    SYD << tr("...........MainWindow::toInputSign...data.map.keys():|%1|")
           .arg(data.map.keys().count());

    foreach(QString k, data.map.keys()) {

        SYD << tr("...........MainWindow::toInputSign...key:|%1|->|%2|")
               .arg(k)
               .arg(data.map[k]);
    }


        QString fname;
    if (parser.operationName().startsWith("Crear_nuevo_grafo")) {

        QString title = MainWindow::convertTitleToOp(data.map["Nombre_del_grafo"]);
        fname = SafetYAWL::pathconf + "/" + "flowfiles/" + title + ".xml";

        SYD << tr("...........MainWindow::toInputSign...saliendo...fname:|%1|")
               .arg(fname);

        QString titletask;

        if (data.map.contains("Titulo_tarea")) {
            titletask = data.map["Titulo_tarea"];
        }
        SYD << tr("...........MainWindow::toInputSign...saliendo...titulo de la tarea:|%1|")
               .arg(titletask);


        QString firsttask = "Tarea1";


        if (data.map.contains("Nombre_tarea")) {
            firsttask = data.map["Nombre_tarea"];
        }


        QFile myflowfile(fname);
        if ( !myflowfile.open(QIODevice::WriteOnly))  {
            SYE << tr("No es posible escribir el archivo \"%1\"")
                   .arg(fname);
            return;
        }
        QTextStream outflow(&myflowfile);
        SYD << tr(".......MainWindow::toInputSign...escribiendo...(1)...");
        outflow << Safet::TEMPLATEFLOWFILE
                   .arg(data.map["Campo_de_la_clave"]) // 0+1
                .arg(data.map["Tabla_para_clave"]) // 1
                .arg(data.map["Campo_para_estado"]) // 2
                .arg(data.map["Opcion_para_estado"]) // 3
                .arg(data.map["Campos_variable"].replace(";",",")) // 4
                .arg(title) // 5
                .arg(QDir::homePath()) // 6
                .arg(firsttask) // 7
                .arg(titletask) // 8
                .arg(fname); // 9 +1


         SYD << tr(".......MainWindow::toInputSign...escribiendo...(2)...");


        myflowfile.close();


        SYD << tr(".......MainWindow::toInputSign...saliendo...");
    }
    else if (parser.operationName().startsWith(tr("Borrar_nodo_de_gr�fico"))) {
        SYD << tr(".....MainWindow::toInputConsole....Borrar_nodo_de_gr�fico......(1)...");
        QString namegraph = MainWindow::replaceMarks(data.map["Cargar_archivo_flujo"]);
        QString nodedel = MainWindow::replaceMarks(data.map["Nodo_a_borrar"]);
        SYD << tr(".........MainWindow::toInputSign....namegraph:|%1|").arg(namegraph);
        SYD << tr(".........MainWindow::toInputSign....nodedel:|%1|").arg(nodedel);
        fname = namegraph;
        SafetYAWL::streamlog.initAllStack();

        qDebug("...Borrar_Nodo...:");

//        if ( true ) {
//            foreach(QString mykey, MainWindow::mymainwindow ->graphs().keys() ) {
//                QPair<QString,QString> mypair = MainWindow::mymainwindow ->graphs()[ mykey ];
//                qDebug("...Borrar_Nodo....mykey:|%s|",qPrintable(mykey));
//                qDebug("...Borrar_Nodo....mypair.first:|%s|",qPrintable(mypair.first));
//                qDebug("...Borrar_Nodo....mypair.second:|%s|",qPrintable(mypair.second));


//            }

//            return;
//        }
        QString result = delNodeToXMLWorkflow(namegraph,nodedel);

        if ( queryForErrors() ) {
                return ;
        }


        if (!result.isEmpty()) {
            _currentjson = tr("Eliminado nodo \"%1\" de \"%2\" satisfactoriamente!")
                    .arg(nodedel)
                    .arg(result);

        }



    }
    else if (parser.operationName().startsWith(tr("Cambiar_conexi�n_de_gr�fico_de_flujo"))) {
        SYD << tr(".....MainWindow::toInputConsole....Cambiar_conexi�n_de_gr�fico_de_flujo...:");
        QString namegraph = MainWindow::replaceMarks(data.map[Safet::LOADFLOWFILE_NAMEFIELD]);
        QString nodesource = MainWindow::replaceMarks(data.map["Nodo_origen"]);
        QString nodetarget = MainWindow::replaceMarks(data.map["Nodo_destino_actual"]);
        QString newnodetarget = MainWindow::replaceMarks(data.map["Nuevo_nodo_destino"]);


        SYD << tr("---MainWindow::toInputConsole....(change)namegraph:|%1|").arg(namegraph);
        SYD << tr("---MainWindow::toInputConsole....(change)nodesource:|%1|").arg(nodesource);
        SYD << tr("---MainWindow::toInputConsole....(change)nodetarget:|%1|").arg(nodetarget);
        SYD << tr("---MainWindow::toInputConsole....(change)newnodetarget:|%1|").arg(newnodetarget);



        SafetYAWL::streamlog.initAllStack();
        QString result = changeConnXMLWorkflow(namegraph,
                                               nodesource,
                                               nodetarget,
                                               newnodetarget);
        if (!result.isEmpty()) {
            _currentjson = tr("Se cambi� la conexi�n en el grafo  \"%1\" de \"%2\""
                              "a \"%3\" satisfactoriamente!\n")
                    .arg(result)
                    .arg(nodetarget)
                    .arg(newnodetarget);
        }
        else {
            queryForErrors();
            return;
        }

    }
    else if (parser.operationName().startsWith(tr("Crear_CRUD"))) {

        qDebug(("..............Data..Crear_CRUD...1"));

        SafetYAWL::streamlog.initAllStack();

        bool result = doCreateCRUD(data);

        if (!result ) {
            qDebug(".....not result....doCreateCRUD");

            queryForErrors();
            return;
            //_currentjson = "No se ejecuta la sentencia!";
        }


    }
    else if (parser.operationName().startsWith(tr("Agregar_nodo_a_gr�fico_de_flujo"))) {
              SYD << tr(".....MainWindow::toInputConsole....Agregar_nodo_a_gr�fico_de_flujo...:");
              QString namegraph = MainWindow::replaceMarks(data.map["Cargar_archivo_flujo"]);
              QString beforenode = MainWindow::replaceMarks(data.map["Nodo_anterior"]);
              QString newnamenode = MainWindow::replaceMarks(data.map["Nombre_nuevo_nodo"]);
              QString paralell = MainWindow::replaceMarks(data.map["Es_paralelo_al_nodo_anterior"]);
              QString foptions = MainWindow::replaceMarks(data.map["Campo_options"]);
              QString fquery = MainWindow::replaceMarks(data.map["Campo_query"]);
              QString ftitle = MainWindow::replaceMarks(data.map["Campo_title"]);
              QString fdocumentsource = MainWindow::replaceMarks(data.map["Campo_documentsource"]);
              QString ftextualinfo = MainWindow::replaceMarks(data.map["Textualinfo"]);


              SYD << tr("---MainWindow::toInputConsole....namegraph:|%1|").arg(namegraph);
              SYD << tr("---MainWindow::toInputConsole....beforenode:|%1|").arg(beforenode);
              SYD << tr("---MainWindow::toInputConsole....newnamenode:|%1|").arg(newnamenode);
              SYD << tr("---MainWindow::toInputConsole....paralell:|%1|").arg(paralell);
              SYD << tr("---MainWindow::toInputConsole....exist paralell:|%1|").arg(data.map.contains("Es_paralelo_al_nodo_anterior"));
              SYD << tr("---MainWindow::toInputConsole....foptions:|%1|").arg(foptions);
              SYD << tr("---MainWindow::toInputConsole....exist foptions:|%1|").arg(data.map.contains("Campo_options"));
              SYD << tr("---MainWindow::toInputConsole....fquery:|%1|").arg(fquery);
              SYD << tr("---MainWindow::toInputConsole....ftitle:|%1|").arg(ftitle);
              SYD << tr("....MainWindow::toInputConsole....fdocumentsource:|%1|").arg(fdocumentsource);
              SYD << tr("....MainWindow::toInputConsole....ftextualinfo:|%1|").arg(ftextualinfo);

                    QString myrolfield = SafetYAWL::getConf()["DefaultValues/design.rolfield"];
                     QString mytsfield = SafetYAWL::getConf()["DefaultValues/design.timestampfield"];
                     QString myqueryfield = SafetYAWL::getConf()["DefaultValues/design.query"];




             if (fquery.isEmpty() ) {
                 if (!myqueryfield.isEmpty()) {
                        fquery = myqueryfield;
                 }
             }


             SYD << tr("....MainWindow::toInputConsole....DEFAULT...rolfield:**|%1|").arg(myrolfield);
             SYD << tr("....MainWindow::toInputConsole....DEFAULT...mytsfield:**|%1|").arg(mytsfield);
             SYD << tr("....MainWindow::toInputConsole....DEFAULT...MYQUERYFIELD...myqueryfield:**|%1|")
                    .arg(myqueryfield);



//              if ( true ) {
//                  return;
//              }



              bool isparalell = false;

              if (paralell == "Si") {
                  isparalell = true;
              }

              SafetYAWL::streamlog.initAllStack();
              QString result = addNodeToXMLWorkflow(namegraph,beforenode,newnamenode,isparalell,foptions,
                                   fquery,ftitle,fdocumentsource,myrolfield,mytsfield,ftextualinfo);



              if (!result.isEmpty()) {
                  _currentjson = tr("Agregado nodo \"%1\" a \"%2\" satisfactoriamente!")
                      .arg(newnamenode)
                          .arg(result);
              }
              else {
                   queryForErrors();
                   return ;
              }

          }




    statusBar()->showMessage(tr("Enviando......ok!"));
    QString message = QString("<table><tr><td><font color=green>%1</font>"
                              "</td></tr></table>").arg(tr("La operaci�n de <b>grafos</b> fue exitosa.<br/> Se escribi� el archivo \"%1\" "
                                                           "....<b>ok!</b>").arg(fname));

    if (!_currentjson.isEmpty()) {
        message = QString("<table><tr><td><font color=green>%1</font>"
                                      "</td></tr></table>").arg(tr("%1 "
                                                                   "....<b>ok!</b>").arg(_currentjson));

    }

    dockShowResult->addHeadHtml(message);
    dockShowResult->renderMessages(false);
    showShowResultWidget(DockSbMenu::Hide);
    showShowResultWidget(DockSbMenu::Show);

}


uint MainWindow::numberOfTask(const QDomNodeList& list) {
    uint result = 0;


    for(int i=0; i< list.count();i++) {
        QDomNode n = list.at(i);
        if (n.nodeName().compare("task",Qt::CaseInsensitive) == 0) {
            result++;
        }
    }


    return result;
}

QString MainWindow::delNodeToXMLWorkflow(const QString& fname,
                          const QString& nodename) {
    QString result;
    QDomDocument doc;
    QFile file(fname);
    if (!QFile::exists(fname) ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no existe en el sistema de archivos"
                  ".No es posible agregar un estado al archivo")
               .arg(fname);

        _currentjson = tr("El archivo de flujo de trabajo \"%1\" no existe en el sistema de archivos"
                          ".No es posible agregar un estado al archivo")
                       .arg(fname);
        return result;
    }

    bool isopen = file.open(QIODevice::ReadOnly);
    if (!isopen ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no se puede leer"
                  ".No es posible agregar un estado al archivo")
               .arg(fname);

        _currentjson = tr("El archivo de flujo de trabajo \"%1\" no se puede leer"
                          ".No es posible agregar un estado al archivo")
                       .arg(fname);

        return result;
    }


    bool iscontent = doc.setContent(&file);

    if (!iscontent) {
        SYE << tr("El formato del archivo XML \"%1\" no es v�lido")
               .arg(fname);
        _currentjson = tr("El formato del archivo XML \"%1\" no es v�lido")
                .arg(fname);
        return result;
    }
    file.close();
    QDomElement docElem = doc.documentElement();
    QDomNode root = docElem.firstChild();


    QDomNode n = root.firstChild();

    QDomElement mydelelement, oldbeforeelement;

    QList<QDomElement> beforeelements;
    QString afternode;

    /**
     *  FIXME: Hacer funci�n para chequear el n�mero de tarea reales
     */
    int ncount = MainWindow::numberOfTask(root.childNodes());
    SYD << tr(".....................MainWindow::delNodeToXMLWorkflow....node count:|%1|")
           .arg(ncount);

    if (ncount < 2 ) {


        SYE << tr("El gr�fico \"%1\"  contiene un �nico nodo. No es posible borrarlo")
               .arg(fname);

        SYD << tr("..................MainWindow::delNodeToXMLWorkflow...result:|%1|")
               .arg(result);

        return result;
    }

    while( !n.isNull() ) {

        if ( n.isElement() ) {
            QDomElement e = n.toElement(); // try to convert the node to an element.
            QDomNamedNodeMap attributeMap = e.attributes();
            QDomNode attribute = attributeMap.namedItem("id");
            if (!attribute.isNull()) {
                QString myid = attribute.nodeValue();
                SYD << tr("...***MainWindow::delNodeToXMLWorkflow..id:|%1|")
                       .arg(myid);

                QDomElement myport = e.firstChildElement("port");
                QDomElement myconn = myport.firstChildElement("connection");

                if (myid == nodename ) {
                    mydelelement  = e;
                    afternode = myconn.attribute("source").simplified();
                    n = n.nextSibling();
                    continue;
                }

                while( !myconn.isNull()) {
                    if (myconn.attribute("source").simplified() == nodename) {
                        qDebug("....delNodeToXmlWorkflow...beforenode..:|%s|",
                               qPrintable(myid));
                        beforeelements.append(e);
                    }
                    myconn = myconn.nextSiblingElement("connection");
                }
            }
            else {
                qDebug("...No atributo ID");

            }

        }

        n = n.nextSibling();
    }

    if (afternode.isEmpty()) {
        SYE << tr("Error en los enlaces del flujo de trabajo (enlace del siguiente nodo)");
        return result;
    }



    foreach(QDomElement beforeelement, beforeelements) {
        // cambiar enlace del anterior  (beforenode)
        oldbeforeelement = beforeelement;
        QDomElement myport = beforeelement.firstChildElement("port");
        QDomElement oldport = myport;
        QDomElement myconn, oldconn, searchconn;
        oldconn = myconn;
        int howconns = 0;

        myconn = myport.firstChildElement("connection");

        while(!myconn.isNull()) {
            qDebug("....before...myconn.attribute:|%s|", qPrintable(myconn.attribute("source")));
            if ( myconn.attribute("source").simplified() == nodename ) {
                searchconn = myconn;
                oldconn = myconn;
            }
            howconns++;
            myconn = myconn.nextSiblingElement("connection");
        }

        Q_ASSERT(howconns >= 1);
        Q_ASSERT(!searchconn.isNull());

        if ( howconns > 1 ) {

            myport.removeChild(searchconn);
            howconns--;
            if (howconns == 1 ) {
                myport.setAttribute("pattern","none");
            }
        }
        else {
            // Obtener el siguiente elemento
            QDomElement mydelport = mydelelement.firstChildElement("port");
            QDomElement mydelconn = mydelport.firstChildElement("connection");

            searchconn.setAttribute("options",mydelconn.attribute("options"));
            searchconn.setAttribute("query",mydelconn.attribute("query"));
            searchconn.setAttribute("source",mydelconn.attribute("source"));
            myport.replaceChild(searchconn,oldconn);

        }

        beforeelement.replaceChild(myport, oldport);
        root.replaceChild(beforeelement,oldbeforeelement);
    }



    // eliminar el nodo

    root.removeChild(mydelelement);

    QFile myfile(fname);
    if ( !myfile.open(QIODevice::WriteOnly))  {
                SYE << tr("No es posible escribir el archivo \"%1\" "
                          "con el nodo \"%2\" agregado")
                .arg(fname)
                       .arg(nodename);
                qDebug("error open file writeonly");
        return result;
    }

    result = fname;

    QTextStream out(&myfile);

    out << doc.toString();

    myfile.close();

    return result;
}



QString MainWindow::replaceMarks(const QString& s) {
    QString result = s;

    result.replace(Safet::COLONMARK,":");
    return result;
}

QString MainWindow::changeConnXMLWorkflow(const QString& fname,
                                          const QString& currnode,
                                          const QString& nextnode,
                                          const QString& newnode,
                                          const QString& newoptions,
                                          const QString& newquery) {
    QString result;
    QDomDocument doc;
    QFile file(fname);
    if (!QFile::exists(fname) ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no existe en el sistema de archivos"
                  ".No es posible cambiar la conexi�n en el archivo")
               .arg(fname);
        return result;
    }

    bool isopen = file.open(QIODevice::ReadOnly);
    if (!isopen ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no se puede leer"
                  ".No es posible cambiar la conexi�n en el archivo")
               .arg(fname);

        return result;
    }


    bool iscontent = doc.setContent(&file);
    if (!iscontent) {
        SYE << tr("El formato del archivo XML \"%1\" no es v�lido")
               .arg(fname);
        return result;
    }

    file.close();
    QDomElement docElem = doc.documentElement();
    QDomNode root = docElem.firstChild();

    qDebug("...MainWindow::changeConnXMLWorkflow.....(1)...");


    QDomNode n = root.firstChild();


    QDomElement mynewconn, oldport, olde;

    QString afternode;
    while( !n.isNull() ) {

        if ( n.isElement() ) {
            QDomElement e = n.toElement(); // try to convert the node to an element.
            olde = e;
            QDomNamedNodeMap attributeMap = e.attributes();
            QDomNode attribute = attributeMap.namedItem("id");
            if (!attribute.isNull()) {
                QString myid = attribute.nodeValue();
                qDebug("...*MainWindow::changeNodeXMLWorkflow..id:|%s|",
                       qPrintable(myid));

                if (myid == currnode) {
                    QDomElement myport = e.firstChildElement("port");
                    oldport = myport;
                    QDomElement myconn = myport.firstChildElement("connection");
                    while(!myconn.isNull()) {
                            afternode = myconn.attribute("source");
                            if (afternode == nextnode) {
                                mynewconn = myconn;
                                mynewconn.setAttribute("source",newnode);
                                if (newoptions.isEmpty()) {
                                   mynewconn.setAttribute("options",newnode);
                                }
                                else {
                                   mynewconn.setAttribute("options",newoptions);
                                }

                                if (!newquery.isEmpty()) {
                                   mynewconn.setAttribute("query",newquery);
                                }
                                myport.replaceChild(mynewconn,myconn);
                                e.replaceChild(myport,oldport);
                                root.replaceChild(e,olde);
                                break;
                            }
                            myconn = myconn.nextSiblingElement("connection");
                    }
                }

            }

        }

        n = n.nextSibling();
    }

    qDebug("...MainWindow::changeConnXMLWorkflow.....(2)...:|%s|",
           qPrintable(fname));
    QFile myfile(fname);
    if ( !myfile.open(QIODevice::WriteOnly))  {
                SYE << tr("No es posible escribir el archivo \"%1\" con el nodo \"%2\" cambiado")
                .arg(fname)
                       .arg(currnode);
                qDebug("error open file writeonly");
        return result;
    }

    result = fname;

    QTextStream out(&myfile);

    out << doc.toString();

    myfile.close();


    return result;

}


// *** generatePagesGraph


QString MainWindow::genPagesXMLWorkflow(const QString& fname, const QString& tablename) {
    QString result;
    QDomDocument doc;
    QFile file(fname);
    if (!QFile::exists(fname) ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no existe en el sistema de archivos"
                  ".No es posible cambiar la conexi�n en el archivo")
               .arg(fname);
        return result;
    }

    bool isopen = file.open(QIODevice::ReadOnly);
    if (!isopen ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no se puede leer"
                  ".No es posible cambiar la conexi�n en el archivo")
               .arg(fname);

        return result;
    }


    bool iscontent = doc.setContent(&file);
    if (!iscontent) {
        SYE << tr("El formato del archivo XML \"%1\" no es v�lido")
               .arg(fname);
        return result;
    }

    file.close();
    QDomElement docElem = doc.documentElement();
    QDomNode root = docElem.firstChild();

    qDebug("...MainWindow::changeConnXMLWorkflow.....(1)...");


    QDomNode n = root.firstChild();


    QDomElement mynewconn, oldport, olde;

    QString afternode;
    while( !n.isNull() ) {

        if ( n.isElement() ) {
            QDomElement e = n.toElement(); // try to convert the node to an element.
            olde = e;
            QDomNamedNodeMap attributeMap = e.attributes();
            QDomNode attribute = attributeMap.namedItem("id");
            if (!attribute.isNull()) {
                QString myid = attribute.nodeValue();
                qDebug("...*MainWindow::changeNodeXMLWorkflow..id:|%s|",
                       qPrintable(myid));

                if (false) {
                    QDomElement myport = e.firstChildElement("port");
                    oldport = myport;
                    QDomElement myconn = myport.firstChildElement("connection");
                    while(!myconn.isNull()) {
                            afternode = myconn.attribute("source");
                            myconn = myconn.nextSiblingElement("connection");
                    }
                }

            }

        }

        n = n.nextSibling();
    }

    qDebug("...MainWindow::changeConnXMLWorkflow.....(2)...:|%s|",
           qPrintable(fname));
    QFile myfile(fname);
    if ( !myfile.open(QIODevice::WriteOnly))  {
                SYE << tr("No es posible escribir el archivo \"%1\" con el nodo cambiado")
                .arg(fname);
                qDebug("error open file writeonly");
        return result;
    }

    result = fname;

    QTextStream out(&myfile);

    out << doc.toString();

    myfile.close();


    return result;

}








// *** addNode

QString MainWindow::addNodeToXMLWorkflow(const QString& fname,
                          const QString& beforenode,
                          const QString& nodename,
                          bool isparallel,
                          const QString& options,
                          const QString& query,
                          const QString& nodetitle,
                          const QString &documentsource,
                          const QString &rolfield,
                          const QString &tsfield,
                          const QString &textualinfo


                          ) {

    SYD << tr("... MainWindow::addNodeToXMLWorkflow....(1)...query:|%1|")
           .arg(query);
    QString result;
    QDomDocument doc;
    QFile file(fname);
    if (!QFile::exists(fname) ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no existe en el sistema de archivos"
                  ".No es posible agregar un estado al archivo")
               .arg(fname);

        return result;
    }

    bool isopen = file.open(QIODevice::ReadOnly);
    if (!isopen ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no se puede leer"
                  ".No es posible agregar un estado al archivo")
               .arg(fname);


        return result;
    }


    bool iscontent = doc.setContent(&file);

    if (!iscontent) {
        SYE << tr("El formato del archivo XML \"%1\" no es v�lido")
               .arg(fname);
        return result;
    }

    file.close();
    QDomElement docElem = doc.documentElement();
    QDomNode root = docElem.firstChild();

    qDebug("...MainWindow::addNodeToXMLWorkflow.....(1)...");


    QDomNode n = root.firstChild();


    QDomElement mynewelement, beforeelement, afterelement;

    QString afternode;
    while( !n.isNull() ) {

        if ( n.isElement() ) {
            QDomElement e = n.toElement(); // try to convert the node to an element.
            QDomNamedNodeMap attributeMap = e.attributes();
            QDomNode attribute = attributeMap.namedItem("id");
            if (!attribute.isNull()) {
                QString myid = attribute.nodeValue();
                qDebug("...*MainWindow::addNodeToXMLWorkflow..id:|%s|",
                       qPrintable(myid));

                if (myid == beforenode) {
                    qDebug("...*MainWindow::addNodeToXMLWorkflow..beforenode id:|%s|",
                           qPrintable(myid));

                    beforeelement = e;
                    QDomElement myport = beforeelement.firstChildElement("port");
                    QDomElement myconn = myport.firstChildElement("connection");
                    afternode = myconn.attribute("source");
                }
                if (myid == afternode) {
                    qDebug("...*MainWindow::addNodeToXMLWorkflow..afternode id:|%s|",
                           qPrintable(myid));

                    afterelement = e;
                    break;
                }


            }
            else {
                qDebug("...No atributo ID");

            }

        }

        n = n.nextSibling();
    }

    if (afternode.isEmpty()) {
        SYE << tr("Error en los enlaces del flujo de trabajo");
        qDebug("afternode is empty");
        return result;
    }
    QDomElement olde = beforeelement;

    // Connection After Node
    QDomElement myafterport, oldp;
    QDomElement myafterconn, oldc;

    myafterport = afterelement.firstChildElement("port");
    myafterconn = myafterport.firstChildElement("connection");
    QString myaftersource = myafterconn.attribute("source");
    QString myafteroptions = myafterconn.attribute("options");
    QString myafterquery = myafterconn.attribute("query");
   QString myoldoption;

    SYD << tr("... MainWindow::addNodeToXMLWorkflow....(1)...myafterquery:|%1|")
           .arg(myafterquery);

    qDebug("... MainWindow::addNodeToXMLWorkflow....(1)...myaftersource:|%s|",
          qPrintable(myaftersource) );



    mynewelement = beforeelement.cloneNode().toElement();
    QString oldnodename = mynewelement.attribute("id");
    mynewelement.setAttribute("id",nodename);
    mynewelement.setAttribute("title",nodetitle);
    mynewelement.setAttribute("textualinfo",textualinfo);

    if ( isparallel ) {
        QDomElement mylastconn;
        myafterport = mynewelement.firstChildElement("port");
        myafterport.setAttribute("pattern","none");

        oldp = myafterport;
        myafterconn = myafterport.firstChildElement("connection");        
             oldc = myafterconn;
        mylastconn = myafterport.lastChildElement("connection");

        while (myafterconn != mylastconn) {
            qDebug("removing last conection...(1)...");
            myafterport.removeChild(mylastconn);
            mylastconn = myafterport.lastChildElement("connection");
        }


        myafterconn.setAttribute("source",myaftersource);
        qDebug("... MainWindow::addNodeToXMLWorkflow....(1)...setting...myaftersource:|%s|",
              qPrintable(myaftersource) );

        myafterconn.setAttribute("query",myafterquery);
        myafterconn.setAttribute("options",myafteroptions);

        myafterport.replaceChild(myafterconn,oldc);
        mynewelement.replaceChild(myafterport,oldp);
    }
    myoldoption = options;

    SYD << tr("...myoldoption...OLDOPTION:|%1|").arg(myoldoption);


    if (myoldoption.isEmpty()) {
         myoldoption = nodename;
    }

    // Variable
    QDomElement myvariable = mynewelement.firstChildElement("variable");
    QDomElement oldvariable = myvariable;

    myvariable.setAttribute("id",QString("v%1")
                            .arg(nodename));
    if (!documentsource.isEmpty()) {
        myvariable.setAttribute("documentsource",
                                documentsource);
    }
    QString myrolfield = myvariable.attribute("rolfield");
    QString timestampfield = myvariable.attribute("timestampfield");

    if (!rolfield.isEmpty()) {

        myrolfield = rolfield;
        myrolfield.replace(QString("{#OPTIONSVALUE}"),myoldoption);
    }
    else {
        myrolfield.replace(QString("'%1'").arg(oldnodename),QString("'%1'").arg(nodename));
    }

    if (!tsfield.isEmpty()) {

        timestampfield = tsfield;
        timestampfield.replace(QString("{#OPTIONSVALUE}"),myoldoption);
    }
    else {
      timestampfield.replace(QString("'%1'").arg(oldnodename),QString("'%1'").arg(nodename));
    }

    SYD << tr("..............MainWindow::addNodeToXMLWorkflow....REPLACING ROLFIELD...myrolfield:|%1|")
           .arg(myrolfield);

    SYD << tr("..............MainWindow::addNodeToXMLWorkflow....REPLACING TSFIELD...mytsfield:|%1|")
           .arg(timestampfield);


    myvariable.setAttribute("rolfield",myrolfield);
    myvariable.setAttribute("timestampfield",timestampfield);

    // reemplazando variable
    mynewelement.replaceChild(myvariable,oldvariable);


    root.insertAfter(mynewelement,beforeelement);

    // Connection
    QDomElement myport = beforeelement.firstChildElement("port");
    QDomElement oldport = myport;
    QDomElement myconn, oldconn;
    myconn = myport.firstChildElement("connection");
    oldconn = myconn;
    if (isparallel ) {
        myconn = myconn.cloneNode().toElement();
    }



    myconn.setAttribute("source",nodename);
    if (!query.isEmpty()) {
        QRegExp rx(Safet::SELECTPATTERN);
        rx.setCaseSensitivity(Qt::CaseInsensitive);
        QString newquery = query;
        if (query.indexOf(rx) == -1 ) {
            QString ds = myvariable.attribute("documentsource");
            SYD << tr("...........MainWindow::addNodeToXMLWorkflow...ds:|%1|")
                   .arg(ds);

            rx.setPattern(Safet::SELECTPATTERN);
            int pos = ds.indexOf(rx);
            SYD << tr("...........MainWindow::addNodeToXMLWorkflow...pos:|%1|")
                   .arg(pos);

            newquery = QString("select  %1 from %2")
                    .arg(query)
                    .arg(rx.cap(2));
            SYD << tr("...........MainWindow::addNodeToXMLWorkflow...newquery:|%1|")
                   .arg(newquery);

        }

        myconn.setAttribute("query",newquery);
    }
    if (!options.isEmpty()) {
        myconn.setAttribute("options",options);
    }
    else {
        myconn.setAttribute("options",nodename);
    }
    if (isparallel) {
        myport.appendChild(myconn);
        myport.setAttribute("pattern", "or");
        beforeelement.replaceChild(myport, oldport);
    }
    else {
        myport.replaceChild(myconn,oldconn);
    }
    beforeelement.replaceChild(myport, oldport);

    // Autofilter
    if (isparallel) {
        QDomElement myautofilter = mynewelement.firstChildElement("autofilter");
        while(!myautofilter.isNull()) {

            QDomElement oldautofilter = myautofilter;
            myautofilter.setAttribute("source",myaftersource);
            mynewelement.replaceChild(myautofilter,oldautofilter);
            myautofilter = myautofilter.nextSiblingElement("autofilter");
        }

    }
    else {
        QDomElement myautofilter = mynewelement.firstChildElement("autofilter");
        while(!myautofilter.isNull()) {

            QDomElement oldautofilter = myautofilter;
            myautofilter.setAttribute("source",nodename);
            mynewelement.replaceChild(myautofilter,oldautofilter);
            myautofilter = myautofilter.nextSiblingElement("autofilter");
        }
    }

    root.replaceChild(beforeelement, olde);


    QFile myfile(fname);
    if ( !myfile.open(QIODevice::WriteOnly))  {
                SYE << tr("No es posible escribir el archivo \"%1\" con el nodo \"%2\" agregado")
                .arg(fname)
                       .arg(nodename);
        return result;
    }

    result = fname;

    QTextStream out(&myfile);

    out << doc.toString();

    myfile.close();

    return result;
}









QString MainWindow::convertOpToTitle(const QString& op) {
    QString result;
    if ( op.isEmpty()) {
        return op;
    }

    result = op;
    result.replace("operacion:","");
    result.replace("_"," ");
    QStringList mylist = result.split(QRegExp("\\b"),QString::SkipEmptyParts);

    if (mylist.isEmpty()) {
        return op;
    }

    bool isupper = true;
    QString myword = mylist.at(0);
    for (int j=0; j < myword.length();j++) {
        if (!myword.at(j).isUpper()) {
            isupper = false;
            break;
        }
    }
    if ( !isupper ) {
        result = myword.mid(0,1).toUpper()+myword.mid(1).toLower();
    }
    else {
        result = myword;
    }
    for(int i=1; i < mylist.count();i++) {
        QString myword = mylist.at(i);
        bool isupper = true;
        for (int j=0; j < myword.length();j++) {
            if (!myword.at(j).isUpper()) {
                isupper = false;
                break;
            }
        }
        if ( !isupper ) {
           myword =  myword.toLower();
        }
        result += " ";
        result += myword;
    }



    return result;
}

QString MainWindow::convertTitleToOp(const QString& op) {
    QString result = op ;

    result.replace(QRegExp("\\s+"),"_");


    return result;
}



void MainWindow::toInputConfigure() {

     Q_CHECK_PTR ( MainWindow::mystatusbar );
     Q_CHECK_PTR ( completingTextEdit );
     MainWindow::mystatusbar->showMessage(tr("Enviando..."));
     SafetTextParser parser(this);
     QString texto = completingTextEdit->toPlainText().toLatin1();
     qDebug("**.. ...(1)..texto: \n|%s|\n", qPrintable(texto));

     parser.setStr( texto.toLatin1() );
     QString str = "agregar,eliminar,actualizar,mostrar";
     QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
     parser.setCommands(str.split(","),commandstr.split(","));
     qDebug("....xml...(1): \n|%s|\n", qPrintable(parser.str()));
     QString xml = parser.toXml();
     qDebug("....xml...(2): \n|%s|\n", qPrintable(xml));
     SafetYAWL::streamlog << SafetLog::Debug << tr("Cadena XML generada de la entrada: \n\"%1\"").arg(xml);
     //myoutput->setPlainText(xml);


     centralWidget->setTabText(6, tr("Salida*"));

     parser.processInput( xml.toLatin1() );
     QString filepath = SafetYAWL::getConfFile().getValue("Input", "input.path").toString();
     QString filename = "defconfigure.xml";

     parser.openXml(filepath + "/" + filename);
     qDebug("...toInputConfigure....filepath: %s", qPrintable(filepath));

     QStringList names = parser.loadNameConfs();

     foreach(QString n, names) {
          QMap<QString,QVariant> conf = centraledit()->findConf(n);
          parser.addConf(conf, n);
     }

     QStringList results = parser.processXml(false);

    if ( queryForErrors() ) {
        qDebug(".......****..queryforerror....");
             return;
     }


      foreach(QString s, results) {
         qDebug("toInputConfigure.......result: %s", qPrintable(s));
         proccessConfFile(s);
     }

      if ( MainWindow::configurator  /* && user say yes */ ) {
          configurator->closeDataSources();
          delete MainWindow::configurator;
          MainWindow::configurator = new SafetYAWL();
          Q_CHECK_PTR( configurator);
          configurator->openDataSources();
      }

     statusBar()->showMessage(tr("Enviando......ok!"));
     QString message = QString("<table><tr><td><font color=green>%1</font>"
                               "</td></tr></table>").arg(tr("La operaci�n de <b>configuraci�n</b> fue exitosa"
                               "....<b>ok!</b>"));;
     dockShowResult->addHeadHtml(message);
     dockShowResult->renderMessages(false);
     showShowResultWidget(DockSbMenu::Hide);
     showShowResultWidget(DockSbMenu::Show);


}




void MainWindow::proccessConfFile(const QString& sql, const QString& filename, bool multiplefields) {

     qDebug("...replaceTextInFile...(1)");
     QString fileconf = filename;
     if ( filename.isEmpty() ) {
         fileconf = SafetYAWL::pathconf+ "/" + "safet.conf";

     }
     qDebug("....proccessConfFile...fileconf: |%s|", qPrintable(fileconf));
     qDebug("....proccessConfFile...sql: |%s|", qPrintable(sql));

     QRegExp rx;
     QString newsql = sql;
     newsql.replace("'","");
     qDebug("...newsql: %s", qPrintable(newsql));
     QString updatePattern = "UPDATE\\s+([�������a-zA-Z0-9_\\.\\(\\)#%][�������a-zA-Z0-9_,'\\.\\(\\)\\-#%\\x3c\\x3e\\x2f]*)"
                             "\\s+SET\\s+"
                             "([�������a-zA-Z0-9_\\.\\(\\)\\*;#%][�������a-zA-Z0-9_,'\\=\\.\\(\\)\\-\\s\\*;#%]*"
                             "[�������a-zA-Z0-9_,'\\=\\.\\(\\)\\-\\*;#]\\s+)?WHERE\\s+"
                             "([�������a-zA-Z0-9_\\.\\(\\)\\*;#%][�������a-zA-Z0-9_,'\\=\\.\\(\\)\\s\\-\\*;#%\\x3c\\x3e\\x2f]*"
                             "[�������a-zA-Z0-9_,'\\=\\.\\(\\)\\-\\*;#%\\x3c\\x3e\\x2f])";

     //UPDATE lista SET database.user.1='vbravo' WHERE database.db.1='dbtracrootve'
     qDebug("        (###)......updatePattern: |%s|", qPrintable(updatePattern));

     QString insertPattern = "INSERT INTO\\s+([a-zA-Z0-9_][a-zA-Z0-9_\\.\\-]*)\\s+"
                             "\\(([a-zA-Z0-9_\\.\\(\\)][a-zA-Z0-9_,'\\=\\.\\(\\)\\-\\*]*)\\)\\s+"
                             "VALUES\\s+\\(([a-zA-Z0-9_'\\./\\(\\);][a-zA-Z0-9_,;'\\=\\.\\-/\\*\\s\\x3c\\x3e\\x2f]*)\\)\\s*";


     QString deletePattern = "DELETE FROM\\s+([�������a-zA-Z0-9_\\.\\(\\)][�������a-zA-Z0-9_,'\\.\\(\\)\\-]*)"
                             "\\s+WHERE\\s+"
                             "([�������a-zA-Z0-9_\\.\\(\\)\\*;][�������a-zA-Z0-9_,'\\=\\.\\(\\)\\s\\-\\*;\\x3c\\x3e\\x2f]*"
                             "[�������a-zA-Z0-9_,'\\=\\.\\(\\)\\-\\*;\\x3c\\x3e\\x2f])";


     bool isdeleting = false;
     rx.setPattern(updatePattern);
     int pos = rx.indexIn(newsql);

     qDebug(".........***proccessConfFile....newsql: %s", qPrintable(newsql));
     qDebug(".........***proccessConfFile....pos: |%d|", pos);
     if ( pos == -1 ) {

        // qDebug("        (###)......insertPattern: |%s|", qPrintable(insertPattern));
         rx.setPattern(insertPattern);
         pos = rx.indexIn(newsql);
         qDebug("  .....reading..INSERT...pos: %d", pos);
         if (pos == -1 ) {
             //qDebug("        (###)......deletePattern: |%s|", qPrintable(deletePattern));
             rx.setPattern(deletePattern);
             pos = rx.indexIn(newsql);
          //   qDebug("  ...deletePattern...pos: %d", pos);
             if (pos == -1 ) {

                 SYE << tr("Ocurrio un error con la sentencia SQL \"%1\" formada por la entrada"
                                                               " Realice una lectura del registro para conocer donde se encuentra"
                                                               "el error").arg(newsql);
                 return;
             }
             else {
                 isdeleting = true;
            //     qDebug("...deletepattern: newsql: %s",qPrintable(newsql));
                 //doInsertInAuthConfFile(rx,MainWindow::DELETEPATTERN);

             }
         }
         else {

             qDebug(".......****proccessConfFile...1...INSERT");
             doInsertInAuthConfFile(rx);


             return;
         }
     }

     QString keyfield, fields;
     if ( !isdeleting ){
         keyfield = rx.cap(3);
         fields = rx.cap(2);
     }
     else {
         keyfield = rx.cap(2);
     }


     qDebug("...pattern: |%s|",qPrintable(rx.pattern()));
     // Para el keyfield
     if ( keyfield.split("=",QString::SkipEmptyParts).count() != 2 ) {
         SafetYAWL::streamlog << SafetLog::Error << tr("Ocurrio un error con la sentencia SQL \"%1\" formada por la entrada."
                                                       "No se encuentra la asignaci�n de campos con el operador '='."
                                                       " Realice una lectura del registro para conocer donde se encuentra"
                                                      "el error").arg(newsql);
         return;
     }
     QString firstkeyfield = keyfield.split("=",QString::SkipEmptyParts).at(0);
     //qDebug("         proccessConfFile...firstkeyfield...(1):%s", qPrintable(firstkeyfield));
     int numberreg = 0;
     QString prefixkey;
     QString secondkeyfield = keyfield.split("=",QString::SkipEmptyParts).at(1);
     qDebug("         proccessConfFile...secondkeyfield...(1):%s", qPrintable(secondkeyfield));


     if (multiplefields) {
         if (firstkeyfield.endsWith(".*")  ) {

             prefixkey = firstkeyfield.mid(0,firstkeyfield.length()-2);
             //qDebug("**prefixkey: %s", qPrintable(prefixkey));
             numberreg = SafetYAWL::getAuthConf().getNumberRegister(secondkeyfield,prefixkey,true);

             //qDebug("**numberreg: %d", numberreg);
             if (numberreg > 0 ) {
                 firstkeyfield = QString("%1.%2").arg(prefixkey)
                                 .arg(numberreg);
               //  qDebug("   (*)proccessConfFile...firstkeyfield...(2):%s", qPrintable(firstkeyfield));
             }


         }
     }

     //qDebug("   processConffile...(1)...firstkeyfield: %s", qPrintable(firstkeyfield));
     QString searchtext = tr("\\s*(%1)\\s*\\=\\s*([�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$\\(\\)#%\\x3c\\x3e\\x2f]"
                             "[�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$\\s\\(\\);#%\\x3c\\x3e\\x2f]*)"
                             "[�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$\\(\\);#%\\x3c\\x3e\\x2f]")
             .arg(firstkeyfield);
     QString replacetext;
     QString currentfirstkeyfield;
     if ( !isdeleting ) {
        replacetext = tr ("%1 = %2").arg(firstkeyfield).arg(secondkeyfield);

     }
     else {
          replacetext = "";
     }
     currentfirstkeyfield = firstkeyfield;


     SafetYAWL::replaceTextInFile(fileconf,
                                  searchtext,
                                  replacetext);

     qDebug("\n\nMainWidow::processConfFile......searchtext:\n%s\nreplacetext:\n%s\n\n",
            qPrintable(searchtext),
            qPrintable(replacetext));

     // Para los otros campos que no son claves

     if (isdeleting) {
        fields = searchFieldsInAuthConf(firstkeyfield);
        qDebug(".........fields:|%s|", qPrintable(fields));
    }

     //qDebug("   processConffile...(2)...fields.count():%d",fields.count());
     QStringList listfields;
     qDebug("...FIELDS:|%s|",qPrintable(fields));
     QStringList originfields = fields.split(",",QString::SkipEmptyParts);
     foreach(QString o, originfields)    {
         if (!listfields.contains(o)) {
             listfields.append(o);
         }
     }


     foreach( QString s, listfields ) {
         if ( s.split("=",QString::SkipEmptyParts).count() != 2 && !isdeleting) {
             continue;
         }
         if (!isdeleting) {
            firstkeyfield = s.split("=",QString::SkipEmptyParts).at(0);
        }
         else {
            firstkeyfield = s;
         }
         //qDebug("...firstkeyfield: %s", qPrintable(firstkeyfield));
         if (multiplefields && !isdeleting) {
             //qDebug("numberreg: %d", numberreg);
             if (firstkeyfield.endsWith(".*")  && numberreg > 0 ) {
                 firstkeyfield = QString("%1.%2").arg(firstkeyfield.mid(0,firstkeyfield.length()-2))
                                 .arg(numberreg);
                 //qDebug("...proccessConfFile...firstkeyfield...(2):%s", qPrintable(firstkeyfield));


             }
         }
         if (!isdeleting) {
            secondkeyfield = s.split("=",QString::SkipEmptyParts).at(1).trimmed();
        }
         else {
             secondkeyfield = "";
         }
         searchtext = tr("\\s*(%1)\\s*\\=\\s*([�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$,;\\x3c\\x3e\\x2f]"
                         "[�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$\\s,;\\x3c\\x3e\\x2f]*"
                         "[�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$,;\\x3c\\x3e\\x2f])").arg(firstkeyfield);
         if (!isdeleting) {
            replacetext  = tr("%1 = %2").arg(firstkeyfield).arg(secondkeyfield);


        }
         else {
             //qDebug("searchtext: %s", qPrintable(searchtext));
             replacetext ="";
         }

        // qDebug("           ...processConfUsers...replacetext:|%s|",qPrintable(replacetext));
         SafetYAWL::replaceTextInFile(fileconf,
                                      searchtext,
                                      replacetext);

//         qDebug(".....processing fields...searchtext:\n|%s|\nreplacetext(first):|%s|\n",
//                qPrintable(searchtext),
//                qPrintable(replacetext));

     }

     // Reorganizar los otros campos mayotres a numberreg
     if (isdeleting) {

         numberreg++;
         int result;
         listfields.push_front(currentfirstkeyfield);
         foreach(QString f, listfields) {
             qDebug("...LISTFIELDS...f:|%s|", qPrintable(f));
         }

         while(true){


             foreach( QString s, listfields ) {
                 QString currprefkey = s.split(".").at(0)+"."+
                         s.split(".").at(1);
                 QString nextkey = QString("%1.%2").arg(currprefkey)
                         .arg(numberreg);

                 qDebug("......WHILETRUE...nextkey: %s",qPrintable(nextkey));
                 searchtext = tr("\\s*(%1)\\s*\\=\\s*([�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$,;\\(\\)\\x3c\\x3e\\x2f]"
                                 "[�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$\\s,;\\(\\)\\x3c\\x3e\\x2f]*"
                                 "[�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$,;\\(\\)\\x3c\\x3e\\x2f])").arg(nextkey);

                 QString replacekey = QString("%1.%2").arg(currprefkey)
                         .arg(numberreg-1);
                 qDebug("........SEQUENCEREPLACING...replacekey: |%s|",qPrintable(replacekey));

                 replacetext  = tr("%1 = ||cap||").arg(replacekey);

                 result = SafetYAWL::replaceTextInFile(fileconf,
                                                       searchtext,
                                                       replacetext,
                                                       Qt::CaseSensitive,
                                                       2);

                 qDebug(".....**SEQUENCEREPLACING...searchtext:\n|%s|\nreplacetext(first):|%s|\n",
                        qPrintable(searchtext),
                        qPrintable(replacetext));

             }

             if ( !result ) {
                 qDebug("...numberreg:|%d|",numberreg);
                 break;
             }
             numberreg++;


         }
     }

}


QString MainWindow::searchFieldsInAuthConf(const QString& key) {
    qDebug("searchFieldsInAuthConf...Clave...key: %s", qPrintable(key));
    QString retfields;
    QString exp;

    if ( key.split(".").count() < 3 ) {
        return QString("");
    }
    exp = "[a-zA-Z0-9]+/("+key.split(".").at(0)+"\\.[a-zA-Z0-9]+\\."+key.split(".").at(2)+")";

    QRegExp rx(exp);
    //qDebug("searchFieldsInAuthConf...Clave...RegExp: %s", qPrintable(exp));
    for(int i =0; i < SafetYAWL::getAuthConf().keys().count() ; i++){
        QString currentkey = SafetYAWL::getAuthConf().keys().at(i);

        int pos = rx.indexIn(currentkey);
        if ( pos >= 0 ) {
            if ( rx.cap(1) != key ) {
                retfields += rx.cap(1) +",";
                //qDebug("searchFieldsInAuthConf...Clave...CurrentKey: %s", qPrintable(rx.cap(1)));
            }
        }
    }
    if ( !retfields.isEmpty()){
        retfields.chop(1);
    }

    qDebug("...retfields: %s", qPrintable(retfields));
    return retfields;

}

void MainWindow::doInsertInAuthConfFile(QRegExp& rx) {


    qDebug("...doInsertInAuthConfFile...capturas...1: %s", qPrintable(rx.cap(1)));
    qDebug("...doInsertInAuthConfFile...capturas...2: %s", qPrintable(rx.cap(2)));
    qDebug("...doInsertInAuthConfFile...capturas...3: %s", qPrintable(rx.cap(3)));

    QStringList fields, values;

    fields = rx.cap(2).split(",");
    values = rx.cap(3).split(",");

    QString fileconf = SafetYAWL::pathconf+ "/" + "auth.conf";
        int countuser = 1;
        QString replacetext,newfield;

        bool isreplacing = true;
        QString sectiontext;
        while (isreplacing ) {

            QString  newtext;

            if (sectiontext.isEmpty() && fields.count() > 0 ) {
                QString firstfield = fields.at(0);
                sectiontext = firstfield.mid(0,1).toUpper()+firstfield.mid(1);
                sectiontext = sectiontext.split(".").at(0);
                //qDebug("    ..........sectiontext:%s", qPrintable(sectiontext));
            }
            for(int i = 0; i < fields.count(); i++) {
                newfield = fields.at(i);
                newfield.chop(2);
                //qDebug("...fields.at(%d): %s", i, qPrintable(newfield));
                QString subfield = newfield.mid(0,1).toUpper()+newfield.mid(1);
                newfield = QString("%1.%2").arg(subfield).arg(countuser);
                QString firstfield = newfield.split(".").at(0);
                QString key = firstfield +"/"+newfield.mid(firstfield.length()+1);
               // qDebug("...key: %s",qPrintable(key));

                newfield = newfield.toLower();
                if (!SafetYAWL::getAuthConf().contains(key) ) {
                    newtext = QString("%1").
                              arg(newfield)
                              +" = " + values.at(i) + "\n";
                    replacetext += newtext;
                   // qDebug("! replacing:%s",qPrintable(newtext));
                    isreplacing = false;
                }
                else {
                    replacetext += QString("%1").arg(newfield)
                                   +" = " + SafetYAWL::getAuthConf()[ key ] + "\n";
                }
            }

            replacetext += "\n";
            countuser++;


        }



        qDebug();
        qDebug("...**replaceSectionInFile...:\n%s",
               qPrintable(replacetext));
        qDebug();

        SafetYAWL::replaceSectionInFile(fileconf,sectiontext,replacetext);

}

void MainWindow::restoreWindowState()
{
        QSettings settings;
        QPoint pos = settings.value("windowPos").toPoint();
        if (!pos.isNull())
                move(pos);
        resize(settings.value("windowSize", QSize(780, 580)).toSize());
        if (settings.value("windowMaximized", false).toBool())
                setWindowState(windowState() ^ Qt::WindowMaximized);
        QByteArray docksinfo = settings.value("docks").toByteArray();
        qDebug("MainWindow::restoreWindowState...docksinfo: %d",docksinfo.count());
        restoreState(docksinfo);
}



void MainWindow::saveWindowState()
{
        QSettings settings;
        settings.setValue("windowGeometry", saveGeometry());
        settings.setValue("windowMaximized", isMaximized());
        settings.setValue("windowPos", m_lastPos);
        settings.setValue("windowSize", m_lastSize);
        settings.setValue("docks", saveState());

}


void MainWindow::closeEvent(QCloseEvent *event)
{
        if (maybeSave()) {
                saveWindowState();
                event->accept();
        }
        else {
                event->ignore();
        }

        delete assistant;
}


void MainWindow::toChangeUser() {
    qDebug("...MainWindow::toChangeUser()...");
    LoginDialog mydialog(this);
    int result = mydialog.exec();
    if ( result == QDialog::Accepted) {
//            changeUserButton->setText(QString("%1")
//                                      .arg(MainWindow::currentaccount));
            checkGoPrincipal();


    }
}

void MainWindow::resizeEvent(QResizeEvent *event)
{

    if (!isMaximized())
        m_lastSize = size();
    if ( changeUserButton == NULL ) {
//        changeUserButton = new QPushButton(QString("%1")
//                                           .arg(MainWindow::currentaccount),
//                                           standardbar);

//        standardbar->addWidget(changeUserButton);
//        changeUserButton->setVisible(false);
//        connect(changeUserButton,SIGNAL(clicked()),this,SLOT(toChangeUser()));
    }
    if (titleApplication == NULL ) {
        QString title = SafetYAWL::getAuthConf()["Titles/application"];
        if ( !title.isEmpty()) {
//            titleApplication = new QLabel();
//            QLabel *myspaceLabel = new QLabel();
//            Q_CHECK_PTR(titleApplication);
//            titleApplication->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
//            titleApplication->setText("<font color=\"#000000\">"+title+"</font>");

//            myspaceLabel->setText("                    ");
//            standardbar->addWidget(myspaceLabel);
//            standardbar->addWidget(titleApplication);

              QString newtitle =  tr("SAFET - InFlow/%1-%2")
                                     .arg(SafetYAWL::getNumberVersion())
                                     .arg(SafetYAWL::getRevisionVersion()) ;
              newtitle += " - " + title;

              setWindowTitle(newtitle);
        }
    }


    QWidget::resizeEvent(event);
    if ( event == NULL ) {
        return;
    }

    if ( stackWidget == NULL ) {
        return;
    }
    int centralw = stackWidget->geometry().width();
    int centralh = stackWidget->geometry().height();

    int neww = centralw-70*2;
    //int newh = 310;
    int newh = centralh-200*2;

    // *******
    // Ajuste de Tama�o para  Formulario
    // *******
    if ( completingTextEditForm != NULL ) {
        completingTextEditForm->setMinimumHeight(50);
        completingTextEditForm->setGeometry(60,80,neww,newh);
        QRect brect = completingButtonForm->geometry();
        QRect trect = completingTextEditForm->geometry();

        int posh = trect.bottomLeft().y()+7;

        showdockButtonForm->setGeometry(60,posh,210,brect.height());
        int posw = 60+trect.width()-310;

        if ( posw < showdockButtonForm->geometry().bottomRight().x()) {
            posw = showdockButtonForm->geometry().bottomRight().x();
            cancelButtonForm->setGeometry(posw+10,posh,150,brect.height());
            completingButtonForm->setGeometry(posw+170,posh,150,brect.height());
        }
        else {
            cancelButtonForm->setGeometry(60+trect.width()-310,posh,150,brect.height());
            completingButtonForm->setGeometry(60+trect.width()-150,posh,150,brect.height());
        }

    }
    // *******
    // Ajuste de Tama�o para  Formulario
    // *******


    // *******
    // Ajuste de Tama�o para  Consulta
    // *******

    if ( completingTextEditCons != NULL ) {
        completingTextEditCons->setGeometry(60,80,neww,newh);

        QRect trect = completingTextEditCons->geometry();

        completingTextEditCons->setMinimumHeight(50);
        QRect brect = completingButtonCons->geometry();

        //completingButt
        int posh = trect.bottomLeft().y()+7;

        showdockButtonCons->setGeometry(60,posh,210,brect.height());
        int posw = 60+trect.width()-310;

        if ( posw < showdockButtonCons->geometry().bottomRight().x()) {
            posw = showdockButtonCons->geometry().bottomRight().x();
            cancelButtonCons->setGeometry(posw+10,posh,150,brect.height());
            completingButtonCons->setGeometry(posw+170,posh,150,brect.height());
        }
        else {
            cancelButtonCons->setGeometry(60+trect.width()-310,posh,150,brect.height());
            completingButtonCons->setGeometry(60+trect.width()-150,posh,150,brect.height());
        }


        listEditCons->setMinimumHeight(30);
        listEditCons->setGeometry(60,
                                  trect.bottomLeft().y()+
                                  completingButtonCons->geometry().height()+12,
                                  30+trect.width()/2,trect.height()/3-20);
        QRect listRect = listEditCons->geometry();

        buttonListEditConsAdd->setGeometry(listRect.bottomLeft().x(),
                                           listRect.bottomLeft().y()+7,
                                           20,20);
        buttonListEditConsEdit->setGeometry(buttonListEditConsAdd->geometry().bottomRight().x()+10,
                                            listRect.bottomLeft().y()+7,
                                            20,20);
        buttonListEditConsDel->setGeometry(buttonListEditConsEdit->geometry().bottomRight().x()+10,
                                           listRect.bottomLeft().y()+7,
                                           20,20);
        buttonListEditConsSave->setGeometry(buttonListEditConsDel->geometry().bottomRight().x()+10,
                                            listRect.bottomLeft().y()+7,
                                            20,20);

    }

    if ( completingTextEditSign != NULL ) {
        completingTextEditSign->setMinimumHeight(50);
        completingTextEditSign->setGeometry(60,80,neww,newh);
        QRect brect = completingButtonSign->geometry();
        QRect trect = completingTextEditSign->geometry();

        int posh = trect.bottomLeft().y()+7;

        showdockButtonSign->setGeometry(60,posh,210,brect.height());
        int posw = 60+trect.width()-310;

        if ( posw < showdockButtonSign->geometry().bottomRight().x()) {
            posw = showdockButtonSign->geometry().bottomRight().x();
            cancelButtonSign->setGeometry(posw+10,posh,150,brect.height());
            completingButtonSign->setGeometry(posw+170,posh,150,brect.height());
        }
        else {
            cancelButtonSign->setGeometry(60+trect.width()-310,posh,150,brect.height());
            completingButtonSign->setGeometry(60+trect.width()-150,posh,150,brect.height());
        }


    }

    if ( completingTextEditConf != NULL ) {

        completingTextEditConf->setMinimumHeight(50);
        completingTextEditConf->setGeometry(60,80,neww,newh);
        QRect brect = completingButtonConf->geometry();
        QRect trect = completingTextEditConf->geometry();

        int posh = trect.bottomLeft().y()+7;

        showdockButtonConf->setGeometry(60,posh,210,brect.height());
        int posw = 60+trect.width()-310;

        if ( posw < showdockButtonConf->geometry().bottomRight().x()) {
            posw = showdockButtonConf->geometry().bottomRight().x();
            cancelButtonConf->setGeometry(posw+10,posh,150,brect.height());
            completingButtonConf->setGeometry(posw+170,posh,150,brect.height());
        }
        else {
            cancelButtonConf->setGeometry(60+trect.width()-310,posh,150,brect.height());
            completingButtonConf->setGeometry(60+trect.width()-150,posh,150,brect.height());
        }


    }

    if ( completingTextEditUsers != NULL ) {
        completingTextEditUsers->setMinimumHeight(50);
        completingTextEditUsers->setGeometry(60,80,neww,newh);
        QRect brect = completingButtonUsers->geometry();
        QRect trect = completingTextEditUsers->geometry();

        int posh = trect.bottomLeft().y()+7;

        showdockButtonUsers->setGeometry(60,posh,210,brect.height());
        int posw = 60+trect.width()-310;

        if ( posw < showdockButtonUsers->geometry().bottomRight().x()) {
            posw = showdockButtonUsers->geometry().bottomRight().x();
            cancelButtonUsers->setGeometry(posw+10,posh,150,brect.height());
            completingButtonUsers->setGeometry(posw+170,posh,150,brect.height());
        }
        else {
            cancelButtonUsers->setGeometry(60+trect.width()-310,posh,150,brect.height());
            completingButtonUsers->setGeometry(60+trect.width()-150,posh,150,brect.height());
        }
        
    }

    centralw = stackWidget->geometry().width();
    centralh = stackWidget->geometry().height();

    int  newwidth = centralw/3+60;
    int newheight = centralh/4+23;

    QRect panelRect = MainWindow::docksbMenu->panelGeometry();
    if ( MainWindow::docksbMenu && MainWindow::dockShowResult) {
        QRect cur = MainWindow::docksbMenu->geometry();
        QRect curShow = MainWindow::dockShowResult->geometry();
        int newx, newy;
        newx = event->size().width()-event->oldSize().width();
        newy = event->size().height()-event->oldSize().height();
        QPoint p = cur.topLeft();
        QPoint pShow = curShow.topLeft();




        // ** ajustando vistas WebView y TextEdits
        foreach(QWebView *myview, stackedwebviews) {
          myview->setGeometry(0,0,centralw, centralh);

        }
        if ( !initApp ) {
            if ( completingTextEditForm != NULL ) {
                connect(completingButtonForm, SIGNAL(clicked()), this, SLOT(toSend()) );
                connect(cancelButtonForm, SIGNAL(clicked()), this, SLOT(toClearTextEdit()) );
                connect(showdockButtonForm,SIGNAL(clicked()),this,SLOT(showSmartMenu()));
            }

            if ( completingTextEditCons != NULL ) {
                connect(completingButtonCons, SIGNAL(clicked()), this, SLOT(toSend()) );
                connect(cancelButtonCons, SIGNAL(clicked()), this, SLOT(toClearTextEdit()) );
                connect(showdockButtonCons,SIGNAL(clicked()),this,SLOT(showSmartMenu()));


                connect(buttonListEditConsAdd,SIGNAL(clicked()),this,
                        SLOT(addToHistoryList()));
                connect(listEditCons,SIGNAL(itemDoubleClicked(QListWidgetItem*)),this,
                        SLOT(insertFromHistoryList(QListWidgetItem*)));

                connect(buttonListEditConsEdit,SIGNAL(clicked()),this,
                        SLOT(editToHistoryList()));
                connect(buttonListEditConsDel,SIGNAL(clicked()),this,
                        SLOT(delToHistoryList()));

                connect(buttonListEditConsSave,SIGNAL(clicked()),this,
                        SLOT(saveToHistoryList()));

            }

            if ( completingTextEditSign != NULL ) {
                connect(completingButtonSign, SIGNAL(clicked()), this, SLOT(toSend()) );
                connect(cancelButtonSign, SIGNAL(clicked()), this, SLOT(toClearTextEdit()) );
                connect(showdockButtonSign,SIGNAL(clicked()),this,SLOT(showSmartMenu()));

            }

            if ( completingTextEditConf != NULL ) {
                connect(completingButtonConf, SIGNAL(clicked()), this, SLOT(toSend()) );
                connect(cancelButtonConf, SIGNAL(clicked()), this, SLOT(toClearTextEdit()) );
                connect(showdockButtonConf,SIGNAL(clicked()),this,SLOT(showSmartMenu()));

            }

            if ( completingTextEditUsers != NULL ) {
                connect(completingButtonUsers, SIGNAL(clicked()), this, SLOT(toSend()) );
                connect(cancelButtonUsers, SIGNAL(clicked()), this, SLOT(toClearTextEdit()) );
                connect(showdockButtonUsers,SIGNAL(clicked()),this,SLOT(showSmartMenu()));

            }

            //MainWindow::docksbMenu->setFeatures(QDockWidget::DockWidgetMovable );
            MainWindow::dockShowResult->setFeatures(QDockWidget::DockWidgetMovable );

            newx = 0;
            newy = 0;





            QRect dockRect = MainWindow::docksbMenu->geometry();
            QRect dockShowRect = MainWindow::dockShowResult->geometry();

            int  newshowwidth = centralw-newwidth-12; // 3 veces lo del menu sbmenu
            int  newshowheight = newheight; // 3 veces lo del menu sbmenu



            dockRect.setSize(QSize(newwidth,newheight));
            dockShowRect.setSize(QSize(newshowwidth,newshowheight));

            dockRect.moveTopLeft(QPoint(centralw-newwidth-4,centralh-newheight+58));
            dockShowRect.moveTopLeft(QPoint(5,centralh-newshowheight+58));


            MainWindow::docksbMenu->setGeometry(dockRect);
            MainWindow::dockShowResult->setGeometry(dockShowRect);

            QRect panelShowRect = MainWindow::dockShowResult->panelGeometry();
            //qDebug("...createDockWindow... centralw:%d ...newwidth: %d",centralw,newwidth);
            panelRect.setSize(QSize(newwidth-8,newheight-25));
            panelShowRect.setSize(QSize(newshowwidth,newshowheight-28));
            MainWindow::docksbMenu->setPanelGeometry(panelRect);
            MainWindow::dockShowResult->setPanelGeometry(panelShowRect);

            initApp = true;
            completingTextEditForm->setFocus();
            //MainWindow::docksbMenu->showNormal();
            showSmartMenuWidget(DockSbMenu::Hide);
            showShowResultWidget(DockSbMenu::Hide);

            if (gviewoutput) { // Colocar la vista de grafico de flujo de trabajo
                MainWindow::gviewoutputwidth = stackWidget->geometry().width();
                MainWindow::gviewoutputheight = stackWidget->geometry().height();
                gviewoutput->scene()->setSceneRect(0,0,MainWindow::gviewoutputwidth,MainWindow::gviewoutputheight);
            }

            return;
        }
        panelRect.setSize(QSize(newwidth-8,newheight-25));
        MainWindow::docksbMenu->setPanelGeometry(panelRect);
        p.setX( p.x()+newx);
        p.setY( p.y()+newy);
        pShow.setX(5);
        pShow.setY(pShow.y()+newy);
        // Calcular nuevo tama?o
        centralw = stackWidget->geometry().width()-12;
        int  newshowwidth = centralw-cur.width(); // 3 veces lo del menu sbmenu
        curShow.setWidth(newshowwidth);

        cur.moveTopLeft(p);
        curShow.moveTopLeft(pShow);
        MainWindow::docksbMenu->setGeometry(cur);
        MainWindow::dockShowResult->setGeometry(curShow);
    }
}


void MainWindow::checkGoPrincipal() {
    if (centralWidget->count() > 6 ) {
        _checkgoprincipal = true;
        centralWidget->setCurrentIndex(6);
    }
    goPrincipal();

}

void MainWindow::moveEvent(QMoveEvent *event)
{
        if (!isMaximized())
                m_lastPos = pos();
        QWidget::moveEvent(event);
}



void MainWindow::showEvent(QShowEvent *event)
{
        m_lastPos = pos();
        m_lastSize = size();
        QWidget::showEvent(event);
}


void MainWindow::doExit() {
    saveWindowState();
    close();
}

bool MainWindow::maybeSave() {

     QMessageBox::StandardButton button = QMessageBox::warning(this, trUtf8("SAFET - Inflow"),
                        trUtf8("&#191;Realmente quiere <b>cerrar</b> SAFET Inflow?"),
                        QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
                if (button == QMessageBox::Cancel)
                        return false;
                if (button == QMessageBox::Yes)
                        return true;
        return false;
}


void MainWindow::setupStackedWebviews(const QIcon& icon, const QString& name, const QString& desc) {



        QFrame *formframe = new QFrame(this);
        QWebView *webView = new QWebView(formframe);
        QRect wrect = centralWidget->rect();
        webView->setGeometry(wrect);
        if ( name == tr("Formulario") ) {
            webView->setUrl(QUrl("qrc:/plantillacuadrodeedicionform.html"));
            completingTextEditForm = new TextEdit(formframe);
            completingTextEditForm->setGeometry(QRect(100, 140, 511, 281));
            completingButtonForm = new QCommandLinkButton(formframe);
            completingButtonForm->setText(tr("Enviar %1").arg(name));
            cancelButtonForm = new QCommandLinkButton(formframe);
            cancelButtonForm->setGeometry(QRect(100, 140, 511, 281));
            cancelButtonForm->setText("Cancelar");
            showdockButtonForm = new QCommandLinkButton(formframe);
            showdockButtonForm->setGeometry(QRect(100, 210, 511, 281));
            showdockButtonForm->setText("Mostrar/Ocultar Campos");


        }
        else if ( name == tr("Consulta") ) {
            webView->setUrl(QUrl("qrc:/plantillacuadrodeedicionconsulta.html"));
            completingTextEditCons = new TextEdit(formframe);
            // Agregando botones
            buttonListEditConsAdd = new QToolButton(formframe);
            buttonListEditConsAdd->setIcon(QIcon(":/add.png"));
            buttonListEditConsEdit = new QToolButton(formframe);
            buttonListEditConsEdit->setIcon(QIcon(":/edit.png"));
            buttonListEditConsDel = new QToolButton(formframe);
            buttonListEditConsDel->setIcon(QIcon(":/del.png"));

            buttonListEditConsSave = new QToolButton(formframe);
            buttonListEditConsSave->setIcon(QIcon(":/ico_save.png"));

            listEditCons = new QListWidget(formframe);
            completingTextEditCons->setGeometry(QRect(100, 140, 511, 281));
            listEditCons->setGeometry(QRect(100, 350, 311, 520));
            completingButtonCons = new QCommandLinkButton(formframe);
            completingButtonCons->setText(tr("Enviar %1").arg(name));
            cancelButtonCons = new QCommandLinkButton(formframe);
            cancelButtonCons->setGeometry(QRect(100, 140, 511, 281));
            cancelButtonCons->setText("Cancelar");
            showdockButtonCons = new QCommandLinkButton(formframe);
            showdockButtonCons->setGeometry(QRect(100, 210, 511, 281));
            showdockButtonCons->setText("Mostrar/Ocultar Campos");

        }
        else if ( name == tr("Dise�o") ) {
            webView->setUrl(QUrl("qrc:/plantillacuadrodeedicionfirmascertificados.html"));
            completingTextEditSign = new TextEdit(formframe);
            completingTextEditSign->setGeometry(QRect(100, 140, 511, 281));
            completingButtonSign = new QCommandLinkButton(formframe);
            completingButtonSign->setText(tr("Enviar"));
            cancelButtonSign = new QCommandLinkButton(formframe);
            cancelButtonSign->setGeometry(QRect(100, 140, 511, 281));
            cancelButtonSign->setText("Cancelar");
            showdockButtonSign = new QCommandLinkButton(formframe);
            showdockButtonSign->setGeometry(QRect(100, 210, 511, 281));
            showdockButtonSign->setText("Mostrar/Ocultar Campos");


        }
        else if ( name == tr("Configuraci�n") ) {
            webView->setUrl(QUrl("qrc:/plantillacuadrodeedicionconfiguracion.html"));
            completingTextEditConf = new TextEdit(formframe);
            completingTextEditConf->setGeometry(QRect(100, 140, 511, 281));
            completingButtonConf = new QCommandLinkButton(formframe);
            completingButtonConf->setText(tr("Enviar %1").arg(name));
            cancelButtonConf = new QCommandLinkButton(formframe);
            cancelButtonConf->setGeometry(QRect(100, 140, 511, 281));
            cancelButtonConf->setText("Cancelar");
            showdockButtonConf = new QCommandLinkButton(formframe);
            showdockButtonConf->setGeometry(QRect(100, 210, 511, 281));
            showdockButtonConf->setText("Mostrar/Ocultar Campos");

        }
        else if ( name == tr("Usuarios/Roles") ) {
            webView->setUrl(QUrl("qrc:/plantillacuadrodeedicionusuarios.html"));
            completingTextEditUsers = new TextEdit(formframe);
            completingTextEditUsers->setGeometry(QRect(100, 140, 511, 281));
            completingButtonUsers = new QCommandLinkButton(formframe);
            completingButtonUsers->setText(tr("Enviar %1").arg(name));
            cancelButtonUsers = new QCommandLinkButton(formframe);
            cancelButtonUsers->setGeometry(QRect(100, 140, 511, 281));
            cancelButtonUsers->setText("Cancelar");
            showdockButtonUsers = new QCommandLinkButton(formframe);
            showdockButtonUsers->setGeometry(QRect(100, 210, 511, 281));
            showdockButtonUsers->setText("Mostrar/Ocultar Campos");

        }

        else {
            SYE
                    << tr("Se presenta un error en la construccion de la interfaz "
                          "porque el nombre de la hoja \"%1\" NO existe")
                    .arg(name);
            return;
        }

        centralWidget->addTab(formframe, icon, name); // 0
        //centralWidget->addTab(formframe, QIcon(":/form.png"), tr("Formulario")); // 0
        stackedwebviews.append(webView);


}

bool MainWindow::searchInHistoryList(const QString& str) {
    if (completingTextEdit == completingTextEditCons ) {
        QListWidget *list = listEditCons;
        Q_CHECK_PTR( list );
        for(int i = 0; i < list->count(); i++) {
            QListWidgetItem *curitem = list->item(i);
            if ( str == curitem->text()) {
                return true;
            }
        }
    }
    return false;

}

void MainWindow::saveToHistoryList() {
    qDebug("...save history...");
    QString field = "generaloptions.consoleactions.*";

//    QString searchtext = tr("\\s*(%1)\\s*\\=\\s*([;a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$\\(\\)"
//                            "\\zero341\\0351\\0355\\0363\\0372;]"
//                            "[;a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$\\s\\(\\)\\zero341\\0351\\0355\\0363\\0372]*)"
//                            "[;a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$\\(\\)\\zero341\\0351\\0355\\0363\\0372]").arg(field);

    QString searchtext = tr("\\s*(%1)\\s*\\=\\s*([^\n]+)").arg(field);


    QString replacetext  = "";
//    qDebug("----------------");
//    qDebug("   **.saveToHistoryList...searchtext: %s", qPrintable(searchtext));
//    qDebug("----------------");
    //qDebug("   **..saveToHistoryList...replacetext: %s", qPrintable(replacetext));
    QString fileconf = SafetYAWL::pathconf+ "/" + "safet.conf";
    bool found = true;

    while( found ) {
        found = SafetYAWL::replaceTextInFile(fileconf,
                                     searchtext,
                                     replacetext);
//        qDebug("...saveToHistoryList...searchtext: |%s|.....found: %d", qPrintable(searchtext), found);
    }
    searchtext = tr("\\[GeneralOptions\\]");
    replacetext = "[GeneralOptions]\n\n";

    if (completingTextEdit == completingTextEditCons ) {
        QListWidget *list = listEditCons;
        qDebug(".....list: %p", list);
        for( int i = 0; i < list->count(); i++) {
            QListWidgetItem *cur = list->item(i);
            QString newvalue = "generaloptions.consoleactions.* = ";
            newvalue+= cur->text().trimmed();
            newvalue+= ";";
            newvalue+= cur->toolTip().trimmed().replace("\n"," ");
            replacetext += newvalue;
            replacetext += "\n";

        }
    }
  //  qDebug("...replacetext: %s",qPrintable(replacetext));

    found = SafetYAWL::replaceTextInFile(fileconf,
                                 searchtext,
                                 replacetext);

    QMessageBox::information (this,"Safet-Inflow", tr("La lista de acciones fue guardada satisfactoriamente"));

}

void MainWindow::addToHistoryList() {

    qDebug("...addToHistoryList...(1)...");
    bool ok;

    QString action = completingTextEdit->toPlainText().trimmed();
    if ( action.isEmpty()) {
        QMessageBox::information (this,"Safet-Inflow", tr("El cuadro de texto de acciones"
                                          "se encuentra vac�o. No es posible guardar acci�n"));

        return;
    }

    QString desc = QInputDialog::getText(this, tr("Acci�n"),
                                         tr("Descripci�n:"), QLineEdit::Normal,
                                         tr(""), &ok);

    if (ok && !desc.isEmpty()) {

        if ( searchInHistoryList(desc)) {
            QMessageBox::information (this,"Safet-Inflow", tr("La descripci�n \"%1\" ya existe en la lista de acciones"
                                              ).arg(desc));
             return;
        }

        if (completingTextEdit == completingTextEditCons ) {
            QListWidget *list = listEditCons;
            Q_CHECK_PTR( list );

            QListWidgetItem* newitem = new QListWidgetItem(QIcon(":/workflow.png"),
                                                          desc,list);


            newitem->setToolTip(action);
            list->addItem(newitem);


        }
    }


}

void MainWindow::loadEditActions() {

    if ( _isloadeditactions ) {
        return;
    }
    _isloadeditactions = true;
    qDebug("....**...loadEditActions()....(1)....");
    QStringList actions  = SafetYAWL::getConf()["GeneralOptions/consoleactions.*"].split(SafetYAWL::LISTSEPARATORCHARACTER,QString::SkipEmptyParts);
    QListWidget *list = NULL;
    if (completingTextEdit == completingTextEditCons ) {

            list = listEditCons;
            Q_CHECK_PTR( list );
            list->clear();
     }
    if ( list == NULL ) {
        return;
    }


    foreach(QString s, actions) {
        QStringList tlist = s.split(";",QString::SkipEmptyParts);
        if ( tlist.count() < 2 ) continue;
        QListWidgetItem* newitem = new QListWidgetItem(QIcon(":/workflow.png"),
                                                      tlist.at(0),list);

        newitem->setToolTip(tlist.at(1));
        list->addItem(newitem);
    }
}

void MainWindow::insertFromHistoryList(QListWidgetItem* item) {
    qDebug("...insertFromHistoryList...");
    if (completingTextEdit == completingTextEditCons ) {
            QListWidget *list = listEditCons;
            Q_CHECK_PTR( list );
            QListWidgetItem* cur = item;
            if ( cur ) {
                if ( completingTextEdit != NULL &&
                     completingTextEdit->currentWidget() != NULL ) {
                    completingTextEdit->currentWidget()->cancelAndClose();
                    toClearTextEdit();
                }
                QString itext = cur->toolTip();
                completingTextEdit->clear();
                completingTextEdit->setPlainText(itext);
                completingTextEdit->setFocus();
                completingTextEdit->updateDockSbMenu(true);
                qDebug("...**completingTextEdit->setFocus()...");


            }
    }
}

void MainWindow::editToHistoryList() {
    if (completingTextEdit == completingTextEditCons ) {
        QListWidget *list = listEditCons;
        Q_CHECK_PTR( list );
        QList<QListWidgetItem *> curitems = list->selectedItems();
         Q_CHECK_PTR(list);

         foreach(QListWidgetItem* item, curitems) {
             qDebug("edititem...");
             QListWidgetItem* cur = item;
             bool ok;
             QString desc = QInputDialog::getText(this, tr("Acci�n"),
                                                  tr("Descripci�n:"), QLineEdit::Normal,
                                                  cur->text(), &ok);

             if ( ok && !desc.isEmpty() ) {
                    cur->setText(desc);
             }

         }
     }

}

void MainWindow::delToHistoryList() {
    if (completingTextEdit == completingTextEditCons ) {
        QListWidget *list = listEditCons;
        Q_CHECK_PTR( list );
        QList<QListWidgetItem *> curitems = list->selectedItems();
         Q_CHECK_PTR(list);
        int result = QMessageBox::question(this, trUtf8("Eliminar elemento(s)"),
                                           tr("�Realmente desea eliminar el(los) elemento(s) seleccionado(s)?"),
                                           QMessageBox::Yes | QMessageBox::No);
        if ( result == QMessageBox::Yes ) {
            foreach(QListWidgetItem* item, curitems) {
                qDebug("removeitem...");
                int row = list->row(item);
                Q_ASSERT( row >= 0 );
                list->takeItem(row);
            }
        }
    }
}

void MainWindow::setupTabWidget() {

    // Para la consola de salida


    setupStackedWebviews(QIcon(":/form.png"), tr("Formulario"));
    //centralWidget->addTab(completingTextEditForm, QIcon(":/form.png"), tr("Formulario")); // 0
    setupStackedWebviews(QIcon(":/terminal.png"), tr("Consulta"));
    setupStackedWebviews(QIcon(":/padlock.png"), tr("Dise�o"));

    completingTextEdit = completingTextEditForm;
    // Para visualizar Listas
    myoutput = new QTextEdit(this);
    //listviewoutput = new QTableView(this);
    gviewoutput = new GraphicsWorkflow(this);
    weboutput = new QWebView(this);
    Q_CHECK_PTR( myoutput );
    Q_CHECK_PTR( weboutput );
    myoutput->setReadOnly(true);
//    centralWidget->addTab(listviewoutput, QIcon(":/list.png"), tr("Lista"));
    centralWidget->addTab(weboutput, QIcon(":/web.png"), tr("Reportes")); // 3
    centralWidget->addTab(gviewoutput, QIcon(":/workflow.png"), tr("Flujo"));  // 4
    setupStackedWebviews(QIcon(":/conf.png"), tr("Configuraci�n"));   // 5
    centralWidget->addTab(myoutput, QIcon(":/salida.png"), tr("Salida")); // 6
    if (centralWidget->count() > 6 ) {
        centralWidget->setCurrentIndex(6);
    }
    setupStackedWebviews(QIcon(":/ico_person_blue_16.png"), tr("Usuarios/Roles"));   // 7

}

void MainWindow::drawWorkflow(const QString& filename) {
    if ( gviewoutput == NULL ) {
        return;
    }
     centralWidget->setTabText(4,tr("Flujo*"));
     qDebug("..................................*->MainWindow::drawWorkflow...:%s",qPrintable(filename));
     qDebug("....**gviewoutput->addItem....(1)...");
     gviewoutput->addItem(filename,commands);
     qDebug("....**gviewoutput->addItem....(2)...");



}


void MainWindow::buildModel(QMap<QString,QVariant>& mymap) {



}


void MainWindow::refreshListView(const QString& doc) {

}

void MainWindow::doQuit() {

     if (maybeSave() ) {
          saveWindowState();
          qApp->quit();
     }
}

void MainWindow::doPrint() {


    QPrintDialog dialog(&printer, this);
    QPainter mypainter;
    QRectF rectsource, recttarget;
    QPoint newpoint;
    if ( dialog.exec()) {

        int index = centralWidget->currentIndex();
        switch( index  ) {
        case 0: // Formulario
            completingTextEdit->print(&printer);
            break;
        case 1:  // Console
            qDebug("....................toSend...............toInputConsole...(0)...");
            completingTextEdit->print(&printer);
            break;
        case 2:  // Certificates Doc signed Management

            break;
        case 3: // Reportes
            if ( weboutput != NULL ) {

                weboutput->print(&printer);
            }
            break;


        case 4: // Flujos
            if ( gviewoutput != NULL ) {
                mypainter.begin(&printer);
                rectsource = gviewoutput->scene()->sceneRect();
                recttarget = mypainter.window();
                newpoint.setX(rectsource.topLeft().x()-200);
                newpoint.setY(rectsource.topLeft().y()+200);

                rectsource.setTopLeft(newpoint);
                gviewoutput->scene()->render(&mypainter);
            }
            break;

             case 5:  // Configuration
                 completingTextEdit->print(&printer);
             default:
                 break;
             }

    }
}

void MainWindow::doPrintDocument(QPrinter* printer) {

        QPainter mypainter;

        int index = centralWidget->currentIndex();
        switch( index  ) {
        case 0: // Formulario
            completingTextEdit->print(printer);
            break;
        case 1:  // Console
            completingTextEdit->print(printer);
            break;
        case 2:  // Certificates Doc signed Management

            break;
        case 3: // Reportes
            if ( weboutput != NULL ) {

                weboutput->print(printer);
            }
            break;

        case 4: // Flujos
            if ( gviewoutput != NULL ) {
                mypainter.begin(printer);
                gviewoutput->scene()->render(&mypainter);
            }
            break;

             case 5:  // Configuration
                 completingTextEdit->print(printer);
             default:
                 break;
             }

}

void MainWindow::doPrintPreview() {

    QPrintPreviewDialog preview(&printer, this);
        connect(&preview, SIGNAL(paintRequested(QPrinter *)),
                this, SLOT(doPrintDocument(QPrinter *)));
    preview.exec();

}



void MainWindow::doAssistantHelp() {

        assistant->showDocumentation("index.html");


}



void MainWindow::doWidgetsOptions() {

     OptionsDialog* mydialog = new OptionsDialog(this);
      int value = mydialog->exec();

     if ( value ==  QDialog::Accepted ) {

          QListWidget *mylist  = mydialog->plugList();
          plugs().clear();

          for( int i = 0; i < mylist->count(); i++ ) {
               plugs().append(mylist->item(i)->text() );
          }

          writeSettings();

     }
     if (mydialog) {
          delete mydialog;
     }

}

void MainWindow::doGeneralOptions() {
     ConfigDialog* mydialog = new ConfigDialog(this);
     Q_CHECK_PTR( mydialog );
     int value = mydialog->exec();
     if ( value == QDialog::Accepted ) {

     }
}


bool MainWindow::loadWidgetPlugins(const QString& f, bool add) {
    if (!configurator->loadWidgetPlugins(f)) {
        SafetYAWL::streamlog
                << SafetLog::Warning
                << tr("No se cargo correctamente el plugins: \"%1\"")
                .arg(f);
        return false;
    }
     QMap<QString,QVariant> conf;
     qDebug("....MainWindow::loadWidgetPlugins...f: %s", qPrintable(f));
     if ( add ) {
          Q_CHECK_PTR( SafetYAWL::listDynWidget );

          Q_ASSERT( SafetYAWL::listDynWidget->count() > 0 );
          WidgetInterface* mywid = SafetYAWL::listDynWidget->at(SafetYAWL::listDynWidget->count()-1);
          Q_CHECK_PTR( mywid );
          conf = mywid->conf();
          qDebug("....loadWidgetPlugins...conf[namewidget]: %s f: %s",
                    qPrintable(conf["namewidget"].toString()), qPrintable(f));
          //Q_ASSERT( conf["namewidget"] == f );
          completingTextEdit->listConfs().append( conf );
     }
     return true;
}



void MainWindow::createMenu()
{
    QAction *optionsAction = new QAction(QIcon(":/general.png"), tr("&Opciones Generales"), this);
    QAction *widgetsAction = new QAction(QIcon(":/widgets.png"), tr("&Opciones de widgets"), this);

    QAction *dataSourcesAction = new QAction(QIcon(":/general.png"), tr("&Fuente de datos"), this);


    QAction *getSignDocumentAction = new QAction(QIcon(":/signed_document.png"), tr("&Incluir documento firmado..."), this);
    QAction *sendSignDocumentAction = new QAction(QIcon(":/manager.png"), tr("&Guardar/Enviar Documento firmado"), this);
    QAction *printAction = new QAction(QIcon(":/print.png"), tr("&Imprimir..."), this);
    QAction *printpreviewAction = new QAction(QIcon(":/printview.png"), tr("&Vista preliminar"), this);
    exitAction = new QAction(QIcon(":/exit.png"), tr("&Cerrar"), this);
    QAction *aboutAct = new QAction(QIcon(":/about.png"), tr("Acerca de..."), this);
    QAction *aboutQtAct = new QAction(QIcon(":/qt-logo.png"), tr("Acerca de Qt"), this);
    QAction *assistanHelpAct = new QAction(QIcon(":/helpf1.png"), tr("Ayuda"), this);
    saveGraph = new QAction(QIcon(":/ico_save.png"),tr("Guardar gr�fico seleccionado"),this);
    restoreGraph = new QAction(QIcon(":/editredo.png"),tr("Restaurar gr�fico"),this);
    compareGraphs = new QAction(QIcon(":/workflow.png"),tr("Comparar gr�ficos"),this);
    fileLoadConfiguration  = new QAction(QIcon(":/load.png"),tr("Cargar Configuraci�n"),this);
    fileLoadFTPConfiguration  = new QAction(QIcon(":/load.png"),tr("Cargar Configuraci�n FTP"),this);
    fileSaveConfiguration  = new QAction(QIcon(":/ico_save.png"),tr("Guardar Configuraci�n"),this);
    fileSaveFTPConfiguration  = new QAction(QIcon(":/ico_save.png"),tr("Guardar Configuraci�n &FTP"),this);
    fileToChangeUser = new QAction(QIcon(":/ico_person_blue_16.png"),tr("Cambiar usuario"),this);

    assistanHelpAct->setShortcut(tr("F1"));

    connect(exitAction, SIGNAL(triggered()), this, SLOT(doQuit()));
    connect(saveGraph, SIGNAL(triggered()), this, SLOT(doSaveGraph()));
    connect(restoreGraph, SIGNAL(triggered()), this, SLOT(doRestoreGraph()));
    connect(compareGraphs, SIGNAL(triggered()), this, SLOT(doCompareGraphs()));

    connect(fileLoadFTPConfiguration, SIGNAL(triggered()), this, SLOT(doLoadFTP()));
    connect(fileSaveFTPConfiguration, SIGNAL(triggered()), this, SLOT(doSaveFTP()));

    connect(fileLoadConfiguration, SIGNAL(triggered()), this, SLOT(doLoadConfiguration()));
    connect(fileSaveConfiguration, SIGNAL(triggered()), this, SLOT(doSaveConfiguration()));

    connect(fileToChangeUser, SIGNAL(triggered()), this, SLOT(toChangeUser()));

    connect(printAction, SIGNAL(triggered()), this, SLOT(doPrint()));
    connect(printpreviewAction, SIGNAL(triggered()), this, SLOT(doPrintPreview()));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(optionsAction, SIGNAL(triggered()), this, SLOT(doGeneralOptions()));
    connect(widgetsAction, SIGNAL(triggered()), this, SLOT(doWidgetsOptions()));
    connect(getSignDocumentAction, SIGNAL(triggered()), this, SLOT(doGetSignDocument()));
    connect(sendSignDocumentAction, SIGNAL(triggered()), this, SLOT(doSendSignDocument()));
    connect(dataSourcesAction, SIGNAL(triggered()), this, SLOT(manageDataSources()));

    //connect(interchgCentralAct, SIGNAL(triggered()), this, SLOT(doInter()));
    connect(assistanHelpAct, SIGNAL(triggered()), this, SLOT(doAssistantHelp()));

    fileMenu = menuBar()->addMenu(tr("&Archivo"));
    fileMenu->addAction(fileLoadConfiguration);
    fileMenu->addAction(fileSaveConfiguration);    
    fileMenu->addSeparator();
    fileMenu->addAction(fileLoadFTPConfiguration);
    fileMenu->addAction(fileSaveFTPConfiguration);
    fileMenu->addSeparator();
    fileMenu->addAction(fileToChangeUser);
    fileMenu->addSeparator();
    fileMenu->addAction(printAction);
    fileMenu->addAction(printpreviewAction);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAction);

    editMenu = menuBar()->addMenu(tr("&Editar"));

    toolsMenu = menuBar()->addMenu(tr("&Herramientas"));
    //toolsMenu->addAction(optionsAction);
    toolsMenu->addAction(widgetsAction);
    toolsMenu->addSeparator();
    toolsMenu->addAction(dataSourcesAction);
    toolsMenu->addSeparator();
    toolsMenu->addAction(getSignDocumentAction);
    toolsMenu->addAction(sendSignDocumentAction);
    toolsMenu->addSeparator();
    toolsMenu->addAction(saveGraph);
    toolsMenu->addAction(restoreGraph);
    toolsMenu->addAction(compareGraphs);

    //  toolsMenu->addAction(interchgCentralAct);

    QMenu* helpMenu = menuBar()->addMenu(tr("Acerca de..."));
    helpMenu->addAction(assistanHelpAct);
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);

}

void MainWindow::doSaveGraph() {
    if ( gviewoutput == NULL ) {
        return;
    }
    SafetYAWL::streamlog.initAllStack();
    QString pathgraphfile = QDir::homePath() +"/" +
                            Safet::datadir + "/graphs" + "/"+
                            QDir::homePath().section("/",-1)+".gph";
    qDebug("...pathgraphfile: |%s|",qPrintable(pathgraphfile));

    if ( gviewoutput->scene() == NULL ) {
        return;
    }
    int i = 0;
    QFile myfile(pathgraphfile);
    if( !myfile.open(QIODevice::WriteOnly | QIODevice::Append)) {
        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("No es posible escribir en la ruta: \"%1\"")
                .arg(pathgraphfile);
        queryForErrors();
        return;
    }
    QDataStream out(&myfile);   // we will serialize the data into the file
    foreach (QGraphicsItem *item, gviewoutput->scene()->selectedItems()) {
        QVariant mydata = item->data(0);
        qDebug("graph |%s|",qPrintable(mydata.toString()));
        if ( graphs().contains(mydata.toString())) {
            bool ok;
            QString text = QInputDialog::getText(this, tr("Inflow - Guardar gr�fico"),
                                                 tr("Nombre del gr�fico %1:")
                                                 .arg(i+1), QLineEdit::Normal,
                                                 QString(""), &ok);

            if ( text.isEmpty() && !ok) {
                showSuccessfulMessage(tr("No se guard� el �ltimo gr�fico seleccionado"));
                return;
            }
            out << text;
            out <<  graphs()[mydata.toString()].first;
            out <<  graphs()[mydata.toString()].second;

            i++;
        }
    }
    myfile.close();

    if (i > 0 ) {
        if (i == 1) {
            showSuccessfulMessage(tr("Se guard� el  gr�fico seleccionado en la ruta: "
                                                        "\"<b>%1</b>\"")
                                                         .arg(pathgraphfile));
        }
        else {
            showSuccessfulMessage(tr("Se guardaron <b>%1</b> gr�ficos seleccionados en la ruta: "
                                                        "\"%2\"")
                                                         .arg(i)
                                                         .arg(pathgraphfile));
        }

    }
    queryForErrors();


}


void MainWindow::doRestoreGraph() {
    SafetYAWL::streamlog.initAllStack();
    QString pathgraphfile = QDir::homePath() +"/" +
                            Safet::datadir + "/graphs" + "/"+
                            QDir::homePath().section("/",-1)+".gph";
    qDebug("...pathgraphfile: |%s|",qPrintable(pathgraphfile));
    QFile myfile(pathgraphfile);
    if( !myfile.open(QIODevice::ReadOnly)) {
        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("No es posible leer gr�ficos de la ruta: \"%1\"")
                .arg(pathgraphfile);
        SafetYAWL::streamlog.stopAllStack();
        return;
    }

     QDataStream in(&myfile);
     QString title;
    QMap<QString,QPair<QString,QString> > items;
    QStringList infos;

    int i = 0;


    while( !in.atEnd()) {
        QPair<QString,QString> mypair;
        in >> title;
        in >> mypair;
        qDebug("title:%s", qPrintable(title));
        qDebug("info:\n%s", qPrintable(mypair.first));
        qDebug("date:%s", qPrintable(mypair.second));
        items [ title ] = mypair;
        i++;
    }

    if (i == 0 ) {
        showSuccessfulMessage(tr("No hay gr�ficos guardados en \"<b>%1</b>\"")
                              .arg(pathgraphfile));
        return;
    }

    //Q_ASSERT( items.count() == infos.count());


    bool ok;
    title = QInputDialog::getItem(this, tr("Seleccione un gr�fico para visualizar"),
                                         tr("Gr�fico:"), items.keys(), 0, false, &ok);
    if (!ok || title.isEmpty()) {
        SafetYAWL::streamlog.stopAllStack();
        return;
    }

    Q_ASSERT(items.contains(title));
    doRenderGraph(items[title].first,items[title].second);

}

void MainWindow::doRenderGraph(const QString& code, const QString& datetag) {
    Q_CHECK_PTR( configurator );
    qDebug("trying configurator...(1)..:%p",configurator);
    if ( configurator == NULL ) {

        qDebug("...configurator is null");
        return;
    }

    if ( SafetYAWL::curOutputInterface == NULL ) {

        qDebug("...curOutputInterface is null");
        configurator->loadPlugins("graphviz");
    }

    //QMap<QString,QString> mymap = SafetYAWL::getConfFile().readFile(SafetYAWL::pathconf + "/" + SafetYAWL::fileconf);
    QMap<QString,QString> mymap =  SafetYAWL::getConf().getMap();
    qDebug("trying curOutputInterface...(1)");
    Q_ASSERT ( SafetYAWL::curOutputInterface != NULL );
    qDebug("Codigo entrada...:\n%s",qPrintable(code));
    QString parsedCodeGraph = SafetYAWL::curOutputInterface->parseCodeGraph(code, mymap);
    qDebug("trying configurator...(2)");
    SafetYAWL::filelog.close();
    SafetYAWL::filelog.open(QIODevice::Append);
    SafetYAWL::streamlog.setDevice(&SafetYAWL::filelog);
    //SafetYAWL::streamlog << SafetLog::Debug << tr("C�digo para el gr�fico generado:\n%1").arg(parsedCodeGraph) << endl;
    qDebug("Codigo Generado...:\n%s",qPrintable(parsedCodeGraph));

    char type[] = "svg";
    QString img = SafetYAWL::curOutputInterface->renderGraph(parsedCodeGraph, type, mymap);
    if (img.startsWith("ERROR")) {
        SafetYAWL::streamlog
            << SafetLog::Error
            << tr("Ocurri� el siguiente error al generar el gr�fico de flujo \"%1\"")
            .arg(img);
        queryForErrors();
        return ;
    }
    SafetYAWL::filelog.close();
    SafetYAWL::filelog.open(QIODevice::Append);
    SafetYAWL::streamlog.setDevice(&SafetYAWL::filelog);

    SafetYAWL::lastgraph = code;
    SafetYAWL::lastinfodate = datetag;
    qDebug("...Dibujando el grafico....(1)...");
    drawWorkflow(img); // Dibujar el flujo
    qDebug("...Dibujando el grafico....(2)...");
    SafetYAWL::streamlog.stopAllStack();

}

void MainWindow::doCompareGraphs() {
    if (gviewoutput == NULL ) {
        return;
    }
    if ( gviewoutput->scene() == NULL ) {
        return;
    }
    int i = 0;
    if ( gviewoutput->scene()->selectedItems().count() != 2 ) {
        QMessageBox::information(this,tr("Inflow -Informaci�n")
                                 ,tr("Para ejecutar la funci�n de comparaci�n"
                                         " es necesario seleccionar exactamente "
                                         " dos (2) gr�ficos"), QMessageBox::Ok);

        return;
    }

    QGraphicsItem *firstitem = gviewoutput->scene()->selectedItems().at(0);
    Q_CHECK_PTR( firstitem);

    QGraphicsItem *seconditem = gviewoutput->scene()->selectedItems().at(1);
    Q_CHECK_PTR( seconditem);

    if ( firstitem->y() > seconditem->y() ) {
        QGraphicsItem *tempitem = NULL;
         tempitem = firstitem;
         firstitem = seconditem;
         seconditem = tempitem;
    }
    QString firstgraph, secondgraph;
    QVariant mydata = firstitem->data(0);
//     qDebug("graph |%s|",qPrintable(mydata.toString()));
    if ( graphs().contains(mydata.toString())) {
//     qDebug(".... (1)graphs()[mydata.toString()].first:\n%s",
//            qPrintable( graphs()[mydata.toString()].first));
     firstgraph = graphs()[mydata.toString()].first;
//     qDebug(".... (2)graphs()[mydata.toString()].second: %s",
//            qPrintable( graphs()[mydata.toString()].second));
    }

     mydata = seconditem->data(0);
//     qDebug("graph |%s|",qPrintable(mydata.toString()));
    if ( graphs().contains(mydata.toString())) {
//     qDebug(".... (1)graphs()[mydata.toString()].first:\n%s",
//            qPrintable( graphs()[mydata.toString()].first));
     secondgraph = graphs()[mydata.toString()].first;
//     qDebug(".... (2)graphs()[mydata.toString()].second: %s",
//            qPrintable( graphs()[mydata.toString()].second));

    }
    SafetYAWL::streamlog.initAllStack();
    QString newgraph = generateGraphCompared(firstgraph, secondgraph);


    if (newgraph.isEmpty()) {
        queryForErrors();
        return;
    }

    // ** Dibujando el grafico
    doRenderGraph(newgraph,"");
}


QString MainWindow::generateGraphCompared(const QString& first, const QString& second) {
    QString result;
    QStringList myfirstlist, mysecondlist;
    myfirstlist = first.split("\n",QString::SkipEmptyParts);
//    qDebug("first:\n|%s|",qPrintable(first));
//    qDebug("....myfirstlist.count(): %d",myfirstlist.count());
    mysecondlist = second.split("\n",QString::SkipEmptyParts);
//    qDebug("second:\n|%s|",qPrintable(second));
//    qDebug("....mysecondlist.count(): %d",mysecondlist.count());

    if ( myfirstlist.count() != mysecondlist.count()) {
        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("Los gr�ficos seleccionados son diferentes."
                      " No tienen la misma cantidad de nodos."
                      " El primer gr�fico tiene \"%1\" nodos. "
                      " El segundo gr�fico tiene \"%2\" nodos." )
                    .arg(myfirstlist.count()-1)
                    .arg(mysecondlist.count()-1);
        return QString();
    }

    int totaltokens = 0;
    QVector<QPair<double,int> > vstats(myfirstlist.count());
    QVector<double> diffporcs(myfirstlist.count());

    bool ok;


    for(int i=0; i < myfirstlist.count(); i++ ) {
        QPair<double,int>  mypair;
        mypair.first = 0;
        mypair.second = 0;
        QString myfirst = myfirstlist.at(i);
        QString mysecond = mysecondlist.at(i);
        qDebug(".....myfirst:|%s|",qPrintable(myfirst));
        qDebug("..-.mysecond:|%s|\n",qPrintable(mysecond));
        QString statsfirst = myfirst.section(",",-2,-1).trimmed();
        QString statssecond = mysecond.section(",",-2,-1).trimmed();
        if (myfirst.left(myfirst.count()-statsfirst.length()+1)!=
            mysecond.left(mysecond.count()-statssecond.length()+1)) {
            qDebug("...entrando a NO comparacion");
            SafetYAWL::streamlog
                    << SafetLog::Error
                    << tr("No se pueden comparar los dos gr�ficos porque son diferentes <br/>"
                       "(<b>%1</b>) (<b>%2</b>)")
                    .arg(myfirst.split(",").at(0))
                    .arg(mysecond.split(",").at(0));

            return QString();
        }
        if ( !statsfirst.startsWith("info") || !statssecond.startsWith("info")) {
            vstats[i] = mypair;

            continue;
        }
        int tokens1 = statsfirst.split(",").at(1).split("...").at(0).toInt(&ok);
        int tokens2 = statssecond.split(",").at(1).split("...").at(0).toInt(&ok);
        mypair.second = tokens2 - tokens1;
        mypair.first = 0;
        vstats[i] = mypair;
        if ( mypair.second > 0 ) {
            totaltokens += mypair.second;
        }
    }

    for(int i=0; i < vstats.count(); i++ ) {
        if ( vstats[i].second > 0  ) {
            vstats[i].first = double(vstats[i].second)/double(totaltokens);

        }
        else {
            vstats[i].first =  0.0;

        }
        diffporcs[i] = 0;

    }

    for(int i=0; i < myfirstlist.count(); i++ ) {
        QString newline;
        bool ok;
        QString myfirst = myfirstlist.at(i);
        QString mysecond = mysecondlist.at(i);
        QString statsfirst = myfirst.section(",",-2,-1).trimmed();
        QString statssecond = mysecond.section(",",-2,-1).trimmed();
        //qDebug("...(2)...statsfirst: |%s|",qPrintable(statsfirst));
        if ( !statsfirst.startsWith("info") || !statssecond.startsWith("info")) {
            newline = myfirst;
            result = result + newline + "\n";
            continue;
        }

        double porc = vstats[i].first;
        QRegExp rx1,rx2;
        rx1.setPattern("\\s*(info\\.task\\.[a-zA-Z0-9\\-]+)\\s*:\\s+([0-9\\.]+)");
        rx2.setPattern("\\s*(info\\.task\\.[a-zA-Z0-9\\-]+)\\s*:\\s+([0-9\\.]+)");
        int pos = rx1.indexIn(statsfirst.split(",").at(0));
        if ( pos == -1 ) {
            newline = myfirst.left(myfirst.length()-statsfirst.length());
            result = result + newline + "\n";
            continue;
        }
        pos = rx2.indexIn(statssecond.split(",").at(0));
        if ( pos == -1 ) {
            qDebug("ERROR ERROR...(1) mainwindow.cpp 3514");
            continue;
        }

        double diffporc = rx2.cap(2).toDouble(&ok)-rx1.cap(2).toDouble(&ok);
        newline = myfirst.left(myfirst.length()-statsfirst.length());
        newline = mysecond.left(mysecond.length()-statssecond.length());

        newline = newline + rx1.cap(1) + QString(":%1,").arg(porc,0,'f',2)
                  + QString("%1...%2")
                  .arg(vstats[i].second)
                  .arg(totaltokens) + QString("...%1").arg(diffporc,0,'f',2);
        //qDebug("newline: |%s|",qPrintable(newline));
        result = result + newline + "\n";
        qDebug();
    }

    result = result.arg(totaltokens);
    qDebug("   **********result:\n|%s|",qPrintable(result));
    return result;
}


void MainWindow::evalEventOnExit(SafetLog::Level l) {
        if ( l == SafetLog::ExitApp ) {
             Q_CHECK_PTR ( MainWindow::mystatusbar );
             MainWindow::mystatusbar->showMessage(tr("Ocurrio un error.Importante: Revise el registro de eventos (log)"), 5000);

        }
}



QString MainWindow::evalEventOnInput(SafetYAWL::TypeInput p, const QString& message, bool& ok) {
        QString str;

        ok = true;

        switch ( p ) {
                case SafetYAWL::PIN: // Ver una implementacion mas segura
                     str = QInputDialog::getText(MainWindow::mymainwindow, tr("Contrase�a"),
                                          message, QLineEdit::Password,
                                          tr(""), &ok);
                break;
                default:;
        }
        return str;
}

QAbstractItemModel *MainWindow::modelFromFile(const QString& fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
        return new QStringListModel(completer);

    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
    QStringList words;

    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        if (!line.isEmpty())
            words << line.trimmed();
    }

    QApplication::restoreOverrideCursor();
    return new QStringListModel(words, completer);
}



void MainWindow::about()
{

    QString alpha, beta, release;
    QRegExp rx;
    rx.setPattern("([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)");

    int pos = rx.indexIn(SafetYAWL::getNumberVersion());
    qDebug("...version: pos: %d",pos);
    if (pos >= 0 ) {
        alpha = rx.cap(2);
        beta = rx.cap(3);
        release = rx.cap(4);
    }

    QMessageBox::about(this, tr("Cr�ditos"),
      tr("<p>Versi�n: %1</p> Alpha: %5 Beta: %6 Release: %7"
      "<p>Revisi�n svn: %2</p>"
      "<p>�ltimo registro svn: %3 </p>"
      "<p><b>SAFET</b>: Sistema Automatizado para la Firma Electr�nica "
      "y Estampado de Tiempo.</p>"
      "<p> %4</p>")
   .arg(SafetYAWL::getNumberVersion())
   .arg(SafetYAWL::getRevisionVersion())
   .arg(SafetYAWL::getLastlogVersion())
   .arg(SafetYAWL::getInfoVersion())
   .arg(alpha)
   .arg(beta)
    .arg(release)
   );
}

// *****************************************************************************************
// M�todo para la Consola


void MainWindow::doGetSignDocument() {
    GetSignDocumentDIalog* mydialog = new GetSignDocumentDIalog(this);
    if ( mydialog->exec()) {
        qDebug("yes");
    }

}


void MainWindow::doSendSignDocument() {
    SendSignDocumentDialog* mydialog = new SendSignDocumentDialog(this);
    if ( mydialog->exec()) {
        qDebug("yes");
    }

}


bool MainWindow::executeParsed() {

     //QMutexLocker locker(mutexConsole());


      if  (!commands.contains('f') ) {
           if ( !commands.contains('h') && !commands.contains('V') && !commands.contains('T') ) {
               streamText << tr("*** No se especifico la ruta del archivo de flujo de trabajo (.xml) *** \n");
               streamText  <<  tr("Opcion: -f <archivo> o --file <archivo> \n");
               sendStreamToOutput();
               return false;
          }
      } else {
           if ( configurator != NULL ) {
                delete configurator;
                configurator = new SafetYAWL();
           }

           configurator->setAutofilters( commands['a']);
           configurator->setRecursivefilters( commands['r']);
           SafetYAWL::streamlog.initAllStack();
           configurator->openXML(commands['f']);

           if (queryForErrors()) {
               //delete configurator;
               qDebug("...retornando.....executedParsed");
               return false;
           }

           configurator->openDataSources();

           configurator->convertXMLtoObjects();


           // *** Ver si el documento de flujo de trabajo tiene parametros
           qDebug("..........................PARAMETERS ..... (1)...");

           bool showdlgpar = SafetYAWL::getConf()["GeneralOptions/parameters.showdialog"]
                             .compare("on",Qt::CaseSensitive) == 0;


           if (showdlgpar ) {
               SafetWorkflow* mywf = configurator->getWorkflow();

               if (mywf) {
                   qDebug();
                   int n = mywf->getParameterlist().count();
                   qDebug(".......mywf->getParameterlist().count():%d",
                          n);
                   if ( n > 0 ) {
                       DialogFlowParameters mydialog(this);
                       for(int i = 0; i < n; i++) {
                           qDebug("title:|%s|", qPrintable(mywf->getParameterlist().at(i)->title()));
                           qDebug("configurekey:|%s|", qPrintable(mywf->getParameterlist().at(i)->configurekey()));

                           if (mywf->getParameterlist().at(i)->type() != "combotask" &&
                                   mywf->getParameterlist().at(i)->title() != tr("Nodo a agregar")  &&
                                   mywf->getParameterlist().at(i)->title() != tr("Textualinfo")  &&
                                   mywf->getParameterlist().at(i)->title() != tr("Paralelo al nodo siguiente"))  {
                                mydialog.parameters().append(mywf->getParameterlist().at(i));
                           }
                       }
                       if (mydialog.parameters().count() > 0 ) {


                            mydialog.layoutParameters();

                           if ( mydialog.exec() == QDialog::Rejected ) {
                               return false;
                           }
                       }


    //                   qDebug(".......values....(1)...");
    //                   for (int i=0; i < mydialog.getValues().count();i++) {
    //                       qDebug("...value: |%s|",qPrintable(mydialog.getValues().at(i)));
    //                   }
    //                   qDebug(".......values....(2)...");
                       qDebug(".....................SafetWorkflow::executedParsed.....(1)...");

                        QMap<QString,QString> parvalues = mydialog.getValues();

                        qDebug(".....................SafetWorkflow::executedParsed.....(*2*)...parvalues.count():|%d|",
                               parvalues.count());

                        qDebug(".....................SafetWorkflow::executedParsed.....(*2*)...mywf:|%p|", mywf);

                        if (mywf != NULL ) {
                         mywf->putParameters(parvalues);
                        }


                       qDebug(".....................SafetWorkflow::executedParsed.....(*3*)...");
                       /*
                        *      FIXME: Chequear par�metros de configuraci�n
                        */
                       int countpar = 0;
                       foreach(SafetParameter* myconfpar, mydialog.parameters()) {
                           if (!myconfpar->configurekey().isEmpty()) {
                               QString confvalue;
                               if (countpar < parvalues.keys().count()) {
                                   QString mykey = parvalues.keys().at(countpar);
                                   confvalue = parvalues[ mykey ];
                               }
                               QString mykey = myconfpar->configurekey();
                               if ( SafetYAWL::getConf().getMap()
                                   .contains(mykey)) {
                                   SafetYAWL::getConf().getMap()[mykey] = confvalue;
                                   qDebug("...**.......countpar:|%d|", countpar);
                                   qDebug("...**..........mykey:|%s|",qPrintable(mykey));
                                   qDebug("...**......confvalue:|%s|",qPrintable(confvalue));
                               }
                           }
                           countpar++;
                       }

                       qDebug("....SafetWorkflow::putParameters..............(2)...");
                   }

                   qDebug();

               }
               qDebug("..........................PARAMETERS ..... (2)...");
           }

      }



     if ( commands.contains('c') ) {
          streamText << tr("Chequeado...!") << endl;
     }

   QMap<int, QString>::const_iterator i;
   for (i = commands.constBegin(); i != commands.constEnd(); ++i) {
        if ( i.key() != 'f' ) {
                bool result = processCommand(i.key() );
                if ( !result ) {
                    return false;
                }
        }
  }

   sendStreamToOutput();

   return true;



}

void MainWindow::setConffileValues(const QMap<QString,QString>& mymap){

    for(int i=0; i< mymap.keys().count();i++) {
        QString mykey = mymap.keys().at(i);
        QString myvalue = mymap[mykey];
        if ( SafetYAWL::getConf().getMap()
                .contains(mykey)) {
            SafetYAWL::getConf().getMap()[mykey] = myvalue;
        }
    }
}





bool MainWindow::parseArgs(const QString& a) {

    commands.clear();
    QStringList args = a.split(QRegExp("\\s+"), QString::SkipEmptyParts);

    int currentOption = 0;
    foreach( QString par, args) {
         QRegExp rx;
         rx.setPattern("\\-[a-zA-Z]");
         int pos = rx.indexIn(par);
         if ( pos > -1 )  {
               Q_ASSERT( par.length() == 2 );
               currentOption = (int)(par.at(1).toAscii());
               qDebug("currentOption: %c", currentOption);
               commands.insert(currentOption,"");
         }
         else {
              Q_ASSERT( commands.contains(currentOption) );
              qDebug("commands['%c'] = %s", currentOption, qPrintable(par));
              commands[currentOption] = par;
         }

    }

}

bool MainWindow::parse(int &argc, char **argv) {
       int c;
       int verbose_flag = 0;
       qDebug("argv[0]: |%s|", argv[0]);
       qDebug("argv[1]: |%s|", argv[1]);
        parsed = true;
       while (1)
         {
             struct option long_options[] =
             {
               /* These options set a flag. */
               {"verbose", no_argument,       &verbose_flag , 1},
               {"brief",   no_argument,       &verbose_flag, 0},
               /* These options don't set a flag.
                  We distinguish them by their indices. */
               {"listtasks",   no_argument,       0, 'l'},
               {"listconditions",   no_argument,       0, 'C'},
               {"check",    no_argument,       0, 'c'},
               {"listdocuments",    no_argument,       0, 'd'},
               {"data",    no_argument,       0, 'D'},
               {"file",  required_argument, 0, 'f'},
               {"autofilter",  required_argument, 0, 'a'},
               {"recursivefilter",  required_argument, 0, 'r'},
               {"task",  required_argument, 0, 't'},
                {"template",  no_argument, 0, 'T'},
               {"variable",  required_argument, 0, 'v'},
               {"json",  required_argument, 0, 'j'},
               {"graph",  required_argument, 0, 'g'},
               {"sign",  no_argument, 0, 's'},
               {"keyvalue",  required_argument, 0, 'k'},
               {"plugins",  required_argument, 0, 'p'},
               {"stat",  required_argument, 0, 'S'},
               {"help",  no_argument, 0, 'h'},
               {"version",  no_argument, 0, 'V'},
               {0, 0, 0, 0}
             };
           /* getopt_long stores the option index here. */
           int option_index = 0;

           c = getopt_long (argc, argv, "lCcf:a:t:Tj:dv:g:k:sS:hVD:p:",
                            long_options, &option_index);

           if (c == -1)
             break;
//           qDebug("optarg: %s", optarg);
           switch (c)
             {
             case 0:
               /* If this option set a flag, do nothing else now. */
               if (long_options[option_index].flag != 0)
                 break;
               streamText << tr("Opcion: ") << long_options[option_index].name ;
               if (optarg)
                 streamText << tr(" con argumento: ") <<  optarg << endl;
               break;
             case 'l':
                        commands['l'] = "list";
               break;
             case 'C':
                        commands['C'] = "list";
               break;
             case 'd':
                        commands['d'] = "documents";
               break;
             case 'D':
                        commands['D'] = optarg;
               break;
             case 'c':
                        commands['c'] = "check";
                       break;
             case 'p':
                        commands['p'] = optarg;
                       break;
              case 'f':
                        commands['f'] = optarg;
                       break;
              case 'a':
                       if (!commands.contains('a') ) {
                            commands['a'] = optarg;
                       }
                       else {
                            commands['a'] = commands['a']+"," +QString(optarg);
                       }
                       break;
              case 'r':
                       if (!commands.contains('r') ) {
                            commands['r'] = optarg;
                       }
                       else {
                            commands['r'] = commands['r']+"," +QString(optarg);
                       }
                       break;
              case 'g':
                        commands['g'] = optarg;
                       break;
             case 'v':
                        commands['v'] = optarg;
                        //(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(!commands['v'].trimmed().startsWith("-"), tr("Debe especificar un nombre de variable, falta este parametro")) );
                        if (commands['v'].trimmed().startsWith("-") ) {
                            SafetYAWL::streamlog
                                    << SafetLog::Error
                                    <<
                            tr("Debe especificar un nombre de variable, falta este parametro");
                        }
                       break;
             case 'k':
                        commands['k'] = optarg;
//			(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(!commands['k'].trimmed().startsWith("-"), tr("Debe especificar un valor para la clave, falta este parametro")) );
                        break;
             case 't':
                        commands['t'] = optarg;
                        break;
             case 'T':
                        commands['T'] = "template";
                        break;
             case 's':
                        commands['s'] = "sign";
                        break;
             case 'S':
                        commands['S'] = optarg;
                        break;
             case 'h':

                        commands['h'] = "help";
                        break;
             case 'V':
                        commands['V'] = "version";
             case '?':
               /* getopt_long already printed an error message. */
               break;

             default:
                        parsed = false;
             }
         }

       //	qDebug("!commands.contains('f'): %d", !commands.contains('f'));
        if ( commands.contains('h') || commands.contains('V') || commands.contains('T')) return true;
        if ( !commands.contains('f')  ) {
                streamText << tr("*** No se especifico la ruta del archivo de flujo de trabajo (.xml) *** \n");
                streamText  <<  tr("Opcion: -f <archivo> o --file <archivo> \n");
//		streamText.flush();
                sendStreamToOutput();
                parsed = false;
                processCommand('f');
        }

       if (verbose_flag)
         streamText << tr("La opci�n (verbose) est� activa\n");

       /* Print any remaining command line arguments (not options). */
       if (optind < argc)
         {
//	   qDebug("optind: %d argc: %d", optind, argc);
           streamText << tr("Parametro(s) invalido(s): ");
           while (optind < argc)
             streamText <<  argv[optind++];
             streamText << endl;
                parsed = false;
         }

        if (!parsed ) return false;

        configurator->openXML(commands['f']);
  //      configurator->openDataSources();
        configurator->convertXMLtoObjects();

        // llamada a la funcion que habilita las fuentes de datos
        //qDebug("MainWindow: configurator->openDataSources()");

        //qDebug("SafetYAWL::databaseOpen: %d", SafetYAWL::databaseOpen);

        //configurator->loadPlugins();

        if ( commands.contains('c') ) {
                streamText << tr("Chequeado...!") << endl;
        }

        return parsed;
}


void MainWindow::sendStreamToOutput() {
     streamText.flush();
     //qDebug("...outputText: |%s|", qPrintable(outputText));
//     if ( myoutput != NULL ) {
//          myoutput->clear();
//          myoutput->setPlainText( outputText );
//     }
//     if (centralWidget != NULL ) {
//          centralWidget->setTabText(4, tr("Salida*"));
//     }
     outputText = "";

}


bool MainWindow::processCommand(int command) {
        bool result = true;
        Q_CHECK_PTR( configurator->getWorkflows().at(0) );
        switch ( command ) {
        case 'f':
            if (commands['f'].length() == 0 ) {
                return false;
            }
// 		e = QFile::exists( commands['f'] );
// 		if (!e) {
// 			streamText << tr("No es posible leer el archivo de flujo de trabajo de nombre: \"%1\" ").arg(commands['f']) << endl;
// 			parsed = false;
// 			quit();
// 		}
                break;
        case 'l':
                listTasks();
                break;
        case 'd':
                qDebug("...listDocuments...(1)...");
                listDocuments();
                break;
        case 'D':
                manageData();
                break;
        case 'g':
                result = genGraph();
                break;
        case 's':
                signDocument();
                break;
        case 'S':
                calStatistics();
                break;
        case 'T':
                /* now Nothing */
                break;
        case 'h':
                displayUsage();
                break;
        case 'V':
                version();
                break;

        default:;
        }

        return result;
}



void MainWindow::displayUsage() {
streamText << tr("Uso: safet --file <archivo de flujo de trabajo> [comandos]") << endl;
streamText << tr("     safet -f <archivo de flujo de trabajo> [comandos]") << endl;
streamText << tr("Cliente Safet de linea de comandos, version 0.1.beta") << endl;
streamText << tr("Tipee 'safet --help ' para ayuda sobre los comandos.") << endl;
streamText << tr("Tipee 'safet --version' o 'safet -V' para ver la version del cliente de linea de comandos.") << endl;
streamText << endl;
streamText << endl;
streamText << tr("Los comandos reciben parametros de tipo variable, clave o subcomando, o algunos") << endl;
streamText << tr("de los comandos no requieren ningun parametro. Si no se proveen par�metros a") << endl;
streamText << tr("estos comandos, se generara un mensaje de error.") << endl;
streamText << endl;
streamText << tr("Comandos disponibles:") << endl;
streamText << endl;
streamText << tr("-f --file=ARCHIVO\tIndica el archivo (ARCHIVO) de flujo de trabajo a procesar]") << endl;
streamText << tr("\t\t\tGeneramente debe tener extension .xml y se valida con el archivo de") << endl;
streamText << tr("\t\t\tdefinicion yawlworkflow.dtd") << endl;
streamText << endl;
streamText << tr("-v --variable=NOMBRE\tSelecciona el nombre de la variable a procesar. La variable") << endl;
streamText << tr("\t\t\tdebe estar incluida en el archivo de flujo de trabajo a procesar, e identificada") << endl;
streamText << tr("\t\t\tcon el atributo 'id'.") << endl;
streamText << endl;
streamText << tr("-k --key=CLAVE\t\tIndica la clave para un comando de firma, u otro comando que") << endl;
streamText << tr("\t\t\tlo requiera, generalmente debe ir acompanada del parametro que selecciona el nombre") <<endl;
streamText << tr("\t\t\tde la variable") << endl;
streamText << endl;
streamText << tr("-s --stat=SUBCOMANDO\tGenera la estadistica seleccionada en el subcomando SUBCOMANDO.") << endl;
streamText << tr("\t\t\tLos subcomandos posibles son \"ndocs\" (lista la cantidad de documentos para todas las") << endl;
streamText << tr("\t\t\tvariables) y \"path\" (indica en que actividad/tarea se encuentra el token segun") << endl;
streamText << tr("\t\t\tla clave indicada con el comando -k o --key)") << endl;
streamText << endl;
streamText << tr("-g --graph=ARCHIVO\tGenera un archivo grafico (png,svg,etc.) del flujo") << endl;
streamText << tr("\t\t\tde trabajo. La extension del archivo indica el formato en que se escribe.") << endl;
streamText << tr("\t\t\tSi no se especifica parametro se genera por defecto un archivo con ") << endl;
streamText << tr("\t\t\tel nombre 'output.png' en el directorio actual") << endl;
streamText << endl;
streamText << tr("-c --check\t\tChequea que el archivo de flujo de trabajo indicado") << endl;
streamText << tr("\t\t\ten la opcion -f o --file cumpla todas las reglas (Sintaxis,enlace con") << endl;
streamText << tr("\t\t\trepositorio de datos, etc.) para ser procesado por un comando") << endl;
streamText << endl;
streamText << tr("-l --listtasks\t\tLista todas las tareas/actividades que contiene el archivo") << endl;
 streamText << tr("\t\t\tde flujo de trabajo indicado en la opcion -f o --file") << endl;
streamText << endl;
streamText << tr("-V --version\t\tMuestra la version actual de la aplicacion de linea ") << endl;
streamText << tr("\t\t\tde comandos 'safet'.") << endl;
streamText << endl;
streamText << tr("-h --help\t\tMuestra este texto de ayuda") << endl;
streamText << endl;
streamText << tr("-d --listdocuments\tMuestra los documentos presentes en la variable indicada") << endl;
streamText << tr(" \t\t\tcon el parametro -v o --variable") << endl;
streamText << endl;
streamText << endl;
streamText << tr("Safet es una herramienta para gestion de flujos de trabajos basados en Redes de ") << endl;
streamText << tr("Petri y patrones (AND,OR,XOR, etc), y que incorpora Firma ") << endl;
streamText << tr("Electronica bajo el modelo de Infraestructura de Clave Publica.") << endl;
streamText << endl;
streamText << endl;
streamText << tr("  Software utilizado principalmente en safet:") << endl;
streamText << endl;
streamText << tr("  Libreria Qt:  referirse a: http://www.trolltech.com") << endl;
streamText << tr("   LibDigidoc:  referirse a: http://www.openxades.org") << endl;
streamText << tr("     Graphviz:  referirse a: http://www.graphviz.org") << endl;
streamText << tr("        DbXml:  referirse a: http://www.sleepycat.com") << endl;
streamText << endl;

}


void MainWindow::calStatistics() {

        if ( commands['S'].compare("ndocs", Qt::CaseInsensitive ) == 0 ) {
                streamText << "Estadisticas para el archivo: <" << commands['f'] << ">:" << endl;
                foreach(QString name,configurator->getWorkflows().at(0)->variablesId()){
                        SafetVariable *v = configurator->getWorkflows().at(0)->searchVariable( name );
                        Q_CHECK_PTR( v );
                        streamText << tr("Variable <") << name << tr(">  Numero de fichas: <") << configurator->getWorkflows().at(0)->numberOfTokens(*v) << ">" << endl;
                }
                streamText << endl;
        } else if ( commands['S'].compare("path", Qt::CaseInsensitive ) == 0 ) {
                //(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(commands['k'].length() > 0 , tr("No se especifico el valor de la clave (--keyvalue <valor>) para firmar (sign) el documento")) );
                SafetYAWL::evalAndCheckExit(commands['k'].length() > 0 , tr("No se especifico el valor de la clave (--keyvalue <valor>) para firmar (sign) el documento"));

                QString info = 	commands['k'];
                (*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(commands['t'].length() > 0 , tr("No se especifico un nombre de tarea  (--task <nombre>) para calcular e imprimir los caminos")) );
                QString result = configurator->getWorkflows().at(0)->printPaths(commands['t'],info);
                streamText << tr("Camino(s) para : ") << info << endl;
                streamText << result << endl;
        } else {
                streamText << tr("Argumento del comando Estadisticas -S <arg> o -stat <arg> invalido. Opciones: ndocs, percent, path") << endl;
        }
}



void MainWindow::signDocument() {
        //(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(commands['k'].length() > 0 , tr("No se especifico el valor de la clave (--keyvalue <valor>) para firmar (sign) el documento")) );
        SafetYAWL::evalAndCheckExit(commands['k'].length() > 0 , tr("No se especifico el valor de la clave (--keyvalue <valor>) para firmar (sign) el documento"));

        //(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(commands['v'].length() > 0 , tr("No se especifico un nombre de variable  (--variable <nombre>) para firmar (sign) el documento")) );
        SafetYAWL::evalAndCheckExit(commands['v'].length() > 0 , tr("No se especifico un nombre de variable  (--variable <nombre>) para firmar (sign) el documento"));


        SafetVariable *v = configurator->getWorkflows().at(0)->searchVariable( commands['v'] );

        //(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(v != NULL , tr("No existe la variable : \"%1\" en el documento de flujo de trabajo").arg(commands['v'])) );
        SafetYAWL::evalAndCheckExit(v != NULL , tr("No existe la variable : \"%1\" en el documento de flujo de trabajo").arg(commands['v']));

        QString derror;
        QString documentid = configurator->getWorkflows().at(0)->signDocument(v, commands['k'],derror);
        if (documentid.length() > 0 ) {
          streamText << tr("Documento con id:")  << documentid << " firmado correctamente OK!" << endl;
          MainWindow::mystatusbar->showMessage(tr("Documento con id: %1 firmado correctamente OK!").arg(documentid));
        }
        else {
           streamText << " NO se introdujo la CLAVE del documento a firmar. NO se realizo la FIRMA." << endl;
           MainWindow::mystatusbar->showMessage(tr("NO se introdujo la CLAVE correcta del documento a firmar. NO se realizo la FIRMA"));
        }

}


bool MainWindow::genGraph() {
    char buffer[512];



         QString info;
         if ( commands.contains('p') ) {
             qDebug("***...command['p']: |%s|",qPrintable(commands['p']));
           configurator->loadPlugins(commands['p']);
         }

         if ( commands.contains('k') ) {
                 info = 	commands['k'];
         }

         strncpy(buffer, qPrintable(SafetYAWL::getConf()["Plugins.Graphviz/graphtype"]), 4);
         SYD << tr("...MainWindow::doRenderGraph.....buffer:|%1|")
                .arg(buffer);

         SafetYAWL::streamlog.initAllStack();
         QString myjson;
         QString img = configurator->getWorkflows().at(0)->generateGraph(buffer, myjson,info);


        SYD << tr("\n\n...MainWindow::genGraph().......:\n|%1|")
         .arg(img);

//        _currenttable = myjson;

//        SYD << tr("\n\n...MainWindow::genGraph()..............currentTable:\n|%1|")
//         .arg(_currenttable);

        if ( img.isEmpty() ) {
                queryForErrors();

             return false ;
         }

         //SafetYAWL::lastinfodate = "";
         /*_currentjson = */ drawWorkflow(img); // Dibujar el flujo



        return true;
}


void MainWindow::doCipherFile(bool iscipher) {


    // ** Cifrando
    if (iscipher) {
        QString fileconf = SafetYAWL::pathconf+ "/" + "auth.conf";
        SafetCipherFile myfile(fileconf);
        qDebug("doCipherFile fileconf: %s", qPrintable(fileconf));
        myfile.open(QIODevice::ReadOnly);
        myfile.setPassword("caracas");
        QString fileconfcipher = SafetYAWL::pathconf+ "/" + "auth.conf.bin";
        qDebug("doCipherFile fileconfcipher: %s", qPrintable(fileconfcipher));
        myfile.cipherInFile(fileconfcipher);
        qDebug("...ok!");
        myfile.close();
    }
    else {
        // ****** Leyendo cifrado
        QString fileconf = SafetYAWL::pathconf+ "/" + "auth.conf.bin";
        SafetCipherFile myfile(fileconf);
        myfile.open(QIODevice::ReadOnly);
        myfile.setPassword("caracas");
        qDebug("..empezando a leer...");
        myfile.readAllCipher();
        QString line;
        while( true ) {
            line = myfile.readCipherLine();
            if (line.isNull()) {
                break;
            }
            qDebug("%s",qPrintable(line));
        }

    }

}


void MainWindow::listTasks()
{
        Q_CHECK_PTR( MainWindow::configurator );
        QString outtasks = configurator->getWorkflows().at(0)->listTasks( commands['C'].length() > 0 );
        streamText << "Lista de identificadores  de tareas/actividades:"  << endl;
                streamText << outtasks;
}



void MainWindow::manageData() {
//	streamText << "Data: <" << commands['D'] << ">" << endl;
        SafetTextParser parser;
        parser.setStr( commands['D'] );
        QString str = "agregar,eliminar,actualizar,mostrar";
        QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
        parser.setCommands(str.split(","),commandstr.split(","));
        QString xml = parser.toXml();
        SafetYAWL::streamlog << SafetLog::Debug << tr("Cadena XML generada de la entrada: \n\"%1\"").arg(xml);
//	qDebug("\n\n....xml:\n%s", qPrintable(xml));
        parser.processInput( xml );
        parser.openXml();
        parser.processXml();
}


void MainWindow::version() {
streamText << SafetYAWL::toUcode(QString(INFO_VERSION)) << endl;

}


void MainWindow::listDocuments(const QString& key) {
    qDebug("...listDocuments()....info: |%s|",
           qPrintable(key));
    char buffer[20];
    strncpy(buffer, "svg", 4);
    qDebug("...configurator...%p",configurator);
    qDebug("...configurator->getWorkflows().at(0)...%p",configurator->getWorkflows().at(0));
    QString code = configurator->getWorkflows().at(0)->generateCodeGraph(buffer,key,false);

    qDebug("...code...");

    QList<MainWindow::Miles> ordermiles;

    if(!code.isEmpty()) { // !code.isEmpty
        qDebug("...listDocuments...key...Listando....");
        SafetWorkflow::ExtraInfoSearchKey& sk = configurator->getWorkflows().at(0)->searchKey();
        qDebug("...sk.key():|%s|", qPrintable(sk.key));
        qDebug("sk.extrainfo.keys(): |%d|", sk.extrainfo.keys().count());

        QDateTime previewdate;
        for( int i = 0; i < sk.extrainfo.keys().count(); i++) {
            QString mykey = sk.extrainfo.keys().at(i);
            qDebug("...%s->%s",
                   qPrintable(mykey),
                   qPrintable(sk.extrainfo[ mykey]));
            QString mydata = sk.extrainfo[ mykey];
            QStringList mylist = mydata.split(QRegExp(";|<br/>"));
            foreach(QString d, mylist) {
                qDebug("...d:|%s|",qPrintable(d));
            }
            if (mylist.count() != 3 ) {
                continue;
            }
            QDateTime mydate = QDateTime::fromString(mylist.at(1),"dd/MM/yyyy hh:mmap");
            if (!mydate.isValid()) {
                qDebug(".....listDocuments....no Valid datetime");
                continue;
            }

            Miles mymile;
            mymile.nametask = mykey;

            mymile.ts = mydate;

            mymile.secondswait = 0;


            mymile.humandate = mylist.at(2);
            mymile.rol = mylist.at(0);

            int pos = -1;
            qDebug("....ordermiles.count():%d",ordermiles.count());
            for(int j=0; j < ordermiles.count(); j++) {
                if ( mymile.ts  < ordermiles.at(j).ts ) {
                    qDebug("...mymile.ts:%s",qPrintable(mymile.ts.toString()));
                    qDebug("...ordermiles.at(%d).ts:%s",j,qPrintable(ordermiles.at(j).ts.toString()));
                    pos = j;
                    break;

                }
            }
            if (pos == -1 ) {
                pos = ordermiles.count();
            }
            qDebug("....insertando en ordermiles...pos: |%d|", pos);
            ordermiles.insert(pos,mymile);
            previewdate  = mydate; // Colocar fecha anterior
        }

        qDebug("....iniciando Ordermiles....(1)....");

        for(int i = 0; i< ordermiles.count();i++) {
            Miles &m = ordermiles[i];
            qDebug("m.task:|%s|", qPrintable(m.nametask));
            qDebug("m.date:|%s|", qPrintable(m.ts.toString()));
            qDebug("*m.humandate:|%s|", qPrintable(m.nametask));
            m.humanwait = tr("n/a");

            if ( i > 0 ) {
                Miles &prev = ordermiles[i-1];
                prev.secondswait = prev.ts.secsTo(m.ts);
                int days = 0;
                prev.humanwait = SafetWorkflow::humanizeDate(days,prev.ts.toString("dd/MM/yyyy hh:mm:ssap"),
                                                             "dd/MM/yyyy hh:mm:ssap",
                                                             m.ts);
                qDebug("m.secondstowait (prev):|%d|", prev.secondswait);
                qDebug("m.humanwait (prev):|%s|", qPrintable(prev.humanwait));

            }
            qDebug("m.rol:|%s|", qPrintable(m.rol));
            qDebug();

        }

        QList<QSqlField> myfields;
        _listprincipalcount = ordermiles.count();
        _listprincipaltitle = tr("Lista por Clave \"%1\"")
                                 .arg(key);
        QString myjson = getJSONMiles(ordermiles,myfields);
        qDebug("...myjson:\n%s\n",
               qPrintable(myjson));

        _listprincipalvariable = "";
        _listprincipalkey = key;

        centralWidget->setTabText(3,tr("Reportes*"));
         generateJScriptreports(myjson,myfields);


    } // !code.isEmpty



}

QString MainWindow::getJSONMiles(const QList<MainWindow::Miles>& miles,
                                 QList<QSqlField>& myfields) {
    QString str;
    QTextStream out(&str);
    myfields.clear();
    QSqlField f;
    f.setName(tr("Tarea"));
    f.setType(QVariant::String);
    myfields.append(f);
    f.setName(tr("Rol"));
    myfields.append(f);
    f.setName(tr("Fecha"));
    myfields.append(f);
    f.setName(tr("Tiempo"));
    myfields.append(f);
    f.setName(tr("Espera"));
    myfields.append(f);

    for( int i = 0; i < miles.count(); i++) {
        QString cadena("");
        out << "\t\t{ ";
            cadena.append(" ");
            cadena.append(tr("Tarea"));
            cadena.append(": \"");
            cadena.append(miles.at(i).nametask);
            cadena.append("\", ");
            cadena.append(" ");
            cadena.append(tr("Rol"));
            cadena.append(": \"");
            cadena.append(miles.at(i).rol);
            cadena.append("\", ");
            cadena.append(" ");
            cadena.append(tr("Fecha"));
            cadena.append(": \"");
            cadena.append(miles.at(i).ts.toString("dd/MM/yyyy hh:mmap"));
            cadena.append("\", ");
            cadena.append(" ");
            cadena.append(tr("Tiempo"));
            cadena.append(": \"");
            cadena.append(miles.at(i).humandate);
            cadena.append("\"");
            cadena.append(", ");
            cadena.append(tr("Espera"));
            cadena.append(": \"");
            cadena.append(miles.at(i).humanwait);
            cadena.append("\"");
            out << cadena ;
            out << "},\n";
            cadena.clear();
    }
    str.chop(2);
    return str;
}

bool MainWindow::addCRUDtoXML(const ParsedSqlToData &data) {
    bool result = false;
    foreach(QString mykey, data.map.keys()) {
        qDebug("......addCRUDtoXML..data[%s]=%s", qPrintable(mykey), qPrintable(data.map[mykey]));
    }
    QString mytable = data.map["nombre_crud"];
    QString fname = QDir::homePath() + "/" + Safet::datadir + "/" + "input" + "/" + "deftrac.xml";

    qDebug("...........addCRUDtoXML....fname:|%s|", qPrintable(fname));

    QDomDocument doc;
    QFile file(fname);
    if (!QFile::exists(fname) ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no existe en el sistema de archivos"
                  ".No es posible agregar un estado al archivo")
               .arg(fname);

        return result;
    }

    bool isopen = file.open(QIODevice::ReadOnly);
    if (!isopen ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no se puede leer"
                  ".No es posible agregar un estado al archivo")
               .arg(fname);


        return result;
    }


    bool iscontent = doc.setContent(&file);

    if (!iscontent) {
        SYE << tr("El formato del archivo XML \"%1\" no es v�lido")
               .arg(fname);
        return result;
    }

    file.close();
    QDomElement docElem = doc.documentElement();
    QDomNode mylast = docElem.lastChild();

    QDomElement mytitle = mylast.cloneNode().toElement();
    QDomElement myinsert = mylast.cloneNode().toElement();
    QDomElement myupdate = mylast.cloneNode().toElement();
    QDomElement mydelete = mylast.cloneNode().toElement();



    QDomNodeList mylist = mytitle.childNodes();

    for(int i=0; i < mylist.count(); i++) {
        mytitle.removeChild(mylist.at(i));
    }

    for(int i=0; i < mylist.count()-1; i++) {
        myinsert.removeChild(mylist.at(i));
    }

    for(int i=0; i < mylist.count()-1; i++) {
        myupdate.removeChild(mylist.at(i));
    }
    for(int i=0; i < mylist.count()-1; i++) {
        mydelete.removeChild(mylist.at(i));
    }


    if (myinsert.firstChild().isNull()) {
        SYE << tr("Ocurrio un error, no hay comanods en la operacion \"%1\"")
               .arg(mytitle.attribute("name"));
        qDebug("error command");
        return false;
    }

    if (myupdate.firstChild().isNull()) {
        SYE << tr("Ocurrio un error, no hay comanods en la operacion update \"%1\"")
               .arg(mytitle.attribute("name"));
        qDebug("error command");
        return false;
    }

    if (mydelete.firstChild().isNull()) {
        SYE << tr("Ocurrio un error, no hay comanods en la operacion delete \"%1\"")
               .arg(mytitle.attribute("name"));
        qDebug("error command");
        return false;
    }


// ********************** INSERT

    QDomElement mycommand = myinsert.firstChild().toElement();

    mycommand.setAttribute("table",mytable);
    mycommand.setAttribute("type","agregar");

    QDomNodeList mynodefields = mycommand.firstChild().childNodes();

    if (mynodefields.count() == 0) {
        SYE << tr("Ocurrio un error, no hay comanods en la operacion \"%1\"")
               .arg(myinsert.attribute("name"));
        qDebug("error command 2");
        return false;
    }

    QDomElement myelement = mycommand.firstChild().firstChild().toElement();



    for(int i=0; i < mynodefields.count(); i++) {
        mycommand.firstChild().removeChild(mynodefields.at(i));
    }

   QDomElement myfield;
    for( int i = 0; i < 25; i++) {

        QString mykey = QString("Campo_%1").arg(i+1);
        if (!data.map.contains(mykey)) {
            break;
        }
        QStringList myvalue = data.map[mykey].split(";");
        if (myvalue.count() < 2) {
            continue;
        }
        qDebug(".......getfields...MYKEY:|%s|",qPrintable(myvalue.at(0)));

        QDomNode mynodefield = myelement.cloneNode();


        myfield = mynodefield.toElement();
        myfield.replaceChild(doc.createTextNode(myvalue.at(0)),myfield.firstChild());


        myfield.setAttribute("title",myvalue.at(0));


        if (myvalue.at(1) == "text") {
            myfield.setAttribute("type","string");
            myfield.setAttribute("mandatory","no");
            myfield.setAttribute("primarykey","no");
        }
        else if (myvalue.at(1) == "serial") {
            QString myseq = QString("%1_id_seq").arg(mytable);
            myfield.setAttribute("sequence", myseq);
            myfield.setAttribute("type","string");
            myfield.setAttribute("primarykey","yes");
            myfield.setAttribute("mandatory","yes");

        }
        else if ( myvalue.at(1) == "integer" && myvalue.at(0) != "id") {
            myfield.setAttribute("type","number");
            myfield.setAttribute("mandatory","no");


        }

        myfield.setAttribute("options","");



        qDebug(".......getfields...setting *nodevalue:|%s|",qPrintable(myfield.nodeValue()));
        mycommand.firstChild().appendChild(myfield);



    }



    mytitle.setAttribute("name",mytable);
    mytitle.removeAttribute("type");
    mytitle.removeAttribute("type");
    myupdate.removeAttribute("type");
    mydelete.removeAttribute("type");

    mytitle.setAttribute("desc","");


    myinsert.setAttribute("name",QString("agregar_%1").arg(mytable));
    myinsert.setAttribute("desc","");



    docElem.insertAfter(mytitle,mylast);
    docElem.insertAfter(myinsert,mytitle);


// *********************** UPDATE



    QDomElement myupcommand = myupdate.firstChild().toElement();

    myupcommand.setAttribute("table",mytable);
    myupcommand.setAttribute("type","actualizar");

    QDomNodeList myupnodefields = myupcommand.firstChild().childNodes();

    if (myupnodefields.count() == 0) {
        SYE << tr("Ocurrio un error, no hay comanods en la operacion \"%1\"")
               .arg(myupdate.attribute("name"));
        qDebug("error command 2");
        return false;
    }

    QDomElement myupelement = myupcommand.firstChild().firstChild().toElement();



    for(int i=0; i < myupnodefields.count(); i++) {
        myupcommand.firstChild().removeChild(myupnodefields.at(i));
    }

   QDomElement myupfield;
    for( int i = 0; i < 25; i++) {

        QString mykey = QString("Campo_%1").arg(i+1);
        if (!data.map.contains(mykey)) {
            break;
        }
        QStringList myvalue = data.map[mykey].split(";");
        if (myvalue.count() < 2) {
            continue;
        }
        qDebug(".......getfields...update....MYKEY:|%s|",qPrintable(myvalue.at(0)));

        QDomNode mynodefield = myupelement.cloneNode();


        myupfield = mynodefield.toElement();
        myupfield.replaceChild(doc.createTextNode(myvalue.at(0)),myupfield.firstChild());


        myupfield.setAttribute("title",myvalue.at(0));


        if (myvalue.at(1) == "text") {
            myupfield.setAttribute("type","string");
            myupfield.setAttribute("mandatory","no");
            myupfield.removeAttribute("primarykey");
            myupfield.setAttribute("options","");
        }
        else if (myvalue.at(1) == "serial") {
            myupfield.setAttribute("type","combolisttable");
            myupfield.setAttribute("changekey","yes");
            myupfield.setAttribute("primarykey","yes");
            myupfield.setAttribute("mandatory","yes");
            myupfield.setAttribute("options",QString("%1:%2").arg(myvalue.at(0)).arg(mytable));

        }
        else if ( myvalue.at(1) == "integer" && myvalue.at(0) != "id") {
            myupfield.setAttribute("type","number");
            myupfield.setAttribute("mandatory","no");
            myupfield.removeAttribute("primarykey");
            myupfield.setAttribute("options","");


        }





        qDebug(".......getfields...setting *nodevalue:|%s|",qPrintable(myupfield.nodeValue()));
        myupcommand.firstChild().appendChild(myupfield);



    }


    myupdate.setAttribute("name",QString("modificar_%1").arg(mytable));
    myupdate.setAttribute("desc","");


    docElem.insertAfter(myupdate,myinsert);




// *********************** DELETE

{
        QDomElement myupcommand = mydelete.firstChild().toElement();

        myupcommand.setAttribute("table",mytable);
        myupcommand.setAttribute("type","eliminar");

        QDomNodeList myupnodefields = myupcommand.firstChild().childNodes();

        if (myupnodefields.count() == 0) {
            SYE << tr("Ocurrio un error, no hay comanods en la operacion \"%1\"")
                   .arg(myinsert.attribute("name"));
            qDebug("error command 2");
            return false;
        }

        QDomElement myelement = myupcommand.firstChild().firstChild().toElement();

        for(int i=0; i < myupnodefields.count(); i++) {
            myupcommand.firstChild().removeChild(myupnodefields.at(i));
        }

       QDomElement myupfield;
        for( int i = 0; i < 25; i++) {

            QString mykey = QString("Campo_%1").arg(i+1);
            if (!data.map.contains(mykey)) {
                break;
            }
            QStringList myvalue = data.map[mykey].split(";");
            if (myvalue.count() < 2) {
                continue;
            }
            qDebug(".......getfields...update....MYKEY:|%s|",qPrintable(myvalue.at(0)));

            QDomNode mynodefield = myelement.cloneNode();


            myupfield = mynodefield.toElement();
            myupfield.replaceChild(doc.createTextNode(myvalue.at(0)),myupfield.firstChild());


            myupfield.setAttribute("title",myvalue.at(0));

            if (myvalue.at(1) != "serial") {
                qDebug("...serial....1");
                continue;
            }
            myupfield.setAttribute("type","combolisttable");
            myupfield.setAttribute("primarykey","yes");
            myupfield.setAttribute("mandatory","yes");
            myupfield.setAttribute("options",QString("%1:%2").arg(myvalue.at(0)).arg(mytable));

            qDebug(".......getfields...setting delete *nodevalue:|%s|",qPrintable(myupfield.nodeValue()));
            myupcommand.firstChild().appendChild(myupfield);
            break;



        }

        mydelete.setAttribute("name",QString("borrar_%1").arg(mytable));
        mydelete.setAttribute("desc","");

        docElem.insertAfter(mydelete,myupdate);


}


// ************* SAVE DOC tree to file

    QFile myfile(fname);
    if ( !myfile.open(QIODevice::WriteOnly))  {
                SYE << tr("No es posible escribir el archivo \"%1\" con los datos agregados")
                .arg(fname);

        return result;
    }

    result = true;

    QTextStream out(&myfile);

    out << doc.toString();

    myfile.close();

    QString myauth1 = QString(""
                              "operacion:Agregar_operaci�n\nNombre_operacion: %1\n"
                              "Permitido_a_usuarios: %2\n"
                              "Tipos_de_permiso: read;write;modify\n"
                              "Permitido_a_roles: Administrador"
                              ""
                )
            .arg(QString("agregar_%1").arg(mytable))
            .arg(SafetYAWL::currentAuthUser());
    QString myauth2 = QString(""
                              "operacion:Agregar_operaci�n Nombre_operacion: %1\n"
                              "Permitido_a_usuarios: %2\n"
                              "Tipos_de_permiso: read;write;modify\n"
                              "Permitido_a_roles: Administrador\n"
                              ""
                )
            .arg(QString("modificar_%1").arg(mytable))
            .arg(SafetYAWL::currentAuthUser());

    QString myauth3 = QString(""
                              "operacion:Agregar_operaci�n Nombre_operacion: %1\n"
                              "Permitido_a_usuarios: %3\n"
                              "Tipos_de_permiso: read;write;modify\n"
                              "Permitido_a_roles: Administrador\n"
                              ""
                )
            .arg(QString("borrar_%1").arg(mytable))
            .arg(SafetYAWL::currentAuthUser());

    qDebug(".........***InputUsers....myauth:\n|%s|\n", qPrintable(myauth1));
    completingTextEdit = completingTextEditUsers;
    setModelCompleter(4); // Colocar para 2

    toInputUsers(myauth1);
    toInputUsers(myauth2);
    toInputUsers(myauth3);

    completingTextEdit = completingTextEditSign;
    setModelCompleter(3); // Colocar para 2

    return result;





}

bool MainWindow::doCreateCRUD(const ParsedSqlToData &data)
{



    QString myfields = "  ";

    for( int i = 0; i < 25; i++) {

        QString mykey = QString("Campo_%1").arg(i+1);
        if (!data.map.contains(mykey)) {
            break;
        }
        QString myvalue = data.map[mykey];

        QString newstring = myvalue.replace(";"," ") + ", ";
        myfields = myfields + newstring;

    }


    myfields.chop(2);

    myfields = myfields.trimmed();

    qDebug("myfields:|%s|", qPrintable(myfields));

    SafetBinaryRepo myrepo;
    myrepo.setDbtype(SafetBinaryRepo::PSQL);



    if (!myrepo.open()) {
           qDebug("...no opened...");
           SYE <<  tr("No es posible abrir la bd");
           return false;
    }

    qDebug("dbopened...1");

    qDebug("creating table...1");
    QString mytable = data.map["nombre_crud"];
    if ( !myrepo.createTable(mytable,myfields) ) {
                qDebug("creating table...not");
               SYE << tr("Fallo la creacion de la tabla \"%1\" con los campos:\"%2\"")
                      .arg(mytable).arg(myfields);
               return false;
    }
    qDebug("creating table...yes!");
    SYD << tr("*************Yes, SafetBinaryRepo....db opened...1");
    addCRUDtoXML(data);
    SYD << tr("*************Yes, SafetBinaryRepo....db opened...2");

    return true;
}

void MainWindow::listDocuments() {

     qDebug("...*listDocuments...commands['v']: %s", qPrintable(commands['v']));
     qDebug("...MainWindow::listDocuments()....");
     QString info;
     if ( commands.contains('k') ) {
             info = 	commands['k'];
             listDocuments(info);
             return;
     }

        SafetVariable* var = configurator->getWorkflows().at(0)->searchVariable( commands['v'] );

        if (var == NULL ) {
                streamText << tr("La variable \"%1\" NO existe en la especificacion de flujo de trabajo. Revise el nombre en el documento de flujo de trabajo").arg(commands['v']) << endl;
                SafetYAWL::streamlog
                        << SafetLog::Error
                        <<  tr("La variable \"%1\" NO existe en la especificacion de flujo de trabajo."
                               "Revise el nombre en el documento de flujo de trabajo")
                        .arg(commands['v']);
                return;
        }

        if ( commands.contains('p') ) {
          configurator->loadPlugins(commands['p']);
        }
        QList<QSqlField> fields;

        SafetVariable *myvar = configurator->getWorkflows().at(0)->searchVariable((commands['v']));

        if ( myvar != NULL ) {
            if ( !myvar->description().isEmpty()) {
                _listprincipaltitle = myvar->description();
            }
            else {
                _listprincipaltitle = QString("%1").arg(commands['v']);
            }
        }
        else {
            _listprincipaltitle = QString("%1").arg(commands['v']);

        }


         QString documents = configurator->getWorkflows().at(0)->getDocuments(commands['v'],
        fields,_listprincipalcount,
        SafetWorkflow::JSON, "");


         _listprincipalvariable = commands['v'];
         _listprincipalkey = "";

         //qDebug("........documents: %s", qPrintable(documents));
        centralWidget->setTabText(3,tr("Reportes*"));
        generateJScriptreports(documents,fields);

}



void MainWindow::iterateMap(const QMap<QString,QVariant>& map) {

     QList<QVariant> mylist = map.values("record");
     for ( int i = 0 ; i < mylist.count(); i++) {
          qDebug("\n..%d. Registro:", i+1);
          qDebug("id: %s", qPrintable(mylist.at(i).toMap()[ "id" ].toString()));
          qDebug("description: %s", qPrintable(mylist.at(i).toMap()[ "description" ].toString()));
          qDebug("summary: %s", qPrintable(mylist.at(i).toMap()[ "summary" ].toString()));
     }

}







void MainWindow::successVerification(QStringList list, const QString& msg) {
/*  // funcion comentada para eliminar dependencia de digidocpp
    qDebug("...........**......MainWindow::successVerification(QStringList list, const QString& msg) {");
    Q_CHECK_PTR(dockShowResult);
    QString message;
    if ( msg.isEmpty()) {
        message = QString("<table><tr><td><font color=green>%1</font></td></tr></table>").arg(tr("Verificaci�n exitosa....<b>ok!</b>"));
    }
    else {
        message = QString("<table><tr><td><font color=green>%1</font></td></tr></table>").arg(msg);
    }

    dockShowResult->addHeadHtml(message);
    qDebug("....successVerification........message: %s",qPrintable(message));
    dockShowResult->addOptionsDigidoc(digiDocument,list);
    showShowResultWidget(DockSbMenu::Show);
*/
}

void MainWindow::successVerification(QStringList list, const QString& msg, SafetDocument doc){
    qDebug("...........**......MainWindow::successVerification(QStringList list, const QString& msg, SafetDocument doc)");

    Q_CHECK_PTR(dockShowResult);
    QString message;
    if ( msg.isEmpty()) {
        message = QString("<table><tr><td><font color=green>%1</font></td></tr></table>").arg(tr("Verificaci�n exitosa....<b>ok!</b>"));
    }
    else {
        message = QString("<table><tr><td><font color=green>%1</font></td></tr></table>").arg(msg);
    }

    dockShowResult->addHeadHtml(message);
    qDebug("....successVerification........message: %s",qPrintable(message));
    //dockShowResult->addOptionsDigidoc(digiDocument,list);
    dockShowResult->addOptionsDigidoc(doc, list);
    showShowResultWidget(DockSbMenu::Show);

}

QString MainWindow::renderMessageStack(QStack<QPair<QDateTime,QString> >& stack, SafetLog::Level l){
    QString result;
    qDebug("...Probando el manejo de errores y advertencias: elementos: -->|%d|", stack.count());
    if ( stack.isEmpty()) return result;
    QString newtable = QString("<table>%1</table><br/>");
    QString newrow = QString("<tr>%1</tr>");
    QString newcol = QString("<td>%2%1</td>");
    QString rows, cols;
    if( l == SafetLog::Error) {
        rows += QString("<tr><td colspan=2><font color=red>%1</font></td></tr>").arg(tr("Los siguientes errores ocurrieron en la operaci&oacute;n:"));
    }
    else {
        rows += QString("<tr><td colspan=2><font color=black>%1</font></td></tr>").arg(tr("Mensajes:"));
    }
    while ( !stack.isEmpty() ) {
        QPair<QDateTime,QString> c = stack.pop();
        if( l == SafetLog::Error) {
            cols = newcol.arg(c.first.toString(SafetYAWL::streamlog.dateFormat())).arg("<font color=red><b>&rarr;</b></font>");
        } else {
            cols = newcol.arg(c.first.toString(SafetYAWL::streamlog.dateFormat())).arg("<font color=black><b>&rarr;</b></font>");
        }
        cols += newcol.arg(c.second).arg("");
        rows += newrow.arg(cols);
    }
    result = newtable.arg(rows);

    return result;
}

// ** Servicios de Escritorio (QDesktopServices)

void MainWindow::browse( const QUrl &url )
{
        QUrl u = url;
        u.setScheme( "file" );
        bool started = false;
#if defined(Q_OS_WIN32)
        started = QProcess::startDetached( "cmd", QStringList() << "/c" <<
                QString( "explorer /select,%1" ).arg( QDir::toNativeSeparators( u.toLocalFile() ) ) );
#elif defined(Q_OS_MAC)
        started = QProcess::startDetached("/usr/bin/osascript", QStringList() <<
                                                                          "-e" << "on run argv" <<
                                                                          "-e" << "set vfile to POSIX file (item 1 of argv)" <<
                                                                          "-e" << "tell application \"Finder\"" <<
                                                                          "-e" << "select vfile" <<
                                                                          "-e" << "activate" <<
                                                                          "-e" << "end tell" <<
                                                                          "-e" << "end run" <<
                                                                          // Commandline arguments
                                                                          u.toLocalFile());
#endif
        if( started )
                return;
        QDesktopServices::openUrl( QUrl::fromLocalFile( QFileInfo( u.toLocalFile() ).absolutePath() ) );
}

void MainWindow::mailTo( const QUrl &url )
{
#if defined(Q_OS_WIN32)
        QString file = url.queryItemValue( "attachment" );
        QByteArray filePath = QDir::toNativeSeparators( file ).toLatin1();
        QByteArray fileName = QFileInfo( file ).fileName().toLatin1();
        QByteArray subject = url.queryItemValue( "subject" ).toLatin1();

        MapiFileDesc doc[1];
        doc[0].ulReserved = 0;
        doc[0].flFlags = 0;
        doc[0].nPosition = -1;
        doc[0].lpszPathName = filePath.data();
        doc[0].lpszFileName = fileName.data();
        doc[0].lpFileType = NULL;

        // Create message
        MapiMessage message;
        message.ulReserved = 0;
        message.lpszSubject = subject.data();
        message.lpszNoteText = "";
        message.lpszMessageType = NULL;
        message.lpszDateReceived = NULL;
        message.lpszConversationID = NULL;
        message.flFlags = 0;
        message.lpOriginator = NULL;
        message.nRecipCount = 0;
        message.lpRecips = NULL;
        message.nFileCount = 1;
        message.lpFiles = (lpMapiFileDesc)&doc;

        QLibrary lib("mapi32");
        typedef ULONG (PASCAL *SendMail)(ULONG,ULONG,MapiMessage*,FLAGS,ULONG);
        SendMail mapi = (SendMail)lib.resolve("MAPISendMail");
        if( mapi )
        {
                mapi( NULL, 0, &message, MAPI_LOGON_UI|MAPI_DIALOG, 0 );
                return;
        }
#elif defined(Q_OS_MAC)
        CFURLRef emailUrl = CFURLCreateWithString(kCFAllocatorDefault, CFSTR("mailto:info@example.com"), NULL), appUrl = NULL;
        bool started = false;
        if(LSGetApplicationForURL(emailUrl, kLSRolesEditor, NULL, &appUrl) == noErr)
        {
                CFStringRef appPath = CFURLCopyFileSystemPath(appUrl, kCFURLPOSIXPathStyle);
                if(appPath != NULL)
                {
                        if(CFStringCompare(appPath, CFSTR("/Applications/Mail.app"), 0) == kCFCompareEqualTo)
                        {
                                started = QProcess::startDetached("/usr/bin/osascript", QStringList() <<
                                        "-e" << "on run argv" <<
                                        "-e" << "set vattachment to (item 1 of argv)" <<
                                        "-e" << "set vsubject to (item 2 of argv)" <<
                                        "-e" << "tell application \"Mail\"" <<
                                        "-e" << "set composeMessage to make new outgoing message at beginning with properties {visible:true}" <<
                                        "-e" << "tell composeMessage" <<
                                        "-e" << "set subject to vsubject" <<
                                        "-e" << "tell content" <<
                                        "-e" << "make new attachment with properties {file name: vattachment} at after the last paragraph" <<
                                        "-e" << "end tell" <<
                                        "-e" << "end tell" <<
                                        "-e" << "activate" <<
                                        "-e" << "end tell" <<
                                        "-e" << "end run" <<
                                        // Commandline arguments
                                        url.queryItemValue("attachment") <<
                                        url.queryItemValue("subject"));
                        }
                        else if(CFStringFind(appPath, CFSTR("Entourage"), 0).location != kCFNotFound)
                        {
                                started = QProcess::startDetached("/usr/bin/osascript", QStringList() <<
                                        "-e" << "on run argv" <<
                                        "-e" << "set vattachment to (item 1 of argv)" <<
                                        "-e" << "set vsubject to (item 2 of argv)" <<
                                        "-e" << "tell application \"Microsoft Entourage\"" <<
                                        "-e" << "set vmessage to make new outgoing message with properties" <<
                                        "-e" << "{subject:vsubject, attachments:vattachment}" <<
                                        "-e" << "open vmessage" <<
                                        "-e" << "activate" <<
                                        "-e" << "end tell" <<
                                        "-e" << "end run" <<
                                        // Commandline arguments
                                        url.queryItemValue("attachment") <<
                                        url.queryItemValue("subject"));
                        }
                        else if(CFStringCompare(appPath, CFSTR("/Applications/Thunderbird.app"), 0) == kCFCompareEqualTo)
                        {
                                // TODO: Handle Thunderbird here? Impossible?
                        }
                        CFRelease(appPath);
                }
                CFRelease(appUrl);
        }
        CFRelease(emailUrl);
        if( started )
                return;
#elif defined(Q_OS_LINUX)
        QByteArray thunderbird;
        QProcess p;
        QStringList env = QProcess::systemEnvironment();
        if( env.indexOf( QRegExp("KDE_FULL_SESSION.*") ) != -1 )
        {
                p.start( "kreadconfig", QStringList()
                        << "--file" << "emaildefaults"
                        << "--group" << "PROFILE_Default"
                        << "--key" << "EmailClient" );
                p.waitForFinished();
                QByteArray data = p.readAllStandardOutput().trimmed();
                if( data.contains("thunderbird") )
                        thunderbird = data;
        }
        else if( env.indexOf( QRegExp("GNOME_DESKTOP_SESSION_ID.*") ) != -1 )
        {
                p.start( "gconftool-2", QStringList()
                        << "--get" << "/desktop/gnome/url-handlers/mailto/command" );
                p.waitForFinished();
                QByteArray data = p.readAllStandardOutput();
                if( data.contains("thunderbird") )
                        thunderbird = data.split(' ').value(0);
        }
        if( !thunderbird.isEmpty() )
        {
                bool started = p.startDetached( thunderbird, QStringList() << "-compose"
                        << QString( "subject='%1',attachment='%2,'" )
                                .arg( url.queryItemValue( "subject" ) )
                                .arg( QUrl::fromLocalFile( url.queryItemValue( "attachment" ) ).toString() ) );
                if( started )
                        return;
        }
#endif
        QDesktopServices::openUrl( url );
}

void MainWindow::manageDataSources(){

    DatabaseConfigDialog * dbConfig = new DatabaseConfigDialog(this);
    //QString driver = "database.driver.";
    QString hostName = "database.host.";
    QString dataBaseName = "database.db.";
    QString userName = "database.user.";
    QString portName = "database.port.";

    int i = 0;
//    QString driverConf = SafetYAWL::getAuthConf()
//                         .getValue("Database", driver+QString::number(i+1)).toString();
    QString hostNameConf = SafetYAWL::getAuthConf()
                           .getValue("Database", hostName+QString::number(i+1)).toString();
    //qDebug("hostname: %s", qPrintable(hostNameConf));

    QString dataBaseNameConf = SafetYAWL::getAuthConf()
                       .getValue("Database", dataBaseName+QString::number(i+1)).toString();
    //qDebug("database: %s", qPrintable(dataBaseNameConf));
    QString userNameConf = SafetYAWL::getAuthConf()
                           .getValue("Database", userName+QString::number(i+1)).toString();
    //qDebug("user: %s", qPrintable(userNameConf));
    QString portConf = SafetYAWL::getAuthConf()
                       .getValue("Database", portName+QString::number(i+1)).toString();
    //qDebug("port: %s", qPrintable(portConf ));

    dbConfig->setConfigValues(hostNameConf,dataBaseNameConf,
                              portConf,userNameConf);
    int result = dbConfig->exec();
    /*if ( result != QDialog::Accepted ){
            return;
    }*/
    if (result == QDialog::Accepted){
        dbConfig->setCancelFlag(false);
    }
    else{
        dbConfig->setCancelFlag(true);
    }
    return;
}

void MainWindow::manageDataSources(DatabaseConfigDialog * dbConfig){

    //DatabaseConfigDialog * dbConfig = new DatabaseConfigDialog(this);
    //QString driver = "database.driver.";
    QString hostName = "database.host.";
    QString dataBaseName = "database.db.";
    QString userName = "database.user.";
    QString portName = "database.port.";

    int i = 0;
//    QString driverConf = SafetYAWL::getAuthConf()
//                         .getValue("Database", driver+QString::number(i+1)).toString();
    QString hostNameConf = SafetYAWL::getAuthConf()
                           .getValue("Database", hostName+QString::number(i+1)).toString();
    //qDebug("hostname: %s", qPrintable(hostNameConf));

    QString dataBaseNameConf = SafetYAWL::getAuthConf()
                       .getValue("Database", dataBaseName+QString::number(i+1)).toString();
    //qDebug("database: %s", qPrintable(dataBaseNameConf));
    QString userNameConf = SafetYAWL::getAuthConf()
                           .getValue("Database", userName+QString::number(i+1)).toString();
    //qDebug("user: %s", qPrintable(userNameConf));
    QString portConf = SafetYAWL::getAuthConf()
                       .getValue("Database", portName+QString::number(i+1)).toString();
    //qDebug("port: %s", qPrintable(portConf ));

    dbConfig->setConfigValues(hostNameConf,dataBaseNameConf,
                              portConf,userNameConf);
    int result = dbConfig->exec();
    /*if ( result != QDialog::Accepted ){
            return;
    }*/
    if (result == QDialog::Accepted){
        dbConfig->setCancelFlag(false);
        //setEnableCompletingButtons(true);
    }
    else{
        dbConfig->setCancelFlag(true);
    }
    return;
}

void MainWindow::setEnableCompletingButtons(bool b){

    qDebug("MainWindow::setEnableCompletingButtons(bool b)");
    // habilitar el boton de enviar
    completingButtonForm->setEnabled(b);
    completingButtonCons->setEnabled(b);
    //completingButtonSign->setEnabled(false);
    completingButtonConf->setEnabled(b);
    completingButtonUsers->setEnabled(b);
}


void MainWindow::doLoadConfiguration() {
    qDebug("...doLoadConfiguration...");
    QString types = tr("Archivos Comprimidos TAR (*.tar);;Todos (*)");
    QString fileName = QFileDialog::getOpenFileName(this,
                                tr("Seleccione un archivo de configuraci�n"),
                                QDir::homePath(),
                                types);

    if (fileName.isEmpty()) {
        return;
    }
    if (!QFile::exists(fileName)) {
        return;
    }
    QStringList mylist = GetSignDocumentDIalog::uncompresstar(fileName);
    qDebug("...(*)...mylist: %d", mylist.count());
    QStringList newlist;

    int maxlenstr = 0;
    QString principaldir;
    foreach(QString s, mylist) {
        if (maxlenstr == 0 ) {
            newlist.push_front(s);
            maxlenstr = s.length();
        }
        else if (s.length() < maxlenstr)  {
            maxlenstr = s.length();
            newlist.push_front(s);
        }
        else{
            newlist.push_back(s);
        }


    }
    Q_ASSERT(newlist.count() > 0 );

    int i = 0;
    principaldir = newlist.at(0);
    foreach(QString s, newlist) {
        qDebug("file: %s", qPrintable(s));
        if ( !processDirTar(s, i == 0) ) {
            return;
        }

        i++;
   }
            qDebug("...myfilelist.count() == 0**");
//            int answer = QMessageBox::question(this,
//                             "SAFET - Inflow",
//                             tr("Se ha cargado <b>correctamente</b> la configuraci�n.<br/>"
//                                "Es necesario <b>Reiniciar</b> para que tenga efecto."
//                                "�Desea <b>Reiniciar</b> ahora?"),
//                             QMessageBox::Yes | QMessageBox::No);


            MainWindow::_isrestarting = true;
            qApp->quit();





}

bool MainWindow::processDirTar(const QString& f, bool isprincipal) {
    if( !f.endsWith(".tar")) {
        SafetYAWL::streamlog
                << SafetLog::Error
               << tr("El archivo de configuracion no tiene el formato "
                     "correcto (%1)")
               .arg(f);
               return false;
    }
    QString newdir = f;
    newdir = f.section("/",-1);
    QString subdir = "."+ newdir.left(newdir.length()-3).replace(".","/");
    newdir = QDir::homePath()+"/"+ subdir;

    bool r;
    QDir mydir(QDir::homePath());
    if (isprincipal && QFile::exists(newdir)) {
        int answer = QMessageBox::question(this,
                              "SAFET - Inflow",
                              tr("Existe un directorio de configuraci�n (%1)"
                                 "�Desea eliminar <b>TODOS</b> los archivos de este directorio"
                                 " para colocar "
                                 "la nueva configuraci�n?")
                              .arg(newdir),
                              QMessageBox::Yes | QMessageBox::No);
        if ( answer == QMessageBox::No ) {
            return false;
        }

        //QString backupconf = SafetYAWL::getTempNameFiles(1).at(0);
        //qDebug("...backupconf...:%s",qPrintable(backupconf));
        //QString deletedir = backupconf;
        //r = mydir.rename(subdir, deletedir);

        qDebug("(1)processDirTar...borrando...:|%s|",qPrintable(subdir));
        doDelAllFilesOnDirectory(QDir::homePath()+"/"+subdir);
        qDebug("(2)processDirTar...borrando...:|%s|",qPrintable(subdir));

        if ( !QFile::exists(subdir+"/xmlrepository")) {
            r = mydir.mkdir(subdir+"/xmlrepository");
            r = mydir.mkdir(subdir+"/xmlrepository/container1");
        }

        if ( !QFile::exists(subdir+"/log")) {
            r = mydir.mkdir(subdir+"/log");
        }



    }
    else {
        if (!QFile::exists(newdir)) {
            r = mydir.mkdir(subdir);
        }
        else {
            qDebug("(3)processDirTar...borrando...:|%s|",qPrintable(QDir::homePath()+"/"+subdir));
            if (!subdir.endsWith("log")) {
                doDelAllFilesOnDirectory(QDir::homePath()+"/"+subdir);
            }
        }

    }


    QStringList mylist = GetSignDocumentDIalog::uncompresstar(f);
    QString searchhome = "/home/[a-zA-Z0-9\\.]+";

    QString replacehome = QDir::homePath();

    foreach(QString source, mylist) {

        QString target = newdir + source.section("/",-1);


        if (source.endsWith(".xml") || source.endsWith(".conf") ) {
//            qDebug("file source: %s", qPrintable(source));
//            qDebug("file target: %s", qPrintable(target));
               SafetYAWL::copyWithRe(source,target,searchhome,
                                     replacehome);
        }
        else if(source.endsWith(".gph") ) {

            target = newdir + QDir::homePath().section("/",-1)+".gph";
            r = QFile::copy(source,target);

        }
        else {
             r = QFile::copy(source,target);
        }

    }

    return true;

}


void MainWindow::doDelAllFilesOnDirectory(const QString &d) {
    QDir dir(d);
    if ( !dir.exists() ) {
        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("No se puede eliminar archivos de un directorio vac�o: \"%1\"")
                .arg(d);
        return;
    }

    dir.setFilter( QDir::Files );
    dir.setSorting(QDir::Size | QDir::Reversed);
    QStringList filters;
    filters << "*";
    dir.setNameFilters( filters );
    QStringList filelist = dir.entryList(QDir::Files);
    SafetYAWL::streamlog
            << SafetLog::Debug
            << tr("(Carga de configuraci�n) "
                  ". N�mero de archivos a eliminar: \"%1\"").arg(filelist.count());
    foreach(QString f, filelist) {
        bool isdeleting = dir.remove(f);
        if ( !isdeleting) {
            SafetYAWL::streamlog
                    << SafetLog::Warning
                    <<
            tr("(Carga de la configuraci�n) No se borr� el archivo: \"%1\"")
                   .arg(f);
        }

    }


}

QString MainWindow::generateListForTar(const QString& folder, QStringList& myfiles,
                                       const QStringList& exts) {


    QString result;
    QString newdir = Safet::datadir;
    if (newdir.startsWith(".")) {
        newdir = newdir.mid(1);
    }
    if (!folder.isEmpty()) {
        result =  QDir::tempPath()+"/"+newdir +"."+folder+".tar";
    }
    else {
        result =  QDir::tempPath()+"/" + newdir +".tar";
    }
    QString confpath = QDir::homePath() + "/" + Safet::datadir + "/" + folder;

    QDir dirconf(confpath);
    dirconf.setFilter(QDir::Files | QDir::NoSymLinks);
    if (!dirconf.exists()) {
        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("Error al intentar abrir el directorio para empaquetado: \"%1\"")
                .arg(confpath);
        return QString("");

    }


    QFileInfoList mylist = dirconf.entryInfoList();

    for (int i=0; i<mylist.size(); i++)
    {
        QFileInfo fileInfo = mylist.at(i);
        foreach(QString e, exts) {
            if (fileInfo.filePath().endsWith("."+e)) {
               myfiles.append(fileInfo.filePath());
               //qDebug("...fileInfo.filePath(): %s", qPrintable(fileInfo.filePath()));
               break;
            }
        }
    }
    SendSignDocumentDialog::compresstar(myfiles,result);
    qDebug("...result: %s",qPrintable(result));
    return result;

}

void MainWindow::doLoadFTP() {

    qDebug("...MainWindow::doLoadFTP....(1)...");

    QString fileName;
    FtpWindow myftpwindow(this,FtpWindow::Downloader);


    myftpwindow.setLocalFileName(fileName);
    myftpwindow.setRemoteFileName(fileName);
    myftpwindow.setWindowTitle(tr("Descargar configuraci�n desde Servidor FTP"));
    int result = myftpwindow.exec();

    qDebug("...MainWindow::doLoadFTP....(1)...result...:|%1|",result);
    if ( result == QDialog::Rejected ) {
        qDebug("...MainWindow::doLoadFTP....(1)...Rejected..");
        return;
    }

    fileName = myftpwindow.localFileName();
    qDebug("...MainWindow::doLoadFTP....(1)...localFileName:|%s|",qPrintable(fileName));

    if (fileName.isEmpty()) {
            qDebug("...MainWindow::doLoadFTP....(2)...");
        return;
    }
    if (!QFile::exists(fileName)) {
            qDebug("...MainWindow::doLoadFTP....(3)...");
        SYE << tr("Al bajar el archivo \"%1\" desde el servidor FTP, no se guard� correctamente")
               .arg(fileName);
        return;
    }
    QStringList mylist = GetSignDocumentDIalog::uncompresstar(fileName);
    qDebug("...(*)...mylist: %d", mylist.count());
    QStringList newlist;

    int maxlenstr = 0;
    QString principaldir;
    foreach(QString s, mylist) {
        if (maxlenstr == 0 ) {
            newlist.push_front(s);
            maxlenstr = s.length();
        }
        else if (s.length() < maxlenstr)  {
            maxlenstr = s.length();
            newlist.push_front(s);
        }
        else{
            newlist.push_back(s);
        }


    }
    Q_ASSERT(newlist.count() > 0 );

    int i = 0;
    principaldir = newlist.at(0);
    foreach(QString s, newlist) {
        qDebug("file: %s", qPrintable(s));
        if ( !processDirTar(s, i == 0) ) {
            return;
        }

        i++;
   }

            MainWindow::_isrestarting = true;
            qApp->quit();

}

void MainWindow::doSaveFTP() {

    bool ok;
    qDebug("...MainWindow::doSaveFtpConfiguration()....(1)...");
    QString fileName = QInputDialog::getText(this, tr("Guardar configuraci�n en servidor FTP"),
                                            tr("Nombre:"), QLineEdit::Normal,
                                             tr("safetconfiguracion.tar"), &ok);
    if(fileName.isEmpty() || !ok) {
        return;
    }

    fileName = QDir::tempPath() + "/" + fileName;

    qDebug("...MainWindow::doSaveFtpConfiguration()....(1)..fileName:|%s|",
           qPrintable(fileName));

    QStringList myfiles;
    QStringList exts;

    exts.append("xml");
    QString nametar1 = generateListForTar("flowfiles",myfiles,exts);

    qDebug();
    myfiles.clear();
    QString nametar2 = generateListForTar("input",myfiles,exts);

    qDebug();
    myfiles.clear();
    exts.clear();
    exts.append("dtd");
    QString nametar3 = generateListForTar("dtd",myfiles,exts);

    myfiles.clear();
    exts.clear();
    exts.append("gph");
    QString nametar4 = generateListForTar("graphs",myfiles,exts);



    myfiles.clear();
    exts.clear();
    exts.append("html");
    exts.append("png");
    exts.append("gif");
    exts.append("js");
    exts.append("css");
    QString nametar5 = generateListForTar("reports",myfiles,exts);

    myfiles.clear();
    exts.clear();
    exts.append("png");
    QString nametar6 = generateListForTar("images",myfiles,exts);


    myfiles.clear();
    exts.clear();
    exts.append("pem");
    exts.append("crl");
    QString nametar7 = generateListForTar("certs",myfiles,exts);



    myfiles.clear();
    exts.clear();
    exts.append("conf");
    exts.append("bin");
    exts.append("db");
    QString nametar8 = generateListForTar("",myfiles,exts);


    QStringList tarfiles;
    tarfiles.append(nametar1);
    tarfiles.append(nametar2);
    tarfiles.append(nametar3);
    tarfiles.append(nametar4);
    tarfiles.append(nametar5);
    tarfiles.append(nametar6);
    tarfiles.append(nametar7);
    tarfiles.append(nametar8);



    SendSignDocumentDialog::compresstar(tarfiles,fileName);

    FtpWindow myftpwindow;



    myftpwindow.setLocalFileName(fileName);
    QString remoteName = fileName.section("/",-1);
    qDebug("...MainWindow::doSaveFtpConfiguration...setLocalFileName:|%s|",qPrintable(fileName));
    myftpwindow.setRemoteFileName(remoteName);
    myftpwindow.setWindowTitle(tr("Subir configuraci�n a servidor FTP:\"%1\"").arg(remoteName));
        qDebug("...MainWindow::doSaveFtpConfiguration...remoteName:|%s|",qPrintable(remoteName));
    int result = myftpwindow.exec();



}

void MainWindow::doSaveConfiguration() {
    qDebug("...doSaveConfiguration...");

    QString types = tr("Archivos Comprimidos TAR (*.tar);;Todos (*)");
    QString fileName = QFileDialog::getSaveFileName(this,
                                            tr("Seleccione un directorio y un nombre de archivo"),
                                            QDir::homePath()+"/safetconfiguracion.tar",
                                            types);

    if(fileName.isEmpty()) {
        return;
    }
    QStringList myfiles;
    QStringList exts;

    exts.append("xml");
    QString nametar1 = generateListForTar("flowfiles",myfiles,exts);

    qDebug();
    myfiles.clear();
    QString nametar2 = generateListForTar("input",myfiles,exts);

    qDebug();
    myfiles.clear();
    exts.clear();
    exts.append("dtd");
    QString nametar3 = generateListForTar("dtd",myfiles,exts);

    myfiles.clear();
    exts.clear();
    exts.append("gph");
    QString nametar4 = generateListForTar("graphs",myfiles,exts);



    myfiles.clear();
    exts.clear();
    exts.append("html");
    exts.append("png");
    exts.append("gif");
    exts.append("js");
    exts.append("css");
    QString nametar5 = generateListForTar("reports",myfiles,exts);

    myfiles.clear();
    exts.clear();
    exts.append("png");
    QString nametar6 = generateListForTar("images",myfiles,exts);


    myfiles.clear();
    exts.clear();
    exts.append("pem");
    exts.append("crl");
    QString nametar7 = generateListForTar("certs",myfiles,exts);



    myfiles.clear();
    exts.clear();
    exts.append("conf");
    exts.append("bin");
    exts.append("db");
    QString nametar8 = generateListForTar("",myfiles,exts);


    QStringList tarfiles;
    tarfiles.append(nametar1);
    tarfiles.append(nametar2);
    tarfiles.append(nametar3);
    tarfiles.append(nametar4);
    tarfiles.append(nametar5);
    tarfiles.append(nametar6);
    tarfiles.append(nametar7);
    tarfiles.append(nametar8);



    SendSignDocumentDialog::compresstar(tarfiles,fileName);


    QMessageBox::information(this,
                             "Safet-Inflow",
                             tr("Guardada <b>correctamente</b> la configuraci�n"
                                " actual en: \"%1\"")
                             .arg(fileName),
                             QMessageBox::Ok);

}


void MainWindow::ftpCommandFinished(int number, bool error) {

    qDebug("....MainWindow::ftpCommandFinished....(1)...");

    if (error) {
         if (ftp->currentCommand() == QFtp::ConnectToHost) {
                qDebug("...ConnectToHost..MainWindow::ftpCommandFinished....number:|%d|",number);
         }
         else if (ftp->currentCommand() == QFtp::Login) {
             qDebug("...Login..MainWindow::ftpCommandFinished....number:|%d|",number);
         }
    }

}
