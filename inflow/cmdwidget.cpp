/*
* SAFET Sistema Automatizado para la Firma Electr�nica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigaci�n en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los t�rminos de la licencia de
* software GPL versi�n 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea �til, pero SI NINGUNA GARANT�A;
* tampoco las impl��citas garant��as de MERCANTILIDAD o ADECUACIÓN A UN PROP�SITO PARTICULAR.
* Consulte la licencia GPL para m�s detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5� Piso, Boston, MA 02110-1301, USA.
*
*/

#include "cmdwidget.h"
#include <QCryptographicHash>


CmdWidget::CmdWidget(const QString& t, QWidget *parent,bool istextparent)
 : QWidget(parent),
 _conf()
{
    Q_CHECK_PTR( parent );

    _istextparent = istextparent;
    if ( isTextParent()) {
        _texteditparent = qobject_cast<QTextEdit*>(parent);
        if ( _texteditparent == NULL ) {
             qDebug(".......ERROR...ERROR...CmdWidget::CmdWidget...._texteditparent = NULL");
        }
        texteditpos = _texteditparent->textCursor().position();
    }
    lineedit = NULL;
    lblvalidator = NULL;
    quitbutton = NULL;
    mainLayout = NULL;
    label      = NULL;
    checkbutton = NULL;
    okbutton = NULL;
    _caption = t;    
    principalWidget = NULL;
    _validator = NULL;
    _ispassword = false;
    setFocus(Qt::ActiveWindowFocusReason);
}

QRect CmdWidget::getGeoParams() const {
     QRect result;
     if ( lineedit == NULL) return QRect(0,0,280,36);

     result.setHeight( lineedit->height() );
     result.setWidth( lineedit->width()+lblvalidator->width()+quitbutton->width()+okbutton->width() );

     return result;
}


void CmdWidget::setText(const QString &newText)  {
    if (lineedit) {
        lineedit->setText(newText);
    }
}

void CmdWidget::buildWidget() {

     lineedit = new QLineEdit;
     lineedit->setGeometry(0,0,120,36);
     lblvalidator = new QLabel;
     lblvalidator->setGeometry(0,0,30,36);
     if ( isTextParent()) {
         okbutton = new QToolButton;
         okbutton->setGeometry(0,0,30,36);
         okbutton->setIcon(QIcon(":/yes.png"));
         quitbutton = new QToolButton;
         quitbutton->setGeometry(0,0,30,36);
         quitbutton->setText( "X");
     }
     mainLayout = new QHBoxLayout;
     label      = new QLabel;
     lblvalidator->setText( "<b><font color=\"Red\">-</font></b>" );


     QFont curFont;
     curFont.setFamily("Helvetica");
     curFont.setBold(true);
     label->setFont(curFont);
     label->setText( _caption +":" );
     mainLayout->addWidget(lineedit);
     QString mytip = tr("Campo de edici�n de texto\nEscriba Ctrl+L para finalizar");
     if ( conf().contains("validation")) {
         QStringList mylist = conf()["validation"].toString().split("::");
         if (mylist.count() > 1 ) {
             QString usertip = mylist.at(1);
             mytip = usertip;
         }
     }
     _usertooltip = mytip;
     lineedit->setToolTip(mytip);

     mainLayout->addWidget(lblvalidator);
     if (isTextParent()) {
         mainLayout->addWidget(quitbutton);
         mainLayout->addWidget(okbutton);
         mainLayout->setSpacing( 1 );

        connect(quitbutton, SIGNAL(clicked()), _texteditparent, SLOT(cancelAndClose()) );
         connect(okbutton, SIGNAL(clicked()), _texteditparent, SLOT(insertAndClose()) );
     }
     connect(lineedit, SIGNAL(textChanged(const QString&)), this, SLOT(changeLblValidator(const QString&)));
     setLayout(mainLayout);
     connect(lineedit, SIGNAL(returnPressed()), this, SLOT(doReturn()));
     lineedit->setFocus();
     qDebug("...options: |%s|",qPrintable(conf()["options"].toString()));
     if ( !conf()["options"].toString().isEmpty()) {
         principalWidget = lineedit;
        setOptionsProperties(conf()["options"].toString().split(",")); // Colocar las propiedades
    }
}


void CmdWidget::doReturn() {

    if (!isTextParent()) {
        return;
    }
    int ch = _texteditparent->toPlainText().mid(_texteditparent->textCursor().position(),1).at(0)
             .toAscii();
    if ( ch == 0 || ch == 10) {

        _texteditparent->moveCursor(QTextCursor::Left, QTextCursor::KeepAnchor);
        ch = _texteditparent->toPlainText().mid(_texteditparent->textCursor().position(),1).at(0).toAscii();
        _texteditparent->textCursor().removeSelectedText();        

    }
}

CmdWidget::~CmdWidget()
{
    if ( lineedit != NULL ) {
        delete lineedit;
    }
}

bool CmdWidget::isValid(QString& value) {
    if (value.isEmpty()) {
        return false;
    }
    else {
        if ( validator() == NULL  ) {
            return true;
        }
        else {

            int pos = 0;
            if (validator()->validate(value, pos ) == QValidator::Acceptable ) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    return true;
}


void CmdWidget::setOptionsProperties(const QStringList ps) {
    if (principalWidget == NULL ) {
        qDebug("...principalWidget == NULL...");
        return;
    }
    foreach(QString p, ps){
        QStringList  fieldproperty = p.split("::");
        if ( fieldproperty.count() > 1 ) {
            QString localproperty = fieldproperty.at(0);
            QVariant value = fieldproperty.at(1);
            bool result = principalWidget->setProperty(qPrintable(localproperty),value);
            qDebug("      CmdWidget::setOptionsProperties...localproperty: %s", qPrintable(localproperty));
            qDebug("      CmdWidget::setOptionsProperties...value: %s", qPrintable(value.toString()));
            if (!result) {                
                qDebug("La propiedad de nombre \"%s\" no existe",qPrintable(localproperty));
            }
            else {
                if (QString("echoMode") == localproperty &&
                    QString("Password") == value.toString() ) {
                    qDebug("isPassword");
                    _ispassword = true;
                }
            }

        }
    }

}

void CmdWidget::cancelAndClose() {
//    qDebug("...***cancelAndClose...CmdWidget...");
    if (!isTextParent()) {
        return;
    }
    if ( _texteditparent ) {
        if (!prevValue().isEmpty()) {
            _texteditparent->insertPlainText(prevValue());
            //_texteditparent->insertPlainText("\n");
        }
        setPrevValue("");

    }
    close();
}

void CmdWidget::insertAndClose() {
    qDebug("...insertAndClose...CmdWidget...");
    if (!isTextParent()) {
        return;
    }
    if ( _texteditparent ) {
        if ( lineedit) {            
                _texteditparent->insertPlainText(lineedit->text());                        
            _texteditparent->insertPlainText("\n");

        }
    }

    close();
}

QString CmdWidget::text() const {
    if (lineedit != NULL ) {
        return lineedit->text();
    }
    return QString("");
}
void CmdWidget::setFocus ( Qt::FocusReason reason ) {


     QWidget::setFocus ( reason );
     if ( lineedit != NULL ) {
          lineedit->setFocus( reason);
     }
     
}



void CmdWidget::changeLblValidator(const QString& text) {

    QString mytext = text;
    if ( validator() == NULL ) {
        if ( !text.isEmpty()) {
            lblvalidator->setText( "<b><font color=\"Green\">+</font></b>" );
        }
        else {
            lblvalidator->setText( "<b><font color=\"Red\">-</font></b>" );
        }
    }
    else  {
     int pos = 0;
     if ( validator()->validate(mytext, pos ) == QValidator::Acceptable ) {
	lblvalidator->setText( "<b><font color=\"Green\">+</font></b>" );
     }
     else {
	 lblvalidator->setText( "<b><font color=\"Red\">-</font></b>" );
       }      
    }

	
}



