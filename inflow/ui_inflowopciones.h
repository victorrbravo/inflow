/********************************************************************************
** Form generated from reading UI file 'inflowopciones.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INFLOWOPCIONES_H
#define UI_INFLOWOPCIONES_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QPushButton>
#include <QtGui/QTabWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OptionDialog
{
public:
    QGridLayout *gridLayout;
    QDialogButtonBox *buttonBox;
    QTabWidget *tabOptions;
    QWidget *tab1;
    QGridLayout *gridLayout_2;
    QHBoxLayout *ly1;
    QLabel *label;
    QLineEdit *filenameLine;
    QPushButton *loadButton;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_3;
    QPushButton *addButton;
    QPushButton *checkButton;
    QPushButton *delButton;
    QListWidget *listWidget;

    void setupUi(QDialog *OptionDialog)
    {
        if (OptionDialog->objectName().isEmpty())
            OptionDialog->setObjectName(QString::fromUtf8("OptionDialog"));
        OptionDialog->resize(475, 314);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(OptionDialog->sizePolicy().hasHeightForWidth());
        OptionDialog->setSizePolicy(sizePolicy);
        gridLayout = new QGridLayout(OptionDialog);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        buttonBox = new QDialogButtonBox(OptionDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Apply|QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 1, 0, 1, 1);

        tabOptions = new QTabWidget(OptionDialog);
        tabOptions->setObjectName(QString::fromUtf8("tabOptions"));
        tab1 = new QWidget();
        tab1->setObjectName(QString::fromUtf8("tab1"));
        gridLayout_2 = new QGridLayout(tab1);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        ly1 = new QHBoxLayout();
        ly1->setObjectName(QString::fromUtf8("ly1"));
        label = new QLabel(tab1);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(111, 0));

        ly1->addWidget(label);

        filenameLine = new QLineEdit(tab1);
        filenameLine->setObjectName(QString::fromUtf8("filenameLine"));
        filenameLine->setMinimumSize(QSize(255, 0));

        ly1->addWidget(filenameLine);

        loadButton = new QPushButton(tab1);
        loadButton->setObjectName(QString::fromUtf8("loadButton"));
        loadButton->setMinimumSize(QSize(27, 0));
        loadButton->setMaximumSize(QSize(27, 27));

        ly1->addWidget(loadButton);


        gridLayout_2->addLayout(ly1, 0, 0, 1, 2);

        groupBox = new QGroupBox(tab1);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout_3 = new QGridLayout(groupBox);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        addButton = new QPushButton(groupBox);
        addButton->setObjectName(QString::fromUtf8("addButton"));

        gridLayout_3->addWidget(addButton, 0, 0, 1, 1);

        checkButton = new QPushButton(groupBox);
        checkButton->setObjectName(QString::fromUtf8("checkButton"));

        gridLayout_3->addWidget(checkButton, 1, 0, 1, 1);

        delButton = new QPushButton(groupBox);
        delButton->setObjectName(QString::fromUtf8("delButton"));

        gridLayout_3->addWidget(delButton, 2, 0, 1, 1);


        gridLayout_2->addWidget(groupBox, 1, 0, 1, 1);

        listWidget = new QListWidget(tab1);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setMinimumSize(QSize(286, 0));

        gridLayout_2->addWidget(listWidget, 1, 1, 1, 1);

        tabOptions->addTab(tab1, QString());

        gridLayout->addWidget(tabOptions, 0, 0, 1, 1);


        retranslateUi(OptionDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), OptionDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), OptionDialog, SLOT(reject()));

        tabOptions->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(OptionDialog);
    } // setupUi

    void retranslateUi(QDialog *OptionDialog)
    {
        OptionDialog->setWindowTitle(QApplication::translate("OptionDialog", "Opciones", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("OptionDialog", "Agregar Widget:", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        loadButton->setToolTip(QApplication::translate("OptionDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Escoger archivo utilizando el cuadro de di\303\241logo de <span style=\" font-weight:600; color:#00007f;\">\"Abrir Archivo\"</span></p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        loadButton->setText(QApplication::translate("OptionDialog", "...", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("OptionDialog", "Operaciones", 0, QApplication::UnicodeUTF8));
        addButton->setText(QApplication::translate("OptionDialog", "Agregar", 0, QApplication::UnicodeUTF8));
        checkButton->setText(QApplication::translate("OptionDialog", "Chequear", 0, QApplication::UnicodeUTF8));
        delButton->setText(QApplication::translate("OptionDialog", "Eliminar", 0, QApplication::UnicodeUTF8));
        tabOptions->setTabText(tabOptions->indexOf(tab1), QApplication::translate("OptionDialog", "Gesti\303\263n de Widgets", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class OptionDialog: public Ui_OptionDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INFLOWOPCIONES_H
