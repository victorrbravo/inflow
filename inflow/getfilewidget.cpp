/*
* SAFET Sistema Automatizado para la Firma Electr�nica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigaci�n en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los t�rminos de la licencia de
* software GPL versi�n 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea �til, pero SI NINGUNA GARANT�A;
* tampoco las impl��citas garant��as de MERCANTILIDAD o ADECUACIÓN A UN PROP�SITO PARTICULAR.
* Consulte la licencia GPL para m�s detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5� Piso, Boston, MA 02110-1301, USA.
*
*/

/****************************************************************************
**
** Copyright (C) 2004-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.trolltech.com/products/qt/opensource.html
**
** If you are unsure which license is appropriate for your use, please
** review the following information:
** http://www.trolltech.com/products/qt/licensing.html or contact the
** sales department at sales@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include <QtGui>
#include <QToolButton>
#include <QMessageBox>
#include "SafetYAWL.h"
#include "getfilewidget.h"

GetFileWidget::GetFileWidget(const QString& t, QWidget *parent, bool istextparent )
    : CmdWidget(t, parent,istextparent)
{
    filecombo = NULL;
}

GetFileWidget::~GetFileWidget() {
    if ( filecombo != NULL ) {
        delete filecombo;
    }

}


void GetFileWidget::setText(const QString &newText) {
    if (filecombo) {
        filecombo->setEditText(newText);
    }
 }


void GetFileWidget::showDialog() {
    QString fileName;

   QString types = tr("Todos (*);;Archivos de Texto (*.txt)");

   if ( conf().contains("filter") ) {
       types = conf()["filter"].toString();
   }

    QString curdir = QDir::homePath()+ "/" + Safet::datadir;
    QString dialogtype = conf()["options"].toString();
    if (dialogtype.startsWith("open") || dialogtype.isEmpty() ) {
        fileName = QFileDialog::getOpenFileName(this,
                                                tr("Seleccione un archivo"),
                                                curdir,
                                                types);

    }
    else if ( dialogtype.startsWith("save") ) {

             fileName = QFileDialog::getSaveFileName(this,
                                                     tr("Seleccione un directorio y un nombre de archivo"),
                                                     QDir::homePath(),
                                                     types);
    }
    else if (dialogtype.startsWith("dir") ) {
             fileName = QFileDialog::getSaveFileName(this,
                                                     tr("Seleccione un Directorio"),
                                                     QDir::homePath());
   }
    else {
        SafetYAWL::streamlog << SafetLog::Error << tr("No se defini� correctamente el atributo \"%1\" "
                                                      "de un campo \"filename\" "
                                                      "(campos posibles open,save,dir").arg(dialogtype);
    }

    if (!fileName.isEmpty()) {
        filecombo->setEditText(fileName);

         filecombo->setFocus( Qt::PopupFocusReason );
    }
}



bool GetFileWidget::isValid(QString& value) {
     QString optionsstr = conf()["options"].toString();
     if ( optionsstr.startsWith("save")) {
         return CmdWidget::isValid(value);
     }
     else {

        if ( !QFile::exists(value)) {
            return false;
        }
    }
    return true;
}

void GetFileWidget::buildWidget() {
    mainLayout = new QHBoxLayout;
    filecombo = new QComboBox;
    filenamebutton = new QToolButton;
    filenamebutton->setText( "...");
    if (isTextParent()) {
        okbutton = new QToolButton;
        okbutton->setGeometry(0,0,30,36);
        okbutton->setIcon(QIcon(":/yes.png"));
        quitbutton = new QToolButton;
        quitbutton->setText( "X");
    }

    filecombo->setEditable(true);
    mainLayout->addWidget(filecombo);
    QString mytip = tr("Campo de Selecci�n de Archivo\nEscriba Ctrl+L para finalizar");
    if ( conf().contains("validation")) {
        QStringList mylist = conf()["validation"].toString().split("::");
        if (mylist.count() > 1 ) {
            QString usertip = mylist.at(1);
            mytip = usertip;
        }
    }
    _usertooltip = mytip;
    filecombo->setToolTip(mytip);

    mainLayout->addWidget( filenamebutton);
    mainLayout->addWidget(quitbutton);
    mainLayout->addWidget(okbutton);

   setLayout(mainLayout);
   if (isTextParent()) {
       connect(quitbutton, SIGNAL(clicked()), _texteditparent, SLOT(cancelAndClose()) );
       connect(okbutton, SIGNAL(clicked()), _texteditparent, SLOT(insertAndClose()) );
   }

    connect(filenamebutton, SIGNAL(clicked()), this, SLOT(showDialog()) );
    updateCombo();
    filecombo->setEditText("");


}

void GetFileWidget::setFocus ( Qt::FocusReason reason ) {
     QWidget::setFocus ( reason );
     filecombo->setFocus( reason);
     
     
}
void GetFileWidget::insertAndClose() {    
    if ( _texteditparent ) {
        if ( filecombo) {
            QString value = filecombo->currentText().simplified();
            _texteditparent->insertPlainText(value.toLatin1());
            _texteditparent->insertPlainText("\n");
        }
    }

    close();
}

void GetFileWidget::updateCombo() {
    if ( filecombo != NULL ) {
        QString mime = SafetYAWL::getConf()["Widgets/getfilewidget.*"];
        QStringList mimes  = mime.
                         split(SafetYAWL::LISTSEPARATORCHARACTER,QString::SkipEmptyParts);
        filecombo->clear();
        qDebug("...GetFileWidget::updateCombo()....mime:|%s|", qPrintable(mime));
        qDebug("...GetFileWidget::updateCombo()....mimes...count:%d", mimes.count());
        filecombo->addItems( mimes );
    }

}

QString GetFileWidget::text() const {
        QString result = "";
        if ( filecombo != NULL) {              
             result = filecombo->currentText();
         }
        return result;
}               
