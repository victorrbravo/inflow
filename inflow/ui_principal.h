/********************************************************************************
** Form generated from reading UI file 'principal.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PRINCIPAL_H
#define UI_PRINCIPAL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QScrollArea>
#include <QtGui/QSpacerItem>
#include <QtGui/QStackedWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Principal
{
public:
    QVBoxLayout *MainLayout;
    QFrame *background;
    QVBoxLayout *backgroundLayout;
    QHBoxLayout *headerLayout;
    QSpacerItem *headerSpacer;
    QPushButton *toSettingsButton;
    QFrame *headerLine;
    QPushButton *toHelpButton;
    QFrame *headerLine2;
    QHBoxLayout *infoLayout;
    QSpacerItem *infoSpacer;
    QFrame *infoFrame;
    QGridLayout *infoFrameLayout;
    QLabel *infoLogo;
    QStackedWidget *infoStack;
    QLabel *infoCard;
    QWidget *infoMobile;
    QGridLayout *infoMobileLayout;
    QSpacerItem *infoMobileSpacer;
    QStackedWidget *stack;
    QWidget *home;
    QVBoxLayout *homeLayout;
    QSpacerItem *homeTopSpacer;
    QGridLayout *homeButtonLayout;
    QLabel *homeButtonLabel;
    QSpacerItem *homeButtonSpacer;
    QPushButton *toInputButton;
    QPushButton *toConsoleButton;
    QPushButton *toSignButton;
    QPushButton *toExitButton;
    QSpacerItem *homePadding;
    QSpacerItem *homeBottomSpacer;
    QWidget *intro;
    QVBoxLayout *introLayout;
    QLabel *introContent;
    QSpacerItem *introSpacer;
    QCheckBox *introCheck;
    QDialogButtonBox *introButtons;
    QWidget *sign;
    QGridLayout *signLayout;
    QLabel *signContentLabel;
    QLabel *signSignerLabel;
    QDialogButtonBox *signButtons;
    QFrame *signSignerRole;
    QGridLayout *signSignerRoleLayout;
    QLabel *signRoleLabel;
    QLineEdit *signRoleInput;
    QLabel *signResolutionLabel;
    QLineEdit *signResolutionInput;
    QLabel *signCityLabel;
    QLineEdit *signCityInput;
    QLabel *signStateLabel;
    QLineEdit *signStateInput;
    QLabel *signCountryLabel;
    QLineEdit *signCountryInput;
    QLabel *signZipLabel;
    QLineEdit *signZipInput;
    QSpacerItem *signSignerSpacer;
    QLabel *signSigner;
    QSpacerItem *locationSpacer;
    QFrame *signContentFrame;
    QVBoxLayout *verticalLayout_2;
    QLabel *signAddFile;
    QWidget *view;
    QGridLayout *viewLayout;
    QLabel *viewFileName;
    QLabel *viewFileStatus;
    QLabel *viewContentLabel;
    QScrollArea *viewSignaturesScroll;
    QWidget *viewSignatures;
    QVBoxLayout *viewSignaturesLayout;
    QSpacerItem *viewSignaturesSpacer;
    QLabel *viewEmail;
    QLabel *viewPrint;
    QLabel *viewBrowse;
    QDialogButtonBox *viewButtons;
    QHBoxLayout *viewSignaturesLabelLayout;
    QLabel *viewSignaturesLabel;
    QLabel *viewSignaturesError;
    QFrame *viewContentFrame;
    QVBoxLayout *verticalLayout;
    QLabel *viewSaveAs;

    void setupUi(QWidget *Principal)
    {
        if (Principal->objectName().isEmpty())
            Principal->setObjectName(QString::fromUtf8("Principal"));
        Principal->resize(1027, 767);
        Principal->setAcceptDrops(true);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/digidoc_icon_48x48.png"), QSize(), QIcon::Normal, QIcon::Off);
        Principal->setWindowIcon(icon);
        Principal->setStyleSheet(QString::fromUtf8("* {\n"
"color: #355670;\n"
"font: 12px \"Arial, Liberation Sans\";\n"
"}\n"
"\n"
"#background\n"
"{ background-image: url(\":/backgroundsafet.png\"); \n"
"background-color: rgb(125, 125, 125);\n"
"background-repeat:no-repeat;\n"
"background-attachment:scroll;\n"
"background-position:center; \n"
"}\n"
"\n"
"/* widgets */\n"
"QPushButton\n"
"{\n"
"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #406A9E, stop: 0.75 #00355F);\n"
"border: 0px;\n"
"border-radius: 2px;\n"
"color: white;\n"
"margin-left: 1px;\n"
"margin-right: 1px;\n"
"min-width: 75px;\n"
"padding: 1px 7px;\n"
"}\n"
"\n"
"QDialogButtonBox QPushButton, #home QPushButton\n"
"{ padding: 4px 7px; }\n"
"\n"
"QPushButton:hover, QPushButton:focus\n"
"{\n"
"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F1C157, stop:0.28 #FADF91, stop:1 #EBA927);\n"
"color: #00355F;\n"
"}\n"
"\n"
"QPushButton:disabled\n"
"{ background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #A3AFC0, stop: 0.75 #80909D); }\n"
"\n"
""
                        "/* header */\n"
"#headerLine, #headerLine2\n"
"{ background-color: #00355F; }\n"
"\n"
"#cards, #languages\n"
"{\n"
"background-color: #00355F;\n"
"border: 0px;\n"
"margin-right: 0px;\n"
"color: white;\n"
"}\n"
"\n"
"#cards QListView, #languages QListView\n"
"{\n"
"background-color: white;\n"
"border: 0px;\n"
"color: #355670;\n"
"}\n"
"\n"
"#cards::down-arrow, #languages::down-arrow\n"
"{ image: url(\"/languages_button.png\"); }\n"
"\n"
"#cards::drop-down, #languages::drop-down\n"
"{ border: 0px; }\n"
"\n"
"#settings, #help\n"
"{\n"
"background-color: #00355F;\n"
"color: #afc6d1;\n"
"font: bold;\n"
"min-width: 0px;\n"
"}\n"
"\n"
"#infoFrame\n"
"{\n"
"background-color: rgba( 255, 255, 255, 153 );\n"
"border-radius: 3px;\n"
"color: #668696;\n"
"padding-left: 10px;\n"
"padding-right: 10px;\n"
"}\n"
"\n"
"#infoFrame QLabel\n"
"{ font: bold; }\n"
"\n"
"/* content */\n"
"#homeButtonLabel,\n"
"#viewContentLabel, #viewSignaturesLabel,\n"
"#signContentLabel, #signSigner, #signSignerLabel\n"
"{\n"
"color: #00355F;\n"
"fo"
                        "nt: bold;\n"
"}\n"
"\n"
"#home, #intro, #sign, #view,\n"
"#viewSignatures\n"
"{ background-color: transparent; }\n"
"\n"
"#signContentFrame, #viewContentFrame\n"
"{\n"
"background-color: rgba( 255, 255, 255, 200 );\n"
"border: 1px solid gray;\n"
"color: #355670;\n"
"}\n"
"\n"
"#signContentView, #viewContentView\n"
"{\n"
"background-color: transparent;\n"
"padding: 5px;\n"
"}\n"
"\n"
"#viewSignatures\n"
"{ color: #71889A; }\n"
"\n"
"#signSignerRole, #viewSignaturesScroll\n"
"{\n"
"background-color: rgba( 255, 255, 255, 200 );\n"
"border: 1px solid gray;\n"
"padding: 5px;\n"
"}"));
        MainLayout = new QVBoxLayout(Principal);
        MainLayout->setSpacing(0);
        MainLayout->setContentsMargins(0, 0, 0, 0);
        MainLayout->setObjectName(QString::fromUtf8("MainLayout"));
        background = new QFrame(Principal);
        background->setObjectName(QString::fromUtf8("background"));
        backgroundLayout = new QVBoxLayout(background);
        backgroundLayout->setSpacing(0);
        backgroundLayout->setObjectName(QString::fromUtf8("backgroundLayout"));
        backgroundLayout->setContentsMargins(20, 0, 20, 20);
        headerLayout = new QHBoxLayout();
        headerLayout->setSpacing(10);
        headerLayout->setObjectName(QString::fromUtf8("headerLayout"));
        headerSpacer = new QSpacerItem(0, 45, QSizePolicy::Expanding, QSizePolicy::Minimum);

        headerLayout->addItem(headerSpacer);

        toSettingsButton = new QPushButton(background);
        toSettingsButton->setObjectName(QString::fromUtf8("toSettingsButton"));
        QFont font;
        font.setFamily(QString::fromUtf8("Arial, Liberation Sans"));
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        toSettingsButton->setFont(font);

        headerLayout->addWidget(toSettingsButton);

        headerLine = new QFrame(background);
        headerLine->setObjectName(QString::fromUtf8("headerLine"));
        headerLine->setMaximumSize(QSize(2, 20));
        headerLine->setFrameShadow(QFrame::Plain);
        headerLine->setFrameShape(QFrame::VLine);

        headerLayout->addWidget(headerLine);

        toHelpButton = new QPushButton(background);
        toHelpButton->setObjectName(QString::fromUtf8("toHelpButton"));
        toHelpButton->setFont(font);

        headerLayout->addWidget(toHelpButton);

        headerLine2 = new QFrame(background);
        headerLine2->setObjectName(QString::fromUtf8("headerLine2"));
        headerLine2->setMaximumSize(QSize(2, 20));
        headerLine2->setFrameShadow(QFrame::Plain);
        headerLine2->setFrameShape(QFrame::VLine);

        headerLayout->addWidget(headerLine2);


        backgroundLayout->addLayout(headerLayout);

        infoLayout = new QHBoxLayout();
        infoLayout->setObjectName(QString::fromUtf8("infoLayout"));
        infoLayout->setContentsMargins(-1, 10, -1, 10);
        infoSpacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        infoLayout->addItem(infoSpacer);

        infoFrame = new QFrame(background);
        infoFrame->setObjectName(QString::fromUtf8("infoFrame"));
        infoFrame->setMinimumSize(QSize(387, 115));
        infoFrameLayout = new QGridLayout(infoFrame);
        infoFrameLayout->setContentsMargins(0, 0, 0, 0);
        infoFrameLayout->setObjectName(QString::fromUtf8("infoFrameLayout"));
        infoLogo = new QLabel(infoFrame);
        infoLogo->setObjectName(QString::fromUtf8("infoLogo"));
        infoLogo->setCursor(QCursor(Qt::PointingHandCursor));
        infoLogo->setAlignment(Qt::AlignCenter);

        infoFrameLayout->addWidget(infoLogo, 0, 1, 1, 1);

        infoStack = new QStackedWidget(infoFrame);
        infoStack->setObjectName(QString::fromUtf8("infoStack"));
        infoCard = new QLabel();
        infoCard->setObjectName(QString::fromUtf8("infoCard"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Arial, Liberation Sans"));
        font1.setBold(true);
        font1.setItalic(false);
        font1.setWeight(75);
        infoCard->setFont(font1);
        infoStack->addWidget(infoCard);
        infoMobile = new QWidget();
        infoMobile->setObjectName(QString::fromUtf8("infoMobile"));
        infoMobileLayout = new QGridLayout(infoMobile);
        infoMobileLayout->setContentsMargins(0, 0, 0, 0);
        infoMobileLayout->setObjectName(QString::fromUtf8("infoMobileLayout"));
        infoMobileSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Preferred);

        infoMobileLayout->addItem(infoMobileSpacer, 0, 0, 1, 1);

        infoStack->addWidget(infoMobile);

        infoFrameLayout->addWidget(infoStack, 1, 1, 1, 1);


        infoLayout->addWidget(infoFrame);


        backgroundLayout->addLayout(infoLayout);

        stack = new QStackedWidget(background);
        stack->setObjectName(QString::fromUtf8("stack"));
        home = new QWidget();
        home->setObjectName(QString::fromUtf8("home"));
        homeLayout = new QVBoxLayout(home);
        homeLayout->setObjectName(QString::fromUtf8("homeLayout"));
        homeLayout->setContentsMargins(75, 0, 75, 0);
        homeTopSpacer = new QSpacerItem(0, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        homeLayout->addItem(homeTopSpacer);

        homeButtonLayout = new QGridLayout();
        homeButtonLayout->setObjectName(QString::fromUtf8("homeButtonLayout"));
        homeButtonLabel = new QLabel(home);
        homeButtonLabel->setObjectName(QString::fromUtf8("homeButtonLabel"));
        homeButtonLabel->setFont(font1);

        homeButtonLayout->addWidget(homeButtonLabel, 0, 0, 1, 2);

        homeButtonSpacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        homeButtonLayout->addItem(homeButtonSpacer, 0, 2, 1, 1);

        toInputButton = new QPushButton(home);
        toInputButton->setObjectName(QString::fromUtf8("toInputButton"));
        toInputButton->setFont(font);

        homeButtonLayout->addWidget(toInputButton, 1, 1, 1, 1);

        toConsoleButton = new QPushButton(home);
        toConsoleButton->setObjectName(QString::fromUtf8("toConsoleButton"));
        toConsoleButton->setFont(font);

        homeButtonLayout->addWidget(toConsoleButton, 2, 1, 1, 1);

        toSignButton = new QPushButton(home);
        toSignButton->setObjectName(QString::fromUtf8("toSignButton"));
        toSignButton->setFont(font);

        homeButtonLayout->addWidget(toSignButton, 3, 1, 1, 1);

        toExitButton = new QPushButton(home);
        toExitButton->setObjectName(QString::fromUtf8("toExitButton"));
        toExitButton->setFont(font);

        homeButtonLayout->addWidget(toExitButton, 4, 1, 1, 1);

        homePadding = new QSpacerItem(10, 0, QSizePolicy::Minimum, QSizePolicy::Minimum);

        homeButtonLayout->addItem(homePadding, 1, 0, 1, 1);


        homeLayout->addLayout(homeButtonLayout);

        homeBottomSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        homeLayout->addItem(homeBottomSpacer);

        stack->addWidget(home);
        intro = new QWidget();
        intro->setObjectName(QString::fromUtf8("intro"));
        introLayout = new QVBoxLayout(intro);
        introLayout->setContentsMargins(0, 0, 0, 0);
        introLayout->setObjectName(QString::fromUtf8("introLayout"));
        introContent = new QLabel(intro);
        introContent->setObjectName(QString::fromUtf8("introContent"));
        introContent->setWordWrap(true);

        introLayout->addWidget(introContent);

        introSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        introLayout->addItem(introSpacer);

        introCheck = new QCheckBox(intro);
        introCheck->setObjectName(QString::fromUtf8("introCheck"));

        introLayout->addWidget(introCheck);

        introButtons = new QDialogButtonBox(intro);
        introButtons->setObjectName(QString::fromUtf8("introButtons"));
        introButtons->setStandardButtons(QDialogButtonBox::Cancel);

        introLayout->addWidget(introButtons);

        stack->addWidget(intro);
        sign = new QWidget();
        sign->setObjectName(QString::fromUtf8("sign"));
        signLayout = new QGridLayout(sign);
        signLayout->setContentsMargins(0, 0, 0, 0);
        signLayout->setObjectName(QString::fromUtf8("signLayout"));
        signLayout->setHorizontalSpacing(10);
        signLayout->setVerticalSpacing(4);
        signContentLabel = new QLabel(sign);
        signContentLabel->setObjectName(QString::fromUtf8("signContentLabel"));
        signContentLabel->setFont(font1);

        signLayout->addWidget(signContentLabel, 0, 0, 1, 1);

        signSignerLabel = new QLabel(sign);
        signSignerLabel->setObjectName(QString::fromUtf8("signSignerLabel"));
        signSignerLabel->setFont(font1);

        signLayout->addWidget(signSignerLabel, 0, 1, 1, 1);

        signButtons = new QDialogButtonBox(sign);
        signButtons->setObjectName(QString::fromUtf8("signButtons"));
        signButtons->setStandardButtons(QDialogButtonBox::Cancel);

        signLayout->addWidget(signButtons, 2, 0, 1, 2);

        signSignerRole = new QFrame(sign);
        signSignerRole->setObjectName(QString::fromUtf8("signSignerRole"));
        signSignerRoleLayout = new QGridLayout(signSignerRole);
        signSignerRoleLayout->setObjectName(QString::fromUtf8("signSignerRoleLayout"));
        signRoleLabel = new QLabel(signSignerRole);
        signRoleLabel->setObjectName(QString::fromUtf8("signRoleLabel"));
        signRoleLabel->setFont(font);

        signSignerRoleLayout->addWidget(signRoleLabel, 1, 0, 1, 1);

        signRoleInput = new QLineEdit(signSignerRole);
        signRoleInput->setObjectName(QString::fromUtf8("signRoleInput"));

        signSignerRoleLayout->addWidget(signRoleInput, 1, 1, 1, 1);

        signResolutionLabel = new QLabel(signSignerRole);
        signResolutionLabel->setObjectName(QString::fromUtf8("signResolutionLabel"));
        signResolutionLabel->setFont(font);

        signSignerRoleLayout->addWidget(signResolutionLabel, 2, 0, 1, 1);

        signResolutionInput = new QLineEdit(signSignerRole);
        signResolutionInput->setObjectName(QString::fromUtf8("signResolutionInput"));

        signSignerRoleLayout->addWidget(signResolutionInput, 2, 1, 1, 1);

        signCityLabel = new QLabel(signSignerRole);
        signCityLabel->setObjectName(QString::fromUtf8("signCityLabel"));
        signCityLabel->setFont(font);

        signSignerRoleLayout->addWidget(signCityLabel, 4, 0, 1, 1);

        signCityInput = new QLineEdit(signSignerRole);
        signCityInput->setObjectName(QString::fromUtf8("signCityInput"));

        signSignerRoleLayout->addWidget(signCityInput, 4, 1, 1, 1);

        signStateLabel = new QLabel(signSignerRole);
        signStateLabel->setObjectName(QString::fromUtf8("signStateLabel"));
        signStateLabel->setFont(font);

        signSignerRoleLayout->addWidget(signStateLabel, 5, 0, 1, 1);

        signStateInput = new QLineEdit(signSignerRole);
        signStateInput->setObjectName(QString::fromUtf8("signStateInput"));

        signSignerRoleLayout->addWidget(signStateInput, 5, 1, 1, 1);

        signCountryLabel = new QLabel(signSignerRole);
        signCountryLabel->setObjectName(QString::fromUtf8("signCountryLabel"));
        signCountryLabel->setFont(font);

        signSignerRoleLayout->addWidget(signCountryLabel, 6, 0, 1, 1);

        signCountryInput = new QLineEdit(signSignerRole);
        signCountryInput->setObjectName(QString::fromUtf8("signCountryInput"));

        signSignerRoleLayout->addWidget(signCountryInput, 6, 1, 1, 1);

        signZipLabel = new QLabel(signSignerRole);
        signZipLabel->setObjectName(QString::fromUtf8("signZipLabel"));
        signZipLabel->setFont(font);

        signSignerRoleLayout->addWidget(signZipLabel, 7, 0, 1, 1);

        signZipInput = new QLineEdit(signSignerRole);
        signZipInput->setObjectName(QString::fromUtf8("signZipInput"));

        signSignerRoleLayout->addWidget(signZipInput, 7, 1, 1, 1);

        signSignerSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        signSignerRoleLayout->addItem(signSignerSpacer, 8, 1, 1, 1);

        signSigner = new QLabel(signSignerRole);
        signSigner->setObjectName(QString::fromUtf8("signSigner"));
        signSigner->setStyleSheet(QString::fromUtf8("font-weight: bold;"));

        signSignerRoleLayout->addWidget(signSigner, 0, 0, 1, 2);

        locationSpacer = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        signSignerRoleLayout->addItem(locationSpacer, 3, 1, 1, 1);


        signLayout->addWidget(signSignerRole, 1, 1, 1, 1);

        signContentFrame = new QFrame(sign);
        signContentFrame->setObjectName(QString::fromUtf8("signContentFrame"));
        signContentFrame->setFrameShape(QFrame::StyledPanel);
        verticalLayout_2 = new QVBoxLayout(signContentFrame);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        signAddFile = new QLabel(signContentFrame);
        signAddFile->setObjectName(QString::fromUtf8("signAddFile"));

        verticalLayout_2->addWidget(signAddFile);


        signLayout->addWidget(signContentFrame, 1, 0, 1, 1);

        stack->addWidget(sign);
        view = new QWidget();
        view->setObjectName(QString::fromUtf8("view"));
        viewLayout = new QGridLayout(view);
        viewLayout->setContentsMargins(0, 0, 0, 0);
        viewLayout->setObjectName(QString::fromUtf8("viewLayout"));
        viewLayout->setHorizontalSpacing(10);
        viewLayout->setVerticalSpacing(4);
        viewFileName = new QLabel(view);
        viewFileName->setObjectName(QString::fromUtf8("viewFileName"));

        viewLayout->addWidget(viewFileName, 0, 0, 1, 2);

        viewFileStatus = new QLabel(view);
        viewFileStatus->setObjectName(QString::fromUtf8("viewFileStatus"));

        viewLayout->addWidget(viewFileStatus, 1, 0, 1, 1);

        viewContentLabel = new QLabel(view);
        viewContentLabel->setObjectName(QString::fromUtf8("viewContentLabel"));
        viewContentLabel->setFont(font1);

        viewLayout->addWidget(viewContentLabel, 2, 0, 1, 1);

        viewSignaturesScroll = new QScrollArea(view);
        viewSignaturesScroll->setObjectName(QString::fromUtf8("viewSignaturesScroll"));
        viewSignaturesScroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        viewSignaturesScroll->setWidgetResizable(true);
        viewSignatures = new QWidget();
        viewSignatures->setObjectName(QString::fromUtf8("viewSignatures"));
        viewSignatures->setGeometry(QRect(0, 0, 88, 18));
        viewSignaturesLayout = new QVBoxLayout(viewSignatures);
        viewSignaturesLayout->setObjectName(QString::fromUtf8("viewSignaturesLayout"));
        viewSignaturesSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        viewSignaturesLayout->addItem(viewSignaturesSpacer);

        viewSignaturesScroll->setWidget(viewSignatures);

        viewLayout->addWidget(viewSignaturesScroll, 3, 1, 1, 1);

        viewEmail = new QLabel(view);
        viewEmail->setObjectName(QString::fromUtf8("viewEmail"));

        viewLayout->addWidget(viewEmail, 4, 0, 1, 1);

        viewPrint = new QLabel(view);
        viewPrint->setObjectName(QString::fromUtf8("viewPrint"));

        viewLayout->addWidget(viewPrint, 4, 1, 1, 1);

        viewBrowse = new QLabel(view);
        viewBrowse->setObjectName(QString::fromUtf8("viewBrowse"));

        viewLayout->addWidget(viewBrowse, 5, 0, 1, 1);

        viewButtons = new QDialogButtonBox(view);
        viewButtons->setObjectName(QString::fromUtf8("viewButtons"));
        viewButtons->setStandardButtons(QDialogButtonBox::Close);

        viewLayout->addWidget(viewButtons, 6, 0, 1, 2);

        viewSignaturesLabelLayout = new QHBoxLayout();
        viewSignaturesLabelLayout->setObjectName(QString::fromUtf8("viewSignaturesLabelLayout"));
        viewSignaturesLabel = new QLabel(view);
        viewSignaturesLabel->setObjectName(QString::fromUtf8("viewSignaturesLabel"));
        viewSignaturesLabel->setFont(font1);

        viewSignaturesLabelLayout->addWidget(viewSignaturesLabel);

        viewSignaturesError = new QLabel(view);
        viewSignaturesError->setObjectName(QString::fromUtf8("viewSignaturesError"));
        viewSignaturesError->setStyleSheet(QString::fromUtf8("font: bold;\n"
"color: red;"));

        viewSignaturesLabelLayout->addWidget(viewSignaturesError);


        viewLayout->addLayout(viewSignaturesLabelLayout, 2, 1, 1, 1);

        viewContentFrame = new QFrame(view);
        viewContentFrame->setObjectName(QString::fromUtf8("viewContentFrame"));
        viewContentFrame->setFrameShape(QFrame::StyledPanel);
        verticalLayout = new QVBoxLayout(viewContentFrame);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        viewSaveAs = new QLabel(viewContentFrame);
        viewSaveAs->setObjectName(QString::fromUtf8("viewSaveAs"));
        viewSaveAs->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        verticalLayout->addWidget(viewSaveAs);


        viewLayout->addWidget(viewContentFrame, 3, 0, 1, 1);

        stack->addWidget(view);

        backgroundLayout->addWidget(stack);


        MainLayout->addWidget(background);


        retranslateUi(Principal);

        infoStack->setCurrentIndex(0);
        stack->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Principal);
    } // setupUi

    void retranslateUi(QWidget *Principal)
    {
        Principal->setWindowTitle(QApplication::translate("Principal", "DigiDoc3 client", 0, QApplication::UnicodeUTF8));
        toSettingsButton->setText(QApplication::translate("Principal", "Preferencias", 0, QApplication::UnicodeUTF8));
        toHelpButton->setText(QApplication::translate("Principal", "Ayuda", 0, QApplication::UnicodeUTF8));
        infoLogo->setText(QApplication::translate("Principal", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'DejaVu Sans'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<table border=\"0\" style=\"-qt-table-type: root; margin-top:4px; margin-bottom:4px; margin-left:4px; margin-right:4px;\">\n"
"<tr>\n"
"<td style=\"border: none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Arial, Liberation Sans'; font-size:12pt; font-weight:600; color:#00007f;\">SAFET</span><span style=\" font-family:'Arial, Liberation Sans';\"> Bajo Licencia </span><a href=\"http://www.gnu.org/licenses/gpl-2.0.html\"><span style=\" font-family:'Arial, Liberation Sans'; text-decoration: underline; color:#0000ff;\">GPLv2</span></a><span style=\" font-family:'Arial, Lib"
                        "eration Sans'; font-weight:600;\">    </span><span style=\" font-family:'Arial, Liberation Sans'; font-weight:600;\">             </span><a href=\"http://www.cenditel.gob.ve\"><span style=\" font-family:'Arial, Liberation Sans'; font-weight:600; text-decoration: underline; color:#ffffff;\">Fundaci\303\263n CENDITEL</span></a><span style=\" font-family:'Arial, Liberation Sans'; font-weight:600;\"> </span><span style=\" font-family:'Arial, Liberation Sans'; font-weight:600;\">2008-2010</span></p></td></tr></table></body></html>", 0, QApplication::UnicodeUTF8));
        homeButtonLabel->setText(QApplication::translate("Principal", "Deseo realizar lo siguiente:", 0, QApplication::UnicodeUTF8));
        toInputButton->setText(QApplication::translate("Principal", "Agregar/modificar informaci\303\263n", 0, QApplication::UnicodeUTF8));
        toConsoleButton->setText(QApplication::translate("Principal", "Realizar Consultas", 0, QApplication::UnicodeUTF8));
        toSignButton->setText(QApplication::translate("Principal", "Firmar/Verificar Documentos", 0, QApplication::UnicodeUTF8));
        toExitButton->setText(QApplication::translate("Principal", "Salir", 0, QApplication::UnicodeUTF8));
        introContent->setText(QApplication::translate("Principal", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" />\n"
"</head><body>\n"
"<p style=\" font: 12px 'Arial,Liberation Sans';\">\n"
"Digital signing.<br /><br />\n"
"Digital signing is equal to physical signing. to sing a document you need an ID-card with a valid and usable signing certificate.<br /><br />\n"
"Signing is done via PIN2 code<br /><br />\n"
"Also and active internet connection is needed.</p>\n"
"</body></html>", 0, QApplication::UnicodeUTF8));
        introCheck->setText(QApplication::translate("Principal", "Skip this intro", 0, QApplication::UnicodeUTF8));
        signContentLabel->setText(QApplication::translate("Principal", "Container content:", 0, QApplication::UnicodeUTF8));
        signSignerLabel->setText(QApplication::translate("Principal", "Signature", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        signRoleLabel->setToolTip(QApplication::translate("Principal", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Arial, Liberation Sans'; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Roll ...</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        signRoleLabel->setText(QApplication::translate("Principal", "Role", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_TOOLTIP
        signResolutionLabel->setToolTip(QApplication::translate("Principal", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Arial, Liberation Sans'; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Resolution ...</p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_TOOLTIP
        signResolutionLabel->setText(QApplication::translate("Principal", "Resolution", 0, QApplication::UnicodeUTF8));
        signCityLabel->setText(QApplication::translate("Principal", "City", 0, QApplication::UnicodeUTF8));
        signStateLabel->setText(QApplication::translate("Principal", "State", 0, QApplication::UnicodeUTF8));
        signCountryLabel->setText(QApplication::translate("Principal", "Country", 0, QApplication::UnicodeUTF8));
        signZipLabel->setText(QApplication::translate("Principal", "Zip", 0, QApplication::UnicodeUTF8));
        signAddFile->setText(QApplication::translate("Principal", "<a href=\"addFile\">Add file</a>", 0, QApplication::UnicodeUTF8));
        viewFileName->setText(QApplication::translate("Principal", "Container:", 0, QApplication::UnicodeUTF8));
        viewFileStatus->setText(QApplication::translate("Principal", "This container is ", 0, QApplication::UnicodeUTF8));
        viewContentLabel->setText(QApplication::translate("Principal", "Container content:", 0, QApplication::UnicodeUTF8));
        viewEmail->setText(QApplication::translate("Principal", "<a href=\"email\">Send container to email</a>", 0, QApplication::UnicodeUTF8));
        viewPrint->setText(QApplication::translate("Principal", "<a href=\"print\">Print summary</a>", 0, QApplication::UnicodeUTF8));
        viewBrowse->setText(QApplication::translate("Principal", "<a href=\"browse\">Browse container location</a>", 0, QApplication::UnicodeUTF8));
        viewSignaturesLabel->setText(QApplication::translate("Principal", "Signature", 0, QApplication::UnicodeUTF8));
        viewSaveAs->setText(QApplication::translate("Principal", "<a href=\"saveAs\">Save files to disk</a>", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Principal: public Ui_Principal {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PRINCIPAL_H
