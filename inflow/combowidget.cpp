/*
* SAFET Sistema Automatizado para la Firma Electr�nica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigaci�n en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los t�rminos de la licencia de
* software GPL versi�n 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea �til, pero SI NINGUNA GARANT�A;
* tampoco las impl��citas garant��as de MERCANTILIDAD o ADECUACIÓN A UN PROP�SITO PARTICULAR.
* Consulte la licencia GPL para m�s detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5� Piso, Boston, MA 02110-1301, USA.
*
*/

#include <QtGui>
#include <QComboBox>
#include <QMessageBox>
#include <QColorDialog>
#include "combowidget.h"
#include "mainwindow.h"
#include "inflowfirmadoc.h"
#include "ui_inflowfirmadoc.h"
#include "textedit.h"

ComboWidget::ComboWidget(const QString& s, ComboWidget::Type t, QWidget *parent,
                         bool istextparent)
    : CmdWidget(s, parent, istextparent)
{
     varbox = NULL;
     signbutton = NULL;
     viewdocbutton = NULL;
     getcolorbutton= NULL;
     _type = t;
     //configurator = new SafetYAWL();
}

ComboWidget::~ComboWidget() {

     //if (configurator) delete configurator;
}

void ComboWidget::setText(const QString &newText) {
    qDebug("...ComboWidget::setText...newText:|%s|",qPrintable(newText));
    if (varbox ) {

        int newindex = varbox->findText(newText,Qt::MatchStartsWith);
        if (newindex == -1 ) {
             QString titlevalue = getTitleValue(newText);
             qDebug("...ComboWidget::setText...titlevalue:|%s|", qPrintable(titlevalue));
             newindex = varbox->findText(titlevalue,Qt::MatchStartsWith);
             if (newindex == -1 ){
                 return;
             }
        }
        varbox->setCurrentIndex(newindex );

    }

}

void ComboWidget::updateCombo() {

    if ( conf().contains("input") ) {
        QString myinput = conf()["input"].toString();
        if (myinput != "yes" && myinput != "no") {
            setInput(myinput);
        }
    }
     switch (type() ) {

          case VariableSafet:
               updateComboVariableSafet();
               break;
     case TaskSafet:
          updateComboTaskSafet();
          break;

         case AutofilterSafet:

              updateComboAutofilterSafet();
              break;
         case RecursivefilterSafet:
             updateComboRecursivefilterSafet();
           break;
          case FieldsSafet:
           updateComboFields();
           break;

         case ListTable:
               updateComboListTable();
               break;
          case ListLiteral:
               updateComboListLiteral();
               break;
          case Flow:
               updateComboFlow();
               break;
     case ConffileSafet:
               updateComboConffileSafet();
               break;

     case ColorSafet:
               updateComboColorSafet();
          default:;
     }

}




void ComboWidget::updateComboConffileSafet(bool inwidget) {
    QStringList options = conf()["options"].toString().split(":");
         if (options.count() < 1 ) {
        SYW
                 << tr("IMPORTANTE, No se defini� correctamente el campo \"options\" --> \"%1\"").arg(conf()["options"].toString());
         return;
    }

    QString field = options.at(0);
    if (!field.endsWith(".*")) {
        SafetYAWL::streamlog
                << SafetLog::Warning
                << tr("IMPORTANTE, Debe finalizar en \".*\". No se defini� "
                      "correctamente el campo \"options\" --> \"%1\"").arg(conf()["options"].toString());

    }
    int count = 1;
    QString pattern = field.mid(0,field.length()-2)+QString(".%1");
    while (true) {
        QString newfield = pattern.arg(count);
        if ( !SafetYAWL::getAuthConf().contains(newfield)) {
            break;
        }
        _itemvaluelist.append(SafetYAWL::getAuthConf()[newfield]);

        count++;
    }
    if ( inwidget ) {
       varbox->addItems( _itemvaluelist );
   }

}

bool ComboWidget::isValid(QString& value) {

    qDebug("...ComboWidget::isValid.....(1)...");
    if ( !CmdWidget::isValid(value)) {

        return false;
    }

    qDebug("...ComboWidget::isValid.....(2)...");
    Q_CHECK_PTR( varbox );
    switch(type()) {
    case ListLiteral:
        if ( !conf().contains("options")) {
            SafetYAWL::streamlog
                    << SafetLog::Error
                    << tr("Falla la validacion del widget combo: \"%1\"")
                    .arg(caption());
            return false;
        }
        generateLiterals(conf()["options"].toString(),false);
                break;
     case FieldsSafet:
         updateComboFields(false);
     break;
    case ListTable:
        updateComboListTable(false);

        break;
    case TaskSafet:
         updateComboTaskSafet(false);
         break;
    case VariableSafet:
        updateComboVariableSafet(false);

        break;
    case AutofilterSafet:
        updateComboAutofilterSafet(false);
        break;
   case RecursivefilterSafet:
        updateComboRecursivefilterSafet(false);
        break;
        break;
   case Flow:
        updateComboFlow(false);
        break;
     case ConffileSafet:
        updateComboConffileSafet(false);
        break;
     case ColorSafet:
        updateComboColorSafet(false);


        break;

    default:;
    }

    if ( type() == ListLiteral) {

        qDebug("ComboWidget...isValid...value:|%s|", qPrintable(value));
        if ( !itemsRealValueList().contains(value)) {
            qDebug("isValid...return false");
            return false;
        }
    }
    else if ( type() == ColorSafet ) {
        if ( !itemsRealValueList().contains(value)) {
            QRegExp rx;
            rx.setPattern("\\#[0-9]{,6}");
            int pos = rx.indexIn(value);
            if (pos == -1 ) {
                return false;
            }

        }

    }
    else if(type() == ListTable) {

        updateVarGlobal(value);
        if ( !itemsRealValueList().contains(getRealValue(value))) {
            return false;
        }
        else {
            value = getRealValue(value);
        }

    }
    else {
        if ( !itemsValueList().contains(value)) {
            return false;
        }
    }
    return true;
}
void ComboWidget::updateVarGlobal(const QString& value) {
    if ( varvalues.keys().contains(value)) {
        qDebug("               *****(1)value: |%s|combovarglobal SafetYAWL::combovarglobal0: |%s|",
               qPrintable(value),qPrintable(SafetYAWL::combovarglobal0));
        SafetYAWL::combovarglobal0 = getVarValue(value);
        qDebug("               *****(2)...value: |%s| combovarglobal SafetYAWL::combovarglobal0: |%s|",
               qPrintable(value),qPrintable(SafetYAWL::combovarglobal0));
    }

}


void ComboWidget::updateComboFlow(bool inwidget) {
     infos.clear();

     if ( conf().contains("path") ) {
          if (MainWindow::configurator != NULL ) delete  MainWindow::configurator;
          MainWindow::configurator = new SafetYAWL();
          Q_CHECK_PTR( MainWindow::configurator );

          SafetYAWL::streamlog
                  << SafetLog::Debug
                  << tr("DOMMODEL ComboFlow: path: |%1|").arg(conf()["path"].toString());

          MainWindow::configurator->openXML(conf()["path"].toString());
          MainWindow::configurator->convertXMLtoObjects();
          MainWindow::configurator->openDataSources();

           SafetWorkflow::NextStates myns = SafetWorkflow::OnlyNext;
          if ( MainWindow::configurator->getWorkflows().count() > 0 ) {             

               infos = MainWindow::configurator->getWorkflows().at(0)
                       ->listNextStates(conf()["keyvalue"].toString(),myns,true);
               if ( infos.count() > 0 ) {
                    if ( infos.at( infos.count()-1) == "<SIGN>" ) {
                         buildButtonWidget(infos);
                         return;
                    }

               }
          }

          _itemvaluelist = infos;
          if ( inwidget ) {
            varbox->addItems( infos );
        }
     }
     foreach(QString s, _itemvaluelist) {
         SYD  << tr("DOMMODEL inwidget: %2 ComboFlow: _itemvaluelist: |%1|").arg(s).arg(inwidget);

     }



}
void ComboWidget::updateComboColorSafet(bool inwidget) {
    if ( !conf().contains("options")) {
          return;
      }
      generateLiterals(conf()["options"].toString());

}

void ComboWidget::selColor() {
    qDebug("...ComboWidget::selColor....");
    if (varbox == NULL ) {
        return;
    }

QColor mycolor;
#if (QT_VERSION > QT_VERSION_CHECK(4, 4, 3)) 
     QColorDialog mydialog;
    if ( mydialog.exec() == QDialog::Accepted ) {
        mycolor = mydialog.selectedColor();
	}
#else
        mycolor = QColorDialog::getColor();
#endif
        QString curcolor =  QString("#%1%2%3").arg(mycolor.red(),2,16)
                 .arg(mycolor.green(),2,16).arg(mycolor.blue(),2,16);
        curcolor.replace(" ","0");
        varbox->addItem(curcolor);
        varbox->setCurrentIndex(varbox->count()-1);
    

}

void ComboWidget::buildButtonWidget(const QStringList& l) {
     qDebug("...buildButtonWidget...");
     Q_ASSERT( l.count() > 0 );
     if ( quitbutton ) {
          close();
          disconnect(quitbutton, 0, 0, 0 );
          delete quitbutton;
          delete okbutton;
          delete varbox;
          delete mainLayout;
          if ( checkbutton ) delete checkbutton;
          if ( viewdocbutton ) delete viewdocbutton;
          setLayout(NULL);
          quitbutton = NULL;
          checkbutton = NULL;
          viewdocbutton = NULL;
          lineedit  = NULL;
          varbox = NULL;
          mainLayout = NULL;
          getcolorbutton = NULL;

          signbutton = new QPushButton;
          mainLayout = new QHBoxLayout;
  //        viewdocbutton  = new QToolButton;
          okbutton = new QToolButton;
          okbutton->setGeometry(0,0,30,36);
          okbutton->setIcon(QIcon(":/yes.png"));
          quitbutton = new QToolButton;

          quitbutton->setText( "X");
          signbutton->setIcon(QIcon(":/firmadoc.png"));
          _cursigners = l.at(0).simplified();
          if (_cursigners == "#ANYBODY#") {
              signbutton->setText(tr("Firmar %1").arg( MainWindow::currentrealname));
              _cursigners = MainWindow::currentrealname;
          }
          else {
            signbutton->setText(tr("Firmar %1").arg(_cursigners));
          }
          mainLayout->addWidget(signbutton);
          // mainLayout->addWidget(viewdocbutton);
          mainLayout->addWidget(quitbutton);
          mainLayout->addWidget(okbutton);
          setLayout(mainLayout);
          connect(quitbutton, SIGNAL(clicked()), _texteditparent, SLOT(cancelAndClose()) );
          connect(signbutton, SIGNAL(clicked()), this, SLOT(viewdoc()) );
          connect(okbutton, SIGNAL(clicked()), _texteditparent, SLOT(insertAndClose()) );


     }

}


void ComboWidget::insertAndClose() {

    if ( _texteditparent ) {
        if ( varbox ) {
            _texteditparent->insertPlainText(getRealValue(varbox->currentText().toLatin1()));
            _texteditparent->insertPlainText("\n");


        }
    }
    close();
}


void ComboWidget::viewdoc() {
     if ( infos.count() == 0 ) return;
     if ( MainWindow::configurator  ) {
     //     QMessageBox::information(this,tr("hola"),tr("viewdoc"));
          QString keyvalue = conf()["keyvalue"].toString();
          if ( infos.count() <= 2 ) {
              qDebug("...viewdoc...infos.count() <= 2");
              return;
          }
          QString namevar = infos.at(infos.count()-2);
          qDebug("..keyvalue..: |%s|", qPrintable(keyvalue));
          qDebug("..namevar..: |%s|", qPrintable(namevar));
          Q_CHECK_PTR( MainWindow::configurator->getWorkflows().at(0) );
          QList<QSqlField> fields;
          int c;
          QString documents = MainWindow::configurator->getWorkflows()
                              .at(0)->getDocuments(namevar,
                                                   fields,
                                                   c,
                                                   SafetWorkflow::JSON,
                                                   keyvalue);
          //qDebug("*docs: -->\n%s", qPrintable(documents));
          qDebug("*documents counts: -->\n%d", c);
          InflowFirmaDoc *dlg = new InflowFirmaDoc(this);
          if ( dlg ) {
              QString derror;
              qDebug(".......keyvalue:|%s|",qPrintable(keyvalue));
              SafetVariable *v = MainWindow::configurator->getWorkflows()
                                 .at(0)->searchVariable(namevar);
              QList<SafetWorkflow::InfoSigner> infosigners = MainWindow::configurator->getWorkflows()
                                                   .at(0)->getSignersDocument(v,keyvalue,derror);

               dlg->m_ui->webDocument->setHtml("");
               dlg->loadReportTemplate();
               dlg->generateJScriptreports(documents,fields,1,_cursigners, infosigners);
               dlg->loadSignersInfo(infosigners);
               int result = dlg->exec();
               if ( result == QDialog::Accepted ) {       
                    Q_CHECK_PTR( v );

                    QString result = MainWindow::configurator->getWorkflows()
                                     .at(0)->signDocument(v,keyvalue,derror);
                    if ( result.length() > 0 ) {
                         qDebug(" ...MainWindow::mymainwindow->toSend();...(1)");

                         qDebug(" ...MainWindow::mymainwindow->toSend();...(2)");
                         signbutton->setText(tr("Firmado:%1|%2").arg(result)
                                             .arg(_cursigners));
                         hashtext = result;
                         signbutton->setStyleSheet("font: 6pt \"Bitstream Vera Sans\";");
                         signbutton->setEnabled(false);

                         TextEdit* mytextedit = qobject_cast<TextEdit*>(_texteditparent);
                         if ( mytextedit) {
                            mytextedit->insertAndClose();
                        }
                         MainWindow::_currentCommonName = _cursigners;
                         MainWindow::mymainwindow->toSend(true);
                         MainWindow::_issigning = false;
                         MainWindow::_currentCommonName = "";
                    }
               }

               if (dlg != NULL ) {
                   delete dlg;
               }
          }
     }
}


void ComboWidget::buildWidget() {

    qDebug("...buildWidget....(1)...");
     mainLayout = new QHBoxLayout;
     varbox = new QComboBox;
       if (isTextParent()) {
        quitbutton = new QToolButton;
         quitbutton->setText( "X");
         okbutton = new QToolButton;
         okbutton->setGeometry(0,0,30,36);
         okbutton->setIcon(QIcon(":/yes.png"));
     }

     Q_CHECK_PTR( varbox );
     qDebug("...buildWidget....(2)...");
     varbox->setEditable(true);
     mainLayout->addWidget(varbox);
     QString mytip = tr("Campo de Selecci�n\nEscriba Ctrl+L para finalizar");
     if ( conf().contains("validation")) {
         QStringList mylist = conf()["validation"].toString().split("::");
         if (mylist.count() > 1 ) {
             QString usertip = mylist.at(1);
             mytip = usertip;
         }
     }
     _usertooltip = mytip;
     varbox->setToolTip(mytip);

     qDebug("...buildWidget....(3)...");
     if ( _type == ColorSafet ) {
         getcolorbutton = new QToolButton(this);
         getcolorbutton->setText(tr("Sel/Color"));
         getcolorbutton->setIcon(QIcon(":/getcolor.png"));
         mainLayout->addWidget(getcolorbutton);
         connect(getcolorbutton, SIGNAL(clicked()), this, SLOT(selColor()) );

     }
     if (isTextParent()) {
     mainLayout->addWidget(quitbutton);
     mainLayout->addWidget(okbutton);

        connect(quitbutton, SIGNAL(clicked()), _texteditparent, SLOT(cancelAndClose()) );
        connect(okbutton, SIGNAL(clicked()), _texteditparent, SLOT(insertAndClose()) );
    }
     qDebug("...buildWidget....(4)...");
     setLayout(mainLayout);
     updateCombo();
     qDebug("...buildWidget....(5)...");
}

void ComboWidget::updateComboListLiteral() {
    if ( !conf().contains("options")) {
          return;
      }
    generateLiterals(conf()["options"].toString());


}

void ComboWidget::generateLiterals(const QString& s, bool inwidget) {
    qDebug("...**ComboWidget...generateLiterals....:%s", qPrintable(s));    
    QStringList vars = s.split(",",QString::SkipEmptyParts);
    if ( inwidget && varbox == NULL ) {
        return;
    }
    int i = 0;
    _itemvaluelist.clear();
    _itemrealvaluelist.clear();
    foreach(QString r, vars) {
        QString value = r.split("::").at(0);
        QString realvalue;
        QVariant info;


        if ( r.split("::").count() > 1 ) {
            info = r.split("::").at(1);

        }
        if ( r.split("::").count() > 2 ) {
            realvalue = r.split("::").at(2);
        }
        qDebug("...value: |%s| ... realvalue.:|%s|..",qPrintable(value),
               qPrintable(realvalue));
        realvalues[value] = realvalue;
        if ( inwidget ) {
               varbox->addItem(value);
               varbox->setItemData(i, info, Qt::ToolTipRole);
        }
        _itemvaluelist.append(value);
        _itemrealvaluelist.append(realvalue);
        i++;
    }

}

QString ComboWidget::getRealValue(const QString& s) {

    if ( !realvalues.keys().contains(s)) {
        return s;
    }
    return realvalues[ s];
}

QString ComboWidget::getTitleValue(const QString& s) {

    QString result;
    foreach(QString k,  realvalues.keys()) {
        if ( realvalues[k] == s) {
            return k;
        }
    }
    return result;
}


QString ComboWidget::getVarValue(const QString& s) {

    if ( !varvalues.keys().contains(s)) {
        return s;
    }
    return varvalues[ s];
}

void ComboWidget::updateComboListTable(bool inwidget) {

    //varbox->setInsertPolicy(QComboBox::InsertAlphabetically);
     QSqlQuery query(SafetYAWL::currentDb ); // <-- puntero a db actual
     QString field, table, where, value;
     if ( !conf().contains("options")) {
         return;
     }


     QStringList options = conf()["options"].toString().split(":");
          if (options.count() < 2 ) {
          SafetYAWL::streamlog
                  << SafetLog::Warning
                  << tr("IMPORTANTE, No se definio correctamente el campo \"options\" --> \"%1\"").arg(conf()["options"].toString());
          return;
     }
     field = options.at(0);

     QString order =  " ORDER BY %1 ASC;";
     qDebug("***....conf().contains(\"order\"):|%s|",
            qPrintable(conf()["order"].toString()));
     if ( conf().contains("order")) {
         QString command = conf()["order"].toString();
         if ( command == "none" ) {
             order = ";";
         }
         else if (command == "desc") {
             order = " ORDER BY %1 DESC;";
         }
     }
     QString realvalue;
     _itemrealvaluelist.clear();
     _itemvaluelist.clear();
     table = options.at(1);
     QString command;
     if ( options.count() > 2 ) {
          where = options.at(2);
          where = proccessWhereOption(where);
          if ( options.count() > 3 ) {
              QString titleField = options.at(3);
              if ( options.count() >  4 ) {
                  QString varglobal = options.at(4);
                  if ( !titleField.isEmpty()) {
                command = QString("SELECT %1,%4,%5 from %2 %3 "+order)
                          .arg(field).arg(table)
                          .arg(where)
                          .arg(titleField)
                          .arg(varglobal);
                }
                  else {
                      command = QString("SELECT %1,'n/a' as safetnotapplicable,%4 "
                                        "from %2 %3 "+order)
                                .arg(field).arg(table)
                                .arg(where)
                                .arg(varglobal);
                  }

              }
              else {
                  if ( !titleField.isEmpty()) {
                command = QString("SELECT %1,%4 from %2 %3 "+order)
                          .arg(field).arg(table)
                          .arg(where)
                          .arg(titleField);
                }
                  else {
                      command = QString("SELECT %1 from %2 %3 "+order)
                                .arg(field).arg(table)
                                .arg(where);
                  }
            }

          }
          else {
            command = QString("SELECT %1 from %2 %3 "+order).arg(field).arg(table).arg(where);
          }
     }
     else {
          command = QString("SELECT %1 from %2 "+order).arg(field).arg(table);
     }

     SYD << tr("Actualizando lista del combo: \"%1\"").arg( command );
     query.prepare(  command );
     bool executed = query.exec();
     if (!executed ) {
          SYW << tr("IMPORTANTE, NO se ejecut� correctamente la consulta de opciones del combo: \"%1\"").arg(command);
          return;
     }
     bool istitlefield  = query.record().count() > 1 && query.record().fieldName(1) != "safetnotapplicable";

     bool isvarglobal = query.record().count() > 2;

     while( query.next() ) {
           realvalue = query.value(0).toString().trimmed();
          if (!_itemrealvaluelist.contains(realvalue) ) {
              _itemrealvaluelist.append(realvalue);
              if ( istitlefield ) {
                  value = query.value(1).toString().trimmed();
              }
              else {
                  value = realvalue;
              }
              _itemvaluelist.append(value);
              realvalues[ value ] = realvalue;
              if ( isvarglobal ) {
//                  qDebug("........isvarglobal:....:value [ %s ] --> %s",
//                         qPrintable(value), qPrintable(query.value(2).toString().trimmed()));
                  varvalues[ value ] = query.value(2).toString().trimmed();
              }
          }

     }
     if ( inwidget ) {
        varbox->addItems( _itemvaluelist );
    }
     if (!isTextParent()) {
         varbox->insertItem(-1,tr("<Cualquiera>"));
         varbox->setCurrentIndex(0);
     }
}



void ComboWidget::updateComboFields(bool inwidget) {

    //varbox->setInsertPolicy(QComboBox::InsertAlphabetically);
     QSqlQuery query(SafetYAWL::currentDb ); // <-- puntero a db actual
//     if ( !conf().contains("options")) {
//         return;
//     }
     QString options;
     QString nametable;

     if (conf().contains("options")) {
        options = conf()["options"].toString();
        nametable = options;
     }

     if (conf().contains("table")) {
        nametable = conf()["table"].toString();
     }

     SYD << tr("..........ComboWidget::updateComboFields....nametable:|%1|")
            .arg(nametable);


     if (nametable.isEmpty()) {

         SYW << tr("No se encuentra el nombre de la tabla. Coloque el nombre de la TABLA en el atributo \"options\" del campo "
                   "o escriba el nombre en el campo \"Tabla_...\"");

         return;
     }



     QString command = QString("SELECT * FROM %1").arg(nametable);
     SYD << tr("..........ComboWidget::updateComboFields....command:|%1|")
            .arg(command);

     query.prepare(  command );
     bool executed = query.exec();
     if (!executed ) {
          SYW << tr("IMPORTANTE, NO se ejecut� correctamente la consulta de opciones del combo: \"%1\"").arg(command);
          return;
     }

     QSqlRecord record = query.record();
     _itemrealvaluelist.clear();
     _itemvaluelist.clear();

     for(int i= 0; i < record.count(); i++ ) {
         _itemrealvaluelist.append(record.fieldName(i));
         _itemvaluelist.append(record.fieldName(i));
     }



    if ( inwidget ) {
        varbox->addItems( _itemvaluelist );
    }

}





QString ComboWidget::proccessWhereOption(const QString& w) {
    qDebug("..proccessWhereOption...(1)");
    QString result = w;
    QString mysearchvalue;
    QRegExp  editpattern;
    editpattern.setPattern("\\{\\{([a-zA-Z0-9_]+)\\}\\}");
    int pos = w.indexOf(editpattern);
    if ( pos == -1 ) {
        return w;
    }
    mysearchvalue = editpattern.cap(1);

    qDebug("...mysearchvalue:|%s|", qPrintable(mysearchvalue));

    if ( _texteditparent == NULL ) {
        qDebug("..._texteditparent == NULL");
        return result;
    }
    TextEdit* mytextedit;
    DomModel *mydommodel;
    mytextedit = qobject_cast<TextEdit*>(_texteditparent);
    if (mytextedit == NULL ) {
        qDebug("...mytextedit NULL");
        return result;
    }
    mydommodel = mytextedit->dommodel();

    QString value  = mydommodel->searchKeyValueOnTextEdit(mysearchvalue);
    result.replace("{{"+mysearchvalue+"}}",value);

    return result;
}












void ComboWidget::updateComboVariableSafet(bool inwidget) {
    qDebug("... ComboWidget::updateComboVariableSafet.......(1).....%p",MainWindow::configurator);
    if (MainWindow::configurator == NULL ) {
        return;
    }
    qDebug("... ComboWidget::updateComboVariableSafet...(2).....conf().contains(\"path\"):%d",conf().contains("path"));
     if ( conf().contains("path") ) {
          delete  MainWindow::configurator;
          MainWindow::configurator = new SafetYAWL();
          Q_CHECK_PTR( MainWindow::configurator );

          qDebug("***...path: %s", qPrintable(conf()["path"].toString()));
          QString myautofilter;
          QString myrecursivefilter;
          MainWindow::configurator->openXML(conf()["path"].toString());
          if ( conf().contains("autofilter")) {
              myautofilter = conf()["autofilter"].toString().trimmed();
              qDebug("        ****ComboWidget::updateComboVariableSafet autofilter: |%s|",
                     qPrintable(myautofilter));
              qDebug("        ****ComboWidget::updateComboVariableSafet conf()[\"path\"].toString(): |%s|",
                     qPrintable(conf()["path"].toString()));
              MainWindow::configurator->setAutofilters(myautofilter);
          }

          if ( conf().contains("recursivefilter")) {
              myrecursivefilter = conf()["recursivefilter"].toString().trimmed();
              qDebug("        ****ComboWidget::updateComboVariableSafet autofilter: |%s|",
                     qPrintable(myrecursivefilter));
              MainWindow::configurator->setRecursivefilters(myrecursivefilter);
          }

          MainWindow::configurator->openDataSources();
          MainWindow::configurator->convertXMLtoObjects();


          if ( MainWindow::configurator->getWorkflows().count() > 0 ) {
               _itemvaluelist = MainWindow::configurator
                                ->getWorkflows().at(0)->variablesId().toList();
          }
          qDebug("..._itemvaluelist.count():%d",
                 _itemvaluelist.count());
          if ( inwidget ) {
            varbox->addItems( _itemvaluelist );
          }
     }
     qDebug("...(4)....***...updateComboVariableSafet....updateComboVar()....");
}

void ComboWidget::updateComboTaskSafet(bool inwidget) {

    SYD << tr("..........ComboWidget::updateComboTaskSafet.......(1)......options:|%1|")
           .arg(conf()["options"].toString());

    bool incfinal = false;
    QStringList myoptions = conf()["options"].toString().split(",",QString::SkipEmptyParts);

        foreach(QString op, myoptions) {
            QString myfield = op.section("::",0,0).trimmed();
            if (myfield.compare("incfinal",Qt::CaseSensitive) == 0) {
                QString myvalue = op.section("::",1,1).trimmed();
                if (myvalue.compare("on",Qt::CaseSensitive) == 0 ) {
                    incfinal = true;
                    break;
                }
            }
        }


    if (MainWindow::configurator == NULL ) {
        return;
    }

    SYD << tr("..........ComboWidget::updateComboTaskSafet.......****->(2)");

     if ( conf().contains("path") ) {

          SYD << tr("..........ComboWidget::updateComboTaskSafet.......****->path:|%1|")
                 .arg(conf()["path"]. toString());

          delete  MainWindow::configurator;
          MainWindow::configurator = new SafetYAWL();
          Q_CHECK_PTR( MainWindow::configurator );


          QString myautofilter;
          QString myrecursivefilter;
          QString mypath = conf()["path"].toString();

          SYD << tr("..........ComboWidget::updateComboTaskSafet.......path:|%1|")
                 .arg(mypath);
          MainWindow::configurator->openXML(mypath);
          if ( conf().contains("autofilter")) {
              myautofilter = conf()["autofilter"].toString().trimmed();
              MainWindow::configurator->setAutofilters(myautofilter);
          }

          if ( conf().contains("recursivefilter")) {
              myrecursivefilter = conf()["recursivefilter"].toString().trimmed();
              MainWindow::configurator->setRecursivefilters(myrecursivefilter);
          }

          MainWindow::configurator->openDataSources();
          MainWindow::configurator->convertXMLtoObjects();


          if ( MainWindow::configurator->getWorkflows().count() > 0 ) {
               QList<SafetTask*> mylist = MainWindow::configurator
                       ->getWorkflows().at(0)->getTasks();

               foreach(SafetTask* t, mylist) {
                   _itemvaluelist.append(t->id());
               }


          }
          if (incfinal ) {
            _itemvaluelist.append("final");
          }
          if ( inwidget ) {
            varbox->addItems( _itemvaluelist );

          }
     }
     else {
         SYD << tr("..........ComboWidget::updateComboTaskSafet..NO CONTAIN path");
     }

}



void ComboWidget::updateComboRecursivefilterSafet(bool inwidget) {
    if (MainWindow::configurator == NULL ) return;
    if ( conf().contains("path") ) {
         delete  MainWindow::configurator;
         MainWindow::configurator = new SafetYAWL();
         Q_CHECK_PTR( MainWindow::configurator );

         qDebug("...path: %s", qPrintable(conf()["path"].toString()));
         MainWindow::configurator->openXML(conf()["path"].toString());

         MainWindow::configurator->convertXMLtoObjects();

         MainWindow::configurator->openDataSources();

         if ( MainWindow::configurator->getWorkflows().count() > 0 ) {
             _itemvaluelist = MainWindow::configurator->getWorkflow()->recursivefiltersId().toList();
         }

         if ( inwidget ) {
            varbox->addItems( _itemvaluelist );
        }
    }

}


void ComboWidget::updateComboAutofilterSafet(bool inwidget) {
     if (MainWindow::configurator == NULL ) return;
     qDebug("...***....(1)....updateComboAutofilterSafet....updateCombo()....");
     if ( conf().contains("path") ) {
          delete  MainWindow::configurator;
          MainWindow::configurator = new SafetYAWL();
          Q_CHECK_PTR( MainWindow::configurator );

          qDebug("...path: %s", qPrintable(conf()["path"].toString()));
          MainWindow::configurator->openXML(conf()["path"].toString());

          MainWindow::configurator->convertXMLtoObjects();
          MainWindow::configurator->openDataSources();

          if ( MainWindow::configurator->getWorkflows().count() > 0 ) {
               _itemvaluelist = MainWindow::configurator->getWorkflow()->autofiltersId().toList();
          }


          if ( inwidget ) {
            varbox->addItems( _itemvaluelist );
        }
     }
     qDebug("...(4)....***...updateComboVariableSafet....updateComboVar()....");
}


QString ComboWidget::text() const {

    QString str;
     if ( varbox != NULL ) {

        str = varbox->currentText();
    }
     else {
         if ( !hashtext.isEmpty()) {
             str = hashtext;
         }

     }
     if (signbutton != NULL && type()==Flow && !_cursigners.isEmpty()) {
         str += "|"+ _cursigners;
     }

    return str;

}

QRect ComboWidget::getGeoParams() const {
     QRect result;
     Q_CHECK_PTR( varbox );
     result.setHeight( 36 );
     result.setWidth( 220 );
     return result;
}


void ComboWidget::setFocus ( Qt::FocusReason reason ) {
     if ( varbox == NULL ) {
          if ( signbutton == NULL ) {
               TextEdit* te = qobject_cast<TextEdit*>(parent());
               Q_CHECK_PTR(  te );
               te->setFocus(reason);
               return;
          } else {
               QWidget::setFocus ( reason );
               signbutton->setFocus(reason);
          }
     } else {
          QWidget::setFocus ( reason );
          varbox->setFocus( reason);
     }
}

