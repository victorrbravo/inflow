/********************************************************************************
** Form generated from reading UI file 'dialogflowparameters.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGFLOWPARAMETERS_H
#define UI_DIALOGFLOWPARAMETERS_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_DialogFlowParameters
{
public:
    QGridLayout *gridLayout;
    QDialogButtonBox *buttonBox;
    QGroupBox *boxLista;

    void setupUi(QDialog *DialogFlowParameters)
    {
        if (DialogFlowParameters->objectName().isEmpty())
            DialogFlowParameters->setObjectName(QString::fromUtf8("DialogFlowParameters"));
        DialogFlowParameters->resize(414, 338);
        gridLayout = new QGridLayout(DialogFlowParameters);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        buttonBox = new QDialogButtonBox(DialogFlowParameters);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout->addWidget(buttonBox, 2, 0, 1, 1);

        boxLista = new QGroupBox(DialogFlowParameters);
        boxLista->setObjectName(QString::fromUtf8("boxLista"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        boxLista->setFont(font);

        gridLayout->addWidget(boxLista, 1, 0, 1, 1);


        retranslateUi(DialogFlowParameters);
        QObject::connect(buttonBox, SIGNAL(accepted()), DialogFlowParameters, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DialogFlowParameters, SLOT(reject()));

        QMetaObject::connectSlotsByName(DialogFlowParameters);
    } // setupUi

    void retranslateUi(QDialog *DialogFlowParameters)
    {
        DialogFlowParameters->setWindowTitle(QApplication::translate("DialogFlowParameters", "Par\303\241metros del Flujo de Trabajo", 0, QApplication::UnicodeUTF8));
        boxLista->setTitle(QApplication::translate("DialogFlowParameters", "Seleccione valores de los par\303\241metros:", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class DialogFlowParameters: public Ui_DialogFlowParameters {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGFLOWPARAMETERS_H
