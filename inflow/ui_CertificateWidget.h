/********************************************************************************
** Form generated from reading UI file 'CertificateWidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CERTIFICATEWIDGET_H
#define UI_CERTIFICATEWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QTabWidget>
#include <QtGui/QTextEdit>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CertificateDialog
{
public:
    QVBoxLayout *WidgetLayout;
    QTabWidget *tabWidget;
    QWidget *general;
    QVBoxLayout *generalLayout;
    QTextEdit *info;
    QWidget *details;
    QVBoxLayout *detailsLayout;
    QTreeWidget *parameters;
    QPlainTextEdit *parameterContent;
    QDialogButtonBox *saveBox;
    QWidget *path;
    QVBoxLayout *pathLayout;
    QGroupBox *pathGroup;
    QVBoxLayout *pathGroupLayout;
    QTreeWidget *pathTree;
    QLabel *statusLabel;
    QPlainTextEdit *status;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *CertificateDialog)
    {
        if (CertificateDialog->objectName().isEmpty())
            CertificateDialog->setObjectName(QString::fromUtf8("CertificateDialog"));
        CertificateDialog->resize(400, 453);
        CertificateDialog->setStyleSheet(QString::fromUtf8("#parameterContent { font: 10pt \"Liberation Mono, Courier New\"; }"));
        WidgetLayout = new QVBoxLayout(CertificateDialog);
        WidgetLayout->setObjectName(QString::fromUtf8("WidgetLayout"));
        tabWidget = new QTabWidget(CertificateDialog);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        general = new QWidget();
        general->setObjectName(QString::fromUtf8("general"));
        generalLayout = new QVBoxLayout(general);
        generalLayout->setObjectName(QString::fromUtf8("generalLayout"));
        info = new QTextEdit(general);
        info->setObjectName(QString::fromUtf8("info"));
        info->setReadOnly(true);

        generalLayout->addWidget(info);

        tabWidget->addTab(general, QString());
        details = new QWidget();
        details->setObjectName(QString::fromUtf8("details"));
        detailsLayout = new QVBoxLayout(details);
        detailsLayout->setObjectName(QString::fromUtf8("detailsLayout"));
        parameters = new QTreeWidget(details);
        parameters->setObjectName(QString::fromUtf8("parameters"));
        parameters->setRootIsDecorated(false);

        detailsLayout->addWidget(parameters);

        parameterContent = new QPlainTextEdit(details);
        parameterContent->setObjectName(QString::fromUtf8("parameterContent"));
        parameterContent->setReadOnly(true);

        detailsLayout->addWidget(parameterContent);

        saveBox = new QDialogButtonBox(details);
        saveBox->setObjectName(QString::fromUtf8("saveBox"));
        saveBox->setStandardButtons(QDialogButtonBox::Save);

        detailsLayout->addWidget(saveBox);

        tabWidget->addTab(details, QString());
        path = new QWidget();
        path->setObjectName(QString::fromUtf8("path"));
        pathLayout = new QVBoxLayout(path);
        pathLayout->setObjectName(QString::fromUtf8("pathLayout"));
        pathGroup = new QGroupBox(path);
        pathGroup->setObjectName(QString::fromUtf8("pathGroup"));
        pathGroupLayout = new QVBoxLayout(pathGroup);
        pathGroupLayout->setObjectName(QString::fromUtf8("pathGroupLayout"));
        pathTree = new QTreeWidget(pathGroup);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(0, QString::fromUtf8("1"));
        pathTree->setHeaderItem(__qtreewidgetitem);
        pathTree->setObjectName(QString::fromUtf8("pathTree"));
        pathTree->header()->setVisible(false);

        pathGroupLayout->addWidget(pathTree);


        pathLayout->addWidget(pathGroup);

        statusLabel = new QLabel(path);
        statusLabel->setObjectName(QString::fromUtf8("statusLabel"));

        pathLayout->addWidget(statusLabel);

        status = new QPlainTextEdit(path);
        status->setObjectName(QString::fromUtf8("status"));

        pathLayout->addWidget(status);

        tabWidget->addTab(path, QString());

        WidgetLayout->addWidget(tabWidget);

        buttonBox = new QDialogButtonBox(CertificateDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Ok);

        WidgetLayout->addWidget(buttonBox);


        retranslateUi(CertificateDialog);
        QObject::connect(buttonBox, SIGNAL(clicked(QAbstractButton*)), CertificateDialog, SLOT(reject()));
        QObject::connect(saveBox, SIGNAL(clicked(QAbstractButton*)), CertificateDialog, SLOT(save()));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(CertificateDialog);
    } // setupUi

    void retranslateUi(QDialog *CertificateDialog)
    {
        CertificateDialog->setWindowTitle(QApplication::translate("CertificateDialog", "Certificate", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(general), QApplication::translate("CertificateDialog", "General", 0, QApplication::UnicodeUTF8));
        QTreeWidgetItem *___qtreewidgetitem = parameters->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("CertificateDialog", "Value", 0, QApplication::UnicodeUTF8));
        ___qtreewidgetitem->setText(0, QApplication::translate("CertificateDialog", "Field", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(details), QApplication::translate("CertificateDialog", "Details", 0, QApplication::UnicodeUTF8));
        pathGroup->setTitle(QApplication::translate("CertificateDialog", "Certification Path", 0, QApplication::UnicodeUTF8));
        statusLabel->setText(QApplication::translate("CertificateDialog", "Certificate status:", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(path), QApplication::translate("CertificateDialog", "Certification Path", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class CertificateDialog: public Ui_CertificateDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CERTIFICATEWIDGET_H
