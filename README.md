 ___ _   _ _____ _     _____        __
|_ _| \ | |  ___| |   / _ \ \      / /
 | ||  \| | |_  | |  | | | \ \ /\ / / 
 | || |\  |  _| | |__| |_| |\ V  V /  
|___|_| \_|_|   |_____\___/  \_/\_/   


                     +-++-++-++-++-++-++-+ +-++-++-+ +-++-++-++-++-+
                     |V||E||R||S||I||O||N| |0||.||1| |a||l||p||h||a|
                     +-++-++-++-++-++-++-+ +-++-++-+ +-++-++-++-++-+

/ * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * ** 
*  I N F L OW   Sistema  Automatizado  para  la   Firma   Electrónica   y   Estampado  de  Tiempo *
*  Versión 0.1 alpha                                                                             *
*                                                                                                *
*  Copyright (C)  2008,2012  Centro Nacional de Desarrollo de Investigación en Tecnologías  *
*  Libres CENDITEL.  Víctor Bravo Bravo (vbravo@cenditel.gob.ve), Antonio Araujo Brett           *
*  (aaraujo@cenditel.gob.ve)                                                                     *
*                                                                                                *
*   Lista de correo-e: seguridad@cenditel.gob.ve                                                 *
*   Con colaboración  de:                                                                        * 
*                                                    *
*   Pedro Buitrago (pbuitrago@cenditel.gob.ve)                                                   *
*                                                                                                *
*  C E N D I T E L   Centro  Nacional  de  Desarrollo  e  Investigación  en  Tecnologías  Libres *
*                                                                                                *
*  Términos de LICENCIA:                                                                         *
*    Éste programa es software libre, usted  puede usarlo  bajo los términos de  la licencia de  *
*    software GPL versión 2.0 o superior, de la Free Software Foundation.                        *
*                                                                                                *
*    Éste programa se distribuye con la esperanza de que sea útil, pero  SIN  NINGUNA GARANTÍA;  *
*    tampoco las implícitas garantías de MERCANTILIDAD o ADECUACIÓN  A UN PROPÓSITO PARTICULAR.  *
*                                                                                                *
*    Consulte la licencia GPL para más detalles. Usted  debe recibir una  copia de la GPL junto  *
*    con este programa; si no, escriba a la Free  Software Foundation  Inc. 51 Franklin Street,  *
*    5º Piso,  Boston,  MA  02110 - 1301, USA.                                                   *
*/ * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * **/


DESCRIPCIÓN
===========
   Software para  la gestión de  información a través de  flujos de trabajo  (workflows)

SISTEMA DE INFORMACIÓN DE EJEMPLO
=================================
   
    Puede descargar los siguientes ejemplos:

     1. Gestor de Tareas y Actividades
     Desde http://www.victorbravo.info/media/archivos/panel.tar

     2. Manejo de asistencia de estudiantes 

     Desde http://www.victorbravo.info/media/archivos/asistencia.tar
  


  INSTALACIÓN DE SAFET V0.1 ALPHA
  ===============================

    Sistema Operativo SO GNU/Linux Debian Lenny 5.0.3, 
    -------------------------------------------------

      Pre-requistos:
        - Conocimiento básico de instalación de paquetes,  y actividades mínimas de administración.

      Paquetes necesarios:
        a) Librería Trolltech Qt >= 4.4  (libqt4-dev)
        b) Librería de Conexión a Datos Qt PSQL (libqt4-sql-psql)
        c) Librería de Conexión a Datos Qt SQLITE (libqt4-sql-sqlite)
        d) Librería XML2 (libxml2-dev)
        e) Librería de archivo tareados (libtar-dev)  
        f) Librería Graphviz (libgraphiz-dev, para plugins de visualización de gráfico)
        g) Libreria libtar-dev (Utilidad "tar" para respaldo de archivos)
        h) Compilador g++ y make



        Desde la shell de Unix, puede instalar todo el conjunto  de  paquetes necesarios [descritos 
        desde el paso "a)" hasta el paso "i)"], a través del siguiente comando:
	
	   # aptitude install g++ libqt4-dev libqt4-sql-psql libxml2-dev  libqt4-sql-sqlite libtar-dev libgraphviz-dev g++ make

      Instalación de SAFET desde los archivos fuentes *.tar.gz
      -------------------------------------------------------

      ¡Importante!: El śimbolo "$" es para usuarios sin privilegios de administrador y  el  símbolo
                   "#" indica que es un usuario con privilegios de administrador (root). Únicamente 
                    la instalación de  paquetes y  el paso número  siete (7)  se  realiza  con  los 
                    privilegios de root, esto evita configuraciones adicionales.

        1. Crear un directorio, en el cual almacenará los archivos fuentes.

           $ mkdir inflow  

        2. Descargar los fuentes (git ...) 

      
        4. Ir al directorio descomprimido cd inflow/

           
        5. Ejecutar "qmake" o "qmake-qt4" para generar el archivo de compilación (Makefile)

             $ qmake-qt4

        6. Si en la pantalla no se muestran errores, realizar la compilación con "make".

             $ make

        7. Realizar la instalación en los directorios del sistema (es necesario tener permisos de administrador o "root")
          
	     $ su  
             Escriba la contraseña de administrador y asegúrese que se está en el directorio safet-0.1.1.alpha)

        8. Instalación como administrador

	  # make install
          ¡Si no se presenta ningún mensaje de error SAFET está instalado correctamente en su sistema!

       
       ¡NOTA IMPORTANTE!: Hasta el paso número diez (10) el sistema SAFET ha sido instalado, y puede utilizarse la interfaz
                          gráfica de usuario -GUI Graphical User Interface- "inflow". Sim embargo, antes de utilizar la GUI
                          inflow, en necesario, comprender los aspectos  básicos del  directorio  ".safet/"  y   configurar
                          la base de datos PosgresSQL.

       Sobre el directorio .safet/
       ---------------------------

       Una vez realizado el paso número diez (10) del proceso de instalación, es importante destacar las características 
       básicas del directorio .safet/, para ello debe ubicarse en el /home del usuario

       1. Ubicar el directorio /home del usuario

           $ cd

       2. Es importante destacar la estructura de los directorios que posee .safet/. Si lista el contenido de .safet con una 
          de árbol, (ej. $ tree -d .safet/), se muestra que el contenido es de nueve (9) directorios.

           .safet/
           |-- certs
           |-- dtd
           |-- flowfiles
           |-- images
           |-- input
           |-- log
           |-- reports
           |-- sql
           `-- xmlrepository
           
           9 directories

       2. En un árbol de información más extenso, puede observarse el contenido de cada uno de los directorios.. Entre paréntesis

          .safet/
          |-- certs  (Certificados de ejemploss y la CRL - Certificate Revocation List -, para verificar firmas elctrónicas)
          |   |-- accenditel.pem
          |   `-- crl_accenditel.crl
          |-- dtd    (Archivos de validación de los *.xml contenidos dentro del directorio flowfiles/ )
          |   `-- yawlworkflow.dtd
          |-- flowfiles (Diversos ejemplos para el flujo de tickets 'workflows')
          |   |-- ticketsfortask.xml
          |   |-- ticketstrac.xml
          |   |-- ticketstracavanzado.xml
          |   |-- ticketstracporcategoria.xml
          |   |-- ticketstracporcategoria3.xml
          |   |-- ticketstractotal.xml
          |   |-- ticketstractotalv2.xml
          |   `-- ticketstracv2.xml
          |-- images    (Imagenes en formato png - Portable Network Graphics -, utilizada en la GUI inflow)
          |   |-- clear.png
          |   |-- default.png
          |   |-- desc.png
          |   |-- firmadoc.png
          |   |-- hito.png
          |   |-- padlock.png
          |   |-- plus.png
          |   |-- resumen.png
          |   `-- yes.png
          |-- input     (Archivos de configuración de entrada de datos, en la GUI inflow)
          |   |-- defconfigure.xml
          |   |-- defconsole.xml
          |   |-- defmanagementsignfile.xml
          |   `-- deftrac.xml
          |-- log       (Archivo de registro de errores)
          |   `-- safet.log
          |-- reports   (Archivos de reportes de sistemas, en la GUI inflow)
          |   |-- animation-min.js
          |   |-- bg_hd.gif
          |   |-- button-min.js
          |   |-- button.css
          |   |-- data.js
          |   |-- datasource-min.js
          |   |-- datatable-min.js
          |   |-- datatable.css
          |   |-- dom-min.js
          |   |-- dpSyntaxHighlighter.css
          |   |-- dragdrop-min.js
          |   |-- element-min.js
          |   |-- event-min.js
          |   |-- paginator-min.js
          |   |-- paginator.css
          |   |-- sf_plantillaLista01.html
          |   |-- title_h_bg.gif
          |   |-- yui.css
          |   |-- yui.gif
          |   `-- yuiloader-min.js
          |-- safet.conf
          |-- sql    (Archivos de las base de datos de ejemplo PostgresSQL)
          |   |-- dbtracsafet28oct09.sql
          |   `-- dbtracseguridad28oct09.sql
          `-- xmlrepository


          Configuración de la base de datos PostgresSQL
          ---------------------------------------------

          1. Como administrador (root) del sistema consfigure la base de datos PostgresSQL ( la 
             versión utilizada para la contrucción de éste archivo README, fue la versión 8.3.7)

             # su - postgres

          2. El prompt o introductor '#' debe haber cambiado a '$' y el usuario ahora es 'postgres'; 
             ejecutar:

             $ psql template1

          3. Crear el usuario y la contraseña de la base de datos PostgresSQL.

             # CREATE USER name_user PASSWORD '123456';

             ¡IMPORTANTE! name_user es el nombre del usurio si el entorno de la shell por defecto es 
             bash aparece usurio@maquina:~$, si el entorno es distinto de bash, ej. sh, ejecutar el 
             comando 'whoami'

          4. Dar permisos para crear bases de datos y crear usuarios, a 'safetPrueba'.

             # ALTER USER name_user CREATEDB CREATEUSER;

             Si la sintaxis es la correcta, saldrá el mensaje de ALTER ROLE. 

          5. Salir de la configuración de la base de datos PostgresSQL.

             # \q

          6. Editar el archivo safet.conf en el /home del usuario.

            # vi .safet/safet.conf

          7. Ir a la línea 165 y cambiar el usaurio 'seguridad'   por el usuario 'name_user', la líneas
             164,165 en safet.conf son:

              # Especifica el nombre de usuario para la fuente de datos
              database.user.1 = seguridad

          8. Ir a la línea 168 y cambiar la contraseña 'merida' por el usuario del sistema 'name_user', la líneas
             164,165 en safet.conf son:

             # Especifica la contrasena de acceso para la fuente de datos
             database.password.1 =  merida


          Utilizar la base de datos ejemplo en .safet/sql
          -----------------------------------------------

          1. Ir al directorio .safet/sql

             $ cd .safet/sql

          2. Crear la base de datos 'dbtracsafet' 

             $ createdb dbtracsafet

          3. Cargar el script de restauración de la base de datos ejemplo.

            $ psql -f dbtracsafet28oct09.sql dbtracsafet

	  $ inflow

  Antes de esto, es posible que necesite configurar los parámetros de conexión con la base de datos,
  SAFET actualmente se conecta con base de datos postgresql, para ello, realice lo siguiente

	

  2. Busque la Seccion [Database] dentro del archivo, y edite los parámetros:

	database.host.1  (para colocar el nombre del servidor), escriba "localhost para servidor local
	database.db.1 (nombre de la base de datos)
	database.port.1 (número del puerto de posgresql, por defecto 5432)
	database.user.1 (nombre del usuario en el servidor postgresql)
	database.password.1 (contraseña del correspondiente usuario)

  3. Guarde el archivo y ejecuta otra vez la aplicación inflow:
	>inflow

     Si se logró conectar satisfactoriamente, entonces debe aparecer en la barra de estado inferior el mensaje:

	"Directorio de archivos $Directorio de usuario$/.safet/input/"

     donde el $Directorio de usuario$ es el directorio de su "home", por ejemplo, si su cuenta es "fulano", 
     debe aparecer:
	"Directorio de archivos /home/fulano/.safet/input/"

  4. Para ver el registro (log) de la aplicación, puede ver el archivo "~/.safet/log/safet.log"

	Por ejemplo,
	>tail -f  ~/.safet/safet.log

  - Para ver la línea de tiempo de desarrollo del proyecto puede acceder a:

	http://seguridad.cenditel.gob.ve/safet/timeline


 ** Importante: cualquier duda, comentario por favor enviar correo-e a seguridad@cenditel.gob.ve


